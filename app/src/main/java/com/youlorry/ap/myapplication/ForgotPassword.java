package com.youlorry.ap.myapplication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Arpit Prajapati on 1/24/17.
 */

public class ForgotPassword extends YouLorryActivity implements View.OnClickListener {

    TextView note_for_forgot_pass_layout;
    EditText mob_number_edit_text;
    Button send_otp_btn;
    ProgressDialog dialogue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_layout);
        setupUI(findViewById(R.id.forgot_page_ll));

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width*0.9),(int)(height*0.4));

        note_for_forgot_pass_layout = (TextView) findViewById(R.id.note_for_forgot_pass_layout);
        mob_number_edit_text = (EditText) findViewById(R.id.mob_number_edit_text);
        send_otp_btn = (Button) findViewById(R.id.send_otp_btn);

        send_otp_btn.setOnClickListener(this);

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.send_otp_btn){

            if (!isNetworkConnected()) {
                new AlertDialog.Builder(this)
                        .setTitle("Check Connection")
                        .setMessage("Internet connection not enable. Please enable it from settings!")
                        .setPositiveButton("try again", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                Intent intent = getIntent();
                                finish();
                                startActivity(intent);
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            } else if(mob_number_edit_text.getText().toString().length() == 0 || mob_number_edit_text.getText().toString().length() < 10){
                mob_number_edit_text.setError("10 digits required!");
            } else {

                dialogue = new ProgressDialog(this);
                dialogue.setTitle("Loading ...");
                dialogue.show();

                URL urlObj = null;
                try {
                    urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-forgotpassword/");
                    HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);
                    urlConnection.setUseCaches(false);
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    //  urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setRequestMethod("POST");
                    urlConnection.connect();

                    JSONObject cred = new JSONObject();

                    cred.put("send_otp", "send_otp");
                    cred.put("forgot_password", "forgot_password");
                    cred.put("contact_number", mob_number_edit_text.getText().toString());

                    Log.d("send_otp", "send_otp");
                    Log.d("forgot_password", "forgot_password");
                    Log.d("contact_number", mob_number_edit_text.getText().toString());

                    OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(cred.toString());
                    wr.flush();
                    wr.close();

                    StringBuilder sb = new StringBuilder();
                    int HttpResult = urlConnection.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        BufferedReader br = new BufferedReader(
                                new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                        }
                        br.close();

                        Log.d("response_data_for_forgot_password", sb.toString());

                        if(sb.toString().equals("user_does_not_exists")){
                            Toast.makeText(this, "User Not Found", Toast.LENGTH_LONG).show();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                             public void run() {
                             dialogue.dismiss();
                             }
                        }, 2000);

                        } else if(sb.toString().equals("sent")){
//                            Log.d("name", name.getText().toString());
                            finish();
                            Toast.makeText(this, "OTP Sent!!!", Toast.LENGTH_LONG).show();
                            Intent in = new Intent(this, OTPNumber.class);
                            Bundle bd = new Bundle();
                            bd.putString("from_key", "forgot_password");
                            bd.putString("contact_number", mob_number_edit_text.getText().toString());
                            in.putExtras(bd);
                            startActivity(in);

                        } else {
                            Toast.makeText(this, "Some error occurs. Please try later!!!", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(this, LoginActivity.class));
                        }

                    }

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
/*                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        dialogue.dismiss();
                    }
                }, 2000);
*/            }
        }
    }
}
