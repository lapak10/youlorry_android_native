package com.youlorry.ap.myapplication.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.youlorry.ap.myapplication.DetailsPageforConfirmedBid;
import com.youlorry.ap.myapplication.Model.Details_Page_confirmBid_Model;
import com.youlorry.ap.myapplication.R;

import java.util.List;


/**
 * Created by lucky on 16/5/17.
 */

public class Accepted_Adapter extends RecyclerView.Adapter<Accepted_Adapter.Viewholder>{
    Context context;
    Accepted_Adapter accepted_adapter;
    Details_Page_confirmBid_Model details_page_confirmBid_model;
    List<Details_Page_confirmBid_Model.IncomingQuotationsBean> incomingQuotationsBeanList;
    List<Details_Page_confirmBid_Model.IncomingQuotationsBean> acceptedList;
    public Accepted_Adapter(DetailsPageforConfirmedBid detailsPageforConfirmedBid, List<Details_Page_confirmBid_Model.IncomingQuotationsBean> incomingQuotationsBeanList) {
        this.context=detailsPageforConfirmedBid;
        this.incomingQuotationsBeanList=incomingQuotationsBeanList;
    }
    // private final List<Details_Page_confirmBid_Model.IncomingQuotationsBean> bidding_models;

    @Override
    public Accepted_Adapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.accepted_adapter_layout, parent, false);
        Accepted_Adapter.Viewholder viewHolder=new Accepted_Adapter.Viewholder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Accepted_Adapter.Viewholder holder, int position) {
        Details_Page_confirmBid_Model.IncomingQuotationsBean incomingQuotationsBean=incomingQuotationsBeanList.get(position);
        if(incomingQuotationsBean!=null )
        {

            String accepted=incomingQuotationsBean.getPost_status();
            if(accepted.equalsIgnoreCase("truck_pay_ok")) {
//            {
//                holder.button_accept.setText("Accepted");
//            }
                List<String> driver_name = incomingQuotationsBean.getDriver_name();
                if(driver_name.size()>0)
                    holder.textView_driverName.setText((driver_name.get(0).toString()));

                List<String> vechile_Number = incomingQuotationsBean.getQuotation_vehicle_number();
                if (vechile_Number.size() > 0)
                    holder.textView_Vechile_Number.setText((vechile_Number.get(0).toString()));

                List<String> leftCount = incomingQuotationsBean.getLeft_count();
                if (leftCount.size() > 0)
                    holder.textView_LeftCount.setText((leftCount.get(0).toString()));

                List<String> number = incomingQuotationsBean.getDriver_number();
                if (number.size() > 0)
                    holder.textView_number.setText((number.get(0).toString()));
                //  holder.textView_driverName
            }
        }
    }


    @Override
    public int getItemCount() {
        return incomingQuotationsBeanList.size();
        // return details_page_confirmBid_model.getIncoming_quotations();
    }
    public class Viewholder extends RecyclerView.ViewHolder {
//        Button button_accept;
        TextView textView_number, days1, hours1, minutes1, seconds1, from_id, to_id, quoted_amt_id, material_id, expairyTime, weight_id;
        TextView textView_driverName, textView_Vechile_Number,textView_LeftCount;

        public Viewholder(View itemView) {
            super(itemView);
            textView_driverName=(TextView) itemView.findViewById(R.id.textView_driverName);
            textView_Vechile_Number=(TextView) itemView.findViewById(R.id.textView_Vechile_Number);
            textView_LeftCount=(TextView) itemView.findViewById(R.id.textView_LeftCount);
            textView_number=(TextView) itemView.findViewById(R.id.textView_number);
//            button_accept=(Button) itemView.findViewById(R.id.button_accept);
//            button_accept.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    ((DetailsPageforConfirmedBid)context).acceptincoming_Quataion(incomingQuotationsBeanList.get(getAdapterPosition()).getID(),incomingQuotationsBeanList.get(getAdapterPosition()).getLoad_ID());
//                }
//            });
        }
    }
}
