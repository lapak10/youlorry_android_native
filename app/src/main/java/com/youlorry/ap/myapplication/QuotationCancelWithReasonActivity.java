package com.youlorry.ap.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Arpit Prajapati on 12/23/16.
 */

public class QuotationCancelWithReasonActivity extends Activity implements View.OnClickListener {

    public String[]quote_cancel_reasons_list_array = {"Freight found offline at higher rate","Freight found for most preferred destination","Freight not available at agreed time","Road Block","Vehicle Breakdown"};
    public String[]quote_cancel_reasons_list_array1 = {"Freight not available anymore","Vehicle found offline at lower rate","Vehicle not reported at agreed time"," Driver's poor track record ","Incomplete documents"};


 //   public String quote_cancel_reasons_json_string_for_transpoter = "{ \"transporter_json_for_cancellation_reason\" :[{\"description\":\"Freight found offline at higher rate\",\"value\":\"1\"},{\"description\":\"Freight found for most preferred destination\"},{\"description\":\"Freight not available at agreed time\"},{\"description\":\"Road Block\"},{\"description\":\"Vehicle Breakdown\"}] }";
 //   public String quote_cancel_reasons_json_string_for_customer = "{ \"customer_json_for_cancellation_reason\" :[{\"description\":\"Freight not available anymore\"},{\"description\":\"Vehicle found offline at lower rate\"},{\"description\":\"Vehicle not reported at time\"},{\"description\":\"Drivers poor track record\"},{\"description\":\"Incomplete documents\"}] }";

    Spinner spinner_for_quote_cancel_reason;
    Context con;
    Button cancel_btn;
    String quotation_id_for_cancellation;
    ImageView page_cancel_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quotation_cancel_with_reason);

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        YoulorrySession session = new YoulorrySession(this);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0)
        {

            Intent in = new Intent(this , LoginActivity.class);
            startActivity(in);

        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        con = this;
        getWindow().setLayout((int) (width*0.9),(int)(height*0.4));
        Bundle bundle = getIntent().getExtras();
        quotation_id_for_cancellation = bundle.getString("quotation_id_for_cancellation");

        if(new YoulorrySession(con).getrole().equals("transporter")){

/*            try{
                JSONObject  jsonRootObject = new JSONObject(quote_cancel_reasons_json_string_for_transpoter);
                JSONArray jsonArray = jsonRootObject.optJSONArray("transporter_json_for_cancellation_reason");
                for(int i=0; i < jsonArray.length(); i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String id = jsonObject.getString(i+1+"");

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
*/
            spinner_for_quote_cancel_reason = (Spinner) findViewById(R.id.quotation_cancel_spinner);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(con,
                    android.R.layout.simple_spinner_item,quote_cancel_reasons_list_array);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_for_quote_cancel_reason.setAdapter(adapter);

        }

        if(new YoulorrySession(con).getrole().equals("customer")){
            spinner_for_quote_cancel_reason = (Spinner) findViewById(R.id.quotation_cancel_spinner);
            ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(con,
                    android.R.layout.simple_spinner_item,quote_cancel_reasons_list_array1);
            adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_for_quote_cancel_reason.setAdapter(adapter1);

        }



        cancel_btn = (Button) findViewById(R.id.button_id_from_quotation_cancel_modal);
        cancel_btn.setOnClickListener(this);
        page_cancel_btn = (ImageView) findViewById(R.id.top_cancel_btn);
        page_cancel_btn.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.top_cancel_btn){
            finish();
        }

        if( view.getId() == R.id.button_id_from_quotation_cancel_modal){

//            Toast.makeText(con, "Clicked!!!", Toast.LENGTH_SHORT).show();
            new AlertDialog.Builder(this)
                    .setTitle("Alert Message")
                    .setMessage("Want to cancel this quotation!")
                    .setIcon(R.drawable.ic_warning_black_24dp)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            // startActivity(new Intent(con, MainActivity.class));
                            finish();

                             StringBuilder sb = new StringBuilder();
                             int selected_quote_id = (int) spinner_for_quote_cancel_reason.getSelectedItemId();
                            Log.d("reason_id_edgfjhgewjhgfhweghfgsdgfewkkfcwe", selected_quote_id+"");
                        //    new Thread(new Runnable() {
                         //       public void run() {

                                    YoulorrySession session12 = new YoulorrySession(con);
                                    String username = session12.getusername();
                                    String password = session12.getpass_word();
                                    String userid = session12.getuser_id();
                                    String role = session12.getrole();

                                    URL urlObj = null;
                                    try {

                                        urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-fetch-my-all-quotations/");
                                        HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                                        urlConnection.setDoOutput(true);
                                        urlConnection.setDoInput(true);
                                        urlConnection.setUseCaches(false);
                                        urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                                        //  urlConnection.setRequestProperty("Accept", "application/json");
                                        urlConnection.setRequestMethod("POST");
                                        urlConnection.connect();

                                        JSONObject cred = new JSONObject();

                                        cred.put("username",username);
                                        cred.put("password",password);
                                        cred.put("userid",userid);
                                        cred.put("role",role);
                                        cred.put("quote_id", quotation_id_for_cancellation.toString());
                                        cred.put("reason", selected_quote_id);
                                        cred.put("action","cancel");

                                        OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                                        wr.write(cred.toString());
                                        wr.flush();
                                        wr.close();

                                        //display what returns the POST request

                                        int HttpResult = urlConnection.getResponseCode();
                                        if (HttpResult == HttpURLConnection.HTTP_OK) {
                                            BufferedReader br = new BufferedReader(
                                                    new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                                            String line = null;
                                            while ((line = br.readLine()) != null) {
                                                sb.append(line);
                                            }
                                            br.close();

                                            Log.d("heeeeeeellllllllooooooooooo11111",sb.toString());

                                        }

                                    } catch (ProtocolException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    Log.d("return-value" , sb.toString());


                            //    }
                          //  }).start();

                            if(sb.toString().trim().compareTo("ok") == 0){
                                Toast.makeText(con, "Success", Toast.LENGTH_LONG).show();
                                new Handler().postDelayed(new Runnable() {
                                    public void run() {
                                        startActivity(new Intent(con, MainActivity.class));
                                        finish();
                                    }
                                },500);
//                         startActivity(new Intent(this, MainActivity.class));
                            }else{
                                Toast.makeText(con, "Failed", Toast.LENGTH_LONG).show();
                                new Handler().postDelayed(new Runnable() {
                                    public void run() {
                                        startActivity(new Intent(con, MainActivity.class));
                                        finish();
                                    }
                                },500);
                            }

                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .show();
        }

    }
}
