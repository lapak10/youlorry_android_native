package com.youlorry.ap.myapplication.Model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by Tycho on 5/5/2017.
 */

public class Date_Difference {
    public static String getBiddingTime(String dateStart, String dateStop) {

//         dateStart = "01/14/2012 09:29:58";
//         dateStop = "01/15/2012 10:31:48";

        //HH converts hour in 24 hours format (0-23), day calculation



        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date dNow = new Date( );
//
//        Calendar cal = Calendar.getInstance();
//        System.out.println(format.format(dNow));
//        dateStart=format.format(cal);

        Date d1 = null;
        Date d2 = null;
        String time=null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            System.out.print(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");
            if(diffSeconds<0)
            {
                return "negative";
            }
            else {

                time = diffHours + ":" + diffMinutes + ":" + diffSeconds;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
//        if (time.contains("-"))
//            return null;
//        else
        return time;
    }







    public static long converttimetomillisecond(String dateStart) {

//         dateStart = "01/14/2012 09:29:58";
//         dateStop = "01/15/2012 10:31:48";

        //HH converts hour in 24 hours format (0-23), day calculation



        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date dNow = new Date( );
//
//        Calendar cal = Calendar.getInstance();
//        System.out.println(format.format(dNow));
//        dateStart=format.format(cal);

        Date d1 = null;
        Date d2 = null;
        long time=0;

        try {
            d1 = format.parse(dateStart);


            //in milliseconds
            long diff = d1.getTime();
            time=diff;

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            System.out.print(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");
//            if(diff<0)
//            {
//                return "negative";
//            }
//            else {
//
//                time = diffHours + ":" + diffMinutes + ":" + diffSeconds;
//            }


        } catch (Exception e) {
            e.printStackTrace();
        }
//        if (time.contains("-"))
//            return null;
//        else
        return time;
    }



    public static String convertmillisecondtohour(Long dateStart) {

        String stringTime=null;
//         dateStart = "01/14/2012 09:29:58";
//         dateStop = "01/15/2012 10:31:48";

        //HH converts hour in 24 hours format (0-23), day calculation



       // SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date dNow = new Date( );
//
//        Calendar cal = Calendar.getInstance();
//        System.out.println(format.format(dNow));
//        dateStart=format.format(cal);

//        Date d1 = null;
//        Date d2 = null;
//        long time=0;

        try {
         //   d1 = format.parse(dateStart);


            //in milliseconds
//            long diff = d1.getTime();
//            time=diff;

            long diffSeconds = dateStart / 1000 % 60;
            long diffMinutes = dateStart / (60 * 1000) % 60;
            long diffHours = dateStart / (60 * 60 * 1000) % 24;
            long diffDays = dateStart / (24 * 60 * 60 * 1000);

            System.out.print(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");
            if(dateStart<0)
            {
                return "Bid Completed";
            }
            else {

                stringTime = String.format("%02d:%02d:%02d",diffHours,diffMinutes,diffSeconds);
                //stringTime=String.valueOf(diffHours)+ ":" +String.valueOf(diffMinutes)+ ":" +String.valueOf(diffSeconds);
              //  time = diffHours + ":" + diffMinutes + ":" + diffSeconds;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
//        if (time.contains("-"))
//            return null;
//        else
        return stringTime;
    }



}
