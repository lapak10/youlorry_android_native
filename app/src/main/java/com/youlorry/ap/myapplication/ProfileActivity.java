package com.youlorry.ap.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Arpit Prajapati on 11/26/16.
 */

public class ProfileActivity extends YouLorryActivity implements View.OnClickListener{

    Button profile_update_btn;
    Context con;
    TextView contact_no,establishing_date_from_json,address_from_json,wel_come_text,pan_from_json,service_tax_from_json,tin_cin_from_json,aadhar_from_json;
    EditText full_address,name_from_json,email_from_json,fax_from_json,company_name_from_json,website_from_json,operated_states_from_json,from_from_json,to_from_json ;
    TextView no_of_trucks_from_json,no_of_trucks,no_of_gps_trucks,truck_types;
    EditText no_of_gps_trucks_from_json;
    CheckBox truck_type1,truck_type2,truck_type3,truck_type4,truck_type5,truck_type6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_page);
        setupUI(findViewById(R.id.profile_page_rl));

//        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        //     ActionBar act_bar = getActionBar();
        //    act_bar.setHomeButtonEnabled(true);

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        YoulorrySession session = new YoulorrySession(this);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(this , LoginActivity.class);
            startActivity(in);

        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        contact_no = (TextView) findViewById(R.id.contact_from_json);
        establishing_date_from_json = (TextView) findViewById(R.id.establishing_date_from_json);
        full_address = (EditText) findViewById(R.id.address_from_json);
        wel_come_text = (TextView) findViewById(R.id.welcome_text);
        pan_from_json = (TextView) findViewById(R.id.pan_from_json);
        service_tax_from_json = (TextView) findViewById(R.id.service_tax_from_json);
        tin_cin_from_json = (TextView) findViewById(R.id.tin_cin_from_json);
        aadhar_from_json = (TextView) findViewById(R.id.aadhar_from_json);

        name_from_json = (EditText) findViewById(R.id.name_from_json);
        email_from_json = (EditText) findViewById(R.id.email_from_json);
        fax_from_json = (EditText) findViewById(R.id.fax_from_json);
        company_name_from_json = (EditText) findViewById(R.id.company_name_from_json);
        website_from_json = (EditText) findViewById(R.id.website_from_json);
        operated_states_from_json = (EditText) findViewById(R.id.operated_states_from_json);
        from_from_json = (EditText) findViewById(R.id.from_from_json);
        to_from_json = (EditText) findViewById(R.id.to_from_json);
        no_of_trucks = (TextView) findViewById(R.id.no_of_trucks);
        no_of_trucks_from_json = (TextView) findViewById(R.id.no_of_trucks_from_json);
        no_of_gps_trucks_from_json = (EditText) findViewById(R.id.no_of_gps_trucks_from_json);
        no_of_gps_trucks = (TextView) findViewById(R.id.no_of_gps_trucks);
        truck_types = (TextView) findViewById(R.id.truck_types);
        truck_type1 = (CheckBox) findViewById(R.id.checkBox_for_truck_type);
        truck_type2 = (CheckBox) findViewById(R.id.checkBox_for_truck_type1);
        truck_type3 = (CheckBox) findViewById(R.id.checkBox_for_truck_type2);
        truck_type4 = (CheckBox) findViewById(R.id.checkBox_for_truck_type3);
        truck_type5 = (CheckBox) findViewById(R.id.checkBox_for_truck_type4);
        truck_type6 = (CheckBox) findViewById(R.id.checkBox_for_truck_type5);

        if(new YoulorrySession(this).getrole().equals("transporter")){
            no_of_trucks_from_json.setVisibility(View.VISIBLE);
            no_of_trucks.setVisibility(View.VISIBLE);
            no_of_gps_trucks_from_json.setVisibility(View.VISIBLE);
            no_of_gps_trucks.setVisibility(View.VISIBLE);
            truck_types.setVisibility(View.VISIBLE);
            truck_type1.setVisibility(View.VISIBLE);
            truck_type2.setVisibility(View.VISIBLE);
            truck_type3.setVisibility(View.VISIBLE);
            truck_type4.setVisibility(View.VISIBLE);
            truck_type5.setVisibility(View.VISIBLE);
            truck_type6.setVisibility(View.VISIBLE);
        }

        if(new YoulorrySession(this).getrole().equals("customer")){
            no_of_trucks_from_json.setVisibility(View.GONE);
            no_of_trucks.setVisibility(View.GONE);
            no_of_gps_trucks_from_json.setVisibility(View.GONE);
            no_of_gps_trucks.setVisibility(View.GONE);
            truck_types.setVisibility(View.GONE);
            truck_type1.setVisibility(View.GONE);
            truck_type2.setVisibility(View.GONE);
            truck_type3.setVisibility(View.GONE);
            truck_type4.setVisibility(View.GONE);
            truck_type5.setVisibility(View.GONE);
            truck_type6.setVisibility(View.GONE);
        }

        con = this;
        new TheTask().execute();
        profile_update_btn = (Button) findViewById(R.id.update_btn);

        profile_update_btn.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                startActivity(new Intent(this, MainActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class TheTask extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;
        String str;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session012 = new YoulorrySession(con);
            String username = session012.getusername();
            String password = session012.getpass_word();
            String userid = session012.getuser_id();
            String role = session012.getrole();

            StringBuilder sb1 = new StringBuilder();
            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-update-user-profile/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);

                OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                wr.flush();
                wr.close();

                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb1.append(line);
                    }
                    br.close();

             //       Toast.makeText(con, sb1.toString(), Toast.LENGTH_SHORT).show();
                    Log.d("profile_details", sb1.toString());

                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return sb1.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

           // Toast.makeText(con, s.toString(), Toast.LENGTH_LONG).show();

            try {

                JSONObject json_data = new JSONObject(s.toString());
                String meta_keys_jsondata = json_data.getString("meta_keys");

                //   Toast.makeText(con, meta_keys_jsondata, Toast.LENGTH_SHORT).show();

                JSONObject jobj = new JSONObject(meta_keys_jsondata);

                if(jobj.has("first_name") || jobj.has("last_name")){
                    String f_name = jobj.getString("first_name");
                    String f_name_from_json = f_name.substring(2, f_name.toString().length() - 2);

                    String l_name = jobj.getString("last_name");
                    String l_name_from_json = l_name.substring(2, l_name.toString().length() - 2);

                    wel_come_text.setText(f_name_from_json + " " + l_name_from_json);
                    name_from_json.setText(f_name_from_json + " " + l_name_from_json);
                }else {
                    wel_come_text.setText("User Name");
                    name_from_json.setHint("Enter Name");
                }


             if(new YoulorrySession(con).getrole().toString().equals("customer")) {
                 if(jobj.has("customer_user_email")){
                     String email = jobj.getString("customer_user_email");
                     String email_from_json1 = email.substring(2, email.toString().length() - 2);
                     email_from_json.setText(email_from_json1);
                 }else {
                     email_from_json.setHint("Email");
                 }
             }
                if(new YoulorrySession(con).getrole().toString().equals("transporter")) {
                    if(jobj.has("transporter_user_email")){
                        String email = jobj.getString("transporter_user_email");
                        String email_from_json1 = email.substring(2, email.toString().length() - 2);
                        email_from_json.setText(email_from_json1);
                    }else {
                        email_from_json.setHint("Email");
                    }
                }

                if(new YoulorrySession(con).getrole().toString().equals("customer")) {
                    if(jobj.has("customer_contact_number")){
                        String contact_no1 =jobj.getString("customer_contact_number");
                        String contact_no_from_json = contact_no1.substring(2, contact_no1.toString().length() - 2);
                        if(contact_no_from_json.length() == 0){
                            contact_no.setText("NA");
                        }else{
                            contact_no.setText(contact_no_from_json);
                        }
                    }else {
                        contact_no.setHint("NA");
                    }
                }

                if(new YoulorrySession(con).getrole().toString().equals("transporter")) {
                    if(jobj.has("transporter_contact_number")){
                        String contact_no1 =jobj.getString("transporter_contact_number");
                        String contact_no_from_json = contact_no1.substring(2, contact_no1.toString().length() - 2);
                        if(contact_no_from_json.length() == 0){
                            contact_no.setText("NA");
                        }else{
                            contact_no.setText(contact_no_from_json);
                        }
                    }
                }

                if(new YoulorrySession(con).getrole().toString().equals("transporter")) {
                    if(jobj.has("transporter_transport_name")){
                        String company_name =jobj.getString("transporter_transport_name");
                        String company_name_from_json1 = company_name.substring(2, company_name.toString().length() - 2);
                        company_name_from_json.setText(company_name_from_json1);
                    }else {
                        company_name_from_json.setHint("Enter Company Name");
                    }
                }
                if(new YoulorrySession(con).getrole().toString().equals("customer")) {
                    if(jobj.has("customer_transport_name")){
                        String company_name =jobj.getString("customer_transport_name");
                        String company_name_from_json1 = company_name.substring(2, company_name.toString().length() - 2);
                        company_name_from_json.setText(company_name_from_json1);
                    }else {
                        company_name_from_json.setHint("Enter Company Name");
                    }
                }

                if(jobj.has("customer_full_address")){
                    String customer_full_address =jobj.getString("customer_full_address");
                    String customer_full_address_from_json = customer_full_address.substring(2, customer_full_address.toString().length() - 2);
                    if(customer_full_address_from_json.length() == 0){
                        full_address.setHint("Fill Full Address");
                    }else{
                        full_address.setText(customer_full_address_from_json);
                    }
                }

                if(jobj.has("pan_number")) {
                    String pan = jobj.getString("pan_number");
                    String pan_from_json1 = pan.substring(2, pan.toString().length() - 2);
                    Log.d("pan_number", pan_from_json1);
                    if (pan_from_json1.length() == 0) {
                        pan_from_json.setText("NA");
                    } else {
                        pan_from_json.setText(pan_from_json1);
                    }
                } else {
                    pan_from_json.setText("NA");
                }

                if(jobj.has("aadhar_number")) {
                String aadhar_no =jobj.getString("aadhar_number");
                String aadhar_no_from_json1 = aadhar_no.substring(2, aadhar_no.toString().length() - 2);
                if(aadhar_no_from_json1.length() == 0){
                    aadhar_from_json.setText("NA");
                }else{
                    aadhar_from_json.setText(aadhar_no_from_json1);
                }
                } else {
                    aadhar_from_json.setText("NA");
                }

                if(jobj.has("customer_fax")){
                    String fax_no =jobj.getString("customer_fax");
                    String fax_no_from_json1 = fax_no.substring(2, fax_no.toString().length() - 2);
                    if(fax_no_from_json1.length() == 0){
                        fax_from_json.setHint("FAX Number");
                    }else{
                        fax_from_json.setText(fax_no_from_json1);
                    }
                } else if(jobj.has("fax")){
                    String fax_no =jobj.getString("fax");
                    String fax_no_from_json1 = fax_no.substring(2, fax_no.toString().length() - 2);
                    if(fax_no_from_json1.length() == 0){
                        fax_from_json.setHint("FAX Number");
                    }else{
                        fax_from_json.setText(fax_no_from_json1);
                    }
                }

                if(jobj.has("transport_website")){
                    String transport_website =jobj.getString("transport_website");
                    String transport_website_from_json = transport_website.substring(2, transport_website.toString().length() - 2);
                    if(transport_website_from_json.length() == 0){
                        website_from_json.setHint("Website Name / URL");
                    }else{
                        website_from_json.setText(transport_website_from_json);
                    }
                } else if(jobj.has("customer_website")){
                    String customer_website =jobj.getString("customer_website");
                    String customer_website_from_json = customer_website.substring(2, customer_website.toString().length() - 2);
                    if(customer_website_from_json.length() == 0){
                        website_from_json.setHint("Website Name / URL");
                    }else{
                        website_from_json.setText(customer_website_from_json);
                    }
                }

                if(jobj.has("transporter_states_being_operated")){
                    String transport_states_being_operated =jobj.getString("transporter_states_being_operated");
                    String transport_states_being_operated_from_json = transport_states_being_operated.substring(2, transport_states_being_operated.toString().length() - 2);
                    if(transport_states_being_operated_from_json.length() == 0){
                        operated_states_from_json.setHint("State Name");
                    }else{
                        operated_states_from_json.setText(transport_states_being_operated_from_json);
                    }
                } else if(jobj.has("customer_states_being_operated")){
                    String customer_states_being_operated =jobj.getString("customer_states_being_operated");
                    String customer_states_being_operated_from_json = customer_states_being_operated.substring(2, customer_states_being_operated.toString().length() - 2);
                    if(customer_states_being_operated_from_json.length() == 0){
                        operated_states_from_json.setHint("State Name");
                    }else{
                        operated_states_from_json.setText(customer_states_being_operated_from_json);
                    }
                }

                if(jobj.has("transporter_number_of_trucks")){
                    String transporter_number_of_trucks =jobj.getString("transporter_number_of_trucks");
                    String transporter_number_of_trucks_from_json = transporter_number_of_trucks.substring(2, transporter_number_of_trucks.toString().length() - 2);
                    if(transporter_number_of_trucks_from_json.length() == 0){
                        no_of_trucks_from_json.setHint("NA");
                    }else{
                        no_of_trucks_from_json.setText(transporter_number_of_trucks_from_json);
                    }
                }

                if(jobj.has("transporter_gps_truck")){
                    String gps_truck =jobj.getString("transporter_gps_truck");
                    String gps_truck_from_json = gps_truck.substring(2, gps_truck.toString().length() - 2);
                    if(gps_truck_from_json.length() == 0){
                        no_of_gps_trucks_from_json.setHint("No. of GPS trucks");
                    }else{
                        no_of_gps_trucks_from_json.setText(gps_truck_from_json);
                    }
                }

                if(jobj.has("from_location")){
                    String from_location =jobj.getString("from_location");
                    String from_location_from_json = from_location.substring(2, from_location.toString().length() - 2);
                    if(from_location_from_json.length() == 0){
                        from_from_json.setHint("From Location");
                    }else{
                        from_from_json.setText(from_location_from_json);
                    }
                }

                if(jobj.has("to_location")){
                    String to_location =jobj.getString("to_location");
                    String to_location_from_json = to_location.substring(2, to_location.toString().length() - 2);
                    if(to_location_from_json.length() == 0){
                        to_from_json.setHint("To Location");
                    }else{
                        to_from_json.setText(to_location_from_json);
                    }
                }

            } catch (JSONException e) {e.printStackTrace();}

            dialogue.dismiss();
        }
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.update_btn){

            YoulorrySession session12 = new YoulorrySession(this);
            String username = session12.getusername();
            String password = session12.getpass_word();
            String userid = session12.getuser_id();
            String role = session12.getrole();

            String email = email_from_json.getText().toString();

            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

            if (!email.matches(emailPattern))
            {
                Toast.makeText(getApplicationContext(),"Invalid email address", Toast.LENGTH_SHORT).show();
            }
            else
            {

            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-update-user-profile/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                //  urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                String[] full_name = name_from_json.getText().toString().split(" ");
                String complete_last_name = "";

                for(int i=1;i<full_name.length;i++)
                {
                    complete_last_name= complete_last_name+full_name[i]+" ";
                }

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);
                cred.put("update_profile", "update_profile");

                cred.put("first_name", full_name[0]);
                cred.put("last_name", complete_last_name);
                cred.put("full_address", full_address.getText().toString());
                cred.put("email_address", email_from_json.getText().toString());
                cred.put("fax", fax_from_json.getText().toString());
                cred.put("transport_name",company_name_from_json.getText().toString());
                cred.put("website",website_from_json.getText().toString());
                cred.put("states_being_operated", operated_states_from_json.getText().toString());
                cred.put("from_location", from_from_json.getText().toString());
                cred.put("to_location", to_from_json.getText().toString());
                cred.put("gps_truck", no_of_gps_trucks_from_json.getText().toString());

                Log.d("first_name", full_name[0]);
                Log.d("last_name", complete_last_name);
/*                if(role.equals("transporter")){
                    cred.put("transporter_first_name", full_name[0]);
                    cred.put("transporter_last_name", complete_last_name);
                    cred.put("transporter_full_address", address_from_json);
                    cred.put("transporter_email_address", email_from_json.getText().toString());
                    cred.put("transporter_fax", fax_from_json.getText().toString());
                    cred.put("transporter_transport_name",company_name_from_json.getText().toString());
                    cred.put("transporter_website",website_from_json.getText().toString());
                    cred.put("transporter_states_being_operated", operated_states_from_json.getText().toString());
                    cred.put("transporter_from_location", from_from_json.getText().toString());
                    cred.put("transporter_to_location", to_from_json.getText().toString());
                }else if(role.equals("customer")){
                    cred.put("customer_first_name", full_name[0]);
                    cred.put("customer_last_name", complete_last_name);
                    cred.put("customer_full_address", address_from_json);
                    cred.put("customer_email_address", email_from_json.getText().toString());
                    cred.put("customer_fax", fax_from_json.getText().toString());
                    cred.put("customer_transport_name",company_name_from_json.getText().toString());
                    cred.put("customer_website",website_from_json.getText().toString());
                    cred.put("customer_states_being_operated", operated_states_from_json.getText().toString());
                    cred.put("customer_from_location", from_from_json.getText().toString());
                    cred.put("customer_to_location", to_from_json.getText().toString());
                }
*/

                    OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                wr.flush();
                wr.close();

                //display what returns the POST request

                StringBuilder sb = new StringBuilder();
                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                    br.close();
                    Log.d("return_data_for_profile_update", sb.toString());
                    if(sb.toString().equals("ok")){
                        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();
                    }
                 //   Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show();
                    new FetchingUserData().execute();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
          }

        }
    }
    class FetchingUserData extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session012 = new YoulorrySession(con);
            String username = session012.getusername();
            String password = session012.getpass_word();
            String userid = session012.getuser_id();
            String role = session012.getrole();

            StringBuilder sb1 = new StringBuilder();
            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-update-user-profile/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);

                OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                wr.flush();
                wr.close();

                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb1.append(line);
                    }
                    br.close();

                    //       Toast.makeText(con, sb1.toString(), Toast.LENGTH_SHORT).show();
                    Log.d("profile_details", sb1.toString());

                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return sb1.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            // Toast.makeText(con, s.toString(), Toast.LENGTH_LONG).show();

            try {

                JSONObject json_data = new JSONObject(s.toString());
                String meta_keys_jsondata = json_data.getString("meta_keys");

                JSONObject jobj = new JSONObject(meta_keys_jsondata);

                if(jobj.has("first_name") || jobj.has("last_name")){

                    String f_name = jobj.getString("first_name");
                    String f_name_from_json = f_name.substring(2, f_name.toString().length() - 2);

                    String l_name = jobj.getString("last_name");
                    String l_name_from_json = l_name.substring(2, l_name.toString().length() - 2);

                    new YoulorrySession(con).setFirstName(f_name_from_json+" "+l_name_from_json);

                }else {
                    new YoulorrySession(con).setFirstName("Not Fetched!");
                }
            } catch (JSONException e) {e.printStackTrace();}
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        startActivity(new Intent(this, MainActivity.class));
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

}
