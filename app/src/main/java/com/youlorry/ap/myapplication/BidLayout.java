package com.youlorry.ap.myapplication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.youlorry.ap.myapplication.Adapter.Bidding_Adapter;
import com.youlorry.ap.myapplication.JavaClasses.InputFilterMinMax;
import com.youlorry.ap.myapplication.Model.Bidding_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import static android.os.Build.ID;

/**
 * Created by Arpit Prajapati on 3/20/2017.
 */

public class BidLayout extends Fragment implements View.OnClickListener {

    Context con;
    //    Button bid_btn;
//    TextView weeks1,days1,hours1,minutes1,seconds1;
    int id_post, place_new_bid_value, place_weight_capacity;
    RecyclerView bidding_recycler_view;
    LinearLayout ll_recyclerView, ll_nodatafound;
    RecyclerView.LayoutManager layoutManager;
    Bidding_Adapter bidding_adapter;
    List<Bidding_Model> bidding_modelList = new ArrayList<>();
    Timer _timer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bid_layout, container, false);

        con = getActivity();
        bidding_recycler_view = (RecyclerView) v.findViewById(R.id.bidding_recycler_view);
        ll_nodatafound = (LinearLayout) v.findViewById(R.id.ll_nodatafound);
        ll_recyclerView = (LinearLayout) v.findViewById(R.id.ll_recyclerView);
        YoulorrySession session = new YoulorrySession(con);

        if (session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0) {

            Intent in = new Intent(con, LoginActivity.class);
            startActivity(in);

        }

//        weeks1 = (TextView) v.findViewById(R.id.w);
//        days1 = (TextView) v.findViewById(R.id.d);
//        hours1 = (TextView) v.findViewById(R.id.h);
//        minutes1 = (TextView) v.findViewById(R.id.m);
//        seconds1 = (TextView) v.findViewById(R.id.s);
//
//        bid_btn = (Button) v.findViewById(R.id.bid_btn_id);
//        bid_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showpopup();
////                Toast.makeText(con, "Success", Toast.LENGTH_SHORT).show();
////                startActivity(new Intent(con, BiddingNewValue.class));
//            }
//        });

        new FetchAllAvailableBids().execute();

        return v;
    }


    private void showpopup(int postid) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.bidding_new_value, null);
        alert.setTitle("");
        alert.setView(alertLayout);
        final AlertDialog dialog = alert.create();
        // dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        LinearLayout linnerlayout_dismiss = (LinearLayout) alertLayout.findViewById(R.id.linearlayout_Dismiss);
        Button btn_updated = (Button) alertLayout.findViewById(R.id.btn_updated);
        final EditText et_place_new_bid_value = (EditText) alertLayout.findViewById(R.id.et_place_new_bid_value);

        final EditText et_place_weight_capacity = (EditText) alertLayout.findViewById(R.id.et_place_weight_capacity);

//        et_place_new_bid_value.setFilters(new InputFilter[]{ new InputFilterMinMax("1","100000000000000000000000")});
//        et_place_weight_capacity.setFilters(new InputFilter[]{ new InputFilterMinMax("1","100000000000000000000000")});


        et_place_new_bid_value.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String x = s.toString();
                if (x.startsWith("0")) {
                    et_place_new_bid_value.setText("");
                    //your stuff here
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        et_place_weight_capacity.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String x = s.toString();
                if (x.startsWith("0")) {
                    et_place_weight_capacity.setText("");
                    //your stuff here
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        });

        btn_updated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    int placevalue;
                    //   if(et_place_new_bid_value.getText().toString().length()>0 && et_place_weight_capacity.getText().toString().length()>0) {
                    if (validateInput_Field(et_place_new_bid_value) && validateInput_Field(et_place_weight_capacity)) {

                        place_new_bid_value = Integer.parseInt(et_place_new_bid_value.getText().toString());
                        place_weight_capacity = Integer.parseInt(et_place_weight_capacity.getText().toString());
                        // if(validatevechile_number(et_place_new_bid_value)&&validatevechile_number(et_place_weight_capacity))
                        new PostBid().execute();
                        dialog.dismiss();
                    }
                    // }
//                   else {
//
//                     Toast.makeText(getActivity(),"Field value Empty",Toast.LENGTH_SHORT).show();
//                   }

                    // dialog.dismiss();
//                    Intent intent=new Intent(Forgot_Password_Activity.this,Login_Activity.class);
//                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();
    }

    private boolean validateInput_Field(EditText editText) {

        if (editText.getText().toString().trim().isEmpty()) {
            editText.setError(getResources().getText(R.string.emptyfield));
            //   name_Seperator_V.setBackgroundColor(getResources().getColor(R.color.red));
            return false;
        } else {
            //  name_RL.setErrorEnabled(false);
            return true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(!bidding_modelList.isEmpty()) {
            _timer.cancel();
        }
    }

    // call this function every 1 sec with a timer
    // close timer before you finish actvity

    private void countDownAction() {
        for (int index = 0; index < this.bidding_modelList.size(); index++) {
            Bidding_Model bModal = bidding_modelList.get(index);
            bModal.decreaseRemainingTime();
        }
        bidding_adapter.notifyDataSetChanged();
        //
        // refresh recycler view data here
        //bidding_recycler_view.not
    }

    public void countdown() {

        SimpleDateFormat serverFormat;
        Calendar c = Calendar.getInstance();
        serverFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        String curr_date = serverFormat.format(c.getTime());

        Log.d("my date", curr_date);

        Time TimerSet = new Time();
        TimerSet.set(00, 19, 17, 30, 3, 2017); //day month year
        TimerSet.normalize(true);
        long millis = TimerSet.toMillis(true);

        Time TimeNow = new Time();
        TimeNow.setToNow(); // set the date to Current Time
        TimeNow.normalize(true);
        long millis2 = TimeNow.toMillis(true);

        Log.d("future date", millis + "");
        Log.d("current date", millis2 + "");

        long millisset = millis - millis2; //subtract current from future to set the time remaining

        Log.d("difference of dates", millisset + "");

        final int smillis = (int) (millis); //convert long to integer to display conversion results
        final int smillis2 = (int) (millis2);

        new CountDownTimer(millisset, 1000) {

            public void onTick(long millisUntilFinished) {

                // decompose difference into days, hours, minutes and seconds
                int weeks = (int) ((millisUntilFinished / 1000) / 604800);
                int days = (int) ((millisUntilFinished / 1000) / 86400);
                int hours = (int) (((millisUntilFinished / 1000) - (days * 86400)) / 3600);
                int minutes = (int) (((millisUntilFinished / 1000) - ((days * 86400) + (hours * 3600))) / 60);
                int seconds = (int) ((millisUntilFinished / 1000) % 60);
                int millicn = (int) (millisUntilFinished / 1000);

//                weeks1.setText("Left :- ");
//                days1.setText(days+"d");
//                hours1.setText(":" +hours+"h");
//                minutes1.setText(":" +minutes+"m");
//                seconds1.setText(":" +seconds+"s");

            }

            public void onFinish() {
//                weeks1.setText("done!");
//                days1.setText("");
//                hours1.setText("");
//                minutes1.setText("");
//                seconds1.setText("");
            }
        }.start();
    }

    @Override
    public void onClick(View view) {

    }

    public void onitemclick(int postid) {
        id_post = postid;
        showpopup(postid);
    }

    class PostBid extends AsyncTask<Void, Void, String> {
        ProgressDialog dialogue;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            YoulorrySession session12 = new YoulorrySession(con);
            String username = session12.getusername();
            String password = session12.getpass_word();
            String userid = session12.getuser_id();
            String role = session12.getrole();

            StringBuilder sb = new StringBuilder();

            URL urlObj = null;
            try {
                urlObj = new URL("http://" + getString(R.string.server_api_url) + ".com/api-factory-load/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                //  urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username", username);
                cred.put("password", password);
                cred.put("userid", userid);
                cred.put("role", role);
                cred.put("factory_load_ID", String.valueOf(id_post));
                cred.put("place_new_bid_value", String.valueOf(place_new_bid_value));
                cred.put("place_weight_capacity", String.valueOf(place_weight_capacity));
                cred.put("action", "update_bid");
                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                wr.flush();
                wr.close();

                //display what returns the POST request
                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();

                    Log.d("Available bids data :", sb.toString());
                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return sb.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialogue.dismiss();
        }

    }


    class FetchAllAvailableBids extends AsyncTask<Void, Void, String> {
        ProgressDialog dialogue;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session12 = new YoulorrySession(con);
            String username = session12.getusername();
            String password = session12.getpass_word();
            String userid = session12.getuser_id();
            String role = session12.getrole();

            StringBuilder sb = new StringBuilder();

            URL urlObj = null;
            try {
                urlObj = new URL("http://" + getString(R.string.server_api_url) + ".com/api-factory-load/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                //  urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username", username);
                cred.put("password", password);
                cred.put("userid", userid);
                cred.put("role", role);
                cred.put("req_key", "get_bidding");
                cred.put("action", "fetch_bid_load");
                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                wr.flush();
                wr.close();

                //display what returns the POST request
                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();

                    Log.d("Available bids data :", sb.toString());
                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return sb.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialogue.dismiss();
            if (s.toString().contains("some error")) {
                ll_recyclerView.setVisibility(View.GONE);
                ll_nodatafound.setVisibility(View.VISIBLE);
            } else {
                ll_nodatafound.setVisibility(View.GONE);
                ll_recyclerView.setVisibility(View.VISIBLE);

                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s.toString());

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = null;
                        try {
                            obj = jsonArray.getJSONObject(i);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Bidding_Model bidding_model = new Bidding_Model();
                        try {
                            bidding_model.setID(obj.getInt("ID"));
                            bidding_model.setLowest_bid_value(obj.getString("lowest_bid_value"));
                            bidding_model.setMy_bid_value(obj.getString("my_bid_value"));
                            bidding_model.setHour_limit(obj.getInt("hour_limit"));
                            bidding_model.setServer_current_time(obj.getString("server_current_time"));
                            bidding_model.setExpiry_time(obj.getString("expiry_time"));
                            JSONObject meta_keys = obj.getJSONObject("meta_keys");

                            Bidding_Model.MetaKeysBean metaKeysBean = new Bidding_Model.MetaKeysBean(meta_keys);

                            bidding_model.setMeta_keys(metaKeysBean);

                            bidding_modelList.add(bidding_model);

//                    {
//
//                    }
                            //    bidding_model.setMeta_keys(obj.getJSONArray("meta_keys"));

//                    organizationsListingModel.setPost_date(obj.getString("name"));
//                    organizationsListingModel.setPost_date_gmt(obj.getString("logo"));
//                    organizationsListingModel.setPost_content(obj.getInt("id"));
//                    organizationsListingModel.setFollowers_count(obj.getInt("followers_count"));
//                    organizationsListingModel.setCampaigns_count(obj.getInt("campaigns_count"));
//                    organizationsListingModelArrayList.add(organizationsListingModel);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                    scheduleTimer();
                    layoutManager = new LinearLayoutManager(getActivity());
                    bidding_recycler_view.setLayoutManager(layoutManager);
                    bidding_adapter = new Bidding_Adapter(getActivity(), bidding_modelList, BidLayout.this);
                    bidding_recycler_view.setAdapter(bidding_adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    public void scheduleTimer() {
        _timer = new Timer();

        _timer.schedule(new TimerTask() {
            @Override
            public void run() {
                // use runOnUiThread(Runnable action)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        countDownAction();
                    }
                });
            }

        }, 1, 1000);
    }
}