package com.youlorry.ap.myapplication;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Arpit Prajapati on 12/20/16.
 */
public class Resultforfindload extends Fragment implements View.OnClickListener{

    String s_city1,d_city1,json_data_for_load_list;
    String result,data="";
    ListView listView1;
    Context con;
    TextView t1;

    ListView list;
    CustomAdapterforfindloadresult adapter;
    public ArrayList<ListModelforloadlistrow> CustomListViewValuesArr11 = new ArrayList<ListModelforloadlistrow>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.result_for_find_load,container,false);


        s_city1 = getArguments().getString("source_key");
        d_city1 = getArguments().getString("destination_key");
        json_data_for_load_list = getArguments().getString("json_data_for_load_list");

        con = getActivity();

        YoulorrySession session = new YoulorrySession(con);
        session.setMatchingloadsforfindload(json_data_for_load_list);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(con , LoginActivity.class);
            startActivity(in);

        }

        try {
            InputMethodManager input = (InputMethodManager) getActivity()
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            input.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }catch(Exception e) {
            e.printStackTrace();
        }

        listView1 = (ListView) v.findViewById(R.id.load_filter_listview_for_findload);
        t1 = (TextView) v.findViewById(R.id.no_matching_data_found);
        new TheTask().execute();

        return v;
    }

    @Override
    public void onClick(View view) {

    }

    class TheTask extends AsyncTask<Void, Void, String> {


        ProgressDialog dialogue;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

            return json_data_for_load_list;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

          //  Toast.makeText(con, s.toString(), Toast.LENGTH_SHORT).show();

            if(s.length() < 20){
                listView1.setVisibility(View.GONE);
                t1.setVisibility(View.VISIBLE);
                t1.setText("No Matching Data Found !!!");
            }

            try {

                JSONArray jsonArray = new JSONArray(s);
                String [] load_list_for_list_view_id = new String[jsonArray.length()];

                Resources res =getResources();
                for(int i=0; i < jsonArray.length(); i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    int id = Integer.parseInt(jsonObject.optString("ID").toString());

                    String meta_keys_jsondata = jsonObject.optString("meta_keys").toString();

                //    String posted_by_name = jsonObject.getString("posted_by");

                    JSONObject jobj = new JSONObject(meta_keys_jsondata);
                    String source_city_jsondata=jobj.getString("load_source_city");
                    String s_city_from_json = source_city_jsondata.substring(2, source_city_jsondata.toString().length() - 2);

                    String destination_city_jsondata=jobj.getString("load_destination_city");
                    String d_city_from_json = destination_city_jsondata.substring(2, destination_city_jsondata.toString().length() - 2);

                    String schedule_date_jsondata=jobj.getString("load_date");
                    String scheduled_date_from_json = schedule_date_jsondata.substring(2, schedule_date_jsondata.toString().length() - 2);

                    String truck_type_jsondata=jobj.getString("load_truck_type");
                    String truc_type_from_jsondata = truck_type_jsondata.substring(2, truck_type_jsondata.toString().length() - 2);

                    String load_material_jsondata=jobj.getString("load_material");
                    String load_material_from_jsondata = load_material_jsondata.substring(2, load_material_jsondata.toString().length() - 2);

                    String left_count_jsondata=jobj.getString("left_count");
                    String left_count_from_json = left_count_jsondata.substring(2, left_count_jsondata.toString().length() - 2);

                    String exp_val_from_json,load_advance_in_percentage_from_json;
                    if(jobj.has("part_amount")){
                        exp_val_from_json=jobj.getString("part_amount");
                        //    String expected_value_jsondata=jobj.getString("part_amount");
                        //    exp_val_from_json = expected_value_jsondata.substring(2, expected_value_jsondata.toString().length() - 2);
                    } else {
                        exp_val_from_json = "NA";
                    }

                    if(jobj.has("part_amount")){
                        load_advance_in_percentage_from_json=jobj.getString("part_advance_of_load");
//                        String load_advance_in_percentage_jsondata=jobj.getString("part_advance_of_load");
//                        String load_advance_in_percentage_from_json = load_advance_in_percentage_jsondata.substring(2, load_advance_in_percentage_jsondata.toString().length() - 2);
                    } else {
                        load_advance_in_percentage_from_json = "NA";
                    }

//                    adapter=new CustomAdapterforfindloadresult( con, CustomListViewValuesArr11,res);
//                    listView1.setAdapter( adapter );

                    load_list_for_list_view_id[i] = s_city_from_json;

                    final ListModelforloadlistrow sched121 = new ListModelforloadlistrow();
                    String[] s_city_split =s_city_from_json.split(",");
                    String[] d_city_split =d_city_from_json.split(",");
                    /******* Firstly take data in model object ******/
                    sched121.setSourcecity2(s_city_split[0]);
                    sched121.setDestinationcity2(d_city_split[0]);
                    sched121.setTrucktype("YLL"+id);
                    sched121.setPrice2(exp_val_from_json);
                    sched121.setAdvPrice(load_advance_in_percentage_from_json);
                    sched121.setDate2(scheduled_date_from_json);
                    sched121.setMaterialType(load_material_from_jsondata);
                    sched121.setWeight(left_count_from_json);
                  //  sched121.setPostedby2(posted_by_name);

                    /******** Take Model Object in ArrayList **********/
                    CustomListViewValuesArr11.add(sched121);
                }
                adapter=new CustomAdapterforfindloadresult( con, CustomListViewValuesArr11,res);
                listView1.setAdapter( adapter );

            } catch (JSONException e) {e.printStackTrace();}
            dialogue.dismiss();
        }
    }

}
