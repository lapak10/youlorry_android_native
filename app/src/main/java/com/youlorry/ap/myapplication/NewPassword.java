package com.youlorry.ap.myapplication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Arpit Prajapati on 1/24/17.
 */

public class NewPassword extends YouLorryActivity implements View.OnClickListener {

    TextView note_for_new_pass_layout;
    EditText new_password_edit_text,confirm_new_password_edit_text;
    Button set_new_pass_btn;
    String key_for_new_pass,contact_number,otp_number_for_new_pass;
    ProgressDialog dialogue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_password_layout);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width*0.9),(int)(height*0.4));

        Bundle bd = getIntent().getExtras();
        key_for_new_pass = bd.getString("key_for_new_pass");
        contact_number = bd.getString("contact_number");
        otp_number_for_new_pass = bd.getString("otp_number_for_new_pass");

        note_for_new_pass_layout = (TextView) findViewById(R.id.note_for_new_pass_layout);
        new_password_edit_text = (EditText) findViewById(R.id.new_password_edit_text);
        confirm_new_password_edit_text = (EditText) findViewById(R.id.confirm_new_password_edit_text);
        set_new_pass_btn = (Button) findViewById(R.id.set_new_pass_btn);

        set_new_pass_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.set_new_pass_btn){

            if(key_for_new_pass.equals("new_pass_key")){

                if(new_password_edit_text.getText().toString().length() == 0 || new_password_edit_text.getText().toString().length() < 5){
                    new_password_edit_text.setError("atleast 5 digits required!");
                } else if(confirm_new_password_edit_text.getText().toString().length() == 0 || confirm_new_password_edit_text.getText().toString().length() < 5){
                    confirm_new_password_edit_text.setError("atleast 5 digits required!");
                } else if(new_password_edit_text.getText().toString().compareTo(confirm_new_password_edit_text.getText().toString())!=0){
                    new AlertDialog.Builder(this)
                            .setTitle("Password Alert!")
                            .setMessage("Both passwords are not same. Please input same password!")
                            .show();
                } else {

                    dialogue = new ProgressDialog(this);
                    dialogue.setTitle("Loading ...");
                    dialogue.show();

                    URL urlObj = null;
                    try {
                        urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-forgotpassword/");
                        HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                        urlConnection.setDoOutput(true);
                        urlConnection.setDoInput(true);
                        urlConnection.setUseCaches(false);
                        urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                        //  urlConnection.setRequestProperty("Accept", "application/json");
                        urlConnection.setRequestMethod("POST");
                        urlConnection.connect();

                        JSONObject cred = new JSONObject();

//                        cred.put("new_password", "new_pass");
                        cred.put("otp_pin", otp_number_for_new_pass);
                        cred.put("forgot_password", "forgot_password");
                        cred.put("contact_number", contact_number);
                        cred.put("new_password", confirm_new_password_edit_text.getText().toString());

                        OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                        wr.write(cred.toString());
                        wr.flush();
                        wr.close();

                        StringBuilder sb = new StringBuilder();
                        int HttpResult = urlConnection.getResponseCode();
                        if (HttpResult == HttpURLConnection.HTTP_OK) {
                            BufferedReader br = new BufferedReader(
                                    new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                            String line = null;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            br.close();

                            Log.d("server_response_for_otp_verification_in_new_password_process", sb.toString());

                            if(sb.toString().equals("invalid_otp")){
                                Toast.makeText(this, "Invalid OTP Number!!!", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(this, LoginActivity.class));
                            } else if (sb.toString().equals("ok")) {
                                Toast.makeText(this, "Password Changed!", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(this, LoginActivity.class));
                            }
                        }

                    } catch (ProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            dialogue.dismiss();
                        }
                    }, 2000);
                 }
                } else {
                Toast.makeText(this, "sorry some error occurs. try later!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
