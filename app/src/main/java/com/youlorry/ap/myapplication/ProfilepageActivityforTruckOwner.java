package com.youlorry.ap.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Arpit Prajapati on 12/20/16.
 */

public class ProfilepageActivityforTruckOwner extends Activity implements View.OnClickListener{

    Button profile_update_btn;
    Context con;
    TextView contact_no,establishing_date_from_json,address_from_json,wel_come_text,pan_from_json,service_tax_from_json,tin_cin_from_json,aadhar_from_json;
    EditText name_from_json,email_from_json,fax_from_json,company_name_from_json,website_from_json,operated_states_from_json,from_from_json,to_from_json ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.t_profile_page);

//        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        //     ActionBar act_bar = getActionBar();
        //    act_bar.setHomeButtonEnabled(true);

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        YoulorrySession session = new YoulorrySession(this);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(this , LoginActivity.class);
            startActivity(in);

        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        contact_no = (TextView) findViewById(R.id.contact_from_json);
        establishing_date_from_json = (TextView) findViewById(R.id.establishing_date_from_json);
        address_from_json = (TextView) findViewById(R.id.address_from_json);
        wel_come_text = (TextView) findViewById(R.id.welcome_text);
        pan_from_json = (TextView) findViewById(R.id.pan_from_json);
        service_tax_from_json = (TextView) findViewById(R.id.service_tax_from_json);
        tin_cin_from_json = (TextView) findViewById(R.id.tin_cin_from_json);
        aadhar_from_json = (TextView) findViewById(R.id.aadhar_from_json);

        name_from_json = (EditText) findViewById(R.id.name_from_json);
        email_from_json = (EditText) findViewById(R.id.email_from_json);
        fax_from_json = (EditText) findViewById(R.id.fax_from_json);
        company_name_from_json = (EditText) findViewById(R.id.company_name_from_json);
        website_from_json = (EditText) findViewById(R.id.website_from_json);
        operated_states_from_json = (EditText) findViewById(R.id.operated_states_from_json);
        from_from_json = (EditText) findViewById(R.id.from_from_json);
        to_from_json = (EditText) findViewById(R.id.to_from_json);

        con = this;
        new TheTask().execute();
        profile_update_btn = (Button) findViewById(R.id.update_btn);

        profile_update_btn.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
                startActivity(new Intent(this, MainActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class TheTask extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;
        String str;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session012 = new YoulorrySession(con);
            String username = session012.getusername();
            String password = session012.getpass_word();
            String userid = session012.getuser_id();
            String role = session012.getrole();

            StringBuilder sb1 = new StringBuilder();
            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-update-user-profile/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);

                OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                wr.flush();
                wr.close();

                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb1.append(line);
                    }
                    br.close();

                    //       Toast.makeText(con, sb1.toString(), Toast.LENGTH_SHORT).show();

                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return sb1.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            // Toast.makeText(con, s.toString(), Toast.LENGTH_LONG).show();

            try {

                JSONObject json_data = new JSONObject(s.toString());
                String meta_keys_jsondata = json_data.getString("meta_keys");

             //   Toast.makeText(con, meta_keys_jsondata, Toast.LENGTH_SHORT).show();

                JSONObject jobj = new JSONObject(meta_keys_jsondata);

                String f_name =jobj.getString("first_name");
                String f_name_from_json = f_name.substring(2, f_name.toString().length() - 2);

                String l_name =jobj.getString("last_name");
                String l_name_from_json = l_name.substring(2, l_name.toString().length() - 2);
                name_from_json.setText(f_name_from_json+" "+l_name_from_json);
                wel_come_text.setText(f_name_from_json+" "+l_name_from_json);
                String email =jobj.getString("customer_user_email_address");
                String email_from_json1 = email.substring(2, email.toString().length() - 2);
                email_from_json.setText(email_from_json1);
                String contact_no1 =jobj.getString("customer_contact_number");
                String contact_no_from_json = contact_no1.substring(2, contact_no1.toString().length() - 2);
                if(contact_no_from_json.length() == 0){
                    contact_no.setText("NA");
                }else{
                    contact_no.setText(contact_no_from_json);
                }
                String full_address =jobj.getString("full_address");
                String full_address_from_json = full_address.substring(2, full_address.toString().length() - 2);

                String locality =jobj.getString("locality");
                String locality_from_json = locality.substring(2, locality.toString().length() - 2);

                String city =jobj.getString("city");
                String city_from_json = city.substring(2, city.toString().length() - 2);

                String state =jobj.getString("state");
                String state_from_json = state.substring(2, state.toString().length() - 2);

                String country =jobj.getString("country");
                String country_from_json = country.substring(2, country.toString().length() - 2);

                String pin_code =jobj.getString("pin_code");
                String pincode_from_json = pin_code.substring(2, pin_code.toString().length() - 2);
                if((full_address_from_json+locality_from_json+city_from_json+state_from_json+country_from_json+pincode_from_json).length() == 0){
                    address_from_json.setText("NA");
                }else{
                    address_from_json.setText(full_address_from_json+", "+locality_from_json+", "+city_from_json+" ("+state_from_json+") "+country_from_json+" - "+pincode_from_json);
                }
                String landline =jobj.getString("landline_number");
                String landline_from_json = landline.substring(2, landline.toString().length() - 2);

                String fax =jobj.getString("fax");
                String fax_from_json1 = fax.substring(2, fax.toString().length() - 2);
                fax_from_json.setText(fax_from_json1);
                String company_name =jobj.getString("customer_transport_name");
                String company_name_from_json1 = company_name.substring(2, company_name.toString().length() - 2);
                company_name_from_json.setText(company_name_from_json1);
                String establishment_date =jobj.getString("customer_business_establishment_date");
                String establishment_date_from_json1 = establishment_date.substring(2, establishment_date.toString().length() - 2);
                if(establishment_date_from_json1.length() == 0){
                    establishing_date_from_json.setText("NA");
                }else{
                    establishing_date_from_json.setText(establishment_date_from_json1);
                }
                String website =jobj.getString("customer_website");
                String website_from_json1 = website.substring(2, website.toString().length() - 2);
                website_from_json.setText(website_from_json1);
                String pan =jobj.getString("customer_pan");
                String pan_from_json1 = pan.substring(2, pan.toString().length() - 2);
                if(pan_from_json1.length() == 0){
                    pan_from_json.setText("NA");
                }else{
                    pan_from_json.setText(pan_from_json1);
                }
                String service_tax =jobj.getString("customer_service_tax");
                String service_tax_from_json1 = service_tax.substring(2, service_tax.toString().length() - 2);
                if(service_tax_from_json1.length() == 0){
                    service_tax_from_json.setText("NA");
                }else{
                    service_tax_from_json.setText(service_tax_from_json1);
                }
                String tin_cin =jobj.getString("customer_tin_cin");
                String tin_cin_from_json1 = tin_cin.substring(2, tin_cin.toString().length() - 2);
                if(tin_cin_from_json1.length() == 0){
                    tin_cin_from_json.setText("NA");
                }else{
                    tin_cin_from_json.setText(tin_cin_from_json1);
                }
                String states_being_operated =jobj.getString("customer_states_being_operated");
                String states_being_operated_from_json = states_being_operated.substring(2, states_being_operated.toString().length() - 2);
                operated_states_from_json.setText(states_being_operated_from_json);
                String customer_from_location =jobj.getString("customer_from_location");
                String customer_from_location_from_json = customer_from_location.substring(2, customer_from_location.toString().length() - 2);
                from_from_json.setText(customer_from_location_from_json);
                String customer_to_location =jobj.getString("customer_to_location");
                String customer_to_location_from_json = customer_to_location.substring(2, customer_to_location.toString().length() - 2);
                to_from_json.setText(customer_to_location_from_json);

            } catch (JSONException e) {e.printStackTrace();}

            dialogue.dismiss();
        }
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.update_btn){

            YoulorrySession session12 = new YoulorrySession(this);
            String username = session12.getusername();
            String password = session12.getpass_word();
            String userid = session12.getuser_id();
            String role = session12.getrole();

            String email = email_from_json.getText().toString();

            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

            if (email.matches(emailPattern))
            {
                Toast.makeText(getApplicationContext(),"Valid email address", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
            }

            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-update-user-profile/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                //  urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                String[] full_name = name_from_json.getText().toString().split(" ");
                String complete_last_name = "";

                for(int i=1;i<full_name.length;i++)
                {
                    complete_last_name= complete_last_name+full_name[i]+" ";
                }

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);
                cred.put("customer_first_name", full_name[0]);
                cred.put("customer_last_name", complete_last_name);
                cred.put("customer_email_address", email_from_json.getText().toString());
                cred.put("customer_fax", fax_from_json.getText().toString());
                cred.put("customer_transport_name",company_name_from_json.getText().toString());
                cred.put("customer_website",website_from_json.getText().toString());
                cred.put("customer_states_being_operated", operated_states_from_json.getText().toString());
                cred.put("customer_from_location", from_from_json.getText().toString());
                cred.put("customer_to_location", to_from_json.getText().toString());

                OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                wr.flush();
                wr.close();

                //display what returns the POST request

                StringBuilder sb = new StringBuilder();
                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                    br.close();

                    Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show();

                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }
}