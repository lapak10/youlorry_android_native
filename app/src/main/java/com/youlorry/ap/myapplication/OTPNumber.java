package com.youlorry.ap.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Arpit Prajapati on 1/24/17.
 */

public class OTPNumber extends YouLorryActivity implements View.OnClickListener {

    TextView note_for_otp_layout;
    EditText otp_number_edit_text;
    Button otp_cancel_btn,otp_submit_btn;
    String from_key,name,mob_number,pass,type_of_user,user_sub_type,pan_num,aadhar_num,contact_number;
    ProgressDialog dialogue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_number_layout);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width*0.9),(int)(height*0.4));

        Bundle bundle = getIntent().getExtras();
        from_key = bundle.getString("from_key");
        name = bundle.getString("first_name");
        mob_number = bundle.getString("contact_number");
        pass = bundle.getString("password");
        type_of_user = bundle.getString("type_of_user");
        user_sub_type = bundle.getString("user_sub_type");
        pan_num = bundle.getString("pan_number");
        aadhar_num = bundle.getString("aadhar_number");
        contact_number = bundle.getString("contact_number");

        note_for_otp_layout = (TextView) findViewById(R.id.note_for_otp_layout);
        otp_number_edit_text = (EditText) findViewById(R.id.otp_number_edit_text);
        otp_cancel_btn = (Button) findViewById(R.id.otp_cancel_btn);
        otp_submit_btn = (Button) findViewById(R.id.otp_submit_btn);

        otp_cancel_btn.setOnClickListener(this);
        otp_submit_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.otp_cancel_btn){
            finish();
        }

        if(view.getId() == R.id.otp_submit_btn){

//            Toast.makeText(this, "otp submitted", Toast.LENGTH_SHORT).show();
            if(from_key.equals("forgot_password")){
                if(otp_number_edit_text.getText().toString().length() == 0 || otp_number_edit_text.getText().toString().length() < 6) {
                    otp_number_edit_text.setError("6 digits reuired!");
                } else {
                      finish();
                      Intent in = new Intent(this, NewPassword.class);
                      Bundle bd = new Bundle();
                      bd.putString("key_for_new_pass", "new_pass_key");
                      bd.putString("contact_number", contact_number);
                      bd.putString("otp_number_for_new_pass", otp_number_edit_text.getText().toString());
                      in.putExtras(bd);
                      startActivity(in);
                }
            } else if(from_key.equals("registration_form")) {

                if(otp_number_edit_text.getText().toString().length() == 0 || otp_number_edit_text.getText().toString().length() < 6) {
                    otp_number_edit_text.setError("6 digits reuired!");
                } else {

                    dialogue = new ProgressDialog(this);
                    dialogue.setTitle("Loading ...");
                    dialogue.show();

                    URL urlObj = null;
                    try {
                        urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-registration/");
                        HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                        urlConnection.setDoOutput(true);
                        urlConnection.setDoInput(true);
                        urlConnection.setUseCaches(false);
                        urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                        //  urlConnection.setRequestProperty("Accept", "application/json");
                        urlConnection.setRequestMethod("POST");
                        urlConnection.connect();

                        JSONObject cred = new JSONObject();

                        cred.put("otp_pin", otp_number_edit_text.getText().toString());
                        cred.put("first_name", name);
                        cred.put("contact_number", mob_number);
                        cred.put("password", pass);
                        cred.put("new_password", pass);
                        cred.put("pan_number", pan_num);
                        cred.put("aadhar_number", aadhar_num);
                        cred.put("type_of_user", type_of_user);
                        cred.put("user_sub_type", user_sub_type);

                        Log.d("first_name", name);
                        Log.d("contact_number", mob_number);
                        Log.d("password", pass);
                        Log.d("new_password", pass);
                        Log.d("pan_number", pan_num);
                        Log.d("aadhar_number", aadhar_num);
                        Log.d("type_of_user", type_of_user);
                        Log.d("user_sub_type", user_sub_type);

                        OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                        wr.write(cred.toString());
                        wr.flush();
                        wr.close();

                        StringBuilder sb = new StringBuilder();
                        int HttpResult = urlConnection.getResponseCode();
                        if (HttpResult == HttpURLConnection.HTTP_OK) {
                            BufferedReader br = new BufferedReader(
                                    new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                            String line = null;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            br.close();

                            Log.d("server_response_for_otp_verification_in_registration_process", sb.toString());

                            if(sb.toString().equals("error_invalid_otp")){
                                Toast.makeText(this, "Invalid OTP Number!!!", Toast.LENGTH_LONG).show();
                            } else if (sb.toString().equals("incomplete_fields_for_registration")) {
                                Toast.makeText(this, "Some error occur. Please try later!!!", Toast.LENGTH_LONG).show();
                                finish();
                                startActivity(new Intent(this, LoginActivity.class));
                            } else {
                                Toast.makeText(this, "Registration Done. Pending Approval!!!", Toast.LENGTH_LONG).show();
                                finish();
                                Intent in = new Intent(this, LoginActivity.class);
                                startActivity(in);
                            }
                        }

                    } catch (ProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                            dialogue.dismiss();
                }
            }
        }
    }
}
