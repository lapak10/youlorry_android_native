package com.youlorry.ap.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Arpit Prajapati on 12/15/16.
 */
public class CustomAdapterforloadlistview extends BaseAdapter implements View.OnClickListener {

    /*********** Declare Used Variables *********/
    private Context activity1;
    private ArrayList data;
    private static LayoutInflater inflater=null;
    public Resources res;
    ListModelforloadlistrow tempValues=null;
    int i=0;

    /*************  CustomAdapterfortrucklistview Constructor *****************/
    public CustomAdapterforloadlistview(Context a, ArrayList d, Resources resLocal) {

        /********** Take passed values **********/
        activity1 = a;
        data=d;
        res = resLocal;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = ( LayoutInflater )activity1.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /******** What is the size of Passed Arraylist Size ************/
    @Override
    public int getCount() {
        if(data.size()<=0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public void onClick(View view) {
        Log.v("CustomAdapter", "=====Row button clicked=====");
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView from_id,to_id,price_id,truck_type_id,posted_by_id,adv_price_id,sch_date,material_type;

    }

    /****** Depends upon data size called for each row , Create each ListView row *****/
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View vi = view;
        CustomAdapterforloadlistview.ViewHolder holder;

        if(view==null){

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.table_item_for_load_list_view, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new CustomAdapterforloadlistview.ViewHolder();
            holder.from_id = (TextView) vi.findViewById(R.id.from_id_1);
            holder.truck_type_id = (TextView) vi.findViewById(R.id.truck_type_id1);
            holder.to_id = (TextView) vi.findViewById(R.id.to_id_1);
            holder.price_id = (TextView) vi.findViewById(R.id.price_id_1);
            holder.adv_price_id = (TextView) vi.findViewById(R.id.adv_price_id_1);
            holder.sch_date = (TextView) vi.findViewById(R.id.sch_date);
            holder.material_type = (TextView) vi.findViewById(R.id.material_type_in_matching_loads);

         //   holder.posted_by_id = (TextView) vi.findViewById(R.id.posted_by_1);

            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        }
        else
            holder=(CustomAdapterforloadlistview.ViewHolder)vi.getTag();

        if(data.size()<=0)
        {
            holder.from_id.setText("No Data");
            holder.truck_type_id.setText("No Data");
            holder.to_id.setText("No Data");
            holder.price_id.setText("No Data");
            holder.adv_price_id.setText("No Data");
            holder.sch_date.setText("No Data");
            holder.material_type.setText("No Data");
         //   holder.posted_by_id.setText("No Data");

        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            tempValues=null;
            tempValues = ( ListModelforloadlistrow ) data.get( i );

            /************  Set Model values in Holder elements ***********/
        //    int res = (Integer.parseInt(tempValues.getPrice2()) * Integer.parseInt(tempValues.getAdvPrice())) / 100;
            String replaced_truck_type = tempValues.getTrucktype().toString().replace("\\","");
            String replaced_material_type = tempValues.getMaterialType().toString().replace("\\","");

            holder.from_id.setText( tempValues.getSourcecity2() );
            holder.to_id.setText( tempValues.getDestinationcity2() );
            holder.truck_type_id.setText( replaced_truck_type);
            holder.price_id.setText( tempValues.getPrice2() );
            holder.adv_price_id.setText( "₹ "+tempValues.getAdvPrice() );
            holder.sch_date.setText( tempValues.getDate2() );
            holder.material_type.setText(replaced_material_type);
         //   holder.posted_by_id.setText( tempValues.getPostedby2() );
            /******** Set Item Click Listner for LayoutInflater for each row *******/

            vi.setOnClickListener(new CustomAdapterforloadlistview.OnItemClickListener( i ));
        }
        return vi;
    }




    /********* Called when Item click in ListView ************/
    private class OnItemClickListener  implements View.OnClickListener {
        private int mPosition;
        ProgressDialog dialogue;

        OnItemClickListener(int position){
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {

            dialogue = new ProgressDialog(activity1);
            dialogue.setTitle("Loading ...");
            dialogue.show();

            Intent in = new Intent(activity1,LoadInfoActivity.class);

            in.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            Bundle args = new Bundle();
            args.putString("truck_list_key_for_json", String.valueOf(mPosition));
            in.putExtras(args);
            activity1.startActivity(in);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    dialogue.dismiss();
                }
            }, 2000);


//                Toast.makeText(activity, "HIIIIIIIIIIIIIIIIIII", Toast.LENGTH_SHORT).show();
            //  TrucklistActivity sct = (TrucklistActivity) activity;

            /****  Call  onItemClick Method inside CustomListViewAndroidExample Class ( See Below )****/

            //  sct.onItemClick(mPosition);
        }
    }

}
