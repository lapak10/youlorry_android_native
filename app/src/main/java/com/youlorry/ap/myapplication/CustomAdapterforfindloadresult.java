package com.youlorry.ap.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Arpit Prajapati on 12/20/16.
 */
public class CustomAdapterforfindloadresult extends BaseAdapter implements View.OnClickListener {
    /*********** Declare Used Variables *********/
    private Context activity;
    private ArrayList<ListModelforloadlistrow> data;
    private static LayoutInflater inflater=null;
    public Resources res;
    ListModelforloadlistrow tempValues=null;
    int i=0;
//    public int recieved_i_val = 0;

    /*************  CustomAdapterfortrucklistview Constructor *****************/
    public CustomAdapterforfindloadresult(Context a, ArrayList<ListModelforloadlistrow> data, Resources resLocal) {

        /********** Take passed values **********/
        activity = a;
        this.data=data;
        res = resLocal;
//        recieved_i_val = recieved_i_value;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = ( LayoutInflater )activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /******** What is the size of Passed Arraylist Size ************/
    @Override
    public int getCount() {
        if(data.size()<=0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public void onClick(View view) {
        Log.v("CustomAdapter", "=====Row button clicked=====");
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView from_id,to_id,price_id,truck_type_id,posted_by_name,adv_price_id,sch_date,material_type;

    }

    /****** Depends upon data size called for each row , Create each ListView row *****/
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View vi = view;
        CustomAdapterforfindloadresult.ViewHolder holder;

        if(view==null){

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
               vi = inflater.inflate(R.layout.table_item_for_load_list_view, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new CustomAdapterforfindloadresult.ViewHolder();
            holder.from_id = (TextView) vi.findViewById(R.id.from_id_1);
            holder.truck_type_id = (TextView) vi.findViewById(R.id.truck_type_id1);
            holder.to_id = (TextView) vi.findViewById(R.id.to_id_1);
            holder.price_id = (TextView) vi.findViewById(R.id.price_id_1);
            holder.adv_price_id = (TextView) vi.findViewById(R.id.adv_price_id_1);
            holder.sch_date = (TextView) vi.findViewById(R.id.sch_date);
            holder.material_type = (TextView) vi.findViewById(R.id.material_type_in_matching_loads);
            //    holder.posted_by_name = (TextView) vi.findViewById(R.id.posted_by_1);

            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        }
        else
            holder=(CustomAdapterforfindloadresult.ViewHolder)vi.getTag();

        if(data.size()<=0)
        {
            holder.from_id.setText("No Data");
            holder.truck_type_id.setText("No Data");
            holder.to_id.setText("No Data");
            holder.price_id.setText("No Data");
            holder.adv_price_id.setText("No Data");
            holder.sch_date.setText("No Data");
            holder.material_type.setText("No Data");
         //   holder.posted_by_name.setText("No Data");

        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            tempValues=null;
            tempValues =   data.get( i );

            /************  Set Model values in Holder elements ***********/
//            int res = (Integer.parseInt(tempValues.getPrice2()) * Integer.parseInt(tempValues.getAdvPrice())) / 100;
            String replaced_truck_type = tempValues.getTrucktype().toString().replace("\\","");
            String replaced_material_type = tempValues.getMaterialType().toString().replace("\\","");

            holder.from_id.setText( tempValues.getSourcecity2() );
            holder.to_id.setText( tempValues.getDestinationcity2() );
            holder.truck_type_id.setText( replaced_truck_type );
            holder.price_id.setText( "₹ "+tempValues.getPrice2() );
            holder.adv_price_id.setText( "₹ "+tempValues.getAdvPrice() );
            holder.sch_date.setText( tempValues.getDate2() );
            holder.material_type.setText(replaced_material_type+" ("+tempValues.getWeight()+"MT)");
        //    holder.posted_by_name.setText( tempValues.getPostedby2() );
            /******** Set Item Click Listner for LayoutInflater for each row *******/

            vi.setOnClickListener(new CustomAdapterforfindloadresult.OnItemClickListener( i ));
        }
        return vi;
    }

    /********* Called when Item click in ListView ************/
    private class OnItemClickListener  implements View.OnClickListener {
        private int mPosition;
        ProgressDialog dialogue;

        OnItemClickListener(int position){
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {


      //     Toast.makeText(activity, mPosition+"" , Toast.LENGTH_SHORT).show();

      /*      Intent in = new Intent(activity,LoadInfoActivityinFindLoad.class);

            Bundle args = new Bundle();
            args.putString("load_list_key_for_json", String.valueOf(mPosition));

            in.putExtras(args);
            activity.startActivity(in);
    */
            dialogue = new ProgressDialog(activity);
            dialogue.setTitle("Loading ...");
            dialogue.show();

            YoulorrySession session_1 = new YoulorrySession(activity);
            String str = session_1.getMatchingloadsforfindload();
            String username,password,userid,role;
            username = session_1.getusername();
            password = session_1.getpass_word();
            userid = session_1.getuser_id();
            role = session_1.getrole();

            try {

//                Thread.sleep(3000);
                String load_id;
                JSONArray jsonArray = new JSONArray(str);
                //  String [] truck_list_for_list_view_id = new String[jsonArray.length()];
                //  Resources res =getResources();
                for(int i=0; i < jsonArray.length(); i++){
                    if(i == mPosition ){

                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        load_id = jsonObject.optString("ID").toString();

                        Log.d("username",username);
                        Log.d("password",password);
                        Log.d("userid",userid);
                        Log.d("role",role);
                        Log.d("load_id_key", load_id);

                        URL urlObj = null;

                        urlObj = new URL("http://"+activity.getString(R.string.server_api_url)+".com/api-get-my-available-trucks-for-given-load/");
                        HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                        urlConnection.setDoOutput(true);
                        urlConnection.setDoInput(true);
                        urlConnection.setUseCaches(false);
                        urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                        urlConnection.setRequestMethod("POST");
                        urlConnection.connect();

                        JSONObject cred = new JSONObject();

                        cred.put("username",username);
                        cred.put("password",password);
                        cred.put("userid",userid);
                        cred.put("role",role);
                        cred.put("load_id_key", load_id);

                        OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                        wr.write(cred.toString());
                        wr.flush();
                        wr.close();

                        //display what returns the POST request

                        StringBuilder sb = new StringBuilder();
                        int HttpResult = urlConnection.getResponseCode();
                        if (HttpResult == HttpURLConnection.HTTP_OK) {
                            //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                            BufferedReader br = new BufferedReader(
                                    new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                            String line = null;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            br.close();
                          //  Toast.makeText(activity, sb.toString(), Toast.LENGTH_SHORT).show();
                            Log.d("vehicle_number",sb.toString());

                            Intent in = new Intent(activity,LoadInfoActivityinFindLoad.class);
                            in.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            Bundle args = new Bundle();
                            args.putString("load_list_key_for_json", String.valueOf(mPosition));
                            args.putString("truck_list_array_from_json", sb.toString());

                            in.putExtras(args);
                            activity.startActivity(in);

                        }

                    }
                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
  //      dialogue.dismiss();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    dialogue.dismiss();
                }
            }, 2000);
        }
    }
}
