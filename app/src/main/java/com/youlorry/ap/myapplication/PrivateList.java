package com.youlorry.ap.myapplication;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by Arpit Prajapati on 3/21/2017.
 */

public class PrivateList extends Fragment implements View.OnClickListener {

    ImageView show_btn,hide_btn;
    Context con;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.private_list, container, false);

        con = getActivity();

        YoulorrySession session = new YoulorrySession(con);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(con , LoginActivity.class);
            startActivity(in);

        }

        show_btn = (ImageView) v.findViewById(R.id.expand_more);
        hide_btn = (ImageView) v.findViewById(R.id.hide_btn);

        final LinearLayout your_private_list_layout = (LinearLayout) v.findViewById(R.id.your_private_list_layout);
        show_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(your_private_list_layout.getVisibility() == View.GONE){
                    your_private_list_layout.setVisibility(View.VISIBLE);

                    hide_btn.setVisibility(View.VISIBLE);
                    show_btn.setVisibility(View.GONE);
                }
            }
        });
        hide_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(your_private_list_layout.getVisibility() == View.VISIBLE){
                    your_private_list_layout.setVisibility(View.GONE);
                    show_btn.setVisibility(View.VISIBLE);
                    hide_btn.setVisibility(View.GONE);
                }
            }
        });


        return v;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onResume() {

        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button
                    getActivity().getFragmentManager().popBackStack();
                    return true;

                }

                return false;
            }
        });
    }
}
