package com.youlorry.ap.myapplication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Arpit Prajapati on 12/7/16.
 */

public class Truckinfowithloadlist extends YouLorryActivity implements View.OnClickListener{

    String truck_list_value_for_json,load_id_array_from_json;
    String wt_data_for_comparision;
    String data="";
    TextView truck_info_in_modal_view,s_city_in_modal,d_city_in_modal,exp_val,truck_type_in_modal,date_in_modal;
    TextView t_s_city,t_d_city,t_quoted_val,t_truck_type,t_date,t_wt_capacity,t_posted_by,y_verified_heading,y_verified,y_tracking_heading,y_tracking;
    Context context;
    Button book_now_btn,add_load_btn;
    ImageView top_cancel_btn;
    public String[]load_list_array = {"No saved load!"};
    EditText quoted_amount_in_modal_view,lp_a,unlp_a,weight_capacity_in_modal_view;
    Spinner spinner_for_load_list;
    String[] load_id_array;
    ImageView y_verified_img,y_tracking_img,y_verified_img_ok,y_tracking_img_ok;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.truck_info_with_load_list);
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        YoulorrySession session = new YoulorrySession(this);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(this , LoginActivity.class);
            startActivity(in);

        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width),(int)(height));

        Bundle bundle = getIntent().getExtras();
        truck_list_value_for_json = bundle.getString("truck_list_key_for_json");
        load_id_array_from_json = bundle.getString("load_list_array_from_json");
        context = this;

        t_s_city = (TextView) findViewById(R.id.t_s_city_in_modal_view);
        t_d_city = (TextView) findViewById(R.id.t_d_city_in_modal_view);
        t_quoted_val = (TextView) findViewById(R.id.t_expected_frieght_value_in_modal_view);
        t_truck_type = (TextView) findViewById(R.id.t_truck_type_in_modal_view);
        t_date = (TextView) findViewById(R.id.t_date_in_modal_view);
        t_wt_capacity = (TextView) findViewById(R.id.t_weight_capacity_in_modal_view);
        t_posted_by = (TextView) findViewById(R.id.posted_by_id);
        top_cancel_btn = (ImageView) findViewById(R.id.top_cancel_btn);
        weight_capacity_in_modal_view = (EditText) findViewById(R.id.weight_capacity_in_modal_view);

        y_verified_img = (ImageView) findViewById(R.id.youlorry_verified_img);
        y_tracking_img = (ImageView) findViewById(R.id.youlorry_tracking_img);
        y_verified_img_ok = (ImageView) findViewById(R.id.youlorry_verified_img_ok);
        y_tracking_img_ok = (ImageView) findViewById(R.id.youlorry_tracking_img_ok);

        y_verified_heading = (TextView) findViewById(R.id.youlorry_verified_inmodalview);
        y_verified = (TextView) findViewById(R.id.youlorry_verified_id);
        y_tracking_heading = (TextView) findViewById(R.id.youlorry_tracking_inmodalview);
        y_tracking = (TextView) findViewById(R.id.youlorry_tracking_id);

        book_now_btn = (Button) findViewById(R.id.button_id_from_truck_info_modal);
        add_load_btn = (Button) findViewById(R.id.add_load_btn);
        quoted_amount_in_modal_view = (EditText) findViewById(R.id.quoted_amount_in_modal_view);
        lp_a = (EditText) findViewById(R.id.loading_address_in_modal_view);
        unlp_a = (EditText) findViewById(R.id.unloading_address_in_modal_view);

        load_id_array = load_id_array_from_json.substring(1, load_id_array_from_json.toString().length() - 1).split(",");
        String[] load_id_array1 = new String[load_id_array.length];

        for(int j = 0; j <load_id_array.length; j++){

            load_id_array1[j] = load_id_array[j].substring(1, load_id_array[j].toString().length() - 1);

        }

        if(load_id_array_from_json.toString().equals("error")){

            spinner_for_load_list = (Spinner) findViewById(R.id.load_spinner);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                    android.R.layout.simple_spinner_item,load_list_array);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_for_load_list.setAdapter(adapter);

            add_load_btn.setVisibility(View.VISIBLE);
            book_now_btn.setVisibility(View.GONE);

        }else {


            spinner_for_load_list = (Spinner) findViewById(R.id.load_spinner);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                    android.R.layout.simple_spinner_item,load_id_array1);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_for_load_list.setAdapter(adapter);

            add_load_btn.setVisibility(View.GONE);
            book_now_btn.setVisibility(View.VISIBLE);

        }

        if(spinner_for_load_list.getSelectedItem().toString() == "no saved load!"){

            add_load_btn.setVisibility(View.VISIBLE);
            book_now_btn.setVisibility(View.GONE);

        }

        TextWatcher fieldValidatorTextWatcher = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (filterLongEnough()) {
                    compare_two_weights();
                }
            }

            private void compare_two_weights() {
                String str =weight_capacity_in_modal_view.getText().toString();
                int a = Integer.parseInt(str);
                int b = Integer.parseInt(wt_data_for_comparision);
                if(a > b){
                    weight_capacity_in_modal_view.setError("fill weight < "+wt_data_for_comparision);
                }else if (a < 1) {
                    weight_capacity_in_modal_view.setError("fill weight < "+wt_data_for_comparision);
                }
            }

            private boolean filterLongEnough() {
                return weight_capacity_in_modal_view.getText().toString().trim().length() > 0;
            }
        };

        weight_capacity_in_modal_view.addTextChangedListener(fieldValidatorTextWatcher);

        book_now_btn.setOnClickListener(this);
        add_load_btn.setOnClickListener(this);
        top_cancel_btn.setOnClickListener(this);

        new TheTask().execute();

    }

    class TheTask extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;
        String str;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(context);
            dialogue.setTitle("Loading ...");
            dialogue.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session_1 = new YoulorrySession(context);
            str = session_1.getMatchingtrucksforfindtruck();
            return str.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                JSONArray jsonArray = new JSONArray(s);
              //  String [] truck_list_for_list_view_id = new String[jsonArray.length()];

                int truck_list_value_for_json_int = Integer.parseInt(truck_list_value_for_json);
              //  Resources res =getResources();
                for(int i=0; i < jsonArray.length(); i++){

                    if(i == truck_list_value_for_json_int ){

                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        id = Integer.parseInt(jsonObject.optString("ID").toString());

                        String posted_by = jsonObject.getString("posted_by");
                        String posted_author = jsonObject.getString("post_author");

                        String str_posted_by_name = "Transporter" ;
                        if(posted_by.equals("transporter_transporter")){
                            str_posted_by_name = "Transporter";
                        } else if(posted_by.equals("fleet_owner")) {
                            str_posted_by_name = "Fleet Owner";
                        } else if(posted_by.equals("broker")) {
                            str_posted_by_name = "broker";
                        } else {
                            str_posted_by_name = "Transporter";
                        }

                        if(posted_by.equals("broker") && posted_author.equals("35")){
                            t_posted_by.setText("Youlorry");
                        } else {
                            t_posted_by.setText(str_posted_by_name);
                        }

                        if(posted_by.equals("broker")){
                            y_verified_img.setVisibility(View.VISIBLE);
                            y_tracking_img.setVisibility(View.VISIBLE);
/*                            y_verified.setText("NO");
                            y_tracking.setText("NO");
                            y_verified.setTextColor(Color.rgb(120,0,0));
                            y_tracking.setTextColor(Color.rgb(120,0,0));
*/
                        } else {
                            y_verified_img_ok.setVisibility(View.VISIBLE);
                            y_tracking_img_ok.setVisibility(View.VISIBLE);
/*                            y_verified.setText("YES");
                            y_tracking.setText("YES");
                            y_verified.setTextColor(Color.rgb(0,110,0));
                            y_tracking.setTextColor(Color.rgb(0,110,0));
*/
                        }

                        if(posted_by.equals("broker") && posted_author.equals("35")){
                            y_verified_img_ok.setVisibility(View.VISIBLE);
                            y_tracking_img_ok.setVisibility(View.VISIBLE);
                            y_verified_img.setVisibility(View.GONE);
                            y_tracking_img.setVisibility(View.GONE);
/*                            y_verified.setText("NO");
                            y_tracking.setText("NO");
                            y_verified.setTextColor(Color.rgb(120,0,0));
                            y_tracking.setTextColor(Color.rgb(120,0,0));
*/
                        }
                        String meta_keys_jsondata = jsonObject.optString("meta_keys").toString();

                        JSONObject jobj = new JSONObject(meta_keys_jsondata);
                        String source_city_jsondata=jobj.getString("source_city");
                        String s_city_from_json = source_city_jsondata.substring(2, source_city_jsondata.toString().length() - 2);
                        t_s_city.setText(s_city_from_json);

                        String destination_city_jsondata=jobj.getString("destination_city");
                        String d_city_from_json = destination_city_jsondata.substring(2, destination_city_jsondata.toString().length() - 2);
                        t_d_city.setText(d_city_from_json);

                        String expected_value_jsondata=jobj.getString("expected_frieght_value");
                        String exp_val_from_json = expected_value_jsondata.substring(2, expected_value_jsondata.toString().length() - 2);
                        t_quoted_val.setText("₹ "+exp_val_from_json);

                        String schedule_date_jsondata=jobj.getString("date");
                        String scheduled_date_from_json = schedule_date_jsondata.substring(2, schedule_date_jsondata.toString().length() - 2);
                        t_date.setText(scheduled_date_from_json);

                        String truck_type_jsondata=jobj.getString("truck_type");
                        String truck_type_from_jsondata = truck_type_jsondata.substring(2, truck_type_jsondata.toString().length() - 2);
                        String replaced_truck_type = truck_type_from_jsondata.replace("\\","");
                        t_truck_type.setText(replaced_truck_type);

                        String weight_capacity_jsondata=jobj.getString("weight_capacity");
                        String weight_capacity_from_jsondata = weight_capacity_jsondata.substring(2, weight_capacity_jsondata.toString().length() - 2);
                        wt_data_for_comparision = weight_capacity_jsondata.substring(2, weight_capacity_jsondata.toString().length() - 2);
                        t_wt_capacity.setText(weight_capacity_from_jsondata+" MT");

//                        data = "Source : "+s_city_from_json+"\n"+"Destination : "+d_city_from_json+"\n" +"Expected value : "+exp_val_from_json+"\n"+"Truck Type : "+truck_type_from_jsondata+"\n" +"Date : "+scheduled_date_from_json+"\n";

                        quoted_amount_in_modal_view.setText(exp_val_from_json);
//                        date_in_modal.setText(scheduled_date_from_json);
                      //  truck_info_in_modal_view.setText(data);

                    }

                }

            } catch (JSONException e) {e.printStackTrace();}

            dialogue.dismiss();
        }
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.top_cancel_btn){
            finish();
        }

        if(view.getId() == R.id.button_id_from_truck_info_modal) {

            Log.d("wt_data_for_comparision", wt_data_for_comparision);
            Log.d("input_weight_value", weight_capacity_in_modal_view.getText().toString());
            if(new YoulorrySession(context).getPANCardNumber().length() == 0 ){
                new AlertDialog.Builder(context)
                        .setTitle("Alert Message")
                        .setMessage("Your PAN Number not saved. Please update your PAN Number with us so that you will allow to send Quotations.")
                        .setIcon(R.drawable.ic_warning_black_24dp)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                startActivity(new Intent(context, MainActivity.class));
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
            } else if(weight_capacity_in_modal_view.getText().toString().length() == 0) {
                weight_capacity_in_modal_view.setError( "Weight is required!" );
            } else if(Integer.parseInt(weight_capacity_in_modal_view.getText().toString()) > Integer.parseInt(wt_data_for_comparision)){
                weight_capacity_in_modal_view.setError("fill weight < "+wt_data_for_comparision);
            }else if(quoted_amount_in_modal_view.getText().toString().length() == 0){
                quoted_amount_in_modal_view.setError( "Quoted Amount is required!" );
            } else if(lp_a.getText().toString().length() == 0) {
                lp_a.setError( "required field!" );
            } else if(unlp_a.getText().toString().length() == 0) {
                unlp_a.setError( "required field!" );
            } else {

                YoulorrySession session12 = new YoulorrySession(this);
                String username = session12.getusername();
                String password = session12.getpass_word();
                String userid = session12.getuser_id();
                String role = session12.getrole();

                String selected_load_id = spinner_for_load_list.getSelectedItem().toString();

                URL urlObj = null;
                try {
                    urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-add-new-load/");
                    HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);
                    urlConnection.setUseCaches(false);
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    //  urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setRequestMethod("POST");
                    urlConnection.connect();

                    JSONObject cred = new JSONObject();

                    cred.put("username",username);
                    cred.put("password",password);
                    cred.put("userid",userid);
                    cred.put("role",role);
                    cred.put("truck_id",id);
                    cred.put("load_id",selected_load_id);
                    cred.put("left_count",weight_capacity_in_modal_view.getText().toString());
                    cred.put("quoted_amount",quoted_amount_in_modal_view.getText().toString());
                    cred.put("loading_address",lp_a.getText().toString());
                    cred.put("unloading_address",unlp_a.getText().toString());
//                cred.put("password", "pass");

                    //   DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
                    //OutputStream os = urlConnection.getOutputStream();
                    //OutputStreamWriter wr = new OutputStreamWriter(os, "UTF-8");
                    OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(cred.toString());
                    // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                    wr.flush();
                    wr.close();

                    //display what returns the POST request

                    StringBuilder sb = new StringBuilder();
                    int HttpResult = urlConnection.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                        BufferedReader br = new BufferedReader(
                                new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                        }
                        br.close();

                        if(sb.toString().equals("ok")){
                            new AlertDialog.Builder(this)
                                    .setTitle("Success Message")
                                    .setMessage("Quotation Sent Successfully!!!")
                                    .setIcon(R.drawable.ic_done_all_black_24dp)
                                    .show();

                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    startActivity(new Intent(context, MainActivity.class));
                                    finish();
                                }
                            },2000);
//                         startActivity(new Intent(this, MainActivity.class));
                        }else{

                            new AlertDialog.Builder(this)
                                    .setTitle("Failed Message")
                                    .setMessage("Your quotation was not added due to some error. Please try again!!!")
                                    .setIcon(R.drawable.ic_warning_black_24dp)
                                    .show();

                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    startActivity(new Intent(context, MainActivity.class));
                                    finish();
                                }
                            },6000);
//                         startActivity(new Intent(this, MainActivity.class));

                        }


                    }

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }

        if(view.getId() == R.id.add_load_btn){

            Log.d("wt_data_for_comparision", wt_data_for_comparision);
            Log.d("input_weight_value", weight_capacity_in_modal_view.getText().toString());
            if(weight_capacity_in_modal_view.getText().toString().length() == 0) {
                weight_capacity_in_modal_view.setError( "Weight is required!" );
            } else if(Integer.parseInt(weight_capacity_in_modal_view.getText().toString()) > Integer.parseInt(wt_data_for_comparision)){
                weight_capacity_in_modal_view.setError("fill weight < "+wt_data_for_comparision);
            }else if(quoted_amount_in_modal_view.getText().toString().length() == 0){
                quoted_amount_in_modal_view.setError( "Quoted Amount is required!" );
            } else if(lp_a.getText().toString().length() == 0) {
                lp_a.setError( "required field!" );
            } else if(unlp_a.getText().toString().length() == 0) {
                unlp_a.setError( "required field!" );
            } else {


                Intent in = new Intent(this,PostaloadinfindtruckActivity.class);

                Bundle args = new Bundle();
                args.putString("truck_id", id+"");
                args.putString("quoted_amount",quoted_amount_in_modal_view.getText().toString());
                args.putString("weight_capacity",weight_capacity_in_modal_view.getText().toString());
                args.putString("loading_address",lp_a.getText().toString());
                args.putString("unloading_address",unlp_a.getText().toString());
                in.putExtras(args);
                finish();
                startActivity(in);

                //finish();
                //startActivity(new Intent(this, PostaloadinfindtruckActivity.class));
            }
        }

    }
}
