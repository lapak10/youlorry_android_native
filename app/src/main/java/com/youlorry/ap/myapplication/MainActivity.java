package com.youlorry.ap.myapplication;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Arpit Prajapati on 11/26/16.
 */


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    NavigationView navigationView;
    Context con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!isNetworkConnected()){
           // Toast.makeText(this, "working", Toast.LENGTH_SHORT).show();

            new AlertDialog.Builder(this)
                    .setTitle("Check Connection")
                    .setMessage("Internet connection not enable. Please enable it from settings!")
                   /* .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                           // startActivity(new Intent(con, MainActivity.class));

                        }
                    })*/
                    .setPositiveButton("try again", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            //Intent intent = new Intent(Intent.ACTION_MANAGE_NETWORK_USAGE);
                            //intent.setClassName("com.android.phone", "com.android.phone.NetworkSetting");
                            //startActivity(intent);
                            Intent intent = getIntent();
                            finish();
                            startActivity(intent);

                        }
                    })
                    /* .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })*/
                    .setIcon(R.drawable.ic_warning_black_24dp)
                    .show();


        }

        YoulorrySession session = new YoulorrySession(this);
        checkSession();
        String str = session.getrole();
        String str1 = "transporter";
        String str2 = "customer";

        //Toast.makeText(this, str , Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, session.getrole() , Toast.LENGTH_SHORT).show();

        if(str.equals(str1)){

          //  Toast.makeText(this, str1 , Toast.LENGTH_SHORT).show();

            hideforTruckOwner();

        if(savedInstanceState == null) {
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.content_main, new Dashboardfortruckowner());
            transaction.commit();
        }


        } else if(str.equals(str2)){

            //Toast.makeText(this, str2 , Toast.LENGTH_SHORT).show();
            hideforCustomer();

        if(savedInstanceState == null) {
            FragmentManager manager = getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.content_main, new Dashboardfortruckowner());
            transaction.commit();
        }


        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

/*        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
*/

        //Bundle bundle = getIntent().getExtras();
        //user_id = bundle.getString("user_id");
        //user_role = bundle.getString("role");
       //user_name = bundle.getString("user_name");

       // TextView t1 = (TextView) findViewById(R.id.t1);
       // String data = user_id +"\n"+user_name+"\n"+user_role;
       // t1.setText(data);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
//        Menu xyz = navigationView.getMenu();
//        if(session.getis_user_factory_vendor().equalsIgnoreCase("true")) {
//            xyz.findItem(R.id.nav_factory_load).setVisible(true);
//        }
//        else
//            xyz.findItem(R.id.nav_factory_load).setVisible(false);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void checkSession() {

        YoulorrySession session = new YoulorrySession(this);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(this, LoginActivity.class);
            finish();
            startActivity(in);
        }
    }

    private void hideforTruckOwner() {

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_home).setVisible(false);
        nav_Menu.findItem(R.id.nav_truck).setVisible(false);
        nav_Menu.findItem(R.id.nav_load).setVisible(false);
        nav_Menu.findItem(R.id.nav_quotation).setVisible(false);
//        YoulorrySession session = new YoulorrySession(this);
//        if(session.getis_user_factory_vendor()=="true") {
//            nav_Menu.findItem(R.id.nav_factory_load).setVisible(true);
//        }
//        else
//            nav_Menu.findItem(R.id.nav_factory_load).setVisible(false);
    }

    private void hideforCustomer() {

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.d_nav_home).setVisible(false);
        nav_Menu.findItem(R.id.d_nav_truck).setVisible(false);
        nav_Menu.findItem(R.id.d_nav_load).setVisible(false);
        nav_Menu.findItem(R.id.d_nav_quotation).setVisible(false);
        nav_Menu.findItem(R.id.nav_factory_load).setVisible(false);


    }
    /** Called when the activity has become visible. */
    @Override
    protected void onResume() {
        super.onResume();
//        Menu nav_Menu = navigationView.getMenu();
//        YoulorrySession session = new YoulorrySession(this);
//        if(session.getis_user_factory_vendor().equalsIgnoreCase("true")) {
//            nav_Menu.findItem(R.id.nav_factory_load).setVisible(true);
//        }
//        else
//            nav_Menu.findItem(R.id.nav_factory_load).setVisible(false);
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        Menu nav_Menu = navigationView.getMenu();
        YoulorrySession session = new YoulorrySession(this);
        if(session.getis_user_factory_vendor().equalsIgnoreCase("true")) {
            nav_Menu.findItem(R.id.nav_factory_load).setVisible(true);
        }
        else
            nav_Menu.findItem(R.id.nav_factory_load).setVisible(false);

/*
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
*/
    }

    boolean doubleBackToExitPressedOnce = false;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "press again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

/*    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        if(new YoulorrySession(this).getrole().toString().equals("transporter")) {
            this.invalidateOptionsMenu();
            MenuItem item = menu.findItem(R.id.expired_load);
            MenuItem item2 = menu.findItem(R.id.my_loads);
            item.setVisible(false);
            item2.setVisible(false);
        }
        if(new YoulorrySession(this).getrole().toString().equals("customer")) {
            this.invalidateOptionsMenu();
            MenuItem item = menu.findItem(R.id.expired_truck);
            MenuItem item2 = menu.findItem(R.id.my_trucks);
            item.setVisible(false);
            item2.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        FragmentTransaction transaction=getFragmentManager().beginTransaction();
        //noinspection SimplifiableIfStatement
//        if (id == R.id.settings) {

//            startActivity(new Intent(this , NewUserAfterLoginForm.class));

//        } else
          if (id == R.id.profile) {
//                finish();
              Intent intent_ap = new Intent(this, ProfileActivity.class);
              intent_ap.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
              startActivity(intent_ap);
              overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//              startActivity(new Intent(this, ProfileActivity.class));
        } /* else if (id == R.id.private_list) {

              PrivateList privateList = new PrivateList();

              overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
              transaction.replace(R.id.content_main, privateList);
              transaction.addToBackStack(null);
              transaction.commit();

          } */ else if (id == R.id.my_loads) {

              MyStuff my_stuff= new MyStuff();

              overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
              transaction.replace(R.id.content_main,my_stuff);
              transaction.addToBackStack(null);
              transaction.commit();

          } else if (id == R.id.my_trucks) {

            MyStuff my_stuff= new MyStuff();

            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            transaction.replace(R.id.content_main,my_stuff);
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (id == R.id.expired_load) {

            //   Toast.makeText(this, "Expired Loads Clicked!!!", Toast.LENGTH_SHORT).show();
            ExpiredStuff exp_stuff= new ExpiredStuff();

            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            transaction.replace(R.id.content_main,exp_stuff);
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (id == R.id.expired_truck) {

//            Toast.makeText(this, "Expired Trucks Clicked!!!", Toast.LENGTH_SHORT).show();
            ExpiredStuff exp_stuff= new ExpiredStuff();

            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            transaction.replace(R.id.content_main,exp_stuff);
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (id == R.id.sign_out) {

            logout();
            Intent in1 = new Intent(this , LoginActivity.class);
            in1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(in1);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        }

        return super.onOptionsItemSelected(item);
    }

    private void logout() {

        YoulorrySession session = new YoulorrySession(this);

        session.setuser_id("");
        session.setusername("");
        session.setpass_word("");
        session.setrole("");
        session.setFirstName("");
        session.setCancelledQuotations("");
        session.setConfirmedQuotations("");
        session.setCurrentloadpostidforbooking("");
        session.setCurrenttruckpostidforbooking("");
        session.setCustomerQuotations("");
        session.setExpiredStuff("");
        session.setHistoryData("");
        session.setMatchingloadsforfindload("");
        session.setMatchingloadsforpostatruck("");
        session.setMatchingtrucksforfindtruck("");
        session.setMatchingtrucksforpostaload("");
        session.setMyStuff("");
        session.setPendingQuotations("");
        session.setTransporterQuotations("");
        session.setWeeklyRecord("");
        session.setPANCardNumber("");

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        PostaloadActivity post_a_load;
        FindtruckActivity find_truck;
        Quotationforcustomer q_for_customer;
        FragmentTransaction transaction=getFragmentManager().beginTransaction();

        if (id == R.id.nav_home) {

            Intent intent1 = new Intent(this, MainActivity.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent1);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        } else if (id == R.id.d_nav_home) {

            Intent intent2 = new Intent(this, MainActivity.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent2);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        }else if (id == R.id.nav_load) {

            post_a_load=new PostaloadActivity();

            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            transaction.replace(R.id.content_main,post_a_load);
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (id == R.id.d_nav_load) {

            FIndLoadActivity post_a_load1= new FIndLoadActivity();

            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            transaction.replace(R.id.content_main,post_a_load1);
            transaction.addToBackStack(null);
            transaction.commit();

        }else if (id == R.id.d_nav_truck) {

            PosttruckActivity post_a_truck=new PosttruckActivity();

            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            transaction.replace(R.id.content_main,post_a_truck);
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (id == R.id.nav_truck) {

            find_truck=new FindtruckActivity();

            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            transaction.replace(R.id.content_main,find_truck);
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (id == R.id.nav_quotation) {

            Intent intent3 = new Intent(this, Quotationforcustomer.class);
            intent3.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent3);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        } else if (id == R.id.d_nav_quotation) {

            Intent intent4 = new Intent(this, Quotationfortransporter.class);
            intent4.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent4);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        }  else if (id == R.id.nav_history) {

            HistoryActivity history_activity=new HistoryActivity();

            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            transaction.replace(R.id.content_main,history_activity);
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (id == R.id.nav_gps) {

            Toast.makeText(this, "Coming Soon !!!", Toast.LENGTH_SHORT).show();

        }
         else if (id == R.id.nav_factory_load) {



                Intent intent5 = new Intent(this, FactoryLoad.class);
                intent5.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent5);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}

