package com.youlorry.ap.myapplication.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.youlorry.ap.myapplication.Model.Date_Difference.convertmillisecondtohour;

/**
 * Created by Tycho on 5/4/2017.
 */

public class Bidding_Model {

    /**
     * ID : 19975
     * post_author : 31
     * post_date : 2017-05-04 11:07:12
     * post_date_gmt : 2017-05-04 11:07:12
     * post_content :
     * post_title : FACTORY Freight
     * post_excerpt :
     * post_status : publish
     * comment_status : closed
     * ping_status : closed
     * post_password :
     * post_name : factory-freight-5
     * to_ping :
     * pinged :
     * post_modified : 2017-05-04 11:07:12
     * post_modified_gmt : 2017-05-04 11:07:12
     * post_content_filtered :
     * post_parent : 0
     * guid : http://nishabhati.com/load/factory-freight-5/
     * menu_order : 0
     * post_type : load
     * post_mime_type :
     * comment_count : 0
     * filter : raw
     * lowest_bid_value : No Bid Yet
     * my_bid_value : Not Bidded yet
     * hour_limit : 24
     * meta_keys : {"load_source_city":["Shohratgarh"],"load_destination_city":["Lucknow"],"load_material":["Tea"],"load_share_with":["a:1:{i:0;i:44;}"],"load_weight_capacity":["10"],"load_pickup_point":["Yes"],"load_drop_point":["No"],"load_rate_per_ton":["1"],"load_time":["13:30"],"load_date":["14-05-2017"],"load_allow_bidding":["on"],"load_factory_user_id":["31"],"mysql_date":["2017-05-14"],"left_count":["10"],"from_factory":["true"]}
     * server_current_time : 2017-05-04 12:02:34
     * max_place_weight : 0
     */

    private int ID;
    private String post_author;
    private String post_date;
    private String post_date_gmt;
    private String post_content;
    private String post_title;
    private String post_excerpt;
    private String post_status;
    private String comment_status;
    private String ping_status;
    private String post_password;
    private String post_name;
    private String to_ping;
    private String pinged;
    private String post_modified;
    private String post_modified_gmt;
    private String post_content_filtered;
    private int post_parent;
    private String guid;
    private int menu_order;
    private String post_type;
    private String post_mime_type;
    private String comment_count;
    private String filter;
    private String lowest_bid_value;
    private String my_bid_value;
    private int hour_limit;
    private MetaKeysBean meta_keys;
    private String server_current_time;
    private String expiry_time;
    private int max_place_weight;

    private long longServerTime;
    private long longExpiryTime;


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getPost_author() {
        return post_author;
    }

    public void setPost_author(String post_author) {
        this.post_author = post_author;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getPost_date_gmt() {
        return post_date_gmt;
    }

    public void setPost_date_gmt(String post_date_gmt) {
        this.post_date_gmt = post_date_gmt;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_excerpt() {
        return post_excerpt;
    }

    public void setPost_excerpt(String post_excerpt) {
        this.post_excerpt = post_excerpt;
    }

    public String getPost_status() {
        return post_status;
    }

    public void setPost_status(String post_status) {
        this.post_status = post_status;
    }

    public String getComment_status() {
        return comment_status;
    }

    public void setComment_status(String comment_status) {
        this.comment_status = comment_status;
    }

    public String getPing_status() {
        return ping_status;
    }

    public void setPing_status(String ping_status) {
        this.ping_status = ping_status;
    }

    public String getPost_password() {
        return post_password;
    }

    public void setPost_password(String post_password) {
        this.post_password = post_password;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    public String getTo_ping() {
        return to_ping;
    }

    public void setTo_ping(String to_ping) {
        this.to_ping = to_ping;
    }

    public String getPinged() {
        return pinged;
    }

    public void setPinged(String pinged) {
        this.pinged = pinged;
    }

    public String getPost_modified() {
        return post_modified;
    }

    public void setPost_modified(String post_modified) {
        this.post_modified = post_modified;
    }

    public String getPost_modified_gmt() {
        return post_modified_gmt;
    }

    public void setPost_modified_gmt(String post_modified_gmt) {
        this.post_modified_gmt = post_modified_gmt;
    }

    public String getPost_content_filtered() {
        return post_content_filtered;
    }

    public void setPost_content_filtered(String post_content_filtered) {
        this.post_content_filtered = post_content_filtered;
    }

    public int getPost_parent() {
        return post_parent;
    }

    public void setPost_parent(int post_parent) {
        this.post_parent = post_parent;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public int getMenu_order() {
        return menu_order;
    }

    public void setMenu_order(int menu_order) {
        this.menu_order = menu_order;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getPost_mime_type() {
        return post_mime_type;
    }

    public void setPost_mime_type(String post_mime_type) {
        this.post_mime_type = post_mime_type;
    }

    public String getComment_count() {
        return comment_count;
    }

    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getLowest_bid_value() {
        return lowest_bid_value;
    }

    public void setLowest_bid_value(String lowest_bid_value) {
        this.lowest_bid_value = lowest_bid_value;
    }

    public String getMy_bid_value() {
        return my_bid_value;
    }

    public void setMy_bid_value(String my_bid_value) {
        this.my_bid_value = my_bid_value;
    }

    public int getHour_limit() {
        return hour_limit;
    }

    public void setHour_limit(int hour_limit) {
        this.hour_limit = hour_limit;
    }

    public MetaKeysBean getMeta_keys() {
        return meta_keys;
    }

    public void setMeta_keys(MetaKeysBean meta_keys) {
        this.meta_keys = meta_keys;
    }

    public String getServer_current_time() {
        return server_current_time;
    }

    public void setServer_current_time(String server_current_time) {
        this.server_current_time = server_current_time;
        this.longServerTime = stringToLongTime(this.server_current_time);
    }
    public String getExpiry_time() {
        return expiry_time;
    }

    public void setExpiry_time(String expiry_time) {
        this.expiry_time = expiry_time;
        this.longExpiryTime = stringToLongTime(this.expiry_time);
    }

    public int getMax_place_weight() {
        return max_place_weight;
    }

    public void setMax_place_weight(int max_place_weight) {
        this.max_place_weight = max_place_weight;
    }

    public String getTimeRemaining(){
        String timerString = "";
        long remaining = this.longExpiryTime - this.longServerTime;
        if(remaining >= 0){
            timerString = longToString(remaining);
        }
        return  timerString;
    }

    public void decreaseRemainingTime(){
        this.longServerTime += 1;
    }

    private long stringToLongTime(String timeSring){

        long longTime=Date_Difference.converttimetomillisecond(timeSring)/1000;

        return longTime;
    }

    private String longToString(long longTime){
      //  String stringTime=String.valueOf(longTime);
   String stringTime=convertmillisecondtohour(longTime * 1000);
        return stringTime;
    }




    public static class MetaKeysBean {
        private List<String> load_source_city;
        private List<String> load_destination_city;
        private List<String> load_material;
        private List<String> load_share_with;
        private List<String> load_weight_capacity;
        private List<String> load_pickup_point;
        private List<String> load_drop_point;
        private List<String> load_rate_per_ton;
        private List<String> load_time;
        private List<String> load_date;
        private List<String> load_allow_bidding;
        private List<String> load_factory_user_id;
        private List<String> mysql_date;
        private List<String> left_count;
        private List<String> from_factory;

        public MetaKeysBean(JSONObject metaKeyObj) {

                this.load_source_city = fetchArray(metaKeyObj, "load_source_city");
                this.load_destination_city = fetchArray(metaKeyObj, "load_destination_city");
                this.load_material = fetchArray(metaKeyObj, "load_material");
                this.load_share_with = fetchArray(metaKeyObj, "load_share_with");
                this.load_weight_capacity = fetchArray(metaKeyObj, "load_weight_capacity");
                this.load_pickup_point = fetchArray(metaKeyObj, "load_pickup_point");
                this.load_drop_point = fetchArray(metaKeyObj, "load_drop_point");
                this.load_rate_per_ton = fetchArray(metaKeyObj, "load_rate_per_ton");
                this.load_time = fetchArray(metaKeyObj, "load_time");
                this.load_date = fetchArray(metaKeyObj, "load_date");
                this.load_allow_bidding = fetchArray(metaKeyObj, "load_allow_bidding");
                this.load_factory_user_id = fetchArray(metaKeyObj, "load_factory_user_id");

                this.mysql_date = fetchArray(metaKeyObj, "mysql_date");
                this.left_count = fetchArray(metaKeyObj, "left_count");
                this.from_factory = fetchArray(metaKeyObj, "from_factory");

        }

        public List<String> fetchArray(JSONObject object, String key){

            List<String> listdata = new ArrayList<String>();
            try {
                JSONArray jArray = object.getJSONArray(key);
                if (jArray != null) {
                    for (int i=0;i<jArray.length();i++){
                        listdata.add(jArray.getString(i));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return listdata;
        }

        public List<String> getLoad_source_city() {
            return load_source_city;
        }

        public void setLoad_source_city(List<String> load_source_city) {
            this.load_source_city = load_source_city;
        }

        public List<String> getLoad_destination_city() {
            return load_destination_city;
        }

        public void setLoad_destination_city(List<String> load_destination_city) {
            this.load_destination_city = load_destination_city;
        }

        public List<String> getLoad_material() {
            return load_material;
        }

        public void setLoad_material(List<String> load_material) {
            this.load_material = load_material;
        }

        public List<String> getLoad_share_with() {
            return load_share_with;
        }

        public void setLoad_share_with(List<String> load_share_with) {
            this.load_share_with = load_share_with;
        }

        public List<String> getLoad_weight_capacity() {
            return load_weight_capacity;
        }

        public void setLoad_weight_capacity(List<String> load_weight_capacity) {
            this.load_weight_capacity = load_weight_capacity;
        }

        public List<String> getLoad_pickup_point() {
            return load_pickup_point;
        }

        public void setLoad_pickup_point(List<String> load_pickup_point) {
            this.load_pickup_point = load_pickup_point;
        }

        public List<String> getLoad_drop_point() {
            return load_drop_point;
        }

        public void setLoad_drop_point(List<String> load_drop_point) {
            this.load_drop_point = load_drop_point;
        }

        public List<String> getLoad_rate_per_ton() {
            return load_rate_per_ton;
        }

        public void setLoad_rate_per_ton(List<String> load_rate_per_ton) {
            this.load_rate_per_ton = load_rate_per_ton;
        }

        public List<String> getLoad_time() {
            return load_time;
        }

        public void setLoad_time(List<String> load_time) {
            this.load_time = load_time;
        }

        public List<String> getLoad_date() {
            return load_date;
        }

        public void setLoad_date(List<String> load_date) {
            this.load_date = load_date;
        }

        public List<String> getLoad_allow_bidding() {
            return load_allow_bidding;
        }

        public void setLoad_allow_bidding(List<String> load_allow_bidding) {
            this.load_allow_bidding = load_allow_bidding;
        }

        public List<String> getLoad_factory_user_id() {
            return load_factory_user_id;
        }

        public void setLoad_factory_user_id(List<String> load_factory_user_id) {
            this.load_factory_user_id = load_factory_user_id;
        }

        public List<String> getMysql_date() {
            return mysql_date;
        }

        public void setMysql_date(List<String> mysql_date) {
            this.mysql_date = mysql_date;
        }

        public List<String> getLeft_count() {
            return left_count;
        }

        public void setLeft_count(List<String> left_count) {
            this.left_count = left_count;
        }

        public List<String> getFrom_factory() {
            return from_factory;
        }

        public void setFrom_factory(List<String> from_factory) {
            this.from_factory = from_factory;
        }
    }
}
