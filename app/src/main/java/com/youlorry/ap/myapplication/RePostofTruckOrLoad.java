package com.youlorry.ap.myapplication;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Arpit Prajapati on 1/4/17.
 */

public class RePostofTruckOrLoad extends YouLorryActivity implements View.OnClickListener,AdapterView.OnItemClickListener,View.OnFocusChangeListener {

    private TextView pickup_date,t_available_date,header_text,note_description,pickup_date_text,adv_in_per_heading,calculated_amt_heading,ch_for_pickup,ch_for_drop,t_available_date_heading;
    private EditText freight_amt,adv_in_percentage,calculated_amt,t_expected_freight_value;
    private CheckBox ch_box_for_pickup,ch_box_for_drop,ch_box_for_pickup1,ch_box_for_drop1;
    private AutoCompleteTextView t_s_city,t_d_city;
    private String expired_stuff_position,repost_id,meta_keys;
    ImageView top_cancel_btn;
    Context con;
    Button repost_btn;
    private TextView pickup_city_heading,drop_city_heading,material_type_heading,truck_type_heading,pickup_time_heading,wt_per_truck_heading,freight_in_rs_heading;
    private TextView t_s_city_heading,t_d_city_heading,vehicle_number_heading,t_truck_type_heading,t_wt_per_truck_heading,t_expected_freight_value_in_inr_heading;
    private AutoCompleteTextView pickup_city,drop_city;
    private EditText wt_per_truck,t_vehicle_number,t_wt_per_truck;
    private Spinner material_spinner,truck_type_spinner,pickup_time_spinner,t_truck_type_spinner;
    ImageView material_spinner_img,truck_type_spinner_img,pickup_time_spinner_img,t_truck_type_spinner_img;

    String defaultTextForMaterialSpinner = "";
    String defaultTextForTruckTypeSpinner = "";
    String defaultTextForScheduleTimeSpinner = "";
    private static final String[]material_paths = {"Jute","Plastic","Building Materials","Chemicals","Coal and Ash","Cotton Seed","Cement","Electronics/Consumer Durables","Fertilizers","Fruits and Vegetables","Furniture and Wooden Products","House Hold Goods","Industrial Equipments","Liquids/Oil","Machinery/Tools/Spares","Medical Instruments","Medicine","Metals","Agro Products","Refrigerated Goods","Textiles Products","Tyres/Rubbers","Auto Parts","Automobile Vehicle"};
    private static final String[]trucktype_paths = {"Container Close Body (20-40 Feet)","Container Fixed (40-70 Feet)","Container Open Body (20-40 Feet)","Container Trucks","Double Dacker","Canter 4.5MT (17/6/6 ft) 4 Wheel","Canter 4MT (9/6/6 ft) 4 Wheel","Canter 7.5MT (19/7/7 ft) 6 Wheel","Canters Jumbo (20/7/7 ft)","Flat Bed Trailers (20-32 ft)","Flat Bed Trailers (40-54 ft)","HCV (Trucks/Trailers)","LCV (Light Comercial Vehicle)", "Low Bed Trailer","Open Body Truck","10 Axle Trailer","Truck 14 Wheel","Truck 15MT (22/7/7 ft) 10 Wheel","Truck 20 MT (28/8/8 ft) 12 Wheel","Truck 9 MT (17/7/7 ft) 6 Wheel","Vehicle/Car Carrier (20-80 ft) Closed"};
    private static final String[]scheduletime_paths = {"12.00 AM","12.30 AM","01.00 AM","01.30 AM","02.00 AM","02.30 AM","03.00 AM","03.30 AM","04.00 AM","04.30 AM","05.00 AM","05.30 AM","06.00 AM","06.30 AM","07.00 AM","07.30 AM","08.00 AM","08.30 AM","09.00 AM","09.30 AM","10.00 AM","10.30 AM","11.00 AM","11.30 AM","12.00 PM","12.30 PM","01.00 PM","01.30 PM","02.00 PM","02.30 PM","03.00 PM","03.30 PM","04.00 PM","04.30 PM","05.00 PM","05.30 PM ","06.00 PM","06.30 PM","07.00 PM","07.30 PM","08.00 PM","08.30 PM","09.00 PM","09.30 PM","10.00 PM","10.30 PM","11.00 PM","11.30 PM "};

    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";

    private static final String API_KEY = "AIzaSyDYclD4OCZvWnicWUqY66HlkhJ-h8WqmHA";
    Calendar myCalendar = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            try {
                updateLabel();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    };

    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    String curr_date = sdf.format(myCalendar.getTime());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(new YoulorrySession(this).getrole().toString().equals("transporter")){
            setContentView(R.layout.repost_truck);
        }else if(new YoulorrySession(this).getrole().toString().equals("customer")){
            setContentView(R.layout.repost_load);
        }

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        YoulorrySession session = new YoulorrySession(this);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(this , LoginActivity.class);
            startActivity(in);

        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        con = this;
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        if(session.getrole().equals("transporter")){
            getWindow().setLayout((int) (width*0.9),(int)(height*0.8));
        }else if(session.getrole().equals("customer")){
            getWindow().setLayout((int) (width*0.9),(int)(height*0.9));
        }
        Bundle bundle = getIntent().getExtras();
        expired_stuff_position = bundle.getString("expired_stuff_position");
        repost_id = bundle.getString("repost_id");
        meta_keys = bundle.getString("meta_keys");

        new FetchingMetaKeysData().execute();

        if(session.getrole().equals("transporter")){

            t_available_date = (TextView) findViewById(R.id.t_date_picker);
            header_text = (TextView) findViewById(R.id.header_text_for_expired_stuff);
            note_description = (TextView) findViewById(R.id.note_description_in_expired_stuff);

            header_text.setText("Re-Post Truck");
            note_description.setText("Your truck posting will expire in 48 hours if not booked");

            t_truck_type_spinner = (Spinner) findViewById(R.id.t_truck_type_spinner);
            t_available_date_heading = (TextView) findViewById(R.id.t_available_date_heading);
            t_expected_freight_value = (EditText) findViewById(R.id.t_expected_freight_value_in_inr);
            t_s_city_heading = (TextView) findViewById(R.id.t_s_city_heading);
            t_d_city_heading = (TextView) findViewById(R.id.t_d_city_heading);
            vehicle_number_heading = (TextView) findViewById(R.id.vehicle_number_heading);
            t_truck_type_heading = (TextView) findViewById(R.id.t_truck_type_heading);
            t_wt_per_truck_heading = (TextView) findViewById(R.id.t_wt_per_truck_heading);
            t_expected_freight_value_in_inr_heading = (TextView) findViewById(R.id.t_expected_freight_value_in_inr_heading);

            t_vehicle_number = (EditText) findViewById(R.id.t_vehicle_number);
            t_wt_per_truck = (EditText) findViewById(R.id.t_wt_per_truck);

            t_s_city = (AutoCompleteTextView) findViewById(R.id.t_source_city);
            t_d_city = (AutoCompleteTextView) findViewById(R.id.t_destination_city);

            t_s_city.setAdapter(new GooglePlacesAutocompleteAdapter(con, R.layout.listview));
            t_d_city.setAdapter(new GooglePlacesAutocompleteAdapter(con, R.layout.listview));

            t_s_city.setOnItemClickListener(this);
            t_d_city.setOnItemClickListener(this);


            t_available_date.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    new DatePickerDialog(con, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });

/*            pickup_date.setVisibility(View.GONE);

            pickup_city_heading.setVisibility(View.GONE);
            pickup_city.setVisibility(View.GONE);

            drop_city_heading.setVisibility(View.GONE);
            drop_city.setVisibility(View.GONE);

            material_type_heading.setVisibility(View.GONE);
            material_spinner.setVisibility(View.GONE);
            material_spinner_img.setVisibility(View.GONE);

            truck_type_heading.setVisibility(View.GONE);
            truck_type_spinner.setVisibility(View.GONE);
            truck_type_spinner_img.setVisibility(View.GONE);

            pickup_time_heading.setVisibility(View.GONE);
            pickup_time_spinner.setVisibility(View.GONE);
            pickup_time_spinner_img.setVisibility(View.GONE);

            pickup_date_text.setVisibility(View.GONE);

            wt_per_truck_heading.setVisibility(View.GONE);
            wt_per_truck.setVisibility(View.GONE);

            freight_in_rs_heading.setVisibility(View.GONE);
            freight_amt.setVisibility(View.GONE);

            adv_in_per_heading.setVisibility(View.GONE);
            adv_in_percentage.setVisibility(View.GONE);

            calculated_amt_heading.setVisibility(View.GONE);
            calculated_amt.setVisibility(View.GONE);

            ch_for_pickup.setVisibility(View.GONE);
            ch_for_drop.setVisibility(View.GONE);
            ch_box_for_pickup.setVisibility(View.GONE);
            ch_box_for_pickup1.setVisibility(View.GONE);
            ch_box_for_drop.setVisibility(View.GONE);
            ch_box_for_drop1.setVisibility(View.GONE);
*/
        }else if(session.getrole().equals("customer")){

            pickup_date = (TextView) findViewById(R.id.date_picker);
            header_text = (TextView) findViewById(R.id.header_text_for_expired_stuff);
            note_description = (TextView) findViewById(R.id.note_description_in_expired_stuff);

            header_text.setText("Re-Post Load");
            note_description.setText("Your load posting will expire in 48 hours if not booked");

            material_spinner = (Spinner) findViewById(R.id.material_spinner);
            truck_type_spinner = (Spinner) findViewById(R.id.truck_type_spinner);
            pickup_time_spinner = (Spinner) findViewById(R.id.pickup_time_spinner);

            pickup_date_text = (TextView) findViewById(R.id.pickup_date_text);
            adv_in_per_heading = (TextView) findViewById(R.id.adv_in_per_heading);
            calculated_amt_heading = (TextView) findViewById(R.id.calculated_amt_heading);
            ch_for_pickup = (TextView) findViewById(R.id.ch_for_pickup);
            ch_for_drop = (TextView) findViewById(R.id.ch_for_drop);

            freight_amt = (EditText) findViewById(R.id.expected_freight_val_in_inr);
            adv_in_percentage = (EditText) findViewById(R.id.advance_in_percent);
            calculated_amt = (EditText) findViewById(R.id.calculated_amt);

            pickup_city_heading = (TextView) findViewById(R.id.pickup_city_heading);
            drop_city_heading = (TextView) findViewById(R.id.drop_city_heading);
            material_type_heading = (TextView) findViewById(R.id.material_type_heading);
            truck_type_heading = (TextView) findViewById(R.id.truck_type_heading);
            pickup_time_heading = (TextView) findViewById(R.id.pickup_time_heading);
            wt_per_truck_heading = (TextView) findViewById(R.id.wt_per_truck_heading);
            freight_in_rs_heading = (TextView) findViewById(R.id.freight_in_rs_heading);

            pickup_city = (AutoCompleteTextView) findViewById(R.id.pickup_city);
            drop_city = (AutoCompleteTextView) findViewById(R.id.drop_city);

            pickup_city.setAdapter(new GooglePlacesAutocompleteAdapter(con, R.layout.listview));
            drop_city.setAdapter(new GooglePlacesAutocompleteAdapter(con, R.layout.listview));

            pickup_city.setOnItemClickListener(this);
            drop_city.setOnItemClickListener(this);

            wt_per_truck = (EditText) findViewById(R.id.wt_per_truck);
/*
            pickup_city.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus) {
                        pickup_city_heading.setVisibility(View.GONE);
                    } else {
                        pickup_city_heading.setVisibility(View.VISIBLE);}
                }
            });

            drop_city.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus) {
                        drop_city_heading.setVisibility(View.GONE);
                    } else {drop_city_heading.setVisibility(View.VISIBLE);}
                }
            });
*/
            material_spinner_img = (ImageView) findViewById(R.id.material_spinner_img);
            truck_type_spinner_img = (ImageView) findViewById(R.id.truck_type_spinner_img);
            pickup_time_spinner_img = (ImageView) findViewById(R.id.pickup_time_spinner_img);
            t_truck_type_spinner_img = (ImageView) findViewById(R.id.t_truck_type_spinner_img);



            ch_box_for_pickup = (CheckBox) findViewById(R.id.checkBox_for_pickup_point);
            ch_box_for_pickup1 = (CheckBox) findViewById(R.id.checkBox_for_pickup_point1);
            ch_box_for_drop = (CheckBox) findViewById(R.id.checkBox_for_drop_point);
            ch_box_for_drop1 = (CheckBox) findViewById(R.id.checkBox_for_drop_point1);


            ch_box_for_pickup.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    ch_box_for_pickup1.setChecked(false);
                    ch_box_for_pickup.setChecked(b);
                }
            });

            ch_box_for_pickup1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    ch_box_for_pickup.setChecked(false);
                    ch_box_for_pickup1.setChecked(b);
                }
            });

            ch_box_for_drop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    ch_box_for_drop1.setChecked(false);
                    ch_box_for_drop.setChecked(b);
                }
            });

            ch_box_for_drop1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    ch_box_for_drop.setChecked(false);
                    ch_box_for_drop1.setChecked(b);
                }
            });

            pickup_date.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    new DatePickerDialog(con, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });

            TextWatcher fieldValidatorTextWatcher = new TextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (filterLongEnough()) {
                        calculate_amt_in_percentage();
                    }
                }

                private void calculate_amt_in_percentage() {
                    String str =adv_in_percentage.getText().toString();
                    int a =Integer.parseInt(str);
                    if(a<70){

                        adv_in_percentage.setError("Min percentage 70");

                    }else if (a>100) {
                        adv_in_percentage.setError("Max percentage 100");
                    }else{
                        if(freight_amt.getText().toString().length() > 1){
                            float b = Integer.parseInt(freight_amt.getText().toString());
                            float result = (b * a) / 100;

                            calculated_amt.setText("₹ "+result);
                        }else {

                            calculated_amt.setText("₹ "+0);

                        }
                    }
                }

                private boolean filterLongEnough() {
                    return adv_in_percentage.getText().toString().trim().length() > 0;
                }
            };

            TextWatcher getExpectedValue = new TextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (filterLongEnough()) {
                        getExpectedValue1();
                    }
                }

                private void getExpectedValue1() {
                    String str1 =freight_amt.getText().toString();
                    int a =Integer.parseInt(str1);
                    if(a<1){

                        freight_amt.setError("only positive value accepted !");

                    } else if(a>99999999) {
                        freight_amt.setError("max length reached !");

                    } else{

                        float b = Integer.parseInt(adv_in_percentage.getText().toString());
                        float result = (b * a) / 100;

                        calculated_amt.setText("₹ "+result);

                    }
                }

                private boolean filterLongEnough() {
                    return freight_amt.getText().toString().trim().length() > 0;
                }
            };
            adv_in_percentage.addTextChangedListener(fieldValidatorTextWatcher);
            freight_amt.addTextChangedListener(getExpectedValue);

/*            vehicle_number_heading.setVisibility(View.GONE);
            t_vehicle_number.setVisibility(View.GONE);
            t_s_city_heading.setVisibility(View.GONE);
            t_s_city.setVisibility(View.GONE);
            t_d_city_heading.setVisibility(View.GONE);
            t_d_city.setVisibility(View.GONE);
            t_truck_type_heading.setVisibility(View.GONE);
            t_truck_type_spinner.setVisibility(View.GONE);
            t_truck_type_spinner_img.setVisibility(View.GONE);
            t_wt_per_truck_heading.setVisibility(View.GONE);
            t_wt_per_truck.setVisibility(View.GONE);
            t_available_date_heading.setVisibility(View.GONE);
            t_available_date.setVisibility(View.GONE);
            t_expected_freight_value_in_inr_heading.setVisibility(View.GONE);
            t_expected_freight_value.setVisibility(View.GONE);
*/
        }

        top_cancel_btn = (ImageView) findViewById(R.id.top_cancel_btn);
        repost_btn = (Button) findViewById(R.id.re_post_button);
        repost_btn.setOnClickListener(this);
        top_cancel_btn.setOnClickListener(this);

    }

    @Override
    public void onFocusChange(View view, boolean b) {

    }

    class FetchingMetaKeysData extends AsyncTask<Void, Void, String> {


        ProgressDialog dialogue;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            return meta_keys.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


/*
            listView.setVisibility(View.GONE);
            t1.setVisibility(View.VISIBLE);
            t1.setText("No Matching Data Found !!!");
*/
            try {

                Log.d("meta_keys_data_for_expired_things",s.toString());

                JSONObject json_data = new JSONObject(s.toString());

                if(new YoulorrySession(con).getrole().equals("transporter")){

                    String source_city_jsondata=json_data.getString("source_city");
                    String s_city_from_json = source_city_jsondata.substring(2, source_city_jsondata.toString().length() - 2);

                    String destination_city_jsondata=json_data.getString("destination_city");
                    String d_city_from_json = destination_city_jsondata.substring(2, destination_city_jsondata.toString().length() - 2);

                    String expected_value_jsondata=json_data.getString("expected_frieght_value");
                    String exp_val_from_json = expected_value_jsondata.substring(2, expected_value_jsondata.toString().length() - 2);

                    String schedule_date_jsondata=json_data.getString("date");
                    String scheduled_date_from_json = schedule_date_jsondata.substring(2, schedule_date_jsondata.toString().length() - 2);

                    String truck_type_jsondata=json_data.getString("truck_type");
                    String truc_type_from_jsondata = truck_type_jsondata.substring(2, truck_type_jsondata.toString().length() - 2);

                    String vehicle_number_jsondata=json_data.getString("vehicle_number");
                    String vehicle_number_from_jsondata = vehicle_number_jsondata.substring(2, vehicle_number_jsondata.toString().length() - 2);

                    String weight_capacity_jsondata=json_data.getString("weight_capacity");
                    String weight_capacity_from_jsondata = weight_capacity_jsondata.substring(2, weight_capacity_jsondata.toString().length() - 2);

                    Log.d("source_city",s_city_from_json);
                    Log.d("destination_city",d_city_from_json);
                    Log.d("expected_frieght_value",exp_val_from_json);
                    Log.d("date",scheduled_date_from_json);
                    Log.d("truck_type",truc_type_from_jsondata);

                    t_s_city.setText(s_city_from_json);
                    t_d_city.setText(d_city_from_json);
                    t_expected_freight_value.setText(exp_val_from_json);
                    t_available_date.setText(scheduled_date_from_json);
                    t_vehicle_number.setText(vehicle_number_from_jsondata);
                    t_wt_per_truck.setText(weight_capacity_from_jsondata);

                    String replaced_truck_type = truc_type_from_jsondata.replace("\\","");

                    defaultTextForTruckTypeSpinner = replaced_truck_type;
                    t_truck_type_spinner.setAdapter(new CustomSpinnerAdapter(con, R.layout.text_view_for_spinner, trucktype_paths, defaultTextForTruckTypeSpinner));



                }
                if(new YoulorrySession(con).getrole().equals("customer")){

                    String load_source_city = json_data.getString("load_source_city");
                    String load_source_city_from_json = load_source_city.substring(2, load_source_city.toString().length() - 2);

                    String load_destination_city = json_data.getString("load_destination_city");
                    String load_destination_city_from_json = load_destination_city.substring(2, load_destination_city.toString().length() - 2);

                    String load_expected_frieght_value = json_data.getString("load_expected_frieght_value");
                    String load_expected_frieght_value_from_json = load_expected_frieght_value.substring(2, load_expected_frieght_value.toString().length() - 2);

                    String load_material = json_data.getString("load_material");
                    String load_material_from_json = load_material.substring(2, load_material.toString().length() - 2);

                    String load_truck_type = json_data.getString("load_truck_type");
                    String load_truck_type_from_json = load_truck_type.substring(2, load_truck_type.toString().length() - 2);

                    String load_date = json_data.getString("load_date");
                    String load_date_from_json = load_date.substring(2, load_date.toString().length() - 2);

                    String load_time = json_data.getString("load_time");
                    String load_time_from_json = load_time.substring(2, load_time.toString().length() - 2);

                    String load_weight_capacity = json_data.getString("load_weight_capacity");
                    String load_weight_capacity_from_json = load_weight_capacity.substring(2, load_weight_capacity.toString().length() - 2);

//                    String load_check_box_for_pickup = json_data.getString("load_check_box_for_pickup");
//                    String load_check_box_for_pickup_from_json = load_check_box_for_pickup.substring(2, load_check_box_for_pickup.toString().length() - 2);

//                    String load_check_box_for_drop = json_data.getString("load_check_box_for_drop");
//                    String load_check_box_for_drop_from_json = load_check_box_for_drop.substring(2, load_check_box_for_drop.toString().length() - 2);

                    String load_advance_in_percentage_from_json;
                    if(json_data.has("load_advance_in_percentage")){
                        String load_advance_in_percentage = json_data.getString("load_advance_in_percentage");
                        load_advance_in_percentage_from_json = load_advance_in_percentage.substring(2, load_advance_in_percentage.toString().length() - 2);
                    } else {
                        load_advance_in_percentage_from_json = "70";
                    }

                    Log.d("load_source_city",load_source_city_from_json);
                    Log.d("load_destination_city",load_destination_city_from_json);
                    Log.d("load_date",load_date_from_json);
                    Log.d("load_weight_capacity",load_weight_capacity_from_json);
                    Log.d("load_expected_frieght_value",load_expected_frieght_value_from_json);
                    Log.d("load_advance_in_percentage",load_advance_in_percentage_from_json);
                    Log.d("load_material",load_material_from_json);
                    Log.d("load_truck_type",load_truck_type_from_json);
                    Log.d("load_time",load_time_from_json);

//                    Log.d("load_check_box_for_pickup",load_check_box_for_pickup);
//                    Log.d("load_check_box_for_drop",load_check_box_for_drop);

                    pickup_city.setText(load_source_city_from_json);
                    drop_city.setText(load_destination_city_from_json);
                    pickup_date.setText(load_date_from_json);
                    wt_per_truck.setText(load_weight_capacity_from_json);
                    freight_amt.setText(load_expected_frieght_value_from_json);
                    adv_in_percentage.setText(load_advance_in_percentage_from_json);
                    String replaced_truck_type = load_truck_type_from_json.replace("\\","");

                    defaultTextForMaterialSpinner = load_material_from_json;
                    material_spinner.setAdapter(new CustomSpinnerAdapter(con, R.layout.text_view_for_spinner, material_paths, defaultTextForMaterialSpinner));

                    defaultTextForTruckTypeSpinner = replaced_truck_type;
                    truck_type_spinner.setAdapter(new CustomSpinnerAdapter(con, R.layout.text_view_for_spinner, trucktype_paths, defaultTextForTruckTypeSpinner));

                    defaultTextForScheduleTimeSpinner = load_time_from_json;
                    pickup_time_spinner.setAdapter(new CustomSpinnerAdapter(con, R.layout.text_view_for_spinner, scheduletime_paths, defaultTextForScheduleTimeSpinner));


                }


            } catch (JSONException e) {e.printStackTrace();}
            dialogue.dismiss();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String str = (String) adapterView.getItemAtPosition(i);
        // Toast.makeText(con, str, Toast.LENGTH_SHORT).show();
    }

    private void updateLabel() throws ParseException {

        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        String pick_date = sdf.format(myCalendar.getTime());

        if(sdf.parse(pick_date).before(sdf.parse(curr_date))) {

            new AlertDialog.Builder(con)
                    .setTitle("Possible Date Required")
                    .setMessage("Please select date from today!")
                    .show();

        }else{

            if(new YoulorrySession(con).getrole().equals("transporter")){
                t_available_date.setText(sdf.format(myCalendar.getTime()));
            }
            if(new YoulorrySession(con).getrole().equals("customer")){
                pickup_date.setText(sdf.format(myCalendar.getTime()));
            }
        }
    }


    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:in");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return (String) resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new Filter.FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.top_cancel_btn){finish();}

        if(view.getId() == R.id.re_post_button){

            if(new YoulorrySession(con).getrole().equals("transporter")){

                String t_s_city1,t_d_city1,t_avail_date,t_freight_amt,vehicle_number,t_wt,t_truck_type_spinner1;

                t_s_city1 = t_s_city.getText().toString();
                t_d_city1 = t_d_city.getText().toString();
                t_avail_date = t_available_date.getText().toString();
                t_freight_amt = t_expected_freight_value.getText().toString();
                vehicle_number = t_vehicle_number.getText().toString();
                t_wt = t_wt_per_truck.getText().toString();

                t_truck_type_spinner1 = t_truck_type_spinner.getSelectedItem().toString();

                if(new YoulorrySession(con).getPANCardNumber().length() == 0 ){
                    new AlertDialog.Builder(con)
                            .setTitle("Alert Message")
                            .setMessage("Your PAN Number not saved. Please update your PAN Number with us so that you will allow to Re-Post.")
                            .setIcon(R.drawable.ic_warning_black_24dp)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    startActivity(new Intent(con, MainActivity.class));
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .show();
                } else if(t_s_city1.length() == 0 ){
                    t_s_city.setError( "Source City required!" );
                } else if(t_d_city1.length() == 0 ){
                    t_d_city.setError( "Destination City required!" );
                } else if(t_vehicle_number.length() == 0 ){
                    t_vehicle_number.setError( "Vehicle Number required!" );
                }else if(t_wt_per_truck.length() == 0 ){
                    t_wt_per_truck.setError( "Weight required!" );
                }  else if(t_avail_date.length() == 0 ){
                    new AlertDialog.Builder(con)
                            .setTitle("Available Date Required")
                            .setMessage("Please select available date!")
                            .show();
                } else if(t_freight_amt.length() == 0 ){
                    t_expected_freight_value.setError( "Expected Amount required!" );
                } else {

                    YoulorrySession session = new YoulorrySession(con);
                    String username = session.getusername();
                    String password = session.getpass_word();
                    String userid = session.getuser_id();
                    String role = session.getrole();

                    URL urlObj = null;
                    try {
                        urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-fetch-my-expired-entity/");
                        HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                        urlConnection.setDoOutput(true);
                        urlConnection.setDoInput(true);
                        urlConnection.setUseCaches(false);
                        urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                        //  urlConnection.setRequestProperty("Accept", "application/json");
                        urlConnection.setRequestMethod("POST");
                        urlConnection.connect();

                        JSONObject cred = new JSONObject();

                        cred.put("username",username);
                        cred.put("password",password);
                        cred.put("userid",userid);
                        cred.put("role",role);
                        cred.put("truck_id",repost_id.toString());
                        cred.put("source_city",t_s_city1);
                        cred.put("destination_city",t_d_city1);
                        cred.put("weight",t_wt);
                        cred.put("vehicle_number",vehicle_number);
                        cred.put("truck_type", t_truck_type_spinner1);
                        cred.put("date",t_avail_date);
                        cred.put("expected_frieght_value",t_freight_amt);

                        Log.d("truck_id",repost_id.toString());
                        Log.d("source_city",t_s_city1);
                        Log.d("destination_city",t_d_city1);
                        Log.d("date",t_avail_date);
                        Log.d("expected_frieght_value",t_freight_amt);
                        Log.d("weight",t_wt);
                        Log.d("vehicle_number",vehicle_number);
                        Log.d("truck_type", t_truck_type_spinner1);

                        OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                        wr.write(cred.toString());
                        wr.flush();
                        wr.close();

                        StringBuilder sb = new StringBuilder();
                        int HttpResult = urlConnection.getResponseCode();
                        if (HttpResult == HttpURLConnection.HTTP_OK) {
                            //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                            BufferedReader br = new BufferedReader(
                                    new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                            String line = null;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            br.close();

                             //   Toast.makeText(con, sb.toString() , Toast.LENGTH_LONG).show();
                            if(sb.toString().equals("ok")){
                                new AlertDialog.Builder(this)
                                        .setTitle("Success Message")
                                        .setMessage("Truck Re-Posted Successfully!!!")
                                        .setIcon(R.drawable.ic_done_all_black_24dp)
                                        .show();

                                new Handler().postDelayed(new Runnable() {
                                    public void run() {
                                        startActivity(new Intent(con, MainActivity.class));
                                        finish();
                                    }
                                },2000);
//                         startActivity(new Intent(this, MainActivity.class));
                            }else{

                                new AlertDialog.Builder(this)
                                        .setTitle("Failed Message")
                                        .setMessage("Your Truck was not Re-Post due to some error. Please try again!!!")
                                        .setIcon(R.drawable.ic_warning_black_24dp)
                                        .show();

                                new Handler().postDelayed(new Runnable() {
                                    public void run() {
                                        startActivity(new Intent(con, MainActivity.class));
                                        finish();
                                    }
                                },6000);
//                         startActivity(new Intent(this, MainActivity.class));

                            }
                        }

                    } catch (ProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            if(new YoulorrySession(con).getrole().equals("customer")){

                String pickup_city1,drop_city1,material_type1,truck_type1,pickup_time1,wt_per_truck1,pickup_date1,freight_amt1,adv_in_per1,pickup,pickup1,drop,drop1;

                pickup_city1 = pickup_city.getText().toString();
                drop_city1 = drop_city.getText().toString();
                material_type1 = material_spinner.getSelectedItem().toString();
                truck_type1 = truck_type_spinner.getSelectedItem().toString();
                pickup_time1 = pickup_time_spinner.getSelectedItem().toString();
                wt_per_truck1 = wt_per_truck.getText().toString();
                pickup_date1 = pickup_date.getText().toString();
                freight_amt1 = freight_amt.getText().toString();
                adv_in_per1 = adv_in_percentage.getText().toString();
                int adv1 = Integer.parseInt(adv_in_percentage.getText().toString());

                if(new YoulorrySession(con).getPANCardNumber().length() == 0 ){
                    new AlertDialog.Builder(con)
                            .setTitle("Alert Message")
                            .setMessage("Your PAN Number not saved. Please update your PAN Number with us so that you will allow to Re-Post.")
                            .setIcon(R.drawable.ic_warning_black_24dp)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    startActivity(new Intent(con, MainActivity.class));
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .show();
                } else if(adv1 < 70 || adv1 > 100){
                    adv_in_percentage.setError( "70 to 100 value allowed !" );
                }else if(adv_in_per1.length() < 2){
                    adv_in_percentage.setError( "Min percentage 70!" );
                } else if(freight_amt1.length() == 0 ){
                    freight_amt.setError( "Freight Amount required!" );
                } else if(adv_in_per1.length() == 0 ){
                    adv_in_percentage.setError( "Advance % required!" );
                } else if(pickup_city.length() == 0 ){
                    pickup_city.setError( "Pick Up City required!" );
                } else if(drop_city.length() == 0 ){
                    drop_city.setError( "Drop City required!" );
                } else if(wt_per_truck.length() == 0 ){
                    wt_per_truck.setError( "Weight required!" );
                } else if( !ch_box_for_pickup.isChecked() && !ch_box_for_pickup1.isChecked() ){
                    new AlertDialog.Builder(con)
                            .setTitle("Pickup Point Required")
                            .setMessage("Please select pickup point!")
                            .show();
                } else if( !ch_box_for_drop.isChecked() && !ch_box_for_drop1.isChecked() ){
                    new AlertDialog.Builder(con)
                            .setTitle("Drop Point Required")
                            .setMessage("Please select drop point!")
                            .show();
                } else if(pickup_date1.length() == 0 ){
                    new AlertDialog.Builder(con)
                            .setTitle("Available Date Required")
                            .setMessage("Please select available date!")
                            .show();
                } else {

                    YoulorrySession session = new YoulorrySession(con);
                    String username = session.getusername();
                    String password = session.getpass_word();
                    String userid = session.getuser_id();
                    String role = session.getrole();

                    URL urlObj = null;
                    try {
                        urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-fetch-my-expired-entity/");
                        HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                        urlConnection.setDoOutput(true);
                        urlConnection.setDoInput(true);
                        urlConnection.setUseCaches(false);
                        urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                        //  urlConnection.setRequestProperty("Accept", "application/json");
                        urlConnection.setRequestMethod("POST");
                        urlConnection.connect();

                        JSONObject cred = new JSONObject();

                        cred.put("username",username);
                        cred.put("password",password);
                        cred.put("userid",userid);
                        cred.put("role",role);
                        cred.put("load_id",repost_id.toString());
                        cred.put("date",pickup_date1);
                        cred.put("expected_frieght_value",freight_amt1);
                        cred.put("advance_in_percentage",adv_in_per1);
                        cred.put("pickup_city",pickup_city1);
                        cred.put("drop_city",drop_city1);
                        cred.put("material_type",material_type1);
                        cred.put("truck_type",truck_type1);
                        cred.put("pickup_time",pickup_time1);
                        cred.put("wt_per_truck",wt_per_truck1);

                        if(ch_box_for_pickup.isChecked()){cred.put("pickup_point", "inside city");}
                        if(ch_box_for_pickup1.isChecked()){cred.put("pickup_point", "outside city");}
                        if(ch_box_for_drop.isChecked()){cred.put("drop_point", "inside city");}
                        if(ch_box_for_drop1.isChecked()){cred.put("drop_point", "outside city");}

                        Log.d("load_id",repost_id.toString());
                        Log.d("date",pickup_date1);
                        Log.d("expected_frieght_value",freight_amt1);
                        Log.d("advance_in_percentage",adv_in_per1);

                        OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                        wr.write(cred.toString());
                        wr.flush();
                        wr.close();

                        StringBuilder sb = new StringBuilder();
                        int HttpResult = urlConnection.getResponseCode();
                        if (HttpResult == HttpURLConnection.HTTP_OK) {
                            //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                            BufferedReader br = new BufferedReader(
                                    new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                            String line = null;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            br.close();

                         //   Toast.makeText(con, sb.toString() , Toast.LENGTH_LONG).show();
                            if(sb.toString().equals("ok")){
                                new AlertDialog.Builder(this)
                                        .setTitle("Success Message")
                                        .setMessage("Load Re-Posted Successfully!!!")
                                        .setIcon(R.drawable.ic_done_all_black_24dp)
                                        .show();

                                new Handler().postDelayed(new Runnable() {
                                    public void run() {
                                        startActivity(new Intent(con, MainActivity.class));
                                        finish();
                                    }
                                },2000);
//                         startActivity(new Intent(this, MainActivity.class));
                            }else{

                                new AlertDialog.Builder(this)
                                        .setTitle("Failed Message")
                                        .setMessage("Your Load was not Re-Post due to some error. Please try again!!!")
                                        .setIcon(R.drawable.ic_warning_black_24dp)
                                        .show();

                                new Handler().postDelayed(new Runnable() {
                                    public void run() {
                                        startActivity(new Intent(con, MainActivity.class));
                                        finish();
                                    }
                                },6000);
//                         startActivity(new Intent(this, MainActivity.class));

                            }
                        }

                    } catch (ProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
