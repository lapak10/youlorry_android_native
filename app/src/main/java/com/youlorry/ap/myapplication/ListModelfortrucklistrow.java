package com.youlorry.ap.myapplication;

/**
 * Created by Arpit Prajapati on 12/1/16.
 */

public class ListModelfortrucklistrow {

        private  String S_city="";
        private  String D_city="";
        private  String Truck_type="";
        private  String Price;
        private  String Posted_by;
        private  String Date;
        private  String weightCapacity;
        private  String vehicleNumber;
        private  String stuffId;

    /*********** Set Methods ******************/

        public void setSourcecity(String S_city)
        {
            this.S_city = S_city;
        }

        public void setDestinationcity(String D_city)
        {
             this.D_city = D_city;
        }

        public void setTrucktype(String Truck_type)
        {
             this.Truck_type = Truck_type;
        }

        public void setPrice(String Price)
        {
            this.Price = Price;
        }

        public void setPostedby(String posted_by)
    {
        this.Posted_by = posted_by;
    }

        public void setSchDate(String sch_date) {this.Date = sch_date;}

        public void setWeightCapacity(String weightCapacity) { this.weightCapacity = weightCapacity; }

        public void setVehicleNumber(String vehicleNumber) { this.vehicleNumber = vehicleNumber; }

        public void setStuffId(String stuffId) { this.stuffId = stuffId; }

    /*********** Get Methods ****************/

        public String getSourcecity()
        {
            return this.S_city;
        }

        public String getDestinationcity()
        {
             return this.D_city;
        }

        public String getTruck_type()
        {
             return this.Truck_type;
        }

        public String getPrice()
    {
        return this.Price;
    }

        public String getPostedby()
    {
        return this.Posted_by;
    }

        public String getSchDate()
    {
        return this.Date;
    }

        public String getWeightCapacity()
    {
        return this.weightCapacity;
    }

        public String getVehicleNumber()
    {
        return this.vehicleNumber;
    }

        public String getStuffId()
    {
        return this.stuffId;
    }
}
