package com.youlorry.ap.myapplication;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Arpit Prajapati on 12/6/16.
 */
public class Dashboardfortruckowner extends Fragment implements View.OnClickListener{

    TextView user_name,header_text_on_dashboard;
    TextView first_icon,second_icon,third_icon,fourth_icon,fifth_icon,first_icon_text,second_icon_text,third_icon_text,fourth_icon_text,fifth_icon_text;
    ListView listView;
    TextView t1;
    ImageView post_truck_img,post_load_img,find_truck_img,find_load_img,sent_quote_img,received_quote_img,my_truck_img,my_load_img,tracking_img;
    TextView post_truck_tv,post_load_tv,find_load_tv,find_truck_tv,my_load_tv,my_truck_tv;
    Context con;
    private Activity con1;
//    CustomAdapterforquotationview adapter;
//    public ArrayList<ListmodalforQuotations> CustomListViewValuesArr1 = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dashboard_for_truck_owner, container, false);

        con = getActivity();

        YoulorrySession session = new YoulorrySession(con);
 //       ClearSession();

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(con , LoginActivity.class);
            startActivity(in);

        }

  /*      try {
            InputMethodManager input = (InputMethodManager) getActivity()
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            input.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }catch(Exception e) {
            e.printStackTrace();
        }
*/
        listView = (ListView) v.findViewById(R.id.c_pending_quotes_listview);
        t1 = (TextView) v.findViewById(R.id.no_matching_data_found);
        first_icon = (TextView) v.findViewById(R.id.first_icon);
        second_icon = (TextView) v.findViewById(R.id.second_icon);
        third_icon = (TextView) v.findViewById(R.id.third_icon);
        fourth_icon = (TextView) v.findViewById(R.id.fourth_icon);
        fifth_icon = (TextView) v.findViewById(R.id.fifth_icon);
        first_icon_text = (TextView) v.findViewById(R.id.first_icon_text);
        second_icon_text = (TextView) v.findViewById(R.id.second_icon_text);
        third_icon_text = (TextView) v.findViewById(R.id.third_icon_text);
        fourth_icon_text = (TextView) v.findViewById(R.id.fourth_icon_text);
        fifth_icon_text = (TextView) v.findViewById(R.id.fifth_icon_text);

        if(session.getrole().equals("transporter")){
            first_icon_text.setText(" Truck Posted");
            post_truck_img = (ImageView) v.findViewById(R.id.post_truck_icon);
            find_load_img = (ImageView) v.findViewById(R.id.find_load_icon);
            my_truck_img = (ImageView) v.findViewById(R.id.my_truck_icon);
            post_truck_tv = (TextView) v.findViewById(R.id.post_truck_tv);
            find_load_tv = (TextView) v.findViewById(R.id.find_load_tv);
            my_truck_tv = (TextView) v.findViewById(R.id.my_truck_tv);
            post_truck_img.setVisibility(View.VISIBLE);
            post_truck_tv.setVisibility(View.VISIBLE);
            find_load_img.setVisibility(View.VISIBLE);
            find_load_tv.setVisibility(View.VISIBLE);
            my_truck_img.setVisibility(View.VISIBLE);
            my_truck_tv.setVisibility(View.VISIBLE);

            post_truck_img.setOnClickListener(this);
            find_load_img.setOnClickListener(this);
            my_truck_img.setOnClickListener(this);
        } else if(session.getrole().equals("customer")){
            first_icon_text.setText(" Load Posted");
            post_load_img = (ImageView) v.findViewById(R.id.post_load_icon);
            find_truck_img = (ImageView) v.findViewById(R.id.find_truck_icon);
            my_load_img = (ImageView) v.findViewById(R.id.my_load_icon);
            post_load_tv = (TextView) v.findViewById(R.id.post_load_tv);
            find_truck_tv = (TextView) v.findViewById(R.id.find_truck_tv);
            my_load_tv = (TextView) v.findViewById(R.id.my_load_tv);
            post_load_img.setVisibility(View.VISIBLE);
            post_load_tv.setVisibility(View.VISIBLE);
            find_truck_img.setVisibility(View.VISIBLE);
            find_truck_tv.setVisibility(View.VISIBLE);
            my_load_img.setVisibility(View.VISIBLE);
            my_load_tv.setVisibility(View.VISIBLE);

            post_load_img.setOnClickListener(this);
            find_truck_img.setOnClickListener(this);
            my_load_img.setOnClickListener(this);
        }

        sent_quote_img = (ImageView) v.findViewById(R.id.sent_quotation_icon);
        received_quote_img = (ImageView) v.findViewById(R.id.received_quotation_icon);
        tracking_img = (ImageView) v.findViewById(R.id.tracking_icon);

        sent_quote_img.setOnClickListener(this);
        received_quote_img.setOnClickListener(this);
        tracking_img.setOnClickListener(this);

        first_icon.setOnClickListener(this);
        second_icon.setOnClickListener(this);
        third_icon.setOnClickListener(this);
        fourth_icon.setOnClickListener(this);
        fifth_icon.setOnClickListener(this);
        first_icon_text.setOnClickListener(this);
        second_icon_text.setOnClickListener(this);
        third_icon_text.setOnClickListener(this);
        fourth_icon_text.setOnClickListener(this);
        fifth_icon_text.setOnClickListener(this);

        String Username = new YoulorrySession(getActivity()).getFirstName().toString();
        user_name = (TextView) v.findViewById(R.id.user_name);
        header_text_on_dashboard = (TextView) v.findViewById(R.id.header_text_on_dashboard);
        user_name.setText("Mr. "+Username);
        header_text_on_dashboard.setText("Welcome "+Username);
//        new TheTask().execute();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        ClearSession();
        new TheTask().execute();
    }

    public void ClearSession(){

        YoulorrySession session = new YoulorrySession(con);
        Log.d("clean_session_data","yes");
        session.setCurrentloadpostidforbooking("");
        session.setCurrenttruckpostidforbooking("");
        session.setMatchingtrucksforpostaload("");
        session.setMatchingtrucksforfindtruck("");
        session.setCustomerQuotations("");
        session.setTransporterQuotations("");
        session.setMatchingloadsforpostatruck("");
        session.setMatchingloadsforfindload("");
        session.setPendingQuotations("");
        session.setConfirmedQuotations("");
        session.setCancelledQuotations("");
        session.setHistoryData("");
        session.setExpiredStuff("");
        session.setMyStuff("");

    }

    @Override
    public void onClick(View view) {

        FragmentTransaction transaction=getFragmentManager().beginTransaction();

        if(view.getId() == R.id.post_truck_icon){
            PosttruckActivity post_truck = new PosttruckActivity();

            transaction.replace(R.id.content_main,post_truck);
            transaction.addToBackStack(null);
            transaction.commit();
        }
        if(view.getId() == R.id.post_load_icon){
            PostaloadActivity post_load = new PostaloadActivity();

            transaction.replace(R.id.content_main,post_load);
            transaction.addToBackStack(null);
            transaction.commit();
        }
        if(view.getId() == R.id.find_load_icon){
            FIndLoadActivity find_load = new FIndLoadActivity();

            transaction.replace(R.id.content_main, find_load);
            transaction.addToBackStack(null);
            transaction.commit();
        }
        if(view.getId() == R.id.find_truck_icon){
            FindtruckActivity find_truck = new FindtruckActivity();

            transaction.replace(R.id.content_main, find_truck);
            transaction.addToBackStack(null);
            transaction.commit();
        }
        if(view.getId() == R.id.sent_quotation_icon){
            if(new YoulorrySession(con).getrole().equals("transporter")){
                Intent in = new Intent(con, Quotationfortransporter.class);
                in.putExtra( "request_page" , 0);
                startActivity(in);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                startActivity(new Intent(con, Quotationfortransporter.class));
            } else if(new YoulorrySession(con).getrole().equals("customer")){
                Intent in = new Intent(con, Quotationforcustomer.class);
                in.putExtra( "request_page" , 0);
                startActivity(in);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                startActivity(new Intent(con, Quotationforcustomer.class));
            }
        }
        if(view.getId() == R.id.received_quotation_icon){
            if(new YoulorrySession(con).getrole().equals("transporter")){
                Intent in = new Intent(con, Quotationfortransporter.class);
                in.putExtra( "request_page" , 1);
                startActivity(in);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                startActivity(new Intent(con, Quotationfortransporter.class));
            } else if(new YoulorrySession(con).getrole().equals("customer")){
                Intent in = new Intent(con, Quotationforcustomer.class);
                in.putExtra( "request_page" , 1);
                startActivity(in);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                startActivity(new Intent(con, Quotationforcustomer.class));
            }
        }
        if(view.getId() == R.id.my_truck_icon || view.getId() == R.id.my_load_icon){
            MyStuff my_stuff= new MyStuff();

            transaction.replace(R.id.content_main,my_stuff);
            transaction.addToBackStack(null);
            transaction.commit();
        }
        if(view.getId() == R.id.tracking_icon){
            Toast.makeText(con, "Coming Soon", Toast.LENGTH_SHORT).show();
        }

        if(view.getId() == R.id.first_icon || view.getId() == R.id.first_icon_text){
      //      Toast.makeText(con, "first", Toast.LENGTH_SHORT).show();
            MyStuff my_stuff= new MyStuff();

            transaction.replace(R.id.content_main,my_stuff);
            transaction.addToBackStack(null);
            transaction.commit();
        }

        if(view.getId() == R.id.second_icon || view.getId() == R.id.second_icon_text ){

            if(new YoulorrySession(con).getrole().equals("transporter")){
                Intent in = new Intent(con, Quotationfortransporter.class);
                in.putExtra( "request_page" , 0);
                startActivity(in);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                startActivity(new Intent(con, Quotationfortransporter.class));
            } else if(new YoulorrySession(con).getrole().equals("customer")){
                Intent in = new Intent(con, Quotationforcustomer.class);
                in.putExtra( "request_page" , 0);
                startActivity(in);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                startActivity(new Intent(con, Quotationforcustomer.class));
            }
            //    Toast.makeText(con, "second", Toast.LENGTH_SHORT).show();
        }

        if(view.getId() == R.id.third_icon || view.getId() == R.id.third_icon_text  ){

            if(new YoulorrySession(con).getrole().equals("transporter")){
                Intent in = new Intent(con, Quotationfortransporter.class);
                in.putExtra( "request_page" , 1);
                startActivity(in);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                startActivity(new Intent(con, Quotationfortransporter.class));
            } else if(new YoulorrySession(con).getrole().equals("customer")){
                Intent in = new Intent(con, Quotationforcustomer.class);
                in.putExtra( "request_page" , 1);
                startActivity(in);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                startActivity(new Intent(con, Quotationforcustomer.class));
            }
            //    Toast.makeText(con, "second", Toast.LENGTH_SHORT).show();
        }

        if(view.getId() == R.id.fourth_icon || view.getId() == R.id.fourth_icon_text ){

            if(new YoulorrySession(con).getrole().equals("transporter")){
                Intent in = new Intent(con, Quotationfortransporter.class);
                in.putExtra( "request_page" , 3);
                startActivity(in);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                startActivity(new Intent(con, Quotationfortransporter.class));
            } else if(new YoulorrySession(con).getrole().equals("customer")){
                Intent in = new Intent(con, Quotationforcustomer.class);
                in.putExtra( "request_page" , 3);
                startActivity(in);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                startActivity(new Intent(con, Quotationforcustomer.class));
            }
            //    Toast.makeText(con, "second", Toast.LENGTH_SHORT).show();
        }

        if(view.getId() == R.id.fifth_icon || view.getId() == R.id.fifth_icon_text ){

            if(new YoulorrySession(con).getrole().equals("transporter")){
                Intent in = new Intent(con, Quotationfortransporter.class);
                in.putExtra( "request_page" , 2);
                startActivity(in);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                startActivity(new Intent(con, Quotationfortransporter.class));
            } else if(new YoulorrySession(con).getrole().equals("customer")){
                Intent in = new Intent(con, Quotationforcustomer.class);
                in.putExtra( "request_page" , 2);
                startActivity(in);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//                startActivity(new Intent(con, Quotationforcustomer.class));
            }
            //    Toast.makeText(con, "second", Toast.LENGTH_SHORT).show();
        }
    }

    class TheTask extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            dialogue = new ProgressDialog(con);
//            dialogue.setTitle("Loading ...");
//            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

           String weeklyRecords =  WeeklyRecords();

            YoulorrySession session = new YoulorrySession(con);
            String username = session.getusername();
            String password = session.getpass_word();
            String userid = session.getuser_id();
            String role = session.getrole();
            session.setWeeklyRecord(weeklyRecords);

            return weeklyRecords.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                Log.d("Weekly_Record", new YoulorrySession(con).getWeeklyRecord());
                JSONObject jobject = new JSONObject(new YoulorrySession(con).getWeeklyRecord());
                String weekly_data = jobject.getString("dashboard_info");
                JSONObject jo = new JSONObject(weekly_data);

                String posted_stuff = jo.optString("posted_entity").toString();
                String quotation_sent = jo.optString("sent_quotation_count").toString();
                String quotation_cancelled = jo.optString("cancelled_quotation_count").toString();
                String quotation_received = jo.optString("received_quotation_count").toString();
                String booking_confirmed = jo.optString("bookings_count").toString();

                first_icon.setText(posted_stuff);
                second_icon.setText(quotation_sent);
                third_icon.setText(quotation_received);
                fourth_icon.setText(quotation_cancelled);
                fifth_icon.setText(booking_confirmed);

                Log.d("first_icon_text", posted_stuff);
                Log.d("second_icon_text", quotation_sent);
                Log.d("third_icon_text", quotation_received);
                Log.d("fourth_icon_text", quotation_cancelled);
                Log.d("fifth_icon_text", booking_confirmed);

                Log.d("pending_data_on_dash", s.toString());

            } catch (JSONException e) {e.printStackTrace();}
//            dialogue.dismiss();
        }
    }

    public String WeeklyRecords(){
        String return_data = "";

        YoulorrySession session = new YoulorrySession(con);
        String username = session.getusername();
        String password = session.getpass_word();
        String userid = session.getuser_id();
        String role = session.getrole();

        StringBuilder sb = new StringBuilder();
        URL urlObj = null;
        try {
            urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/youlorry-api/");
            HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            //  urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.connect();

            JSONObject cred = new JSONObject();

            cred.put("username",username);
            cred.put("password",password);
            cred.put("userid",userid);
            cred.put("role",role);
            cred.put("get_dashboard_info_only", "dashboard_info");

            OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
            wr.write(cred.toString());
            // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
            wr.flush();
            wr.close();

            //display what returns the POST request

            int HttpResult = urlConnection.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();

//                    Toast.makeText(con, sb.toString() , Toast.LENGTH_LONG).show();
                return_data = sb.toString();
            }

        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return return_data;
    }
}
