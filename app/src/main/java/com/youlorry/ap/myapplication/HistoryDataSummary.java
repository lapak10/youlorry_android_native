package com.youlorry.ap.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Arpit Prajapati on 1/3/17.
 */
public class HistoryDataSummary extends Activity implements View.OnClickListener{

    String position1;
    ImageView cancel_btn;
    Context con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_table);

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        YoulorrySession session = new YoulorrySession(this);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(this , LoginActivity.class);
            startActivity(in);

        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        con = this;
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width*0.980),(int)(height*0.6));

        cancel_btn = (ImageView) findViewById(R.id.top_cancel_btn);
        Bundle bundle = getIntent().getExtras();
        position1 = bundle.getString("history_item_position_for_json");
        cancel_btn.setOnClickListener(this);

        new TheTask().execute();
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.top_cancel_btn){
            finish();
        }

    }

    class TheTask extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;
        String str;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session_1 = new YoulorrySession(con);
            str = session_1.getHistoryData();

            return str.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.d("history_data", s.toString());

            int mposition = Integer.parseInt(position1);
            TableLayout stk = (TableLayout) findViewById(R.id.full_detail_table);
            TableRow tbrow0 = new TableRow(con);

            TextView tv0 = new TextView(con);
            tv0.setText("Load ID");
            tv0.setTextColor(getResources().getColor(R.color.form_text_hint_color));
            tv0.setTypeface(Typeface.DEFAULT_BOLD);
            tv0.setGravity(Gravity.CENTER);
            tv0.setPadding(0,0,0,10);
            tv0.setWidth(200);
            tbrow0.addView(tv0);

            TextView tv1 = new TextView(con);
            tv1.setText("Vehicle No.");
            tv1.setTextColor(getResources().getColor(R.color.form_text_hint_color));
            tv1.setTypeface(Typeface.DEFAULT_BOLD);
            tv1.setGravity(Gravity.CENTER);
            tv1.setPadding(0,0,0,10);
            tv1.setWidth(250);
            tbrow0.addView(tv1);

            TextView tv2 = new TextView(con);
            tv2.setText("Pick Up City");
            tv2.setTextColor(getResources().getColor(R.color.form_text_hint_color));
            tv2.setTypeface(Typeface.DEFAULT_BOLD);
            tv2.setGravity(Gravity.CENTER);
            tv2.setPadding(0,0,0,10);
            tv2.setWidth(250);
            tbrow0.addView(tv2);

            TextView tv3 = new TextView(con);
            tv3.setText("Drop City");
            tv3.setPadding(0,0,0,10);
            tv3.setGravity(Gravity.CENTER);
            tv3.setTextColor(getResources().getColor(R.color.form_text_hint_color));
            tv3.setTypeface(Typeface.DEFAULT_BOLD);
            tv3.setWidth(220);
            tbrow0.addView(tv3);

            TextView tv4 = new TextView(con);
            tv4.setText("Truck Type");
            tv4.setTextColor(getResources().getColor(R.color.form_text_hint_color));
            tv4.setTypeface(Typeface.DEFAULT_BOLD);
            tv4.setPadding(0,0,0,10);
            tv4.setGravity(Gravity.CENTER);
            tv4.setWidth(250);
            tbrow0.addView(tv4);

            TextView tv5 = new TextView(con);
            tv5.setText("Material");
            tv5.setPadding(0,0,0,10);
            tv5.setTextColor(getResources().getColor(R.color.form_text_hint_color));
            tv5.setTypeface(Typeface.DEFAULT_BOLD);
            tv5.setGravity(Gravity.CENTER);
            tv5.setWidth(200);
            tbrow0.addView(tv5);

            TextView tv6 = new TextView(con);
            tv6.setText("Weight (MT)");
            tv6.setPadding(0,0,0,10);
            tv6.setTextColor(getResources().getColor(R.color.form_text_hint_color));
            tv6.setTypeface(Typeface.DEFAULT_BOLD);
            tv6.setGravity(Gravity.CENTER);
            tv6.setWidth(220);
            tbrow0.addView(tv6);

            TextView tv7 = new TextView(con);
            tv7.setText("Pick Up Date");
            tv7.setPadding(0,0,0,10);
            tv7.setTextColor(getResources().getColor(R.color.form_text_hint_color));
            tv7.setTypeface(Typeface.DEFAULT_BOLD);
            tv7.setGravity(Gravity.CENTER);
            tv7.setWidth(260);
            tbrow0.addView(tv7);

            TextView tv8 = new TextView(con);
            tv8.setText("Amount (Rs)");
            tv8.setPadding(0,0,0,10);
            tv8.setTextColor(getResources().getColor(R.color.form_text_hint_color));
            tv8.setTypeface(Typeface.DEFAULT_BOLD);
            tv8.setGravity(Gravity.CENTER);
            tv8.setWidth(230);
            tbrow0.addView(tv8);
            stk.addView(tbrow0);

            try {

                JSONArray jsonArray = new JSONArray(s);
                for(int i=0; i < jsonArray.length(); i++){

                    if(i == mposition ){

                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String data_summary_jsondata = jsonObject.optString("data_full").toString();
                        Log.d("data_summary_jsondata", data_summary_jsondata);
                        JSONArray jsonArray1 = new JSONArray(data_summary_jsondata);
                        Log.d("jsonArray1", jsonArray1.length()+"");

                        for(int j=0;j < jsonArray1.length(); j++){

                            JSONObject jsonObject1 = jsonArray1.getJSONObject(j);

                            String load_id = jsonObject1.optString("load_id").toString();
                            String v_no = jsonObject1.optString("vehicle_number").toString();
                            String s_city = jsonObject1.optString("source_city").toString();
                            String d_city = jsonObject1.optString("destination_city").toString();
                            String truck_type = jsonObject1.optString("truck_type").toString();
                            String material = jsonObject1.optString("material").toString();
                            String weight_capacity = jsonObject1.optString("weight_capacity").toString();
                            String date = jsonObject1.optString("date").toString();
                            String quoted_amount = jsonObject1.optString("quoted_amount").toString();

                            Log.d("load_id", load_id);
                            Log.d("vehicle_number", v_no);
                            Log.d("source_city", s_city);
                            Log.d("destination_city", d_city);
                            Log.d("truck_type", truck_type);
                            Log.d("material", material);
                            Log.d("weight_capacity", weight_capacity);
                            Log.d("date", date);
                            Log.d("quoted_amount", quoted_amount);

                            TableRow tbrow = new TableRow(con);
                            if(j % 2 == 0){tbrow.setBackgroundColor(Color.argb(255,240,240,240));}
                            else{tbrow.setBackgroundColor(Color.rgb(255,255,255));}

                            TextView t1 = new TextView(con);
                            t1.setText(load_id);
                            t1.setHint("-");
                            t1.setWidth(200);
                            t1.setTextColor(Color.argb(131,61,59,59));
                            t1.setGravity(Gravity.CENTER);
                            tbrow.addView(t1);

                            TextView t2 = new TextView(con);
                            t2.setText(v_no);
                            t2.setWidth(250);
                            t2.setHint("-");
                            t2.setTextColor(Color.argb(131,61,59,59));
                            t2.setGravity(Gravity.CENTER);
                            tbrow.addView(t2);

                            TextView t3 = new TextView(con);
                            t3.setText(s_city);
                            t3.setWidth(250);
                            t3.setHint("-");
                            t3.setTextColor(Color.argb(131,61,59,59));
                            t3.setGravity(Gravity.CENTER);
                            tbrow.addView(t3);

                            TextView t4 = new TextView(con);
                            t4.setText(d_city);
                            t4.setHint("-");
                            t4.setTextColor(Color.argb(131,61,59,59));
                            t4.setWidth(220);
                            t4.setGravity(Gravity.CENTER);
                            tbrow.addView(t4);

                            TextView t5 = new TextView(con);
                            t5.setText(truck_type);
                            t5.setTextColor(Color.argb(131,61,59,59));
                            t5.setHint("-");
                            t5.setWidth(250);
                            t5.setGravity(Gravity.CENTER);
                            tbrow.addView(t5);

                            TextView t6 = new TextView(con);
                            t6.setText(material);
                            t6.setWidth(200);
                            t6.setHint("-");
                            t6.setTextColor(Color.argb(131,61,59,59));
                            t6.setGravity(Gravity.CENTER);
                            tbrow.addView(t6);

                            TextView t7 = new TextView(con);
                            t7.setText(weight_capacity);
                            t7.setHint("-");
                            t7.setWidth(220);
                            t7.setTextColor(Color.argb(131,61,59,59));
                            t7.setGravity(Gravity.CENTER);
                            tbrow.addView(t7);

                            TextView t8 = new TextView(con);
                            t8.setWidth(260);
                            t8.setHint("-");
                            t8.setText(date);
                            t8.setTextColor(Color.argb(131,61,59,59));
                            t8.setGravity(Gravity.CENTER);
                            tbrow.addView(t8);

                            TextView t9 = new TextView(con);
                            t9.setText(quoted_amount);
                            t9.setWidth(230);
                            t9.setHint("-");
                            t9.setTextColor(Color.argb(131,61,59,59));
                            t9.setGravity(Gravity.CENTER);
                            tbrow.addView(t9);
                            stk.addView(tbrow);

                        }

                    }

                }

            } catch (JSONException e) {e.printStackTrace();}

            dialogue.dismiss();
        }
    }

}
