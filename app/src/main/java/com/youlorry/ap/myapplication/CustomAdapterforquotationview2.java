package com.youlorry.ap.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Arpit Prajapati on 12/29/16.
 */
public class CustomAdapterforquotationview2 extends BaseAdapter implements View.OnClickListener {

    private Context activity;
    private ArrayList data;
    private static LayoutInflater inflater=null;
    public Resources res;
    ListmodalforQuotationsforCancelled tempValues=null;
    int i=0;


    /*************  CustomAdapterfortrucklistview Constructor *****************/
    public CustomAdapterforquotationview2(Context a, ArrayList d, Resources resLocal) {

        /********** Take passed values **********/
        activity = a;
        data=d;
        res = resLocal;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = ( LayoutInflater )activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public int getCount() {
        if(data.size()<=0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int i) { return i; }

    @Override
    public long getItemId(int i) { return i; }

    public static class ViewHolder{

        public TextView from_id,to_id,q_price_id,schedule_date,side_v_line;
        public TextView vehicle_number_id,adv_val_id,material_id,weight_id;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View vi = view;
        CustomAdapterforquotationview1.ViewHolder holder;

        if(view==null){

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.table_for_view_quotation3, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new CustomAdapterforquotationview1.ViewHolder();
            holder.from_id = (TextView) vi.findViewById(R.id.from_id);
            holder.schedule_date = (TextView) vi.findViewById(R.id.scheduled_date_id);
            holder.to_id = (TextView) vi.findViewById(R.id.to_id);
            holder.q_price_id = (TextView) vi.findViewById(R.id.quoted_amt_id);
            holder.side_v_line = (TextView) vi.findViewById(R.id.side_vertical_line);
            holder.vehicle_number_id = (TextView) vi.findViewById(R.id.vehicle_number_id);
            holder.adv_val_id = (TextView) vi.findViewById(R.id.adv_val_id);
            holder.material_id = (TextView) vi.findViewById(R.id.material_id);
            holder.weight_id = (TextView) vi.findViewById(R.id.weight_id);

            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        }
        else
            holder=(CustomAdapterforquotationview1.ViewHolder)vi.getTag();

        if(data.size()<=0)
        {
            holder.from_id.setText("No Data");
            holder.schedule_date.setText("No Data");
            holder.to_id.setText("No Data");
            holder.q_price_id.setText("No Data");
            holder.vehicle_number_id.setText("No Data");
            holder.adv_val_id.setText("No Data");
            holder.material_id.setText("No Data");
            holder.weight_id.setText("No Data");

        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            tempValues=null;
            tempValues = ( ListmodalforQuotationsforCancelled ) data.get( i );


            /************  Set Model values in Holder elements ***********/

            String replaced_material_type = tempValues.getMaterialType().toString().replace("\\","");

            holder.from_id.setText( tempValues.getSourcecity2() );
            holder.to_id.setText( tempValues.getDestinationcity2() );
            holder.schedule_date.setText( tempValues.getScheduleDate2() );
            holder.q_price_id.setText( "Q.V. "+"₹ "+tempValues.getQuotedAmt2() );
            holder.vehicle_number_id.setText( tempValues.getVehicleNumber() );
            holder.adv_val_id.setText( "Exp "+"₹ "+tempValues.getExpectedAmount() );
            holder.material_id.setText( replaced_material_type );
            holder.weight_id.setText( " ("+tempValues.getWeight()+" MT) " );

            /******** Set Item Click Listner for LayoutInflater for each row *******/

            vi.setOnClickListener(new CustomAdapterforquotationview2.OnItemClickListener( i ));
        }
        return vi;
    }

    private class OnItemClickListener implements View.OnClickListener {
        private int mPosition;
        ProgressDialog dialogue;

        OnItemClickListener(int position){
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {

            dialogue = new ProgressDialog(activity);
            dialogue.setTitle("Loading ...");
            dialogue.show();
       //     Toast.makeText(activity, mPosition+"" , Toast.LENGTH_SHORT).show();

            Intent in = new Intent(activity,CustomerpendingquotesFullDetail.class);

            in.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            Bundle args = new Bundle();
            args.putString("position_of_list_in_c_pending_list", String.valueOf(mPosition));
            args.putString("quotation_id_from_listmodal_adapter", tempValues.getQuote_ID2());
            args.putString("from_pending", "cancelled_req");

            in.putExtras(args);
            activity.startActivity(in);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    dialogue.dismiss();
                }
            }, 2000);
        }
    }
}
