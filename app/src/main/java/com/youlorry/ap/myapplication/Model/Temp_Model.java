package com.youlorry.ap.myapplication.Model;

import java.util.List;

/**
 * Created by Tycho on 5/5/2017.
 */

public class Temp_Model {


    /**
     * bid_meta_keys : {"ID":20286,"post_author":"42","post_date":"2017-05-06 11:08:19","post_date_gmt":"2017-05-06 11:08:19","post_content":"","post_title":"F-&gt;20284 B-&gt;42 V-&gt;900","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"f-20284-b-42-v-900","to_ping":"","pinged":"","post_modified":"2017-05-06 11:08:19","post_modified_gmt":"2017-05-06 11:08:19","post_content_filtered":"","post_parent":0,"guid":"http://nishabhati.com/youlorry_bidding/f-20284-b-42-v-900/","menu_order":0,"post_type":"youlorry_bidding","post_mime_type":"","comment_count":"0","filter":"raw","bid_placed_by_id":["42"],"factory_load_ID":["20284"],"bid_rate":["900"],"bid_value":["225000"],"bid_weight_capacity":["250"],"is_confirmed":["1"],"left_count":["185"],"_edit_lock":["1494086459:1"]}
     * float_meta_keys : {"ID":20292,"post_author":"42","post_date":"2017-05-06 12:47:06","post_date_gmt":"2017-05-06 12:47:06","post_content":"","post_title":"20284 to 42","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"20284-to-42","to_ping":"","pinged":"","post_modified":"2017-05-06 12:47:06","post_modified_gmt":"2017-05-06 12:47:06","post_content_filtered":"","post_parent":0,"guid":"http://nishabhati.com/freight/20284-to-42/","menu_order":0,"post_type":"freight","post_mime_type":"","comment_count":"0","filter":"raw","bid_ID":["20286"],"floater_id":["42"],"load_ID":["20284"],"market_rate":["800"],"to_pay":["Direct Trucker"],"truck_type":["Open Body Truck"],"pod_days":["After 7 Days"]}
     * my_trucks : [{"ID":20293,"post_author":"42","post_date":"2017-05-06 12:49:31","post_date_gmt":"2017-05-06 12:49:31","post_content":"","post_title":"Factory Float Quote","post_excerpt":"","post_status":"truck_pay_ok","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"factory-float-quote","to_ping":"","pinged":"","post_modified":"2017-05-06 12:49:31","post_modified_gmt":"2017-05-06 12:49:31","post_content_filtered":"","post_parent":0,"guid":"http://nishabhati.com/piklist-demo/factory-float-quote/","menu_order":0,"post_type":"quotation","post_mime_type":"","comment_count":"0","filter":"raw","load_ID":["20284"],"quotation_vehicle_number":["as23q1234"],"left_count":["25"],"initiated_by":["transporter"],"ghost_transporter":["42"],"factory_details_drivers_mobile_number":["9864098640"],"factory_details_driver_name":["danish"],"bid_ID":["20286"]}]
     * incoming_quotations : [{"ID":20294,"post_author":"44","post_date":"2017-05-06 12:52:04","post_date_gmt":"2017-05-06 12:52:04","post_content":"","post_title":"Quote 20294","post_excerpt":"","post_status":"truck_pay_ok","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"quote-20294","to_ping":"","pinged":"","post_modified":"2017-05-06 12:53:09","post_modified_gmt":"2017-05-06 12:53:09","post_content_filtered":"","post_parent":0,"guid":"http://nishabhati.com/?post_type=quotation&#038;p=20294","menu_order":0,"post_type":"quotation","post_mime_type":"","comment_count":"0","filter":"raw","quoted_amount":["950"],"load_ID":["20284"],"quotation_vehicle_number":["as26a1233"],"freight_to_market_ID":["20292"],"floater_id":["42"],"driver_number":["9864098640"],"left_count":["40"],"driver_name":["dan"],"initiated_by":["transporter"],"ghost_transporter":["44"]}]
     * meta_keys : {"ID":20284,"post_author":"31","post_date":"2017-05-06 11:05:43","post_date_gmt":"2017-05-06 11:05:43","post_content":"","post_title":"FACTORY Freight","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"factory-freight-29","to_ping":"","pinged":"","post_modified":"2017-05-06 11:05:43","post_modified_gmt":"2017-05-06 11:05:43","post_content_filtered":"","post_parent":0,"guid":"http://nishabhati.com/load/factory-freight-29/","menu_order":0,"post_type":"load","post_mime_type":"","comment_count":"0","filter":"raw","load_source_city":["Sonapur, Assam, India"],"load_destination_city":["Silchar, Assam, India"],"load_material":["Cement"],"load_share_with":["a:2:{i:0;s:2:\"42\";i:1;s:2:\"44\";}"],"load_weight_capacity":["500"],"load_pickup_point":["Outside City"],"load_drop_point":["Outside City"],"load_rate_per_ton":["1000"],"load_time":["9:00 AM"],"load_date":["07-05-2017"],"load_allow_bidding":["on"],"load_factory_user_id":["31"],"mysql_date":["2017-05-07"],"from_factory":["true"],"bid_winner_array":["a:1:{i:0;i:20286;}"],"left_count":["250"],"_edit_lock":["1494087148:1"]}
     */

    private BidMetaKeysBean bid_meta_keys;
    private FloatMetaKeysBean float_meta_keys;
    private MetaKeysBean meta_keys;
    private List<MyTrucksBean> my_trucks;
    private List<IncomingQuotationsBean> incoming_quotations;

    public BidMetaKeysBean getBid_meta_keys() {
        return bid_meta_keys;
    }

    public void setBid_meta_keys(BidMetaKeysBean bid_meta_keys) {
        this.bid_meta_keys = bid_meta_keys;
    }

    public FloatMetaKeysBean getFloat_meta_keys() {
        return float_meta_keys;
    }

    public void setFloat_meta_keys(FloatMetaKeysBean float_meta_keys) {
        this.float_meta_keys = float_meta_keys;
    }

    public MetaKeysBean getMeta_keys() {
        return meta_keys;
    }

    public void setMeta_keys(MetaKeysBean meta_keys) {
        this.meta_keys = meta_keys;
    }

    public List<MyTrucksBean> getMy_trucks() {
        return my_trucks;
    }

    public void setMy_trucks(List<MyTrucksBean> my_trucks) {
        this.my_trucks = my_trucks;
    }

    public List<IncomingQuotationsBean> getIncoming_quotations() {
        return incoming_quotations;
    }

    public void setIncoming_quotations(List<IncomingQuotationsBean> incoming_quotations) {
        this.incoming_quotations = incoming_quotations;
    }

    public static class BidMetaKeysBean {
        /**
         * ID : 20286
         * post_author : 42
         * post_date : 2017-05-06 11:08:19
         * post_date_gmt : 2017-05-06 11:08:19
         * post_content :
         * post_title : F-&gt;20284 B-&gt;42 V-&gt;900
         * post_excerpt :
         * post_status : publish
         * comment_status : closed
         * ping_status : closed
         * post_password :
         * post_name : f-20284-b-42-v-900
         * to_ping :
         * pinged :
         * post_modified : 2017-05-06 11:08:19
         * post_modified_gmt : 2017-05-06 11:08:19
         * post_content_filtered :
         * post_parent : 0
         * guid : http://nishabhati.com/youlorry_bidding/f-20284-b-42-v-900/
         * menu_order : 0
         * post_type : youlorry_bidding
         * post_mime_type :
         * comment_count : 0
         * filter : raw
         * bid_placed_by_id : ["42"]
         * factory_load_ID : ["20284"]
         * bid_rate : ["900"]
         * bid_value : ["225000"]
         * bid_weight_capacity : ["250"]
         * is_confirmed : ["1"]
         * left_count : ["185"]
         * _edit_lock : ["1494086459:1"]
         */

        private int ID;
        private String post_author;
        private String post_date;
        private String post_date_gmt;
        private String post_content;
        private String post_title;
        private String post_excerpt;
        private String post_status;
        private String comment_status;
        private String ping_status;
        private String post_password;
        private String post_name;
        private String to_ping;
        private String pinged;
        private String post_modified;
        private String post_modified_gmt;
        private String post_content_filtered;
        private int post_parent;
        private String guid;
        private int menu_order;
        private String post_type;
        private String post_mime_type;
        private String comment_count;
        private String filter;
        private List<String> bid_placed_by_id;
        private List<String> factory_load_ID;
        private List<String> bid_rate;
        private List<String> bid_value;
        private List<String> bid_weight_capacity;
        private List<String> is_confirmed;
        private List<String> left_count;
        private List<String> _edit_lock;

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public String getPost_author() {
            return post_author;
        }

        public void setPost_author(String post_author) {
            this.post_author = post_author;
        }

        public String getPost_date() {
            return post_date;
        }

        public void setPost_date(String post_date) {
            this.post_date = post_date;
        }

        public String getPost_date_gmt() {
            return post_date_gmt;
        }

        public void setPost_date_gmt(String post_date_gmt) {
            this.post_date_gmt = post_date_gmt;
        }

        public String getPost_content() {
            return post_content;
        }

        public void setPost_content(String post_content) {
            this.post_content = post_content;
        }

        public String getPost_title() {
            return post_title;
        }

        public void setPost_title(String post_title) {
            this.post_title = post_title;
        }

        public String getPost_excerpt() {
            return post_excerpt;
        }

        public void setPost_excerpt(String post_excerpt) {
            this.post_excerpt = post_excerpt;
        }

        public String getPost_status() {
            return post_status;
        }

        public void setPost_status(String post_status) {
            this.post_status = post_status;
        }

        public String getComment_status() {
            return comment_status;
        }

        public void setComment_status(String comment_status) {
            this.comment_status = comment_status;
        }

        public String getPing_status() {
            return ping_status;
        }

        public void setPing_status(String ping_status) {
            this.ping_status = ping_status;
        }

        public String getPost_password() {
            return post_password;
        }

        public void setPost_password(String post_password) {
            this.post_password = post_password;
        }

        public String getPost_name() {
            return post_name;
        }

        public void setPost_name(String post_name) {
            this.post_name = post_name;
        }

        public String getTo_ping() {
            return to_ping;
        }

        public void setTo_ping(String to_ping) {
            this.to_ping = to_ping;
        }

        public String getPinged() {
            return pinged;
        }

        public void setPinged(String pinged) {
            this.pinged = pinged;
        }

        public String getPost_modified() {
            return post_modified;
        }

        public void setPost_modified(String post_modified) {
            this.post_modified = post_modified;
        }

        public String getPost_modified_gmt() {
            return post_modified_gmt;
        }

        public void setPost_modified_gmt(String post_modified_gmt) {
            this.post_modified_gmt = post_modified_gmt;
        }

        public String getPost_content_filtered() {
            return post_content_filtered;
        }

        public void setPost_content_filtered(String post_content_filtered) {
            this.post_content_filtered = post_content_filtered;
        }

        public int getPost_parent() {
            return post_parent;
        }

        public void setPost_parent(int post_parent) {
            this.post_parent = post_parent;
        }

        public String getGuid() {
            return guid;
        }

        public void setGuid(String guid) {
            this.guid = guid;
        }

        public int getMenu_order() {
            return menu_order;
        }

        public void setMenu_order(int menu_order) {
            this.menu_order = menu_order;
        }

        public String getPost_type() {
            return post_type;
        }

        public void setPost_type(String post_type) {
            this.post_type = post_type;
        }

        public String getPost_mime_type() {
            return post_mime_type;
        }

        public void setPost_mime_type(String post_mime_type) {
            this.post_mime_type = post_mime_type;
        }

        public String getComment_count() {
            return comment_count;
        }

        public void setComment_count(String comment_count) {
            this.comment_count = comment_count;
        }

        public String getFilter() {
            return filter;
        }

        public void setFilter(String filter) {
            this.filter = filter;
        }

        public List<String> getBid_placed_by_id() {
            return bid_placed_by_id;
        }

        public void setBid_placed_by_id(List<String> bid_placed_by_id) {
            this.bid_placed_by_id = bid_placed_by_id;
        }

        public List<String> getFactory_load_ID() {
            return factory_load_ID;
        }

        public void setFactory_load_ID(List<String> factory_load_ID) {
            this.factory_load_ID = factory_load_ID;
        }

        public List<String> getBid_rate() {
            return bid_rate;
        }

        public void setBid_rate(List<String> bid_rate) {
            this.bid_rate = bid_rate;
        }

        public List<String> getBid_value() {
            return bid_value;
        }

        public void setBid_value(List<String> bid_value) {
            this.bid_value = bid_value;
        }

        public List<String> getBid_weight_capacity() {
            return bid_weight_capacity;
        }

        public void setBid_weight_capacity(List<String> bid_weight_capacity) {
            this.bid_weight_capacity = bid_weight_capacity;
        }

        public List<String> getIs_confirmed() {
            return is_confirmed;
        }

        public void setIs_confirmed(List<String> is_confirmed) {
            this.is_confirmed = is_confirmed;
        }

        public List<String> getLeft_count() {
            return left_count;
        }

        public void setLeft_count(List<String> left_count) {
            this.left_count = left_count;
        }

        public List<String> get_edit_lock() {
            return _edit_lock;
        }

        public void set_edit_lock(List<String> _edit_lock) {
            this._edit_lock = _edit_lock;
        }
    }

    public static class FloatMetaKeysBean {
        /**
         * ID : 20292
         * post_author : 42
         * post_date : 2017-05-06 12:47:06
         * post_date_gmt : 2017-05-06 12:47:06
         * post_content :
         * post_title : 20284 to 42
         * post_excerpt :
         * post_status : publish
         * comment_status : closed
         * ping_status : closed
         * post_password :
         * post_name : 20284-to-42
         * to_ping :
         * pinged :
         * post_modified : 2017-05-06 12:47:06
         * post_modified_gmt : 2017-05-06 12:47:06
         * post_content_filtered :
         * post_parent : 0
         * guid : http://nishabhati.com/freight/20284-to-42/
         * menu_order : 0
         * post_type : freight
         * post_mime_type :
         * comment_count : 0
         * filter : raw
         * bid_ID : ["20286"]
         * floater_id : ["42"]
         * load_ID : ["20284"]
         * market_rate : ["800"]
         * to_pay : ["Direct Trucker"]
         * truck_type : ["Open Body Truck"]
         * pod_days : ["After 7 Days"]
         */

        private int ID;
        private String post_author;
        private String post_date;
        private String post_date_gmt;
        private String post_content;
        private String post_title;
        private String post_excerpt;
        private String post_status;
        private String comment_status;
        private String ping_status;
        private String post_password;
        private String post_name;
        private String to_ping;
        private String pinged;
        private String post_modified;
        private String post_modified_gmt;
        private String post_content_filtered;
        private int post_parent;
        private String guid;
        private int menu_order;
        private String post_type;
        private String post_mime_type;
        private String comment_count;
        private String filter;
        private List<String> bid_ID;
        private List<String> floater_id;
        private List<String> load_ID;
        private List<String> market_rate;
        private List<String> to_pay;
        private List<String> truck_type;
        private List<String> pod_days;

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public String getPost_author() {
            return post_author;
        }

        public void setPost_author(String post_author) {
            this.post_author = post_author;
        }

        public String getPost_date() {
            return post_date;
        }

        public void setPost_date(String post_date) {
            this.post_date = post_date;
        }

        public String getPost_date_gmt() {
            return post_date_gmt;
        }

        public void setPost_date_gmt(String post_date_gmt) {
            this.post_date_gmt = post_date_gmt;
        }

        public String getPost_content() {
            return post_content;
        }

        public void setPost_content(String post_content) {
            this.post_content = post_content;
        }

        public String getPost_title() {
            return post_title;
        }

        public void setPost_title(String post_title) {
            this.post_title = post_title;
        }

        public String getPost_excerpt() {
            return post_excerpt;
        }

        public void setPost_excerpt(String post_excerpt) {
            this.post_excerpt = post_excerpt;
        }

        public String getPost_status() {
            return post_status;
        }

        public void setPost_status(String post_status) {
            this.post_status = post_status;
        }

        public String getComment_status() {
            return comment_status;
        }

        public void setComment_status(String comment_status) {
            this.comment_status = comment_status;
        }

        public String getPing_status() {
            return ping_status;
        }

        public void setPing_status(String ping_status) {
            this.ping_status = ping_status;
        }

        public String getPost_password() {
            return post_password;
        }

        public void setPost_password(String post_password) {
            this.post_password = post_password;
        }

        public String getPost_name() {
            return post_name;
        }

        public void setPost_name(String post_name) {
            this.post_name = post_name;
        }

        public String getTo_ping() {
            return to_ping;
        }

        public void setTo_ping(String to_ping) {
            this.to_ping = to_ping;
        }

        public String getPinged() {
            return pinged;
        }

        public void setPinged(String pinged) {
            this.pinged = pinged;
        }

        public String getPost_modified() {
            return post_modified;
        }

        public void setPost_modified(String post_modified) {
            this.post_modified = post_modified;
        }

        public String getPost_modified_gmt() {
            return post_modified_gmt;
        }

        public void setPost_modified_gmt(String post_modified_gmt) {
            this.post_modified_gmt = post_modified_gmt;
        }

        public String getPost_content_filtered() {
            return post_content_filtered;
        }

        public void setPost_content_filtered(String post_content_filtered) {
            this.post_content_filtered = post_content_filtered;
        }

        public int getPost_parent() {
            return post_parent;
        }

        public void setPost_parent(int post_parent) {
            this.post_parent = post_parent;
        }

        public String getGuid() {
            return guid;
        }

        public void setGuid(String guid) {
            this.guid = guid;
        }

        public int getMenu_order() {
            return menu_order;
        }

        public void setMenu_order(int menu_order) {
            this.menu_order = menu_order;
        }

        public String getPost_type() {
            return post_type;
        }

        public void setPost_type(String post_type) {
            this.post_type = post_type;
        }

        public String getPost_mime_type() {
            return post_mime_type;
        }

        public void setPost_mime_type(String post_mime_type) {
            this.post_mime_type = post_mime_type;
        }

        public String getComment_count() {
            return comment_count;
        }

        public void setComment_count(String comment_count) {
            this.comment_count = comment_count;
        }

        public String getFilter() {
            return filter;
        }

        public void setFilter(String filter) {
            this.filter = filter;
        }

        public List<String> getBid_ID() {
            return bid_ID;
        }

        public void setBid_ID(List<String> bid_ID) {
            this.bid_ID = bid_ID;
        }

        public List<String> getFloater_id() {
            return floater_id;
        }

        public void setFloater_id(List<String> floater_id) {
            this.floater_id = floater_id;
        }

        public List<String> getLoad_ID() {
            return load_ID;
        }

        public void setLoad_ID(List<String> load_ID) {
            this.load_ID = load_ID;
        }

        public List<String> getMarket_rate() {
            return market_rate;
        }

        public void setMarket_rate(List<String> market_rate) {
            this.market_rate = market_rate;
        }

        public List<String> getTo_pay() {
            return to_pay;
        }

        public void setTo_pay(List<String> to_pay) {
            this.to_pay = to_pay;
        }

        public List<String> getTruck_type() {
            return truck_type;
        }

        public void setTruck_type(List<String> truck_type) {
            this.truck_type = truck_type;
        }

        public List<String> getPod_days() {
            return pod_days;
        }

        public void setPod_days(List<String> pod_days) {
            this.pod_days = pod_days;
        }
    }

    public static class MetaKeysBean {
        /**
         * ID : 20284
         * post_author : 31
         * post_date : 2017-05-06 11:05:43
         * post_date_gmt : 2017-05-06 11:05:43
         * post_content :
         * post_title : FACTORY Freight
         * post_excerpt :
         * post_status : publish
         * comment_status : closed
         * ping_status : closed
         * post_password :
         * post_name : factory-freight-29
         * to_ping :
         * pinged :
         * post_modified : 2017-05-06 11:05:43
         * post_modified_gmt : 2017-05-06 11:05:43
         * post_content_filtered :
         * post_parent : 0
         * guid : http://nishabhati.com/load/factory-freight-29/
         * menu_order : 0
         * post_type : load
         * post_mime_type :
         * comment_count : 0
         * filter : raw
         * load_source_city : ["Sonapur, Assam, India"]
         * load_destination_city : ["Silchar, Assam, India"]
         * load_material : ["Cement"]
         * load_share_with : ["a:2:{i:0;s:2:\"42\";i:1;s:2:\"44\";}"]
         * load_weight_capacity : ["500"]
         * load_pickup_point : ["Outside City"]
         * load_drop_point : ["Outside City"]
         * load_rate_per_ton : ["1000"]
         * load_time : ["9:00 AM"]
         * load_date : ["07-05-2017"]
         * load_allow_bidding : ["on"]
         * load_factory_user_id : ["31"]
         * mysql_date : ["2017-05-07"]
         * from_factory : ["true"]
         * bid_winner_array : ["a:1:{i:0;i:20286;}"]
         * left_count : ["250"]
         * _edit_lock : ["1494087148:1"]
         */

        private int ID;
        private String post_author;
        private String post_date;
        private String post_date_gmt;
        private String post_content;
        private String post_title;
        private String post_excerpt;
        private String post_status;
        private String comment_status;
        private String ping_status;
        private String post_password;
        private String post_name;
        private String to_ping;
        private String pinged;
        private String post_modified;
        private String post_modified_gmt;
        private String post_content_filtered;
        private int post_parent;
        private String guid;
        private int menu_order;
        private String post_type;
        private String post_mime_type;
        private String comment_count;
        private String filter;
        private List<String> load_source_city;
        private List<String> load_destination_city;
        private List<String> load_material;
        private List<String> load_share_with;
        private List<String> load_weight_capacity;
        private List<String> load_pickup_point;
        private List<String> load_drop_point;
        private List<String> load_rate_per_ton;
        private List<String> load_time;
        private List<String> load_date;
        private List<String> load_allow_bidding;
        private List<String> load_factory_user_id;
        private List<String> mysql_date;
        private List<String> from_factory;
        private List<String> bid_winner_array;
        private List<String> left_count;
        private List<String> _edit_lock;

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public String getPost_author() {
            return post_author;
        }

        public void setPost_author(String post_author) {
            this.post_author = post_author;
        }

        public String getPost_date() {
            return post_date;
        }

        public void setPost_date(String post_date) {
            this.post_date = post_date;
        }

        public String getPost_date_gmt() {
            return post_date_gmt;
        }

        public void setPost_date_gmt(String post_date_gmt) {
            this.post_date_gmt = post_date_gmt;
        }

        public String getPost_content() {
            return post_content;
        }

        public void setPost_content(String post_content) {
            this.post_content = post_content;
        }

        public String getPost_title() {
            return post_title;
        }

        public void setPost_title(String post_title) {
            this.post_title = post_title;
        }

        public String getPost_excerpt() {
            return post_excerpt;
        }

        public void setPost_excerpt(String post_excerpt) {
            this.post_excerpt = post_excerpt;
        }

        public String getPost_status() {
            return post_status;
        }

        public void setPost_status(String post_status) {
            this.post_status = post_status;
        }

        public String getComment_status() {
            return comment_status;
        }

        public void setComment_status(String comment_status) {
            this.comment_status = comment_status;
        }

        public String getPing_status() {
            return ping_status;
        }

        public void setPing_status(String ping_status) {
            this.ping_status = ping_status;
        }

        public String getPost_password() {
            return post_password;
        }

        public void setPost_password(String post_password) {
            this.post_password = post_password;
        }

        public String getPost_name() {
            return post_name;
        }

        public void setPost_name(String post_name) {
            this.post_name = post_name;
        }

        public String getTo_ping() {
            return to_ping;
        }

        public void setTo_ping(String to_ping) {
            this.to_ping = to_ping;
        }

        public String getPinged() {
            return pinged;
        }

        public void setPinged(String pinged) {
            this.pinged = pinged;
        }

        public String getPost_modified() {
            return post_modified;
        }

        public void setPost_modified(String post_modified) {
            this.post_modified = post_modified;
        }

        public String getPost_modified_gmt() {
            return post_modified_gmt;
        }

        public void setPost_modified_gmt(String post_modified_gmt) {
            this.post_modified_gmt = post_modified_gmt;
        }

        public String getPost_content_filtered() {
            return post_content_filtered;
        }

        public void setPost_content_filtered(String post_content_filtered) {
            this.post_content_filtered = post_content_filtered;
        }

        public int getPost_parent() {
            return post_parent;
        }

        public void setPost_parent(int post_parent) {
            this.post_parent = post_parent;
        }

        public String getGuid() {
            return guid;
        }

        public void setGuid(String guid) {
            this.guid = guid;
        }

        public int getMenu_order() {
            return menu_order;
        }

        public void setMenu_order(int menu_order) {
            this.menu_order = menu_order;
        }

        public String getPost_type() {
            return post_type;
        }

        public void setPost_type(String post_type) {
            this.post_type = post_type;
        }

        public String getPost_mime_type() {
            return post_mime_type;
        }

        public void setPost_mime_type(String post_mime_type) {
            this.post_mime_type = post_mime_type;
        }

        public String getComment_count() {
            return comment_count;
        }

        public void setComment_count(String comment_count) {
            this.comment_count = comment_count;
        }

        public String getFilter() {
            return filter;
        }

        public void setFilter(String filter) {
            this.filter = filter;
        }

        public List<String> getLoad_source_city() {
            return load_source_city;
        }

        public void setLoad_source_city(List<String> load_source_city) {
            this.load_source_city = load_source_city;
        }

        public List<String> getLoad_destination_city() {
            return load_destination_city;
        }

        public void setLoad_destination_city(List<String> load_destination_city) {
            this.load_destination_city = load_destination_city;
        }

        public List<String> getLoad_material() {
            return load_material;
        }

        public void setLoad_material(List<String> load_material) {
            this.load_material = load_material;
        }

        public List<String> getLoad_share_with() {
            return load_share_with;
        }

        public void setLoad_share_with(List<String> load_share_with) {
            this.load_share_with = load_share_with;
        }

        public List<String> getLoad_weight_capacity() {
            return load_weight_capacity;
        }

        public void setLoad_weight_capacity(List<String> load_weight_capacity) {
            this.load_weight_capacity = load_weight_capacity;
        }

        public List<String> getLoad_pickup_point() {
            return load_pickup_point;
        }

        public void setLoad_pickup_point(List<String> load_pickup_point) {
            this.load_pickup_point = load_pickup_point;
        }

        public List<String> getLoad_drop_point() {
            return load_drop_point;
        }

        public void setLoad_drop_point(List<String> load_drop_point) {
            this.load_drop_point = load_drop_point;
        }

        public List<String> getLoad_rate_per_ton() {
            return load_rate_per_ton;
        }

        public void setLoad_rate_per_ton(List<String> load_rate_per_ton) {
            this.load_rate_per_ton = load_rate_per_ton;
        }

        public List<String> getLoad_time() {
            return load_time;
        }

        public void setLoad_time(List<String> load_time) {
            this.load_time = load_time;
        }

        public List<String> getLoad_date() {
            return load_date;
        }

        public void setLoad_date(List<String> load_date) {
            this.load_date = load_date;
        }

        public List<String> getLoad_allow_bidding() {
            return load_allow_bidding;
        }

        public void setLoad_allow_bidding(List<String> load_allow_bidding) {
            this.load_allow_bidding = load_allow_bidding;
        }

        public List<String> getLoad_factory_user_id() {
            return load_factory_user_id;
        }

        public void setLoad_factory_user_id(List<String> load_factory_user_id) {
            this.load_factory_user_id = load_factory_user_id;
        }

        public List<String> getMysql_date() {
            return mysql_date;
        }

        public void setMysql_date(List<String> mysql_date) {
            this.mysql_date = mysql_date;
        }

        public List<String> getFrom_factory() {
            return from_factory;
        }

        public void setFrom_factory(List<String> from_factory) {
            this.from_factory = from_factory;
        }

        public List<String> getBid_winner_array() {
            return bid_winner_array;
        }

        public void setBid_winner_array(List<String> bid_winner_array) {
            this.bid_winner_array = bid_winner_array;
        }

        public List<String> getLeft_count() {
            return left_count;
        }

        public void setLeft_count(List<String> left_count) {
            this.left_count = left_count;
        }

        public List<String> get_edit_lock() {
            return _edit_lock;
        }

        public void set_edit_lock(List<String> _edit_lock) {
            this._edit_lock = _edit_lock;
        }
    }

    public static class MyTrucksBean {
        /**
         * ID : 20293
         * post_author : 42
         * post_date : 2017-05-06 12:49:31
         * post_date_gmt : 2017-05-06 12:49:31
         * post_content :
         * post_title : Factory Float Quote
         * post_excerpt :
         * post_status : truck_pay_ok
         * comment_status : closed
         * ping_status : closed
         * post_password :
         * post_name : factory-float-quote
         * to_ping :
         * pinged :
         * post_modified : 2017-05-06 12:49:31
         * post_modified_gmt : 2017-05-06 12:49:31
         * post_content_filtered :
         * post_parent : 0
         * guid : http://nishabhati.com/piklist-demo/factory-float-quote/
         * menu_order : 0
         * post_type : quotation
         * post_mime_type :
         * comment_count : 0
         * filter : raw
         * load_ID : ["20284"]
         * quotation_vehicle_number : ["as23q1234"]
         * left_count : ["25"]
         * initiated_by : ["transporter"]
         * ghost_transporter : ["42"]
         * factory_details_drivers_mobile_number : ["9864098640"]
         * factory_details_driver_name : ["danish"]
         * bid_ID : ["20286"]
         */

        private int ID;
        private String post_author;
        private String post_date;
        private String post_date_gmt;
        private String post_content;
        private String post_title;
        private String post_excerpt;
        private String post_status;
        private String comment_status;
        private String ping_status;
        private String post_password;
        private String post_name;
        private String to_ping;
        private String pinged;
        private String post_modified;
        private String post_modified_gmt;
        private String post_content_filtered;
        private int post_parent;
        private String guid;
        private int menu_order;
        private String post_type;
        private String post_mime_type;
        private String comment_count;
        private String filter;
        private List<String> load_ID;
        private List<String> quotation_vehicle_number;
        private List<String> left_count;
        private List<String> initiated_by;
        private List<String> ghost_transporter;
        private List<String> factory_details_drivers_mobile_number;
        private List<String> factory_details_driver_name;
        private List<String> bid_ID;

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public String getPost_author() {
            return post_author;
        }

        public void setPost_author(String post_author) {
            this.post_author = post_author;
        }

        public String getPost_date() {
            return post_date;
        }

        public void setPost_date(String post_date) {
            this.post_date = post_date;
        }

        public String getPost_date_gmt() {
            return post_date_gmt;
        }

        public void setPost_date_gmt(String post_date_gmt) {
            this.post_date_gmt = post_date_gmt;
        }

        public String getPost_content() {
            return post_content;
        }

        public void setPost_content(String post_content) {
            this.post_content = post_content;
        }

        public String getPost_title() {
            return post_title;
        }

        public void setPost_title(String post_title) {
            this.post_title = post_title;
        }

        public String getPost_excerpt() {
            return post_excerpt;
        }

        public void setPost_excerpt(String post_excerpt) {
            this.post_excerpt = post_excerpt;
        }

        public String getPost_status() {
            return post_status;
        }

        public void setPost_status(String post_status) {
            this.post_status = post_status;
        }

        public String getComment_status() {
            return comment_status;
        }

        public void setComment_status(String comment_status) {
            this.comment_status = comment_status;
        }

        public String getPing_status() {
            return ping_status;
        }

        public void setPing_status(String ping_status) {
            this.ping_status = ping_status;
        }

        public String getPost_password() {
            return post_password;
        }

        public void setPost_password(String post_password) {
            this.post_password = post_password;
        }

        public String getPost_name() {
            return post_name;
        }

        public void setPost_name(String post_name) {
            this.post_name = post_name;
        }

        public String getTo_ping() {
            return to_ping;
        }

        public void setTo_ping(String to_ping) {
            this.to_ping = to_ping;
        }

        public String getPinged() {
            return pinged;
        }

        public void setPinged(String pinged) {
            this.pinged = pinged;
        }

        public String getPost_modified() {
            return post_modified;
        }

        public void setPost_modified(String post_modified) {
            this.post_modified = post_modified;
        }

        public String getPost_modified_gmt() {
            return post_modified_gmt;
        }

        public void setPost_modified_gmt(String post_modified_gmt) {
            this.post_modified_gmt = post_modified_gmt;
        }

        public String getPost_content_filtered() {
            return post_content_filtered;
        }

        public void setPost_content_filtered(String post_content_filtered) {
            this.post_content_filtered = post_content_filtered;
        }

        public int getPost_parent() {
            return post_parent;
        }

        public void setPost_parent(int post_parent) {
            this.post_parent = post_parent;
        }

        public String getGuid() {
            return guid;
        }

        public void setGuid(String guid) {
            this.guid = guid;
        }

        public int getMenu_order() {
            return menu_order;
        }

        public void setMenu_order(int menu_order) {
            this.menu_order = menu_order;
        }

        public String getPost_type() {
            return post_type;
        }

        public void setPost_type(String post_type) {
            this.post_type = post_type;
        }

        public String getPost_mime_type() {
            return post_mime_type;
        }

        public void setPost_mime_type(String post_mime_type) {
            this.post_mime_type = post_mime_type;
        }

        public String getComment_count() {
            return comment_count;
        }

        public void setComment_count(String comment_count) {
            this.comment_count = comment_count;
        }

        public String getFilter() {
            return filter;
        }

        public void setFilter(String filter) {
            this.filter = filter;
        }

        public List<String> getLoad_ID() {
            return load_ID;
        }

        public void setLoad_ID(List<String> load_ID) {
            this.load_ID = load_ID;
        }

        public List<String> getQuotation_vehicle_number() {
            return quotation_vehicle_number;
        }

        public void setQuotation_vehicle_number(List<String> quotation_vehicle_number) {
            this.quotation_vehicle_number = quotation_vehicle_number;
        }

        public List<String> getLeft_count() {
            return left_count;
        }

        public void setLeft_count(List<String> left_count) {
            this.left_count = left_count;
        }

        public List<String> getInitiated_by() {
            return initiated_by;
        }

        public void setInitiated_by(List<String> initiated_by) {
            this.initiated_by = initiated_by;
        }

        public List<String> getGhost_transporter() {
            return ghost_transporter;
        }

        public void setGhost_transporter(List<String> ghost_transporter) {
            this.ghost_transporter = ghost_transporter;
        }

        public List<String> getFactory_details_drivers_mobile_number() {
            return factory_details_drivers_mobile_number;
        }

        public void setFactory_details_drivers_mobile_number(List<String> factory_details_drivers_mobile_number) {
            this.factory_details_drivers_mobile_number = factory_details_drivers_mobile_number;
        }

        public List<String> getFactory_details_driver_name() {
            return factory_details_driver_name;
        }

        public void setFactory_details_driver_name(List<String> factory_details_driver_name) {
            this.factory_details_driver_name = factory_details_driver_name;
        }

        public List<String> getBid_ID() {
            return bid_ID;
        }

        public void setBid_ID(List<String> bid_ID) {
            this.bid_ID = bid_ID;
        }
    }

    public static class IncomingQuotationsBean {
        /**
         * ID : 20294
         * post_author : 44
         * post_date : 2017-05-06 12:52:04
         * post_date_gmt : 2017-05-06 12:52:04
         * post_content :
         * post_title : Quote 20294
         * post_excerpt :
         * post_status : truck_pay_ok
         * comment_status : closed
         * ping_status : closed
         * post_password :
         * post_name : quote-20294
         * to_ping :
         * pinged :
         * post_modified : 2017-05-06 12:53:09
         * post_modified_gmt : 2017-05-06 12:53:09
         * post_content_filtered :
         * post_parent : 0
         * guid : http://nishabhati.com/?post_type=quotation&#038;p=20294
         * menu_order : 0
         * post_type : quotation
         * post_mime_type :
         * comment_count : 0
         * filter : raw
         * quoted_amount : ["950"]
         * load_ID : ["20284"]
         * quotation_vehicle_number : ["as26a1233"]
         * freight_to_market_ID : ["20292"]
         * floater_id : ["42"]
         * driver_number : ["9864098640"]
         * left_count : ["40"]
         * driver_name : ["dan"]
         * initiated_by : ["transporter"]
         * ghost_transporter : ["44"]
         */

        private int ID;
        private String post_author;
        private String post_date;
        private String post_date_gmt;
        private String post_content;
        private String post_title;
        private String post_excerpt;
        private String post_status;
        private String comment_status;
        private String ping_status;
        private String post_password;
        private String post_name;
        private String to_ping;
        private String pinged;
        private String post_modified;
        private String post_modified_gmt;
        private String post_content_filtered;
        private int post_parent;
        private String guid;
        private int menu_order;
        private String post_type;
        private String post_mime_type;
        private String comment_count;
        private String filter;
        private List<String> quoted_amount;
        private List<String> load_ID;
        private List<String> quotation_vehicle_number;
        private List<String> freight_to_market_ID;
        private List<String> floater_id;
        private List<String> driver_number;
        private List<String> left_count;
        private List<String> driver_name;
        private List<String> initiated_by;
        private List<String> ghost_transporter;

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public String getPost_author() {
            return post_author;
        }

        public void setPost_author(String post_author) {
            this.post_author = post_author;
        }

        public String getPost_date() {
            return post_date;
        }

        public void setPost_date(String post_date) {
            this.post_date = post_date;
        }

        public String getPost_date_gmt() {
            return post_date_gmt;
        }

        public void setPost_date_gmt(String post_date_gmt) {
            this.post_date_gmt = post_date_gmt;
        }

        public String getPost_content() {
            return post_content;
        }

        public void setPost_content(String post_content) {
            this.post_content = post_content;
        }

        public String getPost_title() {
            return post_title;
        }

        public void setPost_title(String post_title) {
            this.post_title = post_title;
        }

        public String getPost_excerpt() {
            return post_excerpt;
        }

        public void setPost_excerpt(String post_excerpt) {
            this.post_excerpt = post_excerpt;
        }

        public String getPost_status() {
            return post_status;
        }

        public void setPost_status(String post_status) {
            this.post_status = post_status;
        }

        public String getComment_status() {
            return comment_status;
        }

        public void setComment_status(String comment_status) {
            this.comment_status = comment_status;
        }

        public String getPing_status() {
            return ping_status;
        }

        public void setPing_status(String ping_status) {
            this.ping_status = ping_status;
        }

        public String getPost_password() {
            return post_password;
        }

        public void setPost_password(String post_password) {
            this.post_password = post_password;
        }

        public String getPost_name() {
            return post_name;
        }

        public void setPost_name(String post_name) {
            this.post_name = post_name;
        }

        public String getTo_ping() {
            return to_ping;
        }

        public void setTo_ping(String to_ping) {
            this.to_ping = to_ping;
        }

        public String getPinged() {
            return pinged;
        }

        public void setPinged(String pinged) {
            this.pinged = pinged;
        }

        public String getPost_modified() {
            return post_modified;
        }

        public void setPost_modified(String post_modified) {
            this.post_modified = post_modified;
        }

        public String getPost_modified_gmt() {
            return post_modified_gmt;
        }

        public void setPost_modified_gmt(String post_modified_gmt) {
            this.post_modified_gmt = post_modified_gmt;
        }

        public String getPost_content_filtered() {
            return post_content_filtered;
        }

        public void setPost_content_filtered(String post_content_filtered) {
            this.post_content_filtered = post_content_filtered;
        }

        public int getPost_parent() {
            return post_parent;
        }

        public void setPost_parent(int post_parent) {
            this.post_parent = post_parent;
        }

        public String getGuid() {
            return guid;
        }

        public void setGuid(String guid) {
            this.guid = guid;
        }

        public int getMenu_order() {
            return menu_order;
        }

        public void setMenu_order(int menu_order) {
            this.menu_order = menu_order;
        }

        public String getPost_type() {
            return post_type;
        }

        public void setPost_type(String post_type) {
            this.post_type = post_type;
        }

        public String getPost_mime_type() {
            return post_mime_type;
        }

        public void setPost_mime_type(String post_mime_type) {
            this.post_mime_type = post_mime_type;
        }

        public String getComment_count() {
            return comment_count;
        }

        public void setComment_count(String comment_count) {
            this.comment_count = comment_count;
        }

        public String getFilter() {
            return filter;
        }

        public void setFilter(String filter) {
            this.filter = filter;
        }

        public List<String> getQuoted_amount() {
            return quoted_amount;
        }

        public void setQuoted_amount(List<String> quoted_amount) {
            this.quoted_amount = quoted_amount;
        }

        public List<String> getLoad_ID() {
            return load_ID;
        }

        public void setLoad_ID(List<String> load_ID) {
            this.load_ID = load_ID;
        }

        public List<String> getQuotation_vehicle_number() {
            return quotation_vehicle_number;
        }

        public void setQuotation_vehicle_number(List<String> quotation_vehicle_number) {
            this.quotation_vehicle_number = quotation_vehicle_number;
        }

        public List<String> getFreight_to_market_ID() {
            return freight_to_market_ID;
        }

        public void setFreight_to_market_ID(List<String> freight_to_market_ID) {
            this.freight_to_market_ID = freight_to_market_ID;
        }

        public List<String> getFloater_id() {
            return floater_id;
        }

        public void setFloater_id(List<String> floater_id) {
            this.floater_id = floater_id;
        }

        public List<String> getDriver_number() {
            return driver_number;
        }

        public void setDriver_number(List<String> driver_number) {
            this.driver_number = driver_number;
        }

        public List<String> getLeft_count() {
            return left_count;
        }

        public void setLeft_count(List<String> left_count) {
            this.left_count = left_count;
        }

        public List<String> getDriver_name() {
            return driver_name;
        }

        public void setDriver_name(List<String> driver_name) {
            this.driver_name = driver_name;
        }

        public List<String> getInitiated_by() {
            return initiated_by;
        }

        public void setInitiated_by(List<String> initiated_by) {
            this.initiated_by = initiated_by;
        }

        public List<String> getGhost_transporter() {
            return ghost_transporter;
        }

        public void setGhost_transporter(List<String> ghost_transporter) {
            this.ghost_transporter = ghost_transporter;
        }
    }
}
