package com.youlorry.ap.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by stark on 1/11/17.
 */
public class MystuffFullDetail extends Activity implements View.OnClickListener {

    Context con;
    String meta_keys,id_for_selected_stuff;
    ImageView cancel_btn;
    Button cancel_my_stuff_btn;
    TextView header_text,vehicle_number_heading, pickup_city_heading, drop_city_heading, material_type_heading, truck_type_heading, weight_heading, pickup_date_heading, exp_val_heading, pickup_point_heading, drop_point_heading, advance_heading, pickup_time_heading, vehicle_number, pickup_city, drop_city, material_type, truck_type, weight, pickup_date, exp_val, pickup_point, drop_point, advance, pickup_time;
    TextView line_under_vehicle_number, line_under_pickup_city, line_under_drop_city, line_under_material_type, line_under_truck_type, line_under_weight, line_under_pickup_date, line_under_expected_value, line_under_pickup_point, line_under_drop_point, line_under_advance, line_under_pickup_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_stuff_full_detail);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        YoulorrySession session = new YoulorrySession(this);

        if (session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0) {

            Intent in = new Intent(this, LoginActivity.class);
            startActivity(in);

        }

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        if(session.getrole().equals("transporter")){
            getWindow().setLayout((int) (width*0.9),(int)(height*0.370));
        }else if(session.getrole().equals("customer")){
            getWindow().setLayout((int) (width*0.9),(int)(height*0.5));
        }
        con = this;
        Bundle bundle = getIntent().getExtras();
        meta_keys = bundle.getString("meta_keys");
        id_for_selected_stuff = bundle.getString("id_for_selected_stuff");

        cancel_btn = (ImageView) findViewById(R.id.top_cancel_btn);
        header_text = (TextView) findViewById(R.id.header_text);
        vehicle_number_heading = (TextView) findViewById(R.id.vehicle_number_inmodalview);
        pickup_city_heading = (TextView) findViewById(R.id.pickup_city_inmodalview);
        drop_city_heading = (TextView) findViewById(R.id.drop_city_inmodalview);
        material_type_heading = (TextView) findViewById(R.id.material_type_inmodalview);
        truck_type_heading = (TextView) findViewById(R.id.truck_type_inmodalview);
        weight_heading = (TextView) findViewById(R.id.weight_inmodalview);
        pickup_date_heading = (TextView) findViewById(R.id.pickup_date_inmodalview);
        exp_val_heading = (TextView) findViewById(R.id.expected_value_inmodalview);
        pickup_point_heading = (TextView) findViewById(R.id.pickup_point_inmodalview);
        drop_point_heading = (TextView) findViewById(R.id.drop_point_inmodalview);
        advance_heading = (TextView) findViewById(R.id.advance_inmodalview);
        pickup_time_heading = (TextView) findViewById(R.id.pickup_time_inmodalview);

        vehicle_number = (TextView) findViewById(R.id.q_vehicle_number_in_modal_view);
        pickup_city = (TextView) findViewById(R.id.q_pickup_city_in_modal_view);
        drop_city = (TextView) findViewById(R.id.q_drop_city_in_modal_view);
        material_type = (TextView) findViewById(R.id.q_material_type_in_modal_view);
        truck_type = (TextView) findViewById(R.id.q_truck_type_in_modal_view);
        weight = (TextView) findViewById(R.id.q_weight_in_modal_view);
        pickup_date = (TextView) findViewById(R.id.q_pickup_date_in_modal_view);
        exp_val = (TextView) findViewById(R.id.q_expected_value_in_modal_view);
        pickup_point = (TextView) findViewById(R.id.q_pickup_point_in_modal_view);
        drop_point = (TextView) findViewById(R.id.q_drop_point_in_modal_view);
        advance = (TextView) findViewById(R.id.q_advance_in_modal_view);
        pickup_time = (TextView) findViewById(R.id.q_pickup_time_in_modal_view);

        line_under_vehicle_number = (TextView) findViewById(R.id.line_under_vehicle_number);
        line_under_pickup_city = (TextView) findViewById(R.id.line_under_pickup_city);
        line_under_drop_city = (TextView) findViewById(R.id.line_under_drop_city);
        line_under_material_type = (TextView) findViewById(R.id.line_under_material_type);
        line_under_truck_type = (TextView) findViewById(R.id.line_under_truck_type);
        line_under_weight = (TextView) findViewById(R.id.line_under_weight);
        line_under_pickup_date = (TextView) findViewById(R.id.line_under_pickup_date);
        line_under_expected_value = (TextView) findViewById(R.id.line_under_expected_value);
        line_under_pickup_point = (TextView) findViewById(R.id.line_under_pickup_point);
        line_under_drop_point = (TextView) findViewById(R.id.line_under_drop_point);
        line_under_advance = (TextView) findViewById(R.id.line_under_advance);
        line_under_pickup_time = (TextView) findViewById(R.id.line_under_pickup_time);
        cancel_my_stuff_btn = (Button) findViewById(R.id.cancel_my_stuff_btn);

        cancel_my_stuff_btn.setOnClickListener(this);
        cancel_btn.setOnClickListener(this);
        new FetchMyStuffDetail().execute();
    }

    class FetchMyStuffDetail extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;
        String str;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            return meta_keys.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                    JSONObject jsonObject = new JSONObject(s);

                    if (new YoulorrySession(con).getrole().equals("transporter")) {

                        String source_city_jsondata = jsonObject.getString("source_city");
                        String source_city_from_json = source_city_jsondata.substring(2, source_city_jsondata.toString().length() - 2);

                        String destination_city_jsondata = jsonObject.getString("destination_city");
                        String destination_city_from_json = destination_city_jsondata.substring(2, destination_city_jsondata.toString().length() - 2);

                        String expected_frieght_value_jsondata = jsonObject.getString("expected_frieght_value");
                        String expected_frieght_value_from_json = expected_frieght_value_jsondata.substring(2, expected_frieght_value_jsondata.toString().length() - 2);

                        String truck_type_jsondata = jsonObject.getString("truck_type");
                        String truck_type_from_json = truck_type_jsondata.substring(2, truck_type_jsondata.toString().length() - 2);

                        String date_jsondata = jsonObject.getString("date");
                        String date_from_json = date_jsondata.substring(2, date_jsondata.toString().length() - 2);

                        String weight_capacity_jsondata = jsonObject.getString("weight_capacity");
                        String weight_capacity_from_json = weight_capacity_jsondata.substring(2, weight_capacity_jsondata.toString().length() - 2);

                        String vehicle_number_jsondata = jsonObject.getString("vehicle_number");
                        String vehicle_number_from_json = vehicle_number_jsondata.substring(2, vehicle_number_jsondata.toString().length() - 2);

                        String replaced_truck_type = truck_type_from_json.toString().replace("\\","");
                        vehicle_number.setText(vehicle_number_from_json);
                        pickup_city.setText(source_city_from_json);
                        drop_city.setText(destination_city_from_json);
                        truck_type.setText(replaced_truck_type);
                        weight.setText(weight_capacity_from_json+" MT");
                        pickup_date.setText(date_from_json);
                        exp_val.setText("₹ "+expected_frieght_value_from_json);

                        header_text.setText("My Trucks Detail");
                        vehicle_number_heading.setVisibility(View.VISIBLE);
                        pickup_city_heading.setVisibility(View.VISIBLE);
                        pickup_city_heading.setText("From ");
                        drop_city_heading.setVisibility(View.VISIBLE);
                        drop_city_heading.setText("To ");
                        material_type_heading.setVisibility(View.GONE);
                        truck_type_heading.setVisibility(View.VISIBLE);
                        weight_heading.setVisibility(View.VISIBLE);
                        weight_heading.setText("Capacity ");
                        pickup_date_heading.setVisibility(View.VISIBLE);
                        exp_val_heading.setVisibility(View.VISIBLE);
                        exp_val_heading.setText("Amount (Rs) ");
                        pickup_point_heading.setVisibility(View.GONE);
                        drop_point_heading.setVisibility(View.GONE);
                        advance_heading.setVisibility(View.GONE);
                        pickup_time_heading.setVisibility(View.GONE);
                        pickup_date_heading.setText("Available Date ");

                        vehicle_number.setVisibility(View.VISIBLE);
                        pickup_city.setVisibility(View.VISIBLE);
                        drop_city.setVisibility(View.VISIBLE);
                        material_type.setVisibility(View.GONE);
                        truck_type.setVisibility(View.VISIBLE);
                        weight.setVisibility(View.VISIBLE);
                        pickup_date.setVisibility(View.VISIBLE);
                        exp_val.setVisibility(View.VISIBLE);
                        pickup_point.setVisibility(View.GONE);
                        drop_point.setVisibility(View.GONE);
                        advance.setVisibility(View.GONE);
                        pickup_time.setVisibility(View.GONE);

                        line_under_vehicle_number.setVisibility(View.VISIBLE);
                        line_under_pickup_city.setVisibility(View.VISIBLE);
                        line_under_drop_city.setVisibility(View.VISIBLE);
                        line_under_material_type.setVisibility(View.GONE);
                        line_under_truck_type.setVisibility(View.VISIBLE);
                        line_under_weight.setVisibility(View.VISIBLE);
                        line_under_pickup_date.setVisibility(View.VISIBLE);
                        line_under_expected_value.setVisibility(View.VISIBLE);
                        line_under_pickup_point.setVisibility(View.GONE);
                        line_under_drop_point.setVisibility(View.GONE);
                        line_under_advance.setVisibility(View.GONE);
                        line_under_pickup_time.setVisibility(View.GONE);


                    } else if (new YoulorrySession(con).getrole().equals("customer")) {

                        String load_source_city_jsondata = jsonObject.getString("load_source_city");
                        String load_source_city_from_json = load_source_city_jsondata.substring(2, load_source_city_jsondata.toString().length() - 2);

                        String load_destination_city_jsondata = jsonObject.getString("load_destination_city");
                        String load_destination_city_from_json = load_destination_city_jsondata.substring(2, load_destination_city_jsondata.toString().length() - 2);

                        String load_expected_frieght_value_jsondata = jsonObject.getString("load_expected_frieght_value");
                        String load_expected_frieght_value_from_json = load_expected_frieght_value_jsondata.substring(2, load_expected_frieght_value_jsondata.toString().length() - 2);

                        String load_material_jsondata = jsonObject.getString("load_material");
                        String load_material_from_json = load_material_jsondata.substring(2, load_material_jsondata.toString().length() - 2);

                        String load_truck_type_jsondata = jsonObject.getString("load_truck_type");
                        String load_truck_type_from_json = load_truck_type_jsondata.substring(2, load_truck_type_jsondata.toString().length() - 2);

                        String load_date_jsondata = jsonObject.getString("load_date");
                        String load_date_from_json = load_date_jsondata.substring(2, load_date_jsondata.toString().length() - 2);

                        String load_time_jsondata = jsonObject.getString("load_time");
                        String load_time_from_json = load_time_jsondata.substring(2, load_time_jsondata.toString().length() - 2);

                        String load_weight_capacity_jsondata = jsonObject.getString("load_weight_capacity");
                        String load_weight_capacity_from_json = load_weight_capacity_jsondata.substring(2, load_weight_capacity_jsondata.toString().length() - 2);

                        String load_advance_in_percentage_jsondata = jsonObject.getString("load_advance_in_percentage");
                        String load_advance_in_percentage_from_json = load_advance_in_percentage_jsondata.substring(2, load_advance_in_percentage_jsondata.toString().length() - 2);

                        String left_count_jsondata = jsonObject.getString("left_count");
                        String left_count_from_json = left_count_jsondata.substring(2, left_count_jsondata.toString().length() - 2);

                        String load_pickup_point_from_json,load_drop_point_from_json;
                        if(jsonObject.has("load_pickup_point")){
                            String load_pickup_point_jsondata = jsonObject.getString("load_pickup_point");
                            load_pickup_point_from_json = load_pickup_point_jsondata.substring(2, load_pickup_point_jsondata.toString().length() - 2);
                        } else {
                            load_pickup_point_from_json = "Inside City";
                        }

                        if(jsonObject.has("load_drop_point")){
                            String load_drop_point_jsondata = jsonObject.getString("load_drop_point");
                            load_drop_point_from_json = load_drop_point_jsondata.substring(2, load_drop_point_jsondata.toString().length() - 2);
                        } else {
                            load_drop_point_from_json = "Inside City";
                        }

                        int adv_res = Integer.parseInt(load_expected_frieght_value_from_json)*(Integer.parseInt(load_advance_in_percentage_from_json))/100;

                        String replaced_truck_type = load_truck_type_from_json.toString().replace("\\","");
                        pickup_city.setText(load_source_city_from_json);
                        drop_city.setText(load_destination_city_from_json);
                        material_type.setText(load_material_from_json);
                        pickup_time.setText(load_time_from_json);
                        advance.setText("₹ "+adv_res);
                        truck_type.setText(replaced_truck_type);
                        weight.setText(left_count_from_json+"/"+load_weight_capacity_from_json+" MT");
                        pickup_date.setText(load_date_from_json);
                        exp_val.setText("₹ "+load_expected_frieght_value_from_json);
                        pickup_point.setText(load_pickup_point_from_json);
                        drop_point.setText(load_drop_point_from_json);

                        header_text.setText("My Loads Detail");
                        vehicle_number_heading.setVisibility(View.GONE);
                        pickup_city_heading.setVisibility(View.VISIBLE);
                        drop_city_heading.setVisibility(View.VISIBLE);
                        truck_type_heading.setVisibility(View.VISIBLE);
                        weight_heading.setVisibility(View.VISIBLE);
                        pickup_date_heading.setVisibility(View.VISIBLE);
                        exp_val_heading.setVisibility(View.VISIBLE);
                        exp_val_heading.setText("Amount (Rs) ");
                        material_type_heading.setVisibility(View.VISIBLE);
                        pickup_point_heading.setVisibility(View.VISIBLE);
                        drop_point_heading.setVisibility(View.VISIBLE);
                        advance_heading.setVisibility(View.VISIBLE);
                        pickup_time_heading.setVisibility(View.VISIBLE);

                        vehicle_number.setVisibility(View.GONE);
                        pickup_city.setVisibility(View.VISIBLE);
                        drop_city.setVisibility(View.VISIBLE);
                        truck_type.setVisibility(View.VISIBLE);
                        weight.setVisibility(View.VISIBLE);
                        pickup_date.setVisibility(View.VISIBLE);
                        exp_val.setVisibility(View.VISIBLE);
                        material_type.setVisibility(View.VISIBLE);
                        pickup_point.setVisibility(View.VISIBLE);
                        drop_point.setVisibility(View.VISIBLE);
                        advance.setVisibility(View.VISIBLE);
                        pickup_time.setVisibility(View.VISIBLE);

                        line_under_vehicle_number.setVisibility(View.GONE);
                        line_under_pickup_city.setVisibility(View.VISIBLE);
                        line_under_drop_city.setVisibility(View.VISIBLE);
                        line_under_truck_type.setVisibility(View.VISIBLE);
                        line_under_weight.setVisibility(View.VISIBLE);
                        line_under_pickup_date.setVisibility(View.VISIBLE);
                        line_under_expected_value.setVisibility(View.VISIBLE);
                        line_under_material_type.setVisibility(View.VISIBLE);
                        line_under_pickup_point.setVisibility(View.VISIBLE);
                        line_under_drop_point.setVisibility(View.VISIBLE);
                        line_under_advance.setVisibility(View.VISIBLE);
                        line_under_pickup_time.setVisibility(View.VISIBLE);

                    }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            dialogue.dismiss();
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.top_cancel_btn){
            finish();
        }

        if(view.getId() == R.id.cancel_my_stuff_btn){

            Log.d("id_for_selected_stuff",id_for_selected_stuff);
            new AlertDialog.Builder(this)
                    .setTitle("Alert Message")
                    .setMessage("Want to delete!")
                    .setIcon(R.drawable.ic_warning_black_24dp)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            finish();
                            StringBuilder sb = new StringBuilder();

                            YoulorrySession session12 = new YoulorrySession(con);
                            String username = session12.getusername();
                            String password = session12.getpass_word();
                            String userid = session12.getuser_id();
                            String role = session12.getrole();

                            URL urlObj = null;
                            try {
                                if (role.equals("transporter")) {
                                    urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-get-my-available-trucks-for-given-load/");
                                } else if (role.equals("customer")) {
                                    urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-get-my-available-loads-for-given-truck/");
                                }
                                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                                urlConnection.setDoOutput(true);
                                urlConnection.setDoInput(true);
                                urlConnection.setUseCaches(false);
                                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                                //  urlConnection.setRequestProperty("Accept", "application/json");
                                urlConnection.setRequestMethod("POST");
                                urlConnection.connect();

                                JSONObject cred = new JSONObject();

                                cred.put("username", username);
                                cred.put("password", password);
                                cred.put("userid", userid);
                                cred.put("role", role);
                                cred.put("key_to_delete", id_for_selected_stuff);

                                OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                                wr.write(cred.toString());
                                wr.flush();
                                wr.close();

                                //display what returns the POST request

                                int HttpResult = urlConnection.getResponseCode();
                                if (HttpResult == HttpURLConnection.HTTP_OK) {
                                    BufferedReader br = new BufferedReader(
                                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                                    String line = null;
                                    while ((line = br.readLine()) != null) {
                                        sb.append(line);
                                    }
                                    br.close();

                                }

                            } catch (ProtocolException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            Log.d("return-value_for_my_stuff_cancellation", sb.toString());

                            if (sb.toString().equals("ok")) {
                                Toast.makeText(con, "Success", Toast.LENGTH_LONG).show();

                                new Handler().postDelayed(new Runnable() {
                                    public void run() {
                                        startActivity(new Intent(con, MainActivity.class));
                                        finish();
                                    }
                                }, 500);
//                         startActivity(new Intent(this, MainActivity.class));
                            } else {

                                Toast.makeText(con, "Failed", Toast.LENGTH_LONG).show();

                                new Handler().postDelayed(new Runnable() {
                                    public void run() {
                                        startActivity(new Intent(con, MainActivity.class));
                                        finish();
                                    }
                                }, 500);
                            }

                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                    .show();
/*
            pickup_city
            drop_city
            material_type
            pickup_time
            advance
            truck_type
            weight
            pickup_date
            exp_val
            pickup_point
            drop_point


            vehicle_number
            pickup_city
            drop_city
            truck_type
            weight
            pickup_date
            exp_val
*/
        }
    }


}
