package com.youlorry.ap.myapplication.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.youlorry.ap.myapplication.BidLayout;
import com.youlorry.ap.myapplication.ConfirmedBidLayout;
import com.youlorry.ap.myapplication.Model.Bidding_Model;
import com.youlorry.ap.myapplication.Model.Confirmed_Bid_Model;
import com.youlorry.ap.myapplication.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tycho on 5/5/2017.
 */

public class Confirmend_bid_Adapter extends  RecyclerView.Adapter<Confirmend_bid_Adapter.Viewholder> {
    Context context;
    Confirmend_bid_Adapter confirmend_bid_adapter;
       List<Confirmed_Bid_Model> confirmed_bid_modelList;
    private List<Bidding_Model.MetaKeysBean> metaKeysBeanList=new ArrayList<>();
    int currentposition=0;
    ConfirmedBidLayout confirmedBidLayout;

    public Confirmend_bid_Adapter(Context context, List<Confirmed_Bid_Model> confirmend_bid_adapter, ConfirmedBidLayout confirmedBidLayout) {
        this.context = context;
        this.confirmed_bid_modelList = confirmend_bid_adapter;
        this.confirmedBidLayout=confirmedBidLayout;
    }
    @Override
    public Confirmend_bid_Adapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_confirmed_bid, parent, false);
        Confirmend_bid_Adapter.Viewholder viewHolder=new Confirmend_bid_Adapter.Viewholder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Confirmend_bid_Adapter.Viewholder holder, int position) {
        Confirmed_Bid_Model confirmed_bid_model=confirmed_bid_modelList.get(position);
        if(confirmed_bid_model!=null && (confirmed_bid_model.getMeta_keys()!=null))
        {
            List<String> source_city = confirmed_bid_model.getMeta_keys().getLoad_source_city();
           String sourcecity= source_city.get(0).toString();
            if(source_city!=null)
            holder.from_id.setText(sourcecity);

            List<String> destination_city = confirmed_bid_model.getMeta_keys().getLoad_destination_city();
            String destination= destination_city.get(0).toString();
            if(destination!=null)
            holder.to_id.setText(destination);

            List<String> load_date = confirmed_bid_model.getMeta_keys().getLoad_date();
            String date= load_date.get(0).toString();
            if(date!=null)
                holder.scheduled_date_id.setText(date);

            List<String> load_material = confirmed_bid_model.getMeta_keys().getLoad_material();
            String loadmat= (load_material.get(0).toString());
            if(loadmat!=null)
            holder.material_id.setText(loadmat);

            if(confirmed_bid_model.getBid_meta_keys()!=null) {
                List<String> load_capacity = confirmed_bid_model.getBid_meta_keys().getBid_weight_capacity();
                List<String> ratevalue=confirmed_bid_model.getBid_meta_keys().getBid_rate();
                List<String> bid_value=confirmed_bid_model.getBid_meta_keys().getBid_value();
                String rate=ratevalue.get(0).toString();
                String capacity= (load_capacity.get(0).toString());
                String bidvalue=bid_value.get(0).toString();
                if(loadmat!=null) {
                    if (capacity != null)
                        holder.material_id.setText((Html.fromHtml("<b><font color=\"#52869c\" size=\"6\"  >" + loadmat+" (" + "</font></b>" +
                                "<font color=\"#52869c\">" + "&nbsp;" + capacity + "&nbsp;" + "MT )" + "</font>")));
                }
                if(ratevalue!=null)
                {
                    if (rate != null)
                        holder.exp_val_MM.setText((Html.fromHtml("<b><font color=\"#52869c\" size=\"6\"  >" +"Rate: ₹" + "</font></b>" +
                                "<font color=\"#52869c\">" + "&nbsp;" + rate + "&nbsp;" + "</font>")));

                }
                if(bid_value!=null)
                {
                    if (bidvalue != null)
                        holder.your_val.setText((Html.fromHtml("<b><font color=\"#52869c\" size=\"6\"  >" +"TA : ₹" + "</font></b>" +
                                "<font color=\"#52869c\">" + "&nbsp;" + bidvalue + "&nbsp;" + "</font>")));

                }

            }
            else {
                List<String> load_capacity = confirmed_bid_model.getMeta_keys().getLoad_weight_capacity();
                String capacity= (load_capacity.get(0).toString());
                List<String> ratevalue=confirmed_bid_model.getMeta_keys().getLoad_rate_per_ton();
                String rate=ratevalue.get(0).toString();
                List<String> load_total_value=confirmed_bid_model.getMeta_keys().getLoad_total_value();
                String total_value=load_total_value.get(0).toString();

                if(loadmat!=null) {
                    if (capacity != null)
                        holder.material_id.setText((Html.fromHtml("<b><font color=\"#52869c\" size=\"6\"  >" +loadmat+ "(" + "</font></b>" +
                                "<font color=\"#52869c\">" + "&nbsp;" + capacity + "&nbsp;" + "MT )" + "</font>")));

                }

                if(ratevalue!=null)
                {
                    if (rate != null)
                        holder.exp_val_MM.setText((Html.fromHtml("<b><font color=\"#52869c\" size=\"6\"  >" +"Rate: ₹" + "</font></b>" +
                                "<font color=\"#52869c\">" + "&nbsp;" + rate + "&nbsp;" + "</font>")));

                }
                if(load_total_value!=null)
                {
                    if (total_value != null)
                        holder.your_val.setText((Html.fromHtml("<b><font color=\"#52869c\" size=\"6\"  >" +"Rate: ₹" + "</font></b>" +
                                "<font color=\"#52869c\">" + "&nbsp;" + total_value + "&nbsp;" + "</font>")));

                }
            }




        }
    }

    @Override
    public int getItemCount() {
        return confirmed_bid_modelList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        Button details_btn;
        TextView weeks1,days1,hours1,minutes1,seconds1,from_id,to_id,quoted_amt_id,material_id,expairyTime,scheduled_date_id;
        TextView lowest_bid,your_bid,exp_val_MM,your_val;

        public Viewholder(View itemView) {
            super(itemView);

            expairyTime=(TextView) itemView.findViewById(R.id.expairyTime);
            your_bid=(TextView) itemView.findViewById(R.id.your_bid);
            details_btn = (Button) itemView.findViewById(R.id.confirmed_bid_details_btn);
            from_id=(TextView) itemView.findViewById(R.id.from_id);
            to_id=(TextView) itemView.findViewById(R.id.to_id);
            quoted_amt_id=(TextView) itemView.findViewById(R.id.quoted_amt_id);
            scheduled_date_id=(TextView) itemView.findViewById(R.id.scheduled_date_id);
            material_id=(TextView) itemView.findViewById(R.id.material_id);
            your_val=(TextView) itemView.findViewById(R.id.your_val);
            exp_val_MM=(TextView) itemView.findViewById(R.id.exp_val_id);
            details_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int factory_load_ID;
                    if(confirmed_bid_modelList.get(getAdapterPosition()).getBid_meta_keys()!=null) {
                        factory_load_ID = confirmed_bid_modelList.get(getAdapterPosition()).getBid_meta_keys().getID();
                    }
                    else
                        factory_load_ID=0;
                    confirmedBidLayout.getDetailsPage(confirmed_bid_modelList.get(getAdapterPosition()).getID(),factory_load_ID);
                }
            });

          //  confirmedBidLayout.getDetailsPage(confirmed_bid_modelList.get(getAdapterPosition()).getID());
        }
    }
}
