package com.youlorry.ap.myapplication.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.youlorry.ap.myapplication.DetailsPageforConfirmedBid;
import com.youlorry.ap.myapplication.Model.Details_Page_confirmBid_Model;
import com.youlorry.ap.myapplication.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tycho on 5/6/2017.
 */

public class Incoming_Adapter  extends RecyclerView.Adapter<Incoming_Adapter.Viewholder>  {
    Context context;
    Incoming_Adapter incoming_adapter;
   Details_Page_confirmBid_Model details_page_confirmBid_model;
    List<Details_Page_confirmBid_Model.IncomingQuotationsBean> incomingQuotationsBeanList;

    public Incoming_Adapter(DetailsPageforConfirmedBid detailsPageforConfirmedBid, List<Details_Page_confirmBid_Model.IncomingQuotationsBean> incomingQuotationsBeanList) {
        this.context=detailsPageforConfirmedBid;
        this.incomingQuotationsBeanList=incomingQuotationsBeanList;

    }
    // private final List<Details_Page_confirmBid_Model.IncomingQuotationsBean> bidding_models;

    @Override
    public Incoming_Adapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.incoming_adapter_layout, parent, false);
        Incoming_Adapter.Viewholder viewHolder=new Incoming_Adapter.Viewholder(v);
        return viewHolder;
    }

    public void delete(int position) { //removes the row
        incomingQuotationsBeanList.remove(position);
        notifyItemRemoved(position);
    }



    @Override
    public void onBindViewHolder(Viewholder holder, int position) {
     Details_Page_confirmBid_Model.IncomingQuotationsBean incomingQuotationsBean=incomingQuotationsBeanList.get(position);
        if(incomingQuotationsBean!=null )
        {
            String accepted=incomingQuotationsBean.getPost_status();
            if(!accepted.equalsIgnoreCase("truck_pay_ok")) {

                List<String> leftCount = incomingQuotationsBean.getLeft_count();
                if (leftCount.size() > 0)
                    holder.textView_Weight_Capacity.setText((leftCount.get(0).toString()));

                List<String> quoted_amount = incomingQuotationsBean.getQuoted_amount();
                if (quoted_amount.size() > 0)
                    holder.textView_Rate.setText((quoted_amount.get(0).toString()));

            }
        }
    }


    @Override
    public int getItemCount() {
        return incomingQuotationsBeanList.size();
       // return details_page_confirmBid_model.getIncoming_quotations();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        Button button_accept;
//        TextView  days1, hours1, minutes1, seconds1, from_id, to_id, quoted_amt_id, material_id, expairyTime, weight_id;
        TextView textView_Rate,textView_Weight_Capacity;

        public Viewholder(View itemView) {
            super(itemView);
            textView_Weight_Capacity=(TextView) itemView.findViewById(R.id.textView_weight);
            textView_Rate=(TextView) itemView.findViewById(R.id.textView_rate);
            button_accept=(Button) itemView.findViewById(R.id.button_accept);
            button_accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((DetailsPageforConfirmedBid)context).acceptincoming_Quataion(incomingQuotationsBeanList.
                            get(getAdapterPosition()).getID(),incomingQuotationsBeanList.get(getAdapterPosition()).getLoad_ID());

                    int newPosition = getAdapterPosition();
                    String accepted=incomingQuotationsBeanList.get(newPosition).getPost_status();
                    if(accepted.equalsIgnoreCase("truck_pay_ok"))
                    {
                        incomingQuotationsBeanList.remove(newPosition);
                        notifyItemRemoved(newPosition);
//                        incoming_adapter.notifyDataSetChanged();
                        notifyItemRangeChanged(newPosition, incomingQuotationsBeanList.size());

                    }
                }
            });
        }
    }
}
