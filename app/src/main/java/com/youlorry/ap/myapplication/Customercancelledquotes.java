package com.youlorry.ap.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Arpit Prajapati on 12/29/16.
 */
public class Customercancelledquotes extends Fragment {

    ListView listView;
    TextView t1;
    Context con;
    CustomAdapterforquotationview2 adapter;
    public ArrayList<ListmodalforQuotationsforCancelled> CustomListViewValuesArr3 = new ArrayList<ListmodalforQuotationsforCancelled>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.customer_cancelled_quotes,container,false);

        con = getActivity();

        YoulorrySession session = new YoulorrySession(con);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(con , LoginActivity.class);
            startActivity(in);

        }

        listView = (ListView) v.findViewById(R.id.c_cancelled_quotes_listview);
        t1 = (TextView) v.findViewById(R.id.no_matching_data_found);
        new TheTask2().execute();
        return v;
    }

    class TheTask2 extends AsyncTask<Void, Void, String> {


        ProgressDialog dialogue;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session = new YoulorrySession(con);
            String username = session.getusername();
            String password = session.getpass_word();
            String userid = session.getuser_id();
            String role = session.getrole();

            StringBuilder sb = new StringBuilder();
            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-fetch-my-cancelled-quotations/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                //  urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);

                OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                wr.flush();
                wr.close();

                //display what returns the POST request

                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                    br.close();

//                    Toast.makeText(con, sb.toString() , Toast.LENGTH_LONG).show();

                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            YoulorrySession ss = new YoulorrySession(con);
            ss.setCancelledQuotations(sb.toString());

            return sb.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.d("cancelled quotation", s.toString());
            try {

                if (s.equals("error_no_cancelled_quotations_of_yours")) {
                    listView.setVisibility(View.GONE);
                    t1.setVisibility(View.VISIBLE);
                    t1.setText("No Quotations !!!");
                } else {

//                Toast.makeText(con, s.toString(), Toast.LENGTH_SHORT).show();
                Log.d("cancelled quotation", s.toString());

                JSONArray jsonArray = new JSONArray(s);
                String load_source_city_jsondata = "", load_source_city_from_json = "", load_destination_city_jsondata = "", load_destination_city_from_json = "", load_date_jsondata = "", load_date_from_json = "";

                Resources res = getResources();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);


                    String id = jsonObject.optString("ID").toString();
                    String post_status1 = jsonObject.optString("post_status").toString();

                    String meta_keys_jsondata = jsonObject.optString("meta_keys").toString();
                    String load_details_jsondata = jsonObject.optString("load_details").toString();

                    JSONObject jobj = new JSONObject(meta_keys_jsondata);

                    String quoted_amount_jsondata = jobj.getString("quoted_amount");
                    String quoted_amount_from_json = quoted_amount_jsondata.substring(2, quoted_amount_jsondata.toString().length() - 2);

                    String lp_a_jsondata = jobj.getString("load_loading_address");
                    String lp_a_from_json = lp_a_jsondata.substring(2, lp_a_jsondata.toString().length() - 2);

                    String unlp_a_jsondata = jobj.getString("load_unloading_address");
                    String unlp_a_from_json = unlp_a_jsondata.substring(2, unlp_a_jsondata.toString().length() - 2);

                    if (jsonObject.has("load_details")) {

                        JSONObject jobj1 = new JSONObject(load_details_jsondata);

                        load_source_city_jsondata = jobj1.getString("load_source_city");
                        load_source_city_from_json = load_source_city_jsondata.substring(2, load_source_city_jsondata.toString().length() - 2);

                        load_destination_city_jsondata = jobj1.getString("load_destination_city");
                        load_destination_city_from_json = load_destination_city_jsondata.substring(2, load_destination_city_jsondata.toString().length() - 2);

                        load_date_jsondata = jobj1.getString("load_date");
                        load_date_from_json = load_date_jsondata.substring(2, load_date_jsondata.toString().length() - 2);
                    } else {

                        load_date_from_json = "NA";
                        load_source_city_from_json = "NA";
                        load_destination_city_from_json = "NA";

                    }

                    JSONObject jobj2 = new JSONObject(load_details_jsondata);

                    String truck_vehicle_number_from_json = "", left_count_from_json = "";

                    if (jobj.has("left_count")) {
                        String left_count_jsondata = jobj.getString("left_count");
                        left_count_from_json = left_count_jsondata.substring(2, left_count_jsondata.toString().length() - 2);
                    } else {
                        left_count_from_json = "NA";
                    }

                    if (jsonObject.has("truck_details")) {

                        String truck_details_jsondata = jsonObject.optString("truck_details").toString();
                        Log.d("truck_datails", truck_details_jsondata);
                        if (truck_details_jsondata.equals("false")) {
                            truck_vehicle_number_from_json = "NA";
                        } else {
                            JSONObject jobj1 = new JSONObject(truck_details_jsondata);

                            if (jobj1.has("vehicle_number")) {
                                String truck_vehicle_number_jsondata = jobj1.getString("vehicle_number");
                                truck_vehicle_number_from_json = truck_vehicle_number_jsondata.substring(2, truck_vehicle_number_jsondata.toString().length() - 2);

                            } else {
                                truck_vehicle_number_from_json = "NA";
                            }
                        }
                    } else {

                        if (jobj.has("quotation_vehicle_number")) {
                            String truck_vehicle_number_jsondata = jobj.getString("quotation_vehicle_number");
                            truck_vehicle_number_from_json = truck_vehicle_number_jsondata.substring(2, truck_vehicle_number_jsondata.toString().length() - 2);

                        } else {
                            truck_vehicle_number_from_json = "NA";
                        }
                    }


                    String new_part_amount_jsondata = "", new_full_amount_jsondata = "", new_advance_amount_jsondata = "";

                    if (jsonObject.has("new_data")) {
                        String new_data_jsondata = jsonObject.optString("new_data").toString();
                        Log.d("new_data", new_data_jsondata);

                        JSONObject jobj22 = new JSONObject(new_data_jsondata);

                        if (jobj22.has("new_part_amount")) {
                            new_part_amount_jsondata = jobj22.getString("new_part_amount");
                        } else {
                            new_part_amount_jsondata = "NA";
                        }

                        if (jobj22.has("new_full_amount")) {
                            new_full_amount_jsondata = jobj22.getString("new_full_amount");

                        } else {
                            new_full_amount_jsondata = "NA";
                        }

                        if (jobj22.has("new_advance_amount")) {
                            new_advance_amount_jsondata = jobj22.getString("new_advance_amount");
                        } else {
                            new_advance_amount_jsondata = "NA";
                        }

                    }

                    String load_material_from_json = "";

                    if (jsonObject.has("load_details")) {

                        String load_details_json_data = jsonObject.optString("load_details").toString();
                        Log.d("load_details", load_details_jsondata);
                        JSONObject jobj3 = new JSONObject(load_details_jsondata);

                        if (jobj3.has("load_material")) {
                            String load_material_jsondata = jobj3.getString("load_material");
                            load_material_from_json = load_material_jsondata.substring(2, load_material_jsondata.toString().length() - 2);

                        } else {
                            load_material_from_json = "NA";
                        }
                    }

                    adapter = new CustomAdapterforquotationview2(con, CustomListViewValuesArr3, res);
                    listView.setAdapter(adapter);

                    final ListmodalforQuotationsforCancelled sched1 = new ListmodalforQuotationsforCancelled();
                    String[] lp_a_split = load_source_city_from_json.split(",");
                    String[] unlp_a_split = load_destination_city_from_json.split(",");
                    /******* Firstly take data in model object ******/

                    sched1.setVehicleNumber(truck_vehicle_number_from_json);
                    sched1.setQuote_ID2(id);
                    sched1.setSourcecity2(lp_a_split[0]);
                    sched1.setPostStatus2(post_status1);
                    sched1.setDestinationcity2(unlp_a_split[0]);
                    sched1.setScheduleDate2(load_date_from_json);
                    sched1.setExpectedAmount(new_part_amount_jsondata);
                    sched1.setWeight(left_count_from_json);
                    sched1.setMaterialType(load_material_from_json);
                    sched1.setQuotedAmt2(new_full_amount_jsondata);
                    /******** Take Model Object in ArrayList **********/
                    CustomListViewValuesArr3.add(sched1);
                }
            }
            } catch (JSONException e) {e.printStackTrace();}
            dialogue.dismiss();
        }
    }
}
