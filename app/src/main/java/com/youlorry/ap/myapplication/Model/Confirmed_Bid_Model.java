package com.youlorry.ap.myapplication.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tycho on 5/4/2017.
 */

public class Confirmed_Bid_Model {


    /**
     * ID : 19972
     * post_author : 31
     * post_date : 2017-05-04 08:53:52
     * post_date_gmt : 2017-05-04 08:53:52
     * post_content :
     * post_title : FACTORY Freight
     * post_excerpt :
     * post_status : publish
     * comment_status : closed
     * ping_status : closed
     * post_password :
     * post_name : factory-freight-4
     * to_ping :
     * pinged :
     * post_modified : 2017-05-04 08:53:52
     * post_modified_gmt : 2017-05-04 08:53:52
     * post_content_filtered :
     * post_parent : 0
     * guid : http://nishabhati.com/load/factory-freight-4/
     * menu_order : 0
     * post_type : load
     * post_mime_type :
     * comment_count : 0
     * filter : raw
     * bid_meta_keys : {"ID":19974,"post_author":"44","post_date":"2017-05-04 08:55:28","post_date_gmt":"2017-05-04 08:55:28","post_content":"","post_title":"F-&gt;19972 B-&gt;44 V-&gt;2100","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"f-19972-b-44-v-2100","to_ping":"","pinged":"","post_modified":"2017-05-04 08:55:28","post_modified_gmt":"2017-05-04 08:55:28","post_content_filtered":"","post_parent":0,"guid":"http://nishabhati.com/youlorry_bidding/f-19972-b-44-v-2100/","menu_order":0,"post_type":"youlorry_bidding","post_mime_type":"","comment_count":"0","filter":"raw","bid_placed_by_id":["44"],"factory_load_ID":["19972"],"bid_rate":["2100"],"bid_value":["210000"],"bid_weight_capacity":["100"],"is_confirmed":["1"],"left_count":["100"],"_edit_lock":["1493987176:1"]}
     * meta_keys : {"load_source_city":["Sonapur"],"load_destination_city":["Tarapith"],"load_material":["Plastic"],"load_share_with":["a:2:{i:0;s:2:\"42\";i:1;s:2:\"44\";}"],"load_weight_capacity":["300"],"load_pickup_point":["Outside City"],"load_drop_point":["Outside City"],"load_rate_per_ton":["1996"],"load_time":["5:00 PM"],"load_date":["05-05-2017"],"load_allow_bidding":["on"],"load_factory_user_id":["31"],"mysql_date":["2017-05-05"],"from_factory":["true"],"bid_winner_array":["a:2:{i:0;i:19974;i:1;i:19973;}"],"left_count":["0"]}
     * weight : 100
     */

    private int ID;
    private String post_author;
    private String post_date;
    private String post_date_gmt;
    private String post_content;
    private String post_title;
    private String post_excerpt;
    private String post_status;
    private String comment_status;
    private String ping_status;
    private String post_password;
    private String post_name;
    private String to_ping;
    private String pinged;
    private String post_modified;
    private String post_modified_gmt;
    private String post_content_filtered;
    private int post_parent;
    private String guid;
    private int menu_order;
    private String post_type;
    private String post_mime_type;
    private String comment_count;
    private String filter;
    private BidMetaKeysBean bid_meta_keys;
    private MetaKeysBean meta_keys;
    private String weight;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getPost_author() {
        return post_author;
    }

    public void setPost_author(String post_author) {
        this.post_author = post_author;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getPost_date_gmt() {
        return post_date_gmt;
    }

    public void setPost_date_gmt(String post_date_gmt) {
        this.post_date_gmt = post_date_gmt;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_excerpt() {
        return post_excerpt;
    }

    public void setPost_excerpt(String post_excerpt) {
        this.post_excerpt = post_excerpt;
    }

    public String getPost_status() {
        return post_status;
    }

    public void setPost_status(String post_status) {
        this.post_status = post_status;
    }

    public String getComment_status() {
        return comment_status;
    }

    public void setComment_status(String comment_status) {
        this.comment_status = comment_status;
    }

    public String getPing_status() {
        return ping_status;
    }

    public void setPing_status(String ping_status) {
        this.ping_status = ping_status;
    }

    public String getPost_password() {
        return post_password;
    }

    public void setPost_password(String post_password) {
        this.post_password = post_password;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    public String getTo_ping() {
        return to_ping;
    }

    public void setTo_ping(String to_ping) {
        this.to_ping = to_ping;
    }

    public String getPinged() {
        return pinged;
    }

    public void setPinged(String pinged) {
        this.pinged = pinged;
    }

    public String getPost_modified() {
        return post_modified;
    }

    public void setPost_modified(String post_modified) {
        this.post_modified = post_modified;
    }

    public String getPost_modified_gmt() {
        return post_modified_gmt;
    }

    public void setPost_modified_gmt(String post_modified_gmt) {
        this.post_modified_gmt = post_modified_gmt;
    }

    public String getPost_content_filtered() {
        return post_content_filtered;
    }

    public void setPost_content_filtered(String post_content_filtered) {
        this.post_content_filtered = post_content_filtered;
    }

    public int getPost_parent() {
        return post_parent;
    }

    public void setPost_parent(int post_parent) {
        this.post_parent = post_parent;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public int getMenu_order() {
        return menu_order;
    }

    public void setMenu_order(int menu_order) {
        this.menu_order = menu_order;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getPost_mime_type() {
        return post_mime_type;
    }

    public void setPost_mime_type(String post_mime_type) {
        this.post_mime_type = post_mime_type;
    }

    public String getComment_count() {
        return comment_count;
    }

    public void setComment_count(String comment_count) {
        this.comment_count = comment_count;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public BidMetaKeysBean getBid_meta_keys() {
        return bid_meta_keys;
    }

    public void setBid_meta_keys(BidMetaKeysBean bid_meta_keys) {
        this.bid_meta_keys = bid_meta_keys;
    }

    public MetaKeysBean getMeta_keys() {
        return meta_keys;
    }

    public void setMeta_keys(MetaKeysBean meta_keys) {
        this.meta_keys = meta_keys;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public static class BidMetaKeysBean {
        /**
         * ID : 19974
         * post_author : 44
         * post_date : 2017-05-04 08:55:28
         * post_date_gmt : 2017-05-04 08:55:28
         * post_content :
         * post_title : F-&gt;19972 B-&gt;44 V-&gt;2100
         * post_excerpt :
         * post_status : publish
         * comment_status : closed
         * ping_status : closed
         * post_password :
         * post_name : f-19972-b-44-v-2100
         * to_ping :
         * pinged :
         * post_modified : 2017-05-04 08:55:28
         * post_modified_gmt : 2017-05-04 08:55:28
         * post_content_filtered :
         * post_parent : 0
         * guid : http://nishabhati.com/youlorry_bidding/f-19972-b-44-v-2100/
         * menu_order : 0
         * post_type : youlorry_bidding
         * post_mime_type :
         * comment_count : 0
         * filter : raw
         * bid_placed_by_id : ["44"]
         * factory_load_ID : ["19972"]
         * bid_rate : ["2100"]
         * bid_value : ["210000"]
         * bid_weight_capacity : ["100"]
         * is_confirmed : ["1"]
         * left_count : ["100"]
         * _edit_lock : ["1493987176:1"]
         */

        private int ID;
        private String post_author;
        private String post_date;
        private String post_date_gmt;
        private String post_content;
        private String post_title;
        private String post_excerpt;
        private String post_status;
        private String comment_status;
        private String ping_status;
        private String post_password;
        private String post_name;
        private String to_ping;
        private String pinged;
        private String post_modified;
        private String post_modified_gmt;
        private String post_content_filtered;
        private int post_parent;
        private String guid;
        private int menu_order;
        private String post_type;
        private String post_mime_type;
        private String comment_count;
        private String filter;
        private List<String> bid_placed_by_id;
        private List<String> factory_load_ID;
        private List<String> bid_rate;
        private List<String> bid_value;
        private List<String> bid_weight_capacity;
        private List<String> is_confirmed;
        private List<String> left_count;
        private List<String> _edit_lock;

        public BidMetaKeysBean(JSONObject metaKeyObj){
            try {


                this.ID = metaKeyObj.getInt("ID");
                this.post_author=metaKeyObj.getString("post_author");
                this.post_date=metaKeyObj.getString("post_date");
                this.post_date_gmt=metaKeyObj.getString("post_date_gmt");
                this.post_content=metaKeyObj.getString("post_content");
                this.post_title=metaKeyObj.getString("post_title");
                this.post_excerpt=metaKeyObj.getString("post_excerpt");
                this.post_status=metaKeyObj.getString("post_status");
                this.comment_status=metaKeyObj.getString("comment_status");
                this.ping_status=metaKeyObj.getString("ping_status");
                this.post_password=metaKeyObj.getString("post_password");
                this.post_name=metaKeyObj.getString("post_name");
                this.to_ping=metaKeyObj.getString("to_ping");
                this.pinged=metaKeyObj.getString("pinged");
                this.post_modified=metaKeyObj.getString("post_modified");
                this.post_modified_gmt=metaKeyObj.getString("post_modified_gmt");
                this.post_content_filtered=metaKeyObj.getString("post_content_filtered");
                this.post_parent=metaKeyObj.getInt("post_parent");
                this.guid=metaKeyObj.getString("guid");
                this.menu_order=metaKeyObj.getInt("menu_order");
                this.post_type=metaKeyObj.getString("post_type");
                this.post_mime_type=metaKeyObj.getString("post_mime_type");
                this.comment_count=metaKeyObj.getString("comment_count");
                this.filter=metaKeyObj.getString("filter");




                this.bid_placed_by_id = fetchArray(metaKeyObj, "bid_placed_by_id");
                this.factory_load_ID = fetchArray(metaKeyObj, "factory_load_ID");
                this.bid_rate = fetchArray(metaKeyObj, "bid_rate");
                this.bid_value = fetchArray(metaKeyObj, "bid_value");
                this.bid_weight_capacity = fetchArray(metaKeyObj, "bid_weight_capacity");
                this.is_confirmed = fetchArray(metaKeyObj, "is_confirmed");
                this.left_count = fetchArray(metaKeyObj, "left_count");
                this._edit_lock = fetchArray(metaKeyObj, "_edit_lock");
            }
            catch (Exception e)
            {

            }
        }

        public List<String> fetchArray(JSONObject object, String key){

            List<String> listdata = new ArrayList<String>();
            try {
                JSONArray jArray = object.getJSONArray(key);
                if (jArray != null) {
                    for (int i=0;i<jArray.length();i++){
                        listdata.add(jArray.getString(i));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return listdata;
        }

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public String getPost_author() {
            return post_author;
        }

        public void setPost_author(String post_author) {
            this.post_author = post_author;
        }

        public String getPost_date() {
            return post_date;
        }

        public void setPost_date(String post_date) {
            this.post_date = post_date;
        }

        public String getPost_date_gmt() {
            return post_date_gmt;
        }

        public void setPost_date_gmt(String post_date_gmt) {
            this.post_date_gmt = post_date_gmt;
        }

        public String getPost_content() {
            return post_content;
        }

        public void setPost_content(String post_content) {
            this.post_content = post_content;
        }

        public String getPost_title() {
            return post_title;
        }

        public void setPost_title(String post_title) {
            this.post_title = post_title;
        }

        public String getPost_excerpt() {
            return post_excerpt;
        }

        public void setPost_excerpt(String post_excerpt) {
            this.post_excerpt = post_excerpt;
        }

        public String getPost_status() {
            return post_status;
        }

        public void setPost_status(String post_status) {
            this.post_status = post_status;
        }

        public String getComment_status() {
            return comment_status;
        }

        public void setComment_status(String comment_status) {
            this.comment_status = comment_status;
        }

        public String getPing_status() {
            return ping_status;
        }

        public void setPing_status(String ping_status) {
            this.ping_status = ping_status;
        }

        public String getPost_password() {
            return post_password;
        }

        public void setPost_password(String post_password) {
            this.post_password = post_password;
        }

        public String getPost_name() {
            return post_name;
        }

        public void setPost_name(String post_name) {
            this.post_name = post_name;
        }

        public String getTo_ping() {
            return to_ping;
        }

        public void setTo_ping(String to_ping) {
            this.to_ping = to_ping;
        }

        public String getPinged() {
            return pinged;
        }

        public void setPinged(String pinged) {
            this.pinged = pinged;
        }

        public String getPost_modified() {
            return post_modified;
        }

        public void setPost_modified(String post_modified) {
            this.post_modified = post_modified;
        }

        public String getPost_modified_gmt() {
            return post_modified_gmt;
        }

        public void setPost_modified_gmt(String post_modified_gmt) {
            this.post_modified_gmt = post_modified_gmt;
        }

        public String getPost_content_filtered() {
            return post_content_filtered;
        }

        public void setPost_content_filtered(String post_content_filtered) {
            this.post_content_filtered = post_content_filtered;
        }

        public int getPost_parent() {
            return post_parent;
        }

        public void setPost_parent(int post_parent) {
            this.post_parent = post_parent;
        }

        public String getGuid() {
            return guid;
        }

        public void setGuid(String guid) {
            this.guid = guid;
        }

        public int getMenu_order() {
            return menu_order;
        }

        public void setMenu_order(int menu_order) {
            this.menu_order = menu_order;
        }

        public String getPost_type() {
            return post_type;
        }

        public void setPost_type(String post_type) {
            this.post_type = post_type;
        }

        public String getPost_mime_type() {
            return post_mime_type;
        }

        public void setPost_mime_type(String post_mime_type) {
            this.post_mime_type = post_mime_type;
        }

        public String getComment_count() {
            return comment_count;
        }

        public void setComment_count(String comment_count) {
            this.comment_count = comment_count;
        }

        public String getFilter() {
            return filter;
        }

        public void setFilter(String filter) {
            this.filter = filter;
        }

        public List<String> getBid_placed_by_id() {
            return bid_placed_by_id;
        }

        public void setBid_placed_by_id(List<String> bid_placed_by_id) {
            this.bid_placed_by_id = bid_placed_by_id;
        }

        public List<String> getFactory_load_ID() {
            return factory_load_ID;
        }

        public void setFactory_load_ID(List<String> factory_load_ID) {
            this.factory_load_ID = factory_load_ID;
        }

        public List<String> getBid_rate() {
            return bid_rate;
        }

        public void setBid_rate(List<String> bid_rate) {
            this.bid_rate = bid_rate;
        }

        public List<String> getBid_value() {
            return bid_value;
        }

        public void setBid_value(List<String> bid_value) {
            this.bid_value = bid_value;
        }

        public List<String> getBid_weight_capacity() {
            return bid_weight_capacity;
        }

        public void setBid_weight_capacity(List<String> bid_weight_capacity) {
            this.bid_weight_capacity = bid_weight_capacity;
        }

        public List<String> getIs_confirmed() {
            return is_confirmed;
        }

        public void setIs_confirmed(List<String> is_confirmed) {
            this.is_confirmed = is_confirmed;
        }

        public List<String> getLeft_count() {
            return left_count;
        }

        public void setLeft_count(List<String> left_count) {
            this.left_count = left_count;
        }

        public List<String> get_edit_lock() {
            return _edit_lock;
        }

        public void set_edit_lock(List<String> _edit_lock) {
            this._edit_lock = _edit_lock;
        }
    }

    public static class MetaKeysBean {
        private List<String> load_source_city;
        private List<String> load_destination_city;
        private List<String> load_material;
        private List<String> load_share_with;
        private List<String> load_weight_capacity;
        private List<String> load_pickup_point;
        private List<String> load_drop_point;
        private List<String> load_rate_per_ton;
        private List<String> load_time;
        private List<String> load_date;
        private List<String> load_allow_bidding;
        private List<String> load_factory_user_id;
        private List<String> mysql_date;
        private List<String> from_factory;
        private List<String> bid_winner_array;
        private List<String> left_count;
        private List<String> load_total_value;


        public MetaKeysBean(JSONObject metaKeyObj){

            this.load_source_city = fetchArray(metaKeyObj, "load_source_city");
            this.load_destination_city = fetchArray(metaKeyObj, "load_destination_city");
            this.load_material = fetchArray(metaKeyObj, "load_material");
            this.load_share_with = fetchArray(metaKeyObj, "load_share_with");
            this.load_total_value=fetchArray(metaKeyObj,"load_total_value");
            this.load_weight_capacity = fetchArray(metaKeyObj, "load_weight_capacity");
            this.load_pickup_point = fetchArray(metaKeyObj, "load_pickup_point");
            this.load_drop_point = fetchArray(metaKeyObj, "load_drop_point");
            this.load_rate_per_ton = fetchArray(metaKeyObj, "load_rate_per_ton");
            this.load_time = fetchArray(metaKeyObj, "load_time");
            this.load_date = fetchArray(metaKeyObj, "load_date");
            this.load_allow_bidding = fetchArray(metaKeyObj, "load_allow_bidding");
            this.load_factory_user_id = fetchArray(metaKeyObj, "load_factory_user_id");

            this.mysql_date = fetchArray(metaKeyObj, "mysql_date");
            this.left_count = fetchArray(metaKeyObj, "left_count");
            this.from_factory = fetchArray(metaKeyObj, "from_factory");
        }

        public List<String> fetchArray(JSONObject object, String key){

            List<String> listdata = new ArrayList<String>();
            try {
                JSONArray jArray = object.getJSONArray(key);
                if (jArray != null) {
                    for (int i=0;i<jArray.length();i++){
                        listdata.add(jArray.getString(i));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return listdata;
        }


        public List<String> getLoad_source_city() {
            return load_source_city;
        }

        public void setLoad_source_city(List<String> load_source_city) {
            this.load_source_city = load_source_city;
        }

        public List<String> getLoad_destination_city() {
            return load_destination_city;
        }

        public void setLoad_destination_city(List<String> load_destination_city) {
            this.load_destination_city = load_destination_city;
        }

        public List<String> getLoad_material() {
            return load_material;
        }

        public void setLoad_material(List<String> load_material) {
            this.load_material = load_material;
        }

        public List<String> getLoad_share_with() {
            return load_share_with;
        }

        public void setLoad_share_with(List<String> load_share_with) {
            this.load_share_with = load_share_with;
        }

        public List<String> getLoad_weight_capacity() {
            return load_weight_capacity;
        }

        public void setLoad_weight_capacity(List<String> load_weight_capacity) {
            this.load_weight_capacity = load_weight_capacity;
        }

        public List<String> getLoad_pickup_point() {
            return load_pickup_point;
        }

        public void setLoad_pickup_point(List<String> load_pickup_point) {
            this.load_pickup_point = load_pickup_point;
        }

        public List<String> getLoad_drop_point() {
            return load_drop_point;
        }

        public void setLoad_drop_point(List<String> load_drop_point) {
            this.load_drop_point = load_drop_point;
        }

        public List<String> getLoad_rate_per_ton() {
            return load_rate_per_ton;
        }

        public void setLoad_rate_per_ton(List<String> load_rate_per_ton) {
            this.load_rate_per_ton = load_rate_per_ton;
        }

        public List<String> getLoad_time() {
            return load_time;
        }

        public void setLoad_time(List<String> load_time) {
            this.load_time = load_time;
        }

        public List<String> getLoad_date() {
            return load_date;
        }

        public void setLoad_date(List<String> load_date) {
            this.load_date = load_date;
        }

        public List<String> getLoad_allow_bidding() {
            return load_allow_bidding;
        }

        public void setLoad_allow_bidding(List<String> load_allow_bidding) {
            this.load_allow_bidding = load_allow_bidding;
        }

        public List<String> getLoad_factory_user_id() {
            return load_factory_user_id;
        }

        public void setLoad_factory_user_id(List<String> load_factory_user_id) {
            this.load_factory_user_id = load_factory_user_id;
        }

        public List<String> getMysql_date() {
            return mysql_date;
        }

        public void setMysql_date(List<String> mysql_date) {
            this.mysql_date = mysql_date;
        }

        public List<String> getFrom_factory() {
            return from_factory;
        }

        public void setFrom_factory(List<String> from_factory) {
            this.from_factory = from_factory;
        }

        public List<String> getBid_winner_array() {
            return bid_winner_array;
        }

        public void setBid_winner_array(List<String> bid_winner_array) {
            this.bid_winner_array = bid_winner_array;
        }

        public List<String> getLeft_count() {
            return left_count;
        }

        public void setLeft_count(List<String> left_count) {
            this.left_count = left_count;
        }
        public List<String> getLoad_total_value() {
            return load_total_value;
        }

        public void setLoad_total_value(List<String> load_total_value) {
            this.load_total_value = load_total_value;
        }
    }
}
