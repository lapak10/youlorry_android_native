package com.youlorry.ap.myapplication;

/**
 * Created by Arpit Prajapati on 12/30/16.
 */

public class ListModelforhistorylistrow {

    private  String H_month="";
    private  String H_no_of_posting="";
    private  String H_no_of_booking="";
    private  String H_no_of_cancellation="";
    private  String H_total_material_wt="";
    private  String H_total_freight_amt="";

    /*********** Set Methods ******************/

    public void setHistoryMonth(String h_month){ this.H_month = h_month; }

    public void setHistoryNoOfPosting(String h_no_of_posting){ this.H_no_of_posting = h_no_of_posting; }

    public void setHistoryNoOfBooking(String h_no_of_booking){ this.H_no_of_booking = h_no_of_booking; }

    public void setHistoryNoOfCancellation(String h_no_of_cancellation){ this.H_no_of_cancellation = h_no_of_cancellation; }

    public void setHistoryTotalMaterialWt(String h_total_material_wt){ this.H_total_material_wt = h_total_material_wt; }

    public void setHistoryTotalFreightAmt(String h_total_freight_amt){ this.H_total_freight_amt = h_total_freight_amt; }

    /*********** Get Methods ****************/

    public String getHistoryMonth()
    {
        return this.H_month;
    }

    public String getHistoryNoOfPosting()
    {
        return this.H_no_of_posting;
    }

    public String getHistoryNoOfBooking()
    {
        return this.H_no_of_booking;
    }

    public String getHistoryNoOfCancellation()
    {
        return this.H_no_of_cancellation;
    }

    public String getHistoryTotalMaterialWt()
    {
        return this.H_total_material_wt;
    }

    public String getHistoryTotalFreightAmt()
    {
        return this.H_total_freight_amt;
    }

}
