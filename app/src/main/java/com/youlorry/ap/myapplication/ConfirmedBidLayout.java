package com.youlorry.ap.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.youlorry.ap.myapplication.Adapter.Bidding_Adapter;
import com.youlorry.ap.myapplication.Adapter.Confirmend_bid_Adapter;
import com.youlorry.ap.myapplication.Model.Bidding_Model;
import com.youlorry.ap.myapplication.Model.Confirmed_Bid_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arpit Prajapati on 3/20/2017.
 */

public class ConfirmedBidLayout extends Fragment implements View.OnClickListener{

    Context con;
   // Button details_btn;
    RecyclerView confirmed_bidding_recycler_view;
    RecyclerView.LayoutManager layoutManager;
    LinearLayout ll_recyclerView,ll_nodatafound;
  Confirmend_bid_Adapter confirmend_bid_adapter;
    List<Confirmed_Bid_Model> confirmed_bid_modelList=new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.confirmed_bid_layout, container, false);

        con = getActivity();

        YoulorrySession session = new YoulorrySession(con);

        if (session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0) {

            Intent in = new Intent(con, LoginActivity.class);
            startActivity(in);

        }
        confirmed_bidding_recycler_view=(RecyclerView) v.findViewById(R.id.bidding_recycler_view);
        ll_nodatafound=(LinearLayout) v.findViewById(R.id.ll_nodatafound);
        ll_recyclerView=(LinearLayout) v.findViewById(R.id.ll_recyclerView);
//        details_btn = (Button) v.findViewById(R.id.confirmed_bid_details_btn);
//        details_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(con, DetailsPageforConfirmedBid.class));
//                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//            }
//        });
        new FetchAllConfirmedBids().execute();
        return v;
    }

    public void getDetailsPage(int id,int factory_load_ID) {
        Intent mIntent = new Intent(getActivity(), DetailsPageforConfirmedBid.class);
      //  Bundle mBundle = new Bundle();
//        mBundle.putString("ID", String.valueOf(id));
//        mBundle.putString("factory_load_ID",factory_load_ID);
//        mIntent.putExtra(mBundle);
        mIntent.putExtra("ID",String.valueOf(id));
        mIntent.putExtra("factory_load_ID",String.valueOf(factory_load_ID));
        startActivity(mIntent);
               getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    class FetchAllConfirmedBids extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session12 = new YoulorrySession(con);
            String username = session12.getusername();
            String password = session12.getpass_word();
            String userid = session12.getuser_id();
            String role = session12.getrole();
            StringBuilder sb = new StringBuilder();

            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-factory-load/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                //  urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);
                cred.put("req_key", "get_confirmed_bids" );
                cred.put("action", "fetch_confirmed_load" );

                OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                wr.flush();
                wr.close();

                //display what returns the POST request
                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();

                    Log.d("Confirmed bids data :", sb.toString());
                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return sb.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialogue.dismiss();
            if(s.toString().contains("some error") )
            {
                ll_recyclerView.setVisibility(View.GONE);
                ll_nodatafound.setVisibility(View.VISIBLE);
            }
else{
                ll_nodatafound.setVisibility(View.GONE);
                ll_recyclerView.setVisibility(View.VISIBLE);
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(s.toString());
                    int arraysize = jsonArray.length();
                    for (int i = 0; i < arraysize; i++) {
                        JSONObject obj = null;
                        try {
                            obj = jsonArray.getJSONObject(i);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Confirmed_Bid_Model bidding_model = new Confirmed_Bid_Model();
                        try {
                            bidding_model.setID(obj.getInt("ID"));
                            bidding_model.setPost_author(obj.getString("post_author"));
                            bidding_model.setPost_date(obj.getString("post_date"));
//                        bidding_model.set(obj.getString("lowest_bid_value"));
//                        bidding_model.setMy_bid_value(obj.getString("my_bid_value"));
//                        bidding_model.setHour_limit(obj.getInt("hour_limit"));

                            String bit_meta = obj.getString("bid_meta_keys");
                            if (!bit_meta.contains("null")) {
                                JSONObject bidMetaKeys = obj.getJSONObject("bid_meta_keys");
                                Confirmed_Bid_Model.BidMetaKeysBean bidMetaKeysBean = new Confirmed_Bid_Model.BidMetaKeysBean(bidMetaKeys);
                                bidding_model.setBid_meta_keys(bidMetaKeysBean);
                            }
                            JSONObject meta_keys = obj.getJSONObject("meta_keys");

                            Confirmed_Bid_Model.MetaKeysBean metaKeysBean = new Confirmed_Bid_Model.MetaKeysBean(meta_keys);

                            bidding_model.setMeta_keys(metaKeysBean);

                            confirmed_bid_modelList.add(bidding_model);

//                    {
//
//                    }
                            //    bidding_model.setMeta_keys(obj.getJSONArray("meta_keys"));

//                    organizationsListingModel.setPost_date(obj.getString("name"));
//                    organizationsListingModel.setPost_date_gmt(obj.getString("logo"));
//                    organizationsListingModel.setPost_content(obj.getInt("id"));
//                    organizationsListingModel.setFollowers_count(obj.getInt("followers_count"));
//                    organizationsListingModel.setCampaigns_count(obj.getInt("campaigns_count"));
//                    organizationsListingModelArrayList.add(organizationsListingModel);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    layoutManager = new LinearLayoutManager(getActivity());
                    confirmed_bidding_recycler_view.setLayoutManager(layoutManager);
                    confirmend_bid_adapter = new Confirmend_bid_Adapter(getActivity(), confirmed_bid_modelList, ConfirmedBidLayout.this);
                    confirmed_bidding_recycler_view.setAdapter(confirmend_bid_adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    public void onClick(View v) {

    }
}