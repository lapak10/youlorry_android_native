package com.youlorry.ap.myapplication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by stark on 1/25/17.
 */

public class NewUserAfterLoginForm extends YouLorryActivity implements View.OnClickListener {

    EditText company_or_business_name,your_email_address,city_name,city_name1,city_name2,city_name3,city_name4,no_of_trucks;
    Spinner trucktype_spinner;
    Button save_details_btn;
    ImageView add_city_edit_text,remove_city_edit_text;
    private static final String[]trucktype_paths = {"Select Truck Type","Container Close Body (20-40 Feet)","Container Fixed (40-70 Feet)","Container Open Body (20-40 Feet)","Container Trucks","Double Dacker", "Canter 4.5MT (17/6/6 ft) 4 Wheel","Canter 4MT (9/6/6 ft) 4 Wheel","Canter 7.5MT (19/7/7 ft) 6 Wheel","Canters Jumbo (20/7/7 ft)", "Flat Bed Trailers (20-32 ft)","Flat Bed Trailers (40-54 ft)","HCV (Trucks/Trailers)","LCV (Light Comercial Vehicle)","Low Bed Trailer","Open Body Truck", "10 Axle Trailer","Truck 14 Wheel","Truck 15MT (22/7/7 ft) 10 Wheel","Truck 20 MT (28/8/8 ft) 12 Wheel","Truck 9 MT (17/7/7 ft) 6 Wheel", "Vehicle/Car Carrier (20-80 ft) Closed"};
    ProgressDialog dialogue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_user_after_login_form);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        company_or_business_name = (EditText) findViewById(R.id.company_or_business_name);
        your_email_address = (EditText) findViewById(R.id.your_email_address);
        city_name = (EditText) findViewById(R.id.city_name);
        city_name1 = (EditText) findViewById(R.id.city_name1);
        city_name2 = (EditText) findViewById(R.id.city_name2);
        city_name3 = (EditText) findViewById(R.id.city_name3);
        city_name4 = (EditText) findViewById(R.id.city_name4);
        no_of_trucks = (EditText) findViewById(R.id.no_of_trucks);

        add_city_edit_text = (ImageView) findViewById(R.id.add_city_edit_text);
        remove_city_edit_text = (ImageView) findViewById(R.id.remove_city_edit_text);

        add_city_edit_text.setOnClickListener(this);
        remove_city_edit_text.setOnClickListener(this);

        trucktype_spinner = (Spinner) findViewById(R.id.trucktype_spinner);
        ArrayAdapter<String> adapter_1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, trucktype_paths);
        trucktype_spinner.setAdapter(adapter_1);

        save_details_btn = (Button) findViewById(R.id.save_details_btn);

        save_details_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.add_city_edit_text){
            city_name1.setVisibility(View.VISIBLE);
            city_name2.setVisibility(View.VISIBLE);
            city_name3.setVisibility(View.VISIBLE);
            city_name4.setVisibility(View.VISIBLE);
            add_city_edit_text.setVisibility(View.GONE);
            remove_city_edit_text.setVisibility(View.VISIBLE);
        }

        if(view.getId() == R.id.remove_city_edit_text){
            city_name1.setVisibility(View.GONE);
            city_name2.setVisibility(View.GONE);
            city_name3.setVisibility(View.GONE);
            city_name4.setVisibility(View.GONE);
            add_city_edit_text.setVisibility(View.VISIBLE);
            remove_city_edit_text.setVisibility(View.GONE);
        }

        if(view.getId() == R.id.save_details_btn){

            if(company_or_business_name.getText().toString().length() == 0 ){
                company_or_business_name.setError("required field.");
            } else if (city_name.getText().toString().length() == 0 && city_name1.getText().toString().length() == 0 && city_name2.getText().toString().length() == 0 && city_name3.getText().toString().length() == 0 && city_name4.getText().toString().length() == 0) {
                city_name.setError("atleast 1 city required.");
            } else if(trucktype_spinner.getSelectedItem().toString() == "Select Truck Type"){
                new AlertDialog.Builder(this)
                        .setTitle("Truck Type Required")
                        .setMessage("Please select truck type!")
                        .show();
            } else if (no_of_trucks.getText().toString().length() == 0 ) {
                no_of_trucks.setError("required field.");
            } else {

                dialogue = new ProgressDialog(this);
                dialogue.setTitle("Loading ...");
                dialogue.show();

                URL urlObj = null;
                try {
                    urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-forgotpassword/");
                    HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);
                    urlConnection.setUseCaches(false);
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    //  urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setRequestMethod("POST");
                    urlConnection.connect();

                    JSONObject cred = new JSONObject();

                    cred.put("", company_or_business_name.getText().toString());
                    cred.put("", your_email_address.getText().toString());
                    cred.put("", city_name.getText().toString());
                    cred.put("", city_name1.getText().toString());
                    cred.put("", city_name2.getText().toString());
                    cred.put("", city_name3.getText().toString());
                    cred.put("", city_name4.getText().toString());
                    cred.put("", trucktype_spinner.getSelectedItem().toString());
                    cred.put("", no_of_trucks.getText().toString());

                    Log.d("", company_or_business_name.getText().toString());
                    Log.d("", your_email_address.getText().toString());
                    Log.d("", city_name.getText().toString());
                    Log.d("", city_name1.getText().toString());
                    Log.d("", city_name2.getText().toString());
                    Log.d("", city_name3.getText().toString());
                    Log.d("", city_name4.getText().toString());
                    Log.d("", trucktype_spinner.getSelectedItem().toString());
                    Log.d("", no_of_trucks.getText().toString());

                    OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(cred.toString());
                    wr.flush();
                    wr.close();

                    StringBuilder sb = new StringBuilder();
                    int HttpResult = urlConnection.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        BufferedReader br = new BufferedReader(
                                new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                        }
                        br.close();

                        Log.d("response_data_for_forgot_password", sb.toString());

                        if(sb.toString().equals("user_does_not_exists")){
                            Toast.makeText(this, "User Not Found", Toast.LENGTH_SHORT).show();
                        } else if(sb.toString().equals("sent")){
//                            Log.d("name", name.getText().toString());
                            finish();
                            Toast.makeText(this, "OTP Sent!!!", Toast.LENGTH_SHORT).show();
                            Intent in = new Intent(this, OTPNumber.class);
                            Bundle bd = new Bundle();
                            bd.putString("from_key", "forgot_password");
//                            bd.putString("contact_number", mob_number_edit_text.getText().toString());
                            in.putExtras(bd);
                            startActivity(in);

                        } else {
                            Toast.makeText(this, "Some error occurs. Please try later!!!", Toast.LENGTH_SHORT).show();
                        }

                    }

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialogue.dismiss();
            }
        }
    }
}
