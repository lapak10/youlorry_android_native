package com.youlorry.ap.myapplication;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Arpit Prajapati on 12/30/16.
 */

public class HistoryActivity extends Fragment implements View.OnClickListener {


    ListView listView;
    TextView t1;
    Context con;

    CustomAdapterforhistorylistview adapter;
    public ArrayList<ListModelforhistorylistrow> CustomListViewValuesArr = new ArrayList<ListModelforhistorylistrow>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.history,container,false);

        con = getActivity();

        YoulorrySession session = new YoulorrySession(con);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(con , LoginActivity.class);
            startActivity(in);

        }

        try {
            InputMethodManager input = (InputMethodManager) getActivity()
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            input.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }catch(Exception e) {
            e.printStackTrace();
        }


        listView = (ListView) v.findViewById(R.id.history_listview);
        t1 = (TextView) v.findViewById(R.id.no_matching_data_found);

        new TheTask().execute();

        return  v;
    }

    class TheTask extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session = new YoulorrySession(con);
            String username = session.getusername();
            String password = session.getpass_word();
            String userid = session.getuser_id();
            String role = session.getrole();

            StringBuilder sb = new StringBuilder();
            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-my-history/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                //  urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);

                OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                wr.flush();
                wr.close();

                //display what returns the POST request

                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                    br.close();

//                    Toast.makeText(con, sb.toString() , Toast.LENGTH_LONG).show();

                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return sb.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            Log.d("history_json_data", s.toString());
            new YoulorrySession(con).setHistoryData(s.toString());

           // Toast.makeText(con, s.toString(), Toast.LENGTH_SHORT).show();

            try {

                if(s.length() < 25){
                    listView.setVisibility(View.GONE);
                    t1.setVisibility(View.VISIBLE);
                    t1.setText("No History Data Found !!!");
                }

                JSONArray jsonArray = null;
                jsonArray = new JSONArray(s.toString());

                Resources res =getResources();
                for(int i=0; i < jsonArray.length(); i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String month = jsonObject.getString("month");
                    String year = jsonObject.getString("year");

                    String data_summary_jsondata = jsonObject.optString("data_summary").toString();

                    JSONObject jobj = new JSONObject(data_summary_jsondata);
                    String bookings_count_jsondata=jobj.getString("bookings_count");
                    String cancellation_count_jsondata=jobj.getString("cancellation_count");
                    String postings_count_jsondata=jobj.getString("postings_count");
                    String total_material_weight_jsondata=jobj.getString("total_material_weight");
                    String total_freight_amount_jsondata=jobj.getString("total_freight_amount");

                        adapter=new CustomAdapterforhistorylistview( con, CustomListViewValuesArr,res);
                        listView.setAdapter( adapter );
                    Log.d("data_summary", data_summary_jsondata);
                    Log.d("month_and_year", month+" "+year);
                    Log.d("bookings_count", bookings_count_jsondata);
                    Log.d("cancellation_count", cancellation_count_jsondata);
                    Log.d("postings_count", postings_count_jsondata);
                    Log.d("total_material_weight", total_material_weight_jsondata);
                    Log.d("total_freight_amount", total_freight_amount_jsondata);

                    final ListModelforhistorylistrow sched = new ListModelforhistorylistrow();

                    /******* Firstly take data in model object ******/
                    sched.setHistoryMonth(month+"/"+year);
                    sched.setHistoryNoOfBooking(bookings_count_jsondata);
                    sched.setHistoryNoOfCancellation(cancellation_count_jsondata);
                    sched.setHistoryNoOfPosting(postings_count_jsondata);
                    sched.setHistoryTotalMaterialWt(total_material_weight_jsondata);
                    sched.setHistoryTotalFreightAmt(total_freight_amount_jsondata);

                    /******** Take Model Object in ArrayList **********/
                    CustomListViewValuesArr.add(sched);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            dialogue.dismiss();
        }
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onResume() {

        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button
//                    startActivity(new Intent(con,MainActivity.class));
                    getActivity().getFragmentManager().popBackStack();
                    return true;

                }
                return false;
            }
        });
    }

}
