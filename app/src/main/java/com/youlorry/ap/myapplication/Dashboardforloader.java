package com.youlorry.ap.myapplication;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

/**
 * Created by Arpit Prajapati on 12/6/16.
 */
public class Dashboardforloader extends Fragment {

    TextView user_name,header_text_on_dashboard;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dashboard_for_loader, container, false);

        try {
            InputMethodManager input = (InputMethodManager) getActivity()
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            input.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }catch(Exception e) {
            e.printStackTrace();
        }

        String Username = new YoulorrySession(getActivity()).getFirstName().toString();
        user_name = (TextView) v.findViewById(R.id.user_name);
        header_text_on_dashboard = (TextView) v.findViewById(R.id.header_text_on_dashboard);
        user_name.setText("Mr. "+Username);
        header_text_on_dashboard.setText("Welcome "+Username);

        return v;
    }
}
