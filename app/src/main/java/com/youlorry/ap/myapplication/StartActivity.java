package com.youlorry.ap.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by Arpit Prajapati on 11/26/16.
 */

public class StartActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_activity);

//        pb.setVisibility(ProgressBar.INVISIBLE);

/*        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        webView = (WebView) findViewById(R.id.web_view);
        webView.loadDataWithBaseURL("file:///android_res/drawable/", "<img src='splash.gif' style='width:"+width/2+"; height:"+height/2+";' />", "text/html", "utf-8", null);
*/
        final Context con = this;
        final YoulorrySession session = new YoulorrySession(con);
        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if(session.getuser_id().length()!=0 && session.getusername().length()!=0 && session.getpass_word().length()!=0 && session.getrole().length()!=0){
                    startActivity(new Intent(con, MainActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(con, LoginActivity.class));
                    finish();
                }
            }
        }, secondsDelayed * 2000);
    }

}
