package com.youlorry.ap.myapplication;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Arpit Prajapati on 12/15/16.
 */
public class LoadlistActivity extends Fragment implements View.OnClickListener{

    String s_city,j_data,d_city,e_f_v_p_truck,material_value,truck_type,schedule_date,schedule_time;
    String result,data="";
    ListView listView;
    TextView t1;
    Context con;

    ListView list;
    CustomAdapterforloadlistview adapter1;
    public ArrayList<ListModelforloadlistrow> CustomListViewValuesArr3 = new ArrayList<ListModelforloadlistrow>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        s_city = getArguments().getString("source_key");
        j_data = getArguments().getString("j_data");
        d_city = getArguments().getString("destination_key");
        e_f_v_p_truck = getArguments().getString("expected_freight_value_per_truck_key");
        material_value = getArguments().getString("material_key");
        truck_type = getArguments().getString("truck_type_key");
        schedule_time = getArguments().getString("schedule_time_key");
        schedule_date = getArguments().getString("schedule_date_key");

        View v = inflater.inflate(R.layout.load_list,container,false);

        con = getActivity();

        YoulorrySession session = new YoulorrySession(con);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(con , LoginActivity.class);
            startActivity(in);

        }
        try {
            InputMethodManager input = (InputMethodManager) getActivity()
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            input.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }catch(Exception e) {
            e.printStackTrace();
        }

        listView = (ListView) v.findViewById(R.id.truck_filter_listview);
        t1 = (TextView) v.findViewById(R.id.no_matching_data_found);

        new TheTask().execute();
        return v;
    }


    class TheTask extends AsyncTask<Void, Void, String> {


        ProgressDialog dialogue;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            return j_data.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                JSONObject json_data = new JSONObject(s.toString());
                String truck_post_id = json_data.getString("truck_id");
                String matching_loads_json_data = json_data.getString("matching_loads");

                Log.d("matching_loads_json_data" , s.toString());
                if(matching_loads_json_data.length() < 20){
                    listView.setVisibility(View.GONE);
                    t1.setVisibility(View.VISIBLE);
                    t1.setText("No Matching Load Found !!!");
                }else {

                    YoulorrySession youlorr_session = new YoulorrySession(con);
                    youlorr_session.setCurrenttruckpostidforbooking(truck_post_id);
                    youlorr_session.setMatchingloadsforpostatruck(matching_loads_json_data);

                    JSONArray jsonArray = new JSONArray(matching_loads_json_data);
                    String [] load_list_for_list_view_id = new String[jsonArray.length()];

                    Resources res =getResources();
                    for(int i=0; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        int id = Integer.parseInt(jsonObject.optString("ID").toString());

                        String meta_keys_jsondata = jsonObject.optString("meta_keys").toString();

                        //      String posted_by_name = jsonObject.getString("posted_by");

                        JSONObject jobj = new JSONObject(meta_keys_jsondata);
                        String source_city_jsondata=jobj.getString("load_source_city");
                        String s_city_from_json = source_city_jsondata.substring(2, source_city_jsondata.toString().length() - 2);

                        String destination_city_jsondata=jobj.getString("load_destination_city");
                        String d_city_from_json = destination_city_jsondata.substring(2, destination_city_jsondata.toString().length() - 2);

                        String expected_value_jsondata=jobj.getString("load_expected_frieght_value");
                        String exp_val_from_json = expected_value_jsondata.substring(2, expected_value_jsondata.toString().length() - 2);

                        String schedule_date_jsondata=jobj.getString("load_date");
                        String scheduled_date_from_json = schedule_date_jsondata.substring(2, schedule_date_jsondata.toString().length() - 2);

                        String truck_type_jsondata=jobj.getString("load_truck_type");
                        String truc_type_from_jsondata = truck_type_jsondata.substring(2, truck_type_jsondata.toString().length() - 2);

                        String load_material_jsondata=jobj.getString("load_material");
                        String load_material_from_jsondata = load_material_jsondata.substring(2, load_material_jsondata.toString().length() - 2);

                        String load_advance_in_percentage_jsondata=jobj.getString("load_advance_in_percentage");
                        String load_advance_in_percentage_from_json = load_advance_in_percentage_jsondata.substring(2, load_advance_in_percentage_jsondata.toString().length() - 2);

//                    Toast.makeText(con, s_city_from_json + "\n" +d_city_from_json+ "\n" +truc_type_from_jsondata+ "\n" +exp_val_from_json, Toast.LENGTH_SHORT).show();

                        if(s_city.equals(s_city_from_json))
                        {
                            adapter1=new CustomAdapterforloadlistview( con, CustomListViewValuesArr3,res);
                            listView.setAdapter( adapter1 );
                        } else{
                            Toast.makeText(getActivity(), "FALSE", Toast.LENGTH_SHORT).show();
                        }

                        load_list_for_list_view_id[i] = s_city_from_json;

                        final ListModelforloadlistrow sched3 = new ListModelforloadlistrow();
                        String[] s_city_split =s_city_from_json.split(",");
                        String[] d_city_split =d_city_from_json.split(",");
                        /******* Firstly take data in model object ******/
                        Log.d("arpit prajapati",s_city_split[0] + "\n" +d_city_split[0]+ "\n" +truc_type_from_jsondata+ "\n" +exp_val_from_json);
                        sched3.setSourcecity2(s_city_split[0]);
                        sched3.setDestinationcity2(d_city_split[0]);
                        sched3.setTrucktype(truc_type_from_jsondata);
                        sched3.setPrice2(exp_val_from_json);
                        sched3.setAdvPrice(load_advance_in_percentage_from_json);
                        sched3.setDate2(scheduled_date_from_json);
                        sched3.setMaterialType(load_material_from_jsondata);
                        //         sched3.setPostedby2(posted_by_name);

                        /******** Take Model Object in ArrayList **********/
                        CustomListViewValuesArr3.add(sched3);
                    }
                }

            } catch (JSONException e) {e.printStackTrace();}
            dialogue.dismiss();
        }
    }

    @Override
    public void onClick(View view) {

    }


    public void onItemClick(int mPosition) {
//        Toast.makeText(con, "dhjkdghkjshg", Toast.LENGTH_SHORT).show();
    }




}
