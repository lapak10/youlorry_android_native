package com.youlorry.ap.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

/**
 * Created by Arpit Prajapati on 12/15/16.
 */

public class Quotationfortransporter extends AppCompatActivity implements TabLayout.OnTabSelectedListener, ViewPager.OnPageChangeListener{

        Context con;
        private TabLayout tabLayout;
        private ViewPager viewPager;
        private int request_page;

        @Override
        protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.quotation_for_transporter);


            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (android.os.Build.VERSION.SDK_INT > 9)
            {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }

            YoulorrySession session = new YoulorrySession(this);

            if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

                Intent in = new Intent(this , LoginActivity.class);
                startActivity(in);

            }
            con = this;
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            Bundle bundle = getIntent().getExtras();
            if(bundle != null) {
                request_page = bundle.getInt("request_page");
            }

            tabLayout = (TabLayout) findViewById(R.id.tabLayout);

            //Adding the tabs using addTab() method
            tabLayout.addTab(tabLayout.newTab().setText("Sent"));
            tabLayout.addTab(tabLayout.newTab().setText("Received"));
            tabLayout.addTab(tabLayout.newTab().setText("Confirmed"));
            tabLayout.addTab(tabLayout.newTab().setText("Cancelled"));
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

            //Initializing viewPager
            viewPager = (ViewPager) findViewById(R.id.pager);

            //Creating our pager adapter
            Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount());

            //Adding adapter to pager
            viewPager.setAdapter(adapter);

            viewPager.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return false;
                }
            });

            viewPager.setCurrentItem(request_page);
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
   //         new TheTask().execute();

            //Adding onTabSelectedListener to swipe views
            tabLayout.addOnTabSelectedListener(this);

        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case android.R.id.home:
//                   NavUtils.navigateUpFromSameTask(this);
                    finish();
                    startActivity(new Intent(this, MainActivity.class));
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    return true;
                 default:
                    return super.onOptionsItemSelected(item);
            }
        }

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            viewPager.setCurrentItem(tab.getPosition());
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        startActivity(new Intent(this, MainActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        viewPager.setCurrentItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


/*        class TheTask extends AsyncTask<Void, Void, String> {


            ProgressDialog dialogue;


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialogue = new ProgressDialog(con);
                dialogue.setTitle("Loading ...");
                dialogue.show();
            }

            @Override
            protected String doInBackground(Void... voids) {

                YoulorrySession session = new YoulorrySession(con);
                String username = session.getusername();
                String password = session.getpass_word();
                String userid = session.getuser_id();
                String role = session.getrole();

                StringBuilder sb = new StringBuilder();
                URL urlObj = null;
                try {
                    urlObj = new URL("http://nishabhati.com/api-fetch-my-all-quotations/");
                    HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);
                    urlConnection.setUseCaches(false);
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    //  urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setRequestMethod("POST");
                    urlConnection.connect();

                    JSONObject cred = new JSONObject();

                    cred.put("username",username);
                    cred.put("password",password);
                    cred.put("userid",userid);
                    cred.put("role",role);

                    OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(cred.toString());
                    // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                    wr.flush();
                    wr.close();

                    //display what returns the POST request

                    int HttpResult = urlConnection.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                        BufferedReader br = new BufferedReader(
                                new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                        }
                        br.close();

//                    Toast.makeText(con, sb.toString() , Toast.LENGTH_LONG).show();
                        Log.d("transpoter_quotations",sb.toString());
                    }

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                return sb.toString();
            }


            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

//            Toast.makeText(con, s.toString() , Toast.LENGTH_SHORT).show();
                YoulorrySession session_2 = new YoulorrySession(con);

                session_2.setCustomerQuotations(s.toString());
                //  Toast.makeText(con, session_2.getCustomerQuotations() , Toast.LENGTH_SHORT).show();

                dialogue.dismiss();
            }
        }
*/

}
