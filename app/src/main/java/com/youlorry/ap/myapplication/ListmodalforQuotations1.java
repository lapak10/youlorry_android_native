package com.youlorry.ap.myapplication;

/**
 * Created by Arpit Prajapati on 12/14/16.
 */

public class ListmodalforQuotations1 {

    private  String S_city="";
    private  String D_city="";
    private  String Schedule_date="";
    private  String Quoted_amount;
    private  String PostStatus1;
    private  String Quote_ID1;
    private  String vehicleNumber="";
    private  String materialType="";
    private  String weight="";
    private  String advanceAmount="";

    /*********** Set Methods ******************/

    public void setSourcecity1(String S_city)
    {
        this.S_city = S_city;
    }

    public void setDestinationcity1(String D_city)
    {
        this.D_city = D_city;
    }

    public void setScheduleDate(String schedule_date)
    {
        this.Schedule_date = schedule_date;
    }

    public void setQuotedAmt(String Price)
    {
        this.Quoted_amount = Price;
    }

    public void setPostStatus(String postStatus1){ this.PostStatus1 = postStatus1; }

    public void setQuote_ID(String quote_id1){ this.Quote_ID1 = quote_id1; }

    public void setVehicleNumber(String vehicleNumber){ this.vehicleNumber = vehicleNumber; }

    public void setMaterialType(String materialType){ this.materialType = materialType; }

    public void setWeight(String weight){ this.weight = weight; }

    public void setAdvanceAmount(String advanceAmount){ this.advanceAmount = advanceAmount; }

    /*********** Get Methods ****************/

    public String getSourcecity1()
    {
        return this.S_city;
    }

    public String getDestinationcity1() { return this.D_city; }

    public String getScheduleDate()
    {
        return this.Schedule_date;
    }

    public String getQuotedAmt()
    {
        return this.Quoted_amount;
    }

    public String getPostStatus()
    {
        return this.PostStatus1;
    }

    public String getQuote_ID1() { return this.Quote_ID1; }

    public String getVehicleNumber() { return this.vehicleNumber; }

    public String getMaterialType() { return this.materialType; }

    public String getWeight() { return this.weight; }

    public String getAdvanceAmount() { return this.advanceAmount; }

}
