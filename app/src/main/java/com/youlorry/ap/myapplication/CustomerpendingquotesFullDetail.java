package com.youlorry.ap.myapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Arpit Prajapati on 12/15/16.
 */
public class CustomerpendingquotesFullDetail extends Activity implements View.OnClickListener{

    String position_of_list_in_c_pending_list,quotation_id,fetching_page_id;
    String data="";
    Context context;
    Button c_pending_btn,c_accept_btn,c_done_btn,c_cancel_btn;
    TextView show_c_quotation_data,load_id,pickup_city,drop_city,weight,pickup_date,exp_val,q_val;
    TextView vehicle_number_heading,load_id_heading,pickup_city_heading,drop_city_heading,weight_heading,pickup_date_heading,exp_val_heading,
            q_val_heading,pickup_point_heading,drop_point_heading,advance_heading,pickup_time_heading,freight_provider_heading,
            company_name_heading,name_heading,mob_heading,loading_point_heading,unloading_point_heading,f_p_pickup_time_heading,
            agreed_f_amt_heading,f_p_adv_amt_heading,truck_supplier_heading,t_s_company_name_heading,t_s_name_heading,t_s_mob_heading,
            t_s_driver_name_heading,t_s_driver_number_heading,t_s_driver_license_number_heading,our_details_heading,o_d_loading_point_heading,
            o_d_unloading_point_heading,o_d_pickup_time_heading,o_d_adv_amt_heading,vehicle_number,pickup_point,drop_point,advance,
            pickup_time,company_name,name,mob,loading_point,unloading_point,f_p_pickup_time,agreed_f_amt,f_p_adv_amt,truck_supplier
            ,t_s_company_name,t_s_name,t_s_mob,t_s_driver_name,t_s_driver_number,t_s_driver_license_number,o_d_loading_point,
            o_d_unloading_point,o_d_pickup_time,o_d_adv_amt;
    TextView line_under_vehicle_number,line_under_load_id,line_under_pickup_city,line_under_drop_city,line_under_weight,line_under_pickup_date,
             line_under_expected_value,line_under_quoted_value,line_under_pickup_point,line_under_drop_point,line_under_advance,line_under_pickup_time,
             line_under_company_name,line_under_freight_provider_name,line_under_mobile_number,line_under_loading_point,line_under_unloading_point,
             line_under_pickup_time1,line_under_agreed_freight_amount,line_under_advance_amount,line_under_t_s_company_name,line_under_t_s_freight_provider_name,
             line_under_t_s_mobile_number,line_under_driver_name,line_under_t_s_driver_number,line_under_t_s_driver_license_number,line_under_o_d_loading_point,
             line_under_o_d_unloading_point,line_under_o_d_pickup_time1,line_under_o_d_advance_amount;
    String id,post_status1;
    ImageView top_cancel_button;
    EditText driver_license_number_at_accept_quote,dn_at_accept_quote,dc_at_accept_quote,pa_at_accept_quote,da_at_accept_quote;
    TextView load_id_for_customer,load_id_heading_for_customer,line_under_load_id_for_customer,our_details_for_tansporter,
            t_o_d_driver_name_inmodalview,t_o_d_driver_name_in_modal_view,line_under_t_o_d_driver_name,
            t_o_d_driver_number_inmodalview,t_o_d_driver_number_in_modal_view,line_under_t_o_d_driver_number,
            t_o_d_driver_license_number_inmodalview,t_o_d_driver_license_number_in_modal_view,line_under_t_o_d_driver_license_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        position_of_list_in_c_pending_list = bundle.getString("position_of_list_in_c_pending_list");
        quotation_id = bundle.getString("quotation_id_from_listmodal_adapter");
        fetching_page_id = bundle.getString("from_pending");

        if(fetching_page_id.trim().toString().equals("cancelled_req")){
            setContentView(R.layout.customer_pending_quotes_full_detail);
        }else if(fetching_page_id.trim().toString().equals("pending_req")){
            setContentView(R.layout.customer_pending_quotes_full_detail);
        }else if(fetching_page_id.trim().toString().equals("sent_req")){
            setContentView(R.layout.customer_pending_quotes_full_detail);
        }else if(fetching_page_id.trim().toString().equals("confirmed_req")){
            setContentView(R.layout.confirmed_quotation_full_detail);

            load_id_for_customer = (TextView) findViewById(R.id.q_load_id_in_modal_view1);
            load_id_heading_for_customer = (TextView) findViewById(R.id.load_id_inmodalview1);
            line_under_load_id_for_customer = (TextView) findViewById(R.id.line_under_load_id1);
            our_details_for_tansporter = (TextView) findViewById(R.id.our_details_heading_text1);
            t_o_d_driver_number_inmodalview = (TextView) findViewById(R.id.t_o_d_driver_number_inmodalview);
            t_o_d_driver_number_in_modal_view = (TextView) findViewById(R.id.t_o_d_driver_number_in_modal_view);
            line_under_t_o_d_driver_number = (TextView) findViewById(R.id.line_under_t_o_d_driver_number);
            t_o_d_driver_license_number_inmodalview = (TextView) findViewById(R.id.t_o_d_driver_license_number_inmodalview);
            t_o_d_driver_license_number_in_modal_view = (TextView) findViewById(R.id.t_o_d_driver_license_number_in_modal_view);
            line_under_t_o_d_driver_license_number = (TextView) findViewById(R.id.line_under_t_o_d_driver_license_number);
            t_o_d_driver_name_inmodalview = (TextView) findViewById(R.id.t_o_d_driver_name_inmodalview);
            t_o_d_driver_name_in_modal_view = (TextView) findViewById(R.id.t_o_d_driver_name_in_modal_view);
            line_under_t_o_d_driver_name = (TextView) findViewById(R.id.line_under_t_o_d_driver_name);
        }

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        YoulorrySession session = new YoulorrySession(this);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(this , LoginActivity.class);
            startActivity(in);

        }

        context = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        show_c_quotation_data = (TextView) findViewById(R.id.c_quotes_info_in_modal_view);
        c_pending_btn = (Button) findViewById(R.id.c_pending_button_id);
        c_accept_btn = (Button) findViewById(R.id.c_accept_btn_id);
      //  c_done_btn = (Button) findViewById(R.id.c_done_btn_id);
        c_cancel_btn = (Button) findViewById(R.id.c_cancel_btn_id);
        top_cancel_button = (ImageView) findViewById(R.id.top_cancel_btn);
        driver_license_number_at_accept_quote = (EditText) findViewById(R.id.driver_license_number_at_accept_quote);
        dn_at_accept_quote = (EditText) findViewById(R.id.driver_name_at_accept_quote);
        dc_at_accept_quote = (EditText) findViewById(R.id.driver_number_at_accept_quote);
        pa_at_accept_quote = (EditText) findViewById(R.id.pickup_address_at_accept_quote);
        da_at_accept_quote = (EditText) findViewById(R.id.drop_address_at_accept_quote);

        load_id = (TextView) findViewById(R.id.q_load_id_in_modal_view);
        pickup_city = (TextView) findViewById(R.id.q_pickup_city_in_modal_view);
        drop_city = (TextView) findViewById(R.id.q_drop_city_in_modal_view);
        weight = (TextView) findViewById(R.id.q_weight_in_modal_view);
        pickup_date = (TextView) findViewById(R.id.q_pickup_date_in_modal_view);
        exp_val = (TextView) findViewById(R.id.q_expected_value_in_modal_view);
        q_val = (TextView) findViewById(R.id.q_quoted_value_in_modal_view);

        vehicle_number_heading = (TextView) findViewById(R.id.vehicle_number_inmodalview);
        load_id_heading = (TextView) findViewById(R.id.load_id_inmodalview);
        pickup_city_heading = (TextView) findViewById(R.id.pickup_city_inmodalview);
        drop_city_heading = (TextView) findViewById(R.id.drop_city_inmodalview);
        weight_heading = (TextView) findViewById(R.id.weight_inmodalview);
        pickup_date_heading = (TextView) findViewById(R.id.pickup_date_inmodalview);
        exp_val_heading = (TextView) findViewById(R.id.expected_value_inmodalview);
        q_val_heading = (TextView) findViewById(R.id.quoted_value_inmodalview);
        pickup_point_heading = (TextView) findViewById(R.id.pickup_point_inmodalview);
        drop_point_heading = (TextView) findViewById(R.id.drop_point_inmodalview);
        advance_heading = (TextView) findViewById(R.id.advance_inmodalview);
        pickup_time_heading = (TextView) findViewById(R.id.pickup_time_inmodalview);
        freight_provider_heading = (TextView) findViewById(R.id.freight_provider_heading_text);
        company_name_heading = (TextView) findViewById(R.id.company_name_inmodalview);
        name_heading = (TextView) findViewById(R.id.freight_provider_name_inmodalview);
        mob_heading = (TextView) findViewById(R.id.mobile_number_inmodalview);
        loading_point_heading = (TextView) findViewById(R.id.loading_point_inmodalview);
        unloading_point_heading = (TextView) findViewById(R.id.unloading_point_inmodalview);
        f_p_pickup_time_heading = (TextView) findViewById(R.id.pickup_time1_inmodalview);
        agreed_f_amt_heading = (TextView) findViewById(R.id.agreed_freight_amount_inmodalview);
        f_p_adv_amt_heading = (TextView) findViewById(R.id.advance_amount_inmodalview);
        truck_supplier_heading = (TextView) findViewById(R.id.truck_supplier_heading_text);
        t_s_company_name_heading = (TextView) findViewById(R.id.t_s_company_name_inmodalview);
        t_s_name_heading = (TextView) findViewById(R.id.t_s_freight_provider_name_inmodalview);
        t_s_mob_heading = (TextView) findViewById(R.id.t_s_mobile_number_inmodalview);
        t_s_driver_name_heading = (TextView) findViewById(R.id.t_s_driver_name_inmodalview);
        t_s_driver_number_heading = (TextView) findViewById(R.id.t_s_driver_number_inmodalview);
        t_s_driver_license_number_heading = (TextView) findViewById(R.id.t_s_driver_license_number_inmodalview);
        our_details_heading = (TextView) findViewById(R.id.our_details_heading_text);
        o_d_loading_point_heading = (TextView) findViewById(R.id.o_d_loading_point_inmodalview);
        o_d_unloading_point_heading = (TextView) findViewById(R.id.o_d_unloading_point_inmodalview);
        o_d_pickup_time_heading = (TextView) findViewById(R.id.o_d_pickup_time1_inmodalview);
        o_d_adv_amt_heading = (TextView) findViewById(R.id.o_d_advance_amount_inmodalview);

        vehicle_number = (TextView) findViewById(R.id.q_vehicle_number_in_modal_view);
        pickup_point = (TextView) findViewById(R.id.q_pickup_point_in_modal_view);
        drop_point = (TextView) findViewById(R.id.q_drop_point_in_modal_view);
        advance = (TextView) findViewById(R.id.q_advance_in_modal_view);
        pickup_time = (TextView) findViewById(R.id.q_pickup_time_in_modal_view);
        company_name = (TextView) findViewById(R.id.q_company_name_in_modal_view);
        name = (TextView) findViewById(R.id.q_freight_provider_name_in_modal_view);
        mob = (TextView) findViewById(R.id.q_mobile_number_in_modal_view);
        loading_point = (TextView) findViewById(R.id.q_loading_point_in_modal_view);
        unloading_point = (TextView) findViewById(R.id.q_unloading_point_in_modal_view);
        f_p_pickup_time = (TextView) findViewById(R.id.q_pickup_time1_in_modal_view);
        agreed_f_amt = (TextView) findViewById(R.id.q_agreed_freight_amount_in_modal_view);
        f_p_adv_amt = (TextView) findViewById(R.id.q_advance_amount_in_modal_view);
        t_s_company_name = (TextView) findViewById(R.id.q_t_s_company_name_in_modal_view);
        t_s_name = (TextView) findViewById(R.id.q_t_s_freight_provider_name_in_modal_view);
        t_s_mob = (TextView) findViewById(R.id.q_t_s_mobile_number_in_modal_view);
        t_s_driver_name = (TextView) findViewById(R.id.q_t_s_driver_name_in_modal_view);
        t_s_driver_number = (TextView) findViewById(R.id.q_t_s_driver_number_in_modal_view);
        t_s_driver_license_number = (TextView) findViewById(R.id.q_t_s_driver_license_number_in_modal_view);
        o_d_loading_point = (TextView) findViewById(R.id.q_o_d_loading_point_in_modal_view);
        o_d_unloading_point = (TextView) findViewById(R.id.q_o_d_unloading_point_in_modal_view);
        o_d_pickup_time = (TextView) findViewById(R.id.q_o_d_pickup_time1_in_modal_view);
        o_d_adv_amt = (TextView) findViewById(R.id.q_o_d_advance_amount_in_modal_view);

        line_under_vehicle_number = (TextView) findViewById(R.id.line_under_vehicle_number);
        line_under_load_id = (TextView) findViewById(R.id.line_under_load_id);
        line_under_pickup_city = (TextView) findViewById(R.id.line_under_pickup_city);
        line_under_drop_city = (TextView) findViewById(R.id.line_under_drop_city);
        line_under_weight = (TextView) findViewById(R.id.line_under_weight);
        line_under_pickup_date = (TextView) findViewById(R.id.line_under_pickup_date);
        line_under_expected_value = (TextView) findViewById(R.id.line_under_expected_value);
        line_under_quoted_value = (TextView) findViewById(R.id.line_under_quoted_value);
        line_under_pickup_point = (TextView) findViewById(R.id.line_under_pickup_point);
        line_under_drop_point = (TextView) findViewById(R.id.line_under_drop_point);
        line_under_advance = (TextView) findViewById(R.id.line_under_advance);
        line_under_pickup_time = (TextView) findViewById(R.id.line_under_pickup_time);
        line_under_company_name = (TextView) findViewById(R.id.line_under_company_name);
        line_under_freight_provider_name = (TextView) findViewById(R.id.line_under_freight_provider_name);
        line_under_mobile_number = (TextView) findViewById(R.id.line_under_mobile_number);
        line_under_loading_point = (TextView) findViewById(R.id.line_under_loading_point);
        line_under_unloading_point = (TextView) findViewById(R.id.line_under_unloading_point);
        line_under_pickup_time1 = (TextView) findViewById(R.id.line_under_pickup_time1);
        line_under_agreed_freight_amount = (TextView) findViewById(R.id.line_under_agreed_freight_amount);
        line_under_advance_amount = (TextView) findViewById(R.id.line_under_advance_amount);
        line_under_t_s_company_name = (TextView) findViewById(R.id.line_under_t_s_company_name);
        line_under_t_s_freight_provider_name = (TextView) findViewById(R.id.line_under_t_s_freight_provider_name);
        line_under_t_s_mobile_number = (TextView) findViewById(R.id.line_under_t_s_mobile_number);
        line_under_driver_name = (TextView) findViewById(R.id.line_under_driver_name);
        line_under_t_s_driver_number = (TextView) findViewById(R.id.line_under_t_s_driver_number);
        line_under_t_s_driver_license_number = (TextView) findViewById(R.id.line_under_t_s_driver_license_number);
        line_under_o_d_loading_point = (TextView) findViewById(R.id.line_under_o_d_loading_point);
        line_under_o_d_unloading_point = (TextView) findViewById(R.id.line_under_o_d_unloading_point);
        line_under_o_d_pickup_time1 = (TextView) findViewById(R.id.line_under_o_d_pickup_time1);
        line_under_o_d_advance_amount = (TextView) findViewById(R.id.line_under_o_d_advance_amount);


        c_pending_btn.setOnClickListener(this);
        c_accept_btn.setOnClickListener(this);
      //  c_done_btn.setOnClickListener(this);
        c_cancel_btn.setOnClickListener(this);
        top_cancel_button.setOnClickListener(this);

        new TheTask().execute();

    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.top_cancel_btn){
            finish();
        }

        if(view.getId() == R.id.c_pending_button_id){
//            Toast.makeText(context, "Pending Clicked!", Toast.LENGTH_SHORT).show();
            finish();
        }

        if(view.getId() == R.id.c_accept_btn_id){

            if(new YoulorrySession(context).getrole().equals("transporter")){

                    //   Toast.makeText(context, "Accepted!!!", Toast.LENGTH_SHORT).show();
                    finish();
                    StringBuilder sb = new StringBuilder();
                    //     new Thread(new Runnable() {
                    //      public void run() {

                    YoulorrySession session12 = new YoulorrySession(context);
                    String username = session12.getusername();
                    String password = session12.getpass_word();
                    String userid = session12.getuser_id();
                    String role = session12.getrole();

                    URL urlObj = null;
                    try {
                        urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-fetch-my-all-quotations/");
                        HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                        urlConnection.setDoOutput(true);
                        urlConnection.setDoInput(true);
                        urlConnection.setUseCaches(false);
                        urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                        //  urlConnection.setRequestProperty("Accept", "application/json");
                        urlConnection.setRequestMethod("POST");
                        urlConnection.connect();

                        JSONObject cred = new JSONObject();

                            cred.put("username",username);
                            cred.put("password",password);
                            cred.put("userid",userid);
                            cred.put("role",role);
                            cred.put("quote_id",id);
                            cred.put("driver_license_number",driver_license_number_at_accept_quote.getText().toString());
                            cred.put("driver_name",dn_at_accept_quote.getText().toString());
                            cred.put("driver_number",dc_at_accept_quote.getText().toString());
                            cred.put("action","accept");

                        OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                        wr.write(cred.toString());
                        wr.flush();
                        wr.close();

                        //display what returns the POST request

                        int HttpResult = urlConnection.getResponseCode();
                        if (HttpResult == HttpURLConnection.HTTP_OK) {
                            BufferedReader br = new BufferedReader(
                                    new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                            String line = null;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            br.close();

                        }

                    } catch (ProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //         }
                    //     }).start();

                    Log.d("return-value" , sb.toString());

                    if(sb.toString().equals("ok")){
                        Toast.makeText(context, "Success", Toast.LENGTH_LONG).show();

                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                startActivity(new Intent(context, MainActivity.class));
                                finish();
                            }
                        },500);
//                         startActivity(new Intent(this, MainActivity.class));
                    }else{

                        Toast.makeText(context, "Failed", Toast.LENGTH_LONG).show();

                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                startActivity(new Intent(context, MainActivity.class));
                                finish();
                            }
                        },500);
                    }

            }
            if(new YoulorrySession(context).getrole().equals("customer")){

                if(pa_at_accept_quote.getText().toString().length()!=0 && da_at_accept_quote.getText().toString().length()!=0){

                    //   Toast.makeText(context, "Accepted!!!", Toast.LENGTH_SHORT).show();
                    finish();
                    StringBuilder sb = new StringBuilder();
                    //     new Thread(new Runnable() {
                    //      public void run() {

                    YoulorrySession session12 = new YoulorrySession(context);
                    String username = session12.getusername();
                    String password = session12.getpass_word();
                    String userid = session12.getuser_id();
                    String role = session12.getrole();

                    URL urlObj = null;
                    try {
                        urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-fetch-my-all-quotations/");
                        HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                        urlConnection.setDoOutput(true);
                        urlConnection.setDoInput(true);
                        urlConnection.setUseCaches(false);
                        urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                        //  urlConnection.setRequestProperty("Accept", "application/json");
                        urlConnection.setRequestMethod("POST");
                        urlConnection.connect();

                        JSONObject cred = new JSONObject();

                            cred.put("username",username);
                            cred.put("password",password);
                            cred.put("userid",userid);
                            cred.put("role",role);
                            cred.put("quote_id",id);
                            cred.put("load_loading_address",pa_at_accept_quote.getText().toString());
                            cred.put("load_unloading_address",da_at_accept_quote.getText().toString());
                            cred.put("action","accept");

                        OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                        wr.write(cred.toString());
                        wr.flush();
                        wr.close();

                        //display what returns the POST request

                        int HttpResult = urlConnection.getResponseCode();
                        if (HttpResult == HttpURLConnection.HTTP_OK) {
                            BufferedReader br = new BufferedReader(
                                    new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                            String line = null;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            br.close();

                        }

                    } catch (ProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //         }
                    //     }).start();

                    Log.d("return-value" , sb.toString());

                    if(sb.toString().equals("ok")){
                        Toast.makeText(context, "Success", Toast.LENGTH_LONG).show();

                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                startActivity(new Intent(context, MainActivity.class));
                                finish();
                            }
                        },500);
//                         startActivity(new Intent(this, MainActivity.class));
                    }else{

                        Toast.makeText(context, "Failed", Toast.LENGTH_LONG).show();

                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                startActivity(new Intent(context, MainActivity.class));
                                finish();
                            }
                        },500);
                    }


                } else {
                    Toast.makeText(context, "Please fill required details.", Toast.LENGTH_SHORT).show();
                }

            }
        }

  /*      if(view.getId() == R.id.c_done_btn_id){
            Toast.makeText(context, "Confirmed Clicked!", Toast.LENGTH_SHORT).show();
            finish();
        }
*/
        if(view.getId() == R.id.c_cancel_btn_id){
           // Toast.makeText(context, "Cancel Clicked!", Toast.LENGTH_SHORT).show();
            finish();
            Intent in = new Intent(context,QuotationCancelWithReasonActivity.class);

            Bundle args = new Bundle();
            args.putString("quotation_id_for_cancellation", id);

            in.putExtras(args);
            startActivity(in);
        }
    }

    class TheTask extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;
        String str;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(context);
            dialogue.setTitle("Loading ...");
            dialogue.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session_1 = new YoulorrySession(context);
            if(fetching_page_id.equals("pending_req")){
                str = session_1.getPendingQuotations();
            }
            if(fetching_page_id.equals("sent_req")){
                str = session_1.getSentQuotations();
            }
            if(fetching_page_id.equals("confirmed_req")){
                str = session_1.getConfirmedQuotations();
            }
            if(fetching_page_id.equals("cancelled_req")){
                str = session_1.getCancelledQuotations();;
            }

            return str.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);

            int width = dm.widthPixels;
            int height = dm.heightPixels;

            try {

                JSONArray jsonArray = new JSONArray(s);
                //  String [] truck_list_for_list_view_id = new String[jsonArray.length()];

                int truck_list_value_for_json_int = Integer.parseInt(position_of_list_in_c_pending_list);
                //  Resources res =getResources();
                for(int i=0; i < jsonArray.length(); i++){

                    if(i == truck_list_value_for_json_int ){

                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        if(new YoulorrySession(context).getrole().equals("customer")){

                            id = jsonObject.optString("ID").toString();
                            Log.d("quote_id" , id+"");


                            post_status1 = jsonObject.optString("post_status").toString();
                            Log.d("post_status" , post_status1);

                            if(fetching_page_id.trim().toString().equals("cancelled_req")){
                                getWindow().setLayout((int) (width*0.950),(int)(height*0.340));
                            }else if(fetching_page_id.trim().toString().equals("pending_req")){
                                if(new YoulorrySession(context).getrole().equals("transporter")){
                                    if(post_status1.equals("sent_by_truck")){
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.5));
                                    } else if(post_status1.equals("req_cust")) {
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.8));
                                    }
                                } else if(new YoulorrySession(context).getrole().equals("customer")){
                                    if(post_status1.equals("sent_by_truck")){
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.7));
                                    } else if(post_status1.equals("req_cust")) {
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.380));
                                    }
                                }

                            } else if(fetching_page_id.trim().toString().equals("sent_req")){
                                if(new YoulorrySession(context).getrole().equals("transporter")){
                                    if(post_status1.equals("sent_by_truck")){
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.5));
                                    } else if(post_status1.equals("req_cust")) {
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.8));
                                    }
                                } else if(new YoulorrySession(context).getrole().equals("customer")){
                                    if(post_status1.equals("sent_by_truck")){
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.7));
                                    } else if(post_status1.equals("req_cust")) {
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.380));
                                    }
                                }

                            } else if(fetching_page_id.trim().toString().equals("confirmed_req")){
                                getWindow().setLayout((int) (width*0.950),(int)(height*0.650));
                            }


                            if(jsonObject.has("trucker_details")){

                                String trucker_details_jsondata = jsonObject.optString("trucker_details").toString();

                                Log.d("trucker_details" , trucker_details_jsondata);
                                JSONObject jobj4 = new JSONObject(trucker_details_jsondata);

                                if(jobj4.has("first_name")){
                                    String first_name_jsondata=jobj4.getString("first_name");
                                    String first_name_from_json = first_name_jsondata.substring(2, first_name_jsondata.toString().length() - 2);
                                    t_s_name.setText(first_name_from_json);
                                } else {
                                    t_s_name.setText("NA");
                                }
                                if(jobj4.has("transporter_transport_name")) {

                                    String transporter_transport_name_jsondata = jobj4.getString("transporter_transport_name");
                                    String transporter_transport_name_from_json = transporter_transport_name_jsondata.substring(2, transporter_transport_name_jsondata.toString().length() - 2);
                                    t_s_company_name.setText(transporter_transport_name_from_json);
                                } else {
                                    t_s_company_name.setText("NA");
                                }
                            }

/*                            if(jsonObject.has("truck_details")){

                                String truck_details_jsondata = jsonObject.optString("truck_details").toString();

                                Log.d("truck_details" , truck_details_jsondata);

                                JSONObject jobj12 = new JSONObject(truck_details_jsondata);

                                String truck_s_city_jsondata=jobj1.getString("source_city");
                                String truck_s_city_from_json = truck_s_city_jsondata.substring(2, truck_s_city_jsondata.toString().length() - 2);

                                String truck_d_city_jsondata=jobj1.getString("destination_city");
                                String truck_d_city_from_json = truck_d_city_jsondata.substring(2, truck_d_city_jsondata.toString().length() - 2);

                                String truck_expected_frieght_value_jsondata=jobj1.getString("expected_frieght_value");
                                String truck_expected_frieght_value_from_json = truck_expected_frieght_value_jsondata.substring(2, truck_expected_frieght_value_jsondata.toString().length() - 2);

                                String truck_truck_type_jsondata=jobj1.getString("truck_type");
                                String truck_truck_type_from_json = truck_truck_type_jsondata.substring(2, truck_truck_type_jsondata.toString().length() - 2);

                                String truck_date_jsondata=jobj1.getString("date");
                                String truck_date_from_json = truck_date_jsondata.substring(2, truck_date_jsondata.toString().length() - 2);

                                if(jobj12.has("vehicle_number")){
                                    String truck_vehicle_number_jsondata=jobj12.getString("vehicle_number");
                                    String truck_vehicle_number_from_json = truck_vehicle_number_jsondata.substring(2, truck_vehicle_number_jsondata.toString().length() - 2);
                                    vehicle_number.setText(truck_vehicle_number_from_json);
                                } else {
                                    vehicle_number.setText("NA");
                                }

                            }
*/
                            if(jsonObject.has("trucker_number")){
                                String transporter_contact_number_jsondata=jsonObject.getString("trucker_number");
//                                String transporter_contact_number_from_json = transporter_contact_number_jsondata.substring(2, transporter_contact_number_jsondata.toString().length() - 2);
                                Log.d("trucker_number", transporter_contact_number_jsondata);
                                t_s_mob.setText(transporter_contact_number_jsondata);
                            } else {
                                t_s_mob.setText("NA");
                            }

                            if(jsonObject.has("meta_keys")){

                                String meta_keys_jsondata = jsonObject.optString("meta_keys").toString();

                                Log.d("meta_keys" , meta_keys_jsondata);

                                JSONObject jobj = new JSONObject(meta_keys_jsondata);

                                if(jobj.has("initiated_by")){
                                    String initiated_by_jsondata=jobj.getString("initiated_by");
                                    String initiated_by_from_json = initiated_by_jsondata.substring(2, initiated_by_jsondata.toString().length() - 2);
                                } else {

                                }

                                if(jobj.has("load_ID")){
                                    String load_ID_jsondata=jobj.getString("load_ID");
                                    String load_ID_from_json = load_ID_jsondata.substring(2, load_ID_jsondata.toString().length() - 2);
                                    load_id.setText("YLL"+load_ID_from_json);
                                } else {
                                    load_id.setText("NA");
                                }

/*                                if(jobj.has("quoted_amount")){
                                    String quoted_amt_jsondata=jobj.getString("quoted_amount");
                                    String quoted_amt_from_json = quoted_amt_jsondata.substring(2, quoted_amt_jsondata.toString().length() - 2);
                                    q_val.setText("₹ "+quoted_amt_from_json);
                                    agreed_f_amt.setText("₹ "+quoted_amt_from_json);
                                } else {
                                    q_val.setText("NA");
                                    agreed_f_amt.setText("NA");
                                }
*/
//                            String truck_ID_jsondata=jobj.getString("truck_ID");

                                //                          String truck_ID_from_json = truck_ID_jsondata.substring(2, truck_ID_jsondata.toString().length() - 2);

                                if(jobj.has("driver_name") && jobj.has("driver_number") ){

                                    String driver_name_jsondata=jobj.getString("driver_name");
                                    String driver_name_from_json = driver_name_jsondata.substring(2, driver_name_jsondata.toString().length() - 2);

                                    t_s_driver_name.setText(driver_name_from_json);
                                } else {
                                    t_s_driver_name.setText("NA");
                                }

                                if(jobj.has("driver_name") && jobj.has("driver_number") ){
                                     String driver_number_jsondata=jobj.getString("driver_number");
                                     String driver_number_from_json = driver_number_jsondata.substring(2, driver_number_jsondata.toString().length() - 2);
                                     t_s_driver_number.setText(driver_number_from_json);
                                } else {
                                    t_s_driver_number.setText("NA");
                                }


                                if(jobj.has("driver_license_number")){
                                    String driver_license_number_jsondata=jobj.getString("driver_license_number");
                                    String driver_license_number_from_json = driver_license_number_jsondata.substring(2, driver_license_number_jsondata.toString().length() - 2);

                                    t_s_driver_license_number.setText(driver_license_number_from_json);
                                }else {
                                    t_s_driver_license_number.setText("Unavailable");
                                }

                            }




                            data="";

                            if(jsonObject.has("load_details")){

                                String load_details_jsondata = jsonObject.optString("load_details").toString();

                                Log.d("load_details" , load_details_jsondata);

                                JSONObject jobj2 = new JSONObject(load_details_jsondata);

                                if(jobj2.has("load_source_city")){
                                    String load_source_city_jsondata=jobj2.getString("load_source_city");
                                    String load_source_city_from_json = load_source_city_jsondata.substring(2, load_source_city_jsondata.toString().length() - 2);
                                    pickup_city.setText(load_source_city_from_json);
                                } else {
                                    pickup_city.setText("NA");
                                }

/*                                if(jobj2.has("load_expected_frieght_value")){
                                    String load_expected_frieght_value_jsondata=jobj2.getString("load_expected_frieght_value");
                                    String load_expected_frieght_value_from_json = load_expected_frieght_value_jsondata.substring(2, load_expected_frieght_value_jsondata.toString().length() - 2);
                                    exp_val.setText("₹ "+load_expected_frieght_value_from_json);
                                } else {
                                    exp_val.setText("NA");
                                }
*/
                                if(jobj2.has("load_destination_city")){
                                    String load_destination_city_jsondata=jobj2.getString("load_destination_city");
                                    String load_destination_city_from_json = load_destination_city_jsondata.substring(2, load_destination_city_jsondata.toString().length() - 2);
                                    drop_city.setText(load_destination_city_from_json);
                                } else {
                                    drop_city.setText("NA");
                                }

/*                                if(jobj2.has("load_material")){
                                    String load_material_jsondata=jobj2.getString("load_material");
                                    String load_material_from_json = load_material_jsondata.substring(2, load_material_jsondata.toString().length() - 2);
                                } else {

                                }

                                if(jobj2.has("load_truck_type")){
                                    String load_truck_type_jsondata=jobj2.getString("load_truck_type");
                                    String load_truck_type_from_json = load_truck_type_jsondata.substring(2, load_truck_type_jsondata.toString().length() - 2);
                                } else {

                                }
*/
                                if(jobj2.has("load_date")){
                                    String load_date_jsondata=jobj2.getString("load_date");
                                    String load_date_from_json = load_date_jsondata.substring(2, load_date_jsondata.toString().length() - 2);
                                    pickup_date.setText(load_date_from_json);
                                } else {
                                    pickup_date.setText("NA");
                                }

                                if(jobj2.has("load_time")){

                                    String load_time_jsondata=jobj2.getString("load_time");
                                    String load_time_from_json = load_time_jsondata.substring(2, load_time_jsondata.toString().length() - 2);
                                    pickup_time.setText(load_time_from_json);
                                    o_d_pickup_time.setText(load_time_from_json);
                                } else {
                                    pickup_time.setText("NA");
                                    o_d_pickup_time.setText("NA");
                                }

                                if(jobj2.has("load_pickup_point")){
                                    String load_pickup_point_jsondata = jobj2.getString("load_pickup_point");
                                    String load_pickup_point_from_json = load_pickup_point_jsondata.substring(2, load_pickup_point_jsondata.toString().length() - 2);
                                    pickup_point.setText(load_pickup_point_from_json);
                                } else {
                                    pickup_point.setText("Inside City");
                                }

                                if(jobj2.has("load_drop_point")){

                                    String load_drop_point_jsondata = jobj2.getString("load_drop_point");
                                    String load_drop_point_from_json = load_drop_point_jsondata.substring(2, load_drop_point_jsondata.toString().length() - 2);
                                    drop_point.setText(load_drop_point_from_json);
                                } else {
                                    drop_point.setText("Inside City");
                                }

/*                                if(jobj2.has("load_advance_in_percentage") && jobj2.has("load_expected_frieght_value")){
                                    String load_advance_in_percentage_jsondata=jobj2.getString("load_advance_in_percentage");
                                    String load_advance_in_percentage_from_json = load_advance_in_percentage_jsondata.substring(2, load_advance_in_percentage_jsondata.toString().length() - 2);

                                    String load_expected_frieght_value_jsondata=jobj2.getString("load_expected_frieght_value");
                                    String load_expected_frieght_value_from_json = load_expected_frieght_value_jsondata.substring(2, load_expected_frieght_value_jsondata.toString().length() - 2);

                                    int adv_amt_result = Integer.parseInt(load_expected_frieght_value_from_json)*Integer.parseInt(load_advance_in_percentage_from_json)/100;
                                    advance.setText("₹ "+adv_amt_result);
                                    o_d_adv_amt.setText("₹ "+adv_amt_result);

                                } else {

                                    advance.setText("NA");
                                    o_d_adv_amt.setText("NA");

                                }
*/
                                if(jsonObject.has("meta_keys")){

                                    String meta_keys_jsondata = jsonObject.optString("meta_keys").toString();
                                    Log.d("meta_keys_inside_load_details" , meta_keys_jsondata);
                                    JSONObject jobj11 = new JSONObject(meta_keys_jsondata);

                                    if(jobj2.has("load_weight_capacity") && jobj11.has("left_count")) {

                                        String load_weight_capacity_jsondata = jobj2.getString("load_weight_capacity");
                                        String load_weight_capacity_from_json = load_weight_capacity_jsondata.substring(2, load_weight_capacity_jsondata.toString().length() - 2);

                                        String left_count_jsondata = jobj11.getString("left_count");
                                        String left_count_from_json = left_count_jsondata.substring(2, left_count_jsondata.toString().length() - 2);

                                        if (post_status1.equals("req_cust") || post_status1.equals("sent_by_truck")) {
                                            Log.d("weight_in_req_cust_or_sent_by_truck" , left_count_from_json + "/" + load_weight_capacity_from_json);
                                            weight.setText(left_count_from_json + "/" + load_weight_capacity_from_json + " MT");
                                        } else if ( post_status1.equals("truck_pay_ok") ) {
                                            weight.setText(left_count_from_json +" MT");
                                        } else if(jsonObject.has("truck_details")){

                                                String truck_details_jsondata = jsonObject.optString("truck_details").toString();

                                                Log.d("truck_details" , truck_details_jsondata);

                                            if(truck_details_jsondata.equals("false")){
                                                weight.setText("NA");
                                            } else {
                                                JSONObject jobj12 = new JSONObject(truck_details_jsondata);
                                                if(jobj12.has("weight_capacity")){
                                                    String truck_weight_capacity_jsondata=jobj12.getString("weight_capacity");
                                                    String truck_weight_capacity_from_json = truck_weight_capacity_jsondata.substring(2, truck_weight_capacity_jsondata.toString().length() - 2);
                                                    Log.d("weight_for_cancelled_quotation", truck_weight_capacity_from_json);
                                                    if(post_status1.toString().equals("cancel_by_load") || post_status1.equals("cancel_by_truck")){
                                                        weight.setText(truck_weight_capacity_from_json+" MT");
                                                    }
                                                }
                                            }

                                            } else {
                                            weight.setText("NA");
                                        }

                                    }

                                    if(jobj11.has("load_loading_address") && jobj11.has("load_unloading_address")){

                                        String load_loading_address_jsondata=jobj11.getString("load_loading_address");
                                        String load_loading_address_from_json = load_loading_address_jsondata.substring(2, load_loading_address_jsondata.toString().length() - 2);

                                        String load_unloading_address_jsondata=jobj11.getString("load_unloading_address");
                                        String load_unloading_address_from_json = load_unloading_address_jsondata.substring(2, load_unloading_address_jsondata.toString().length() - 2);

                                        o_d_loading_point.setText(load_loading_address_from_json);
                                        o_d_unloading_point.setText(load_unloading_address_from_json);
                                    }else {
                                        o_d_loading_point.setText("NA");
                                        o_d_unloading_point.setText("NA");
                                    }

                                    if(jsonObject.has("truck_details")) {

                                        String truck_details_jsondata = jsonObject.optString("truck_details").toString();
                                        Log.d("truck_datails", truck_details_jsondata);
                                        if(truck_details_jsondata.equals("false")){
                                            vehicle_number.setText("NA");
                                        } else {
                                            JSONObject jobj1 = new JSONObject(truck_details_jsondata);

                                            if(jobj1.has("vehicle_number")){
                                                String truck_vehicle_number_jsondata=jobj1.getString("vehicle_number");
                                                String truck_vehicle_number_from_json = truck_vehicle_number_jsondata.substring(2, truck_vehicle_number_jsondata.toString().length() - 2);

                                                vehicle_number.setText(truck_vehicle_number_from_json);
                                            }else if(jobj11.has("vehicle_number")){
                                                String truck_vehicle_number_jsondata=jobj11.getString("vehicle_number");
                                                String truck_vehicle_number_from_json = truck_vehicle_number_jsondata.substring(2, truck_vehicle_number_jsondata.toString().length() - 2);

                                                vehicle_number.setText(truck_vehicle_number_from_json);
                                            }else {
                                                vehicle_number.setText("NA");
                                            }
                                        }
                                    }

                                    if(jsonObject.has("new_data")){
                                        String new_data_jsondata = jsonObject.optString("new_data").toString();
                                        Log.d("new_data", new_data_jsondata);

                                        JSONObject jobj22 = new JSONObject(new_data_jsondata);

                                        if(jobj22.has("new_part_amount")){
                                            String new_part_amount_jsondata=jobj22.getString("new_part_amount");
                                            String new_part_amount_from_json = new_part_amount_jsondata.substring(2, new_part_amount_jsondata.toString().length() - 2);

                                            exp_val.setText("");
                                            exp_val.setText("₹ "+new_part_amount_jsondata);
                                        } else{
                                            exp_val.setText("NA");
                                        }

                                        if(jobj22.has("new_full_amount")){
                                            String new_full_amount_jsondata=jobj22.getString("new_full_amount");
                                            String new_full_amount_from_json = new_full_amount_jsondata.substring(2, new_full_amount_jsondata.toString().length() - 2);

                                            q_val.setText("");
                                            agreed_f_amt.setText("");
                                            q_val.setText("₹ "+new_full_amount_jsondata);
                                            agreed_f_amt.setText("₹ "+new_full_amount_jsondata);
                                            o_d_adv_amt.setText("");
                                            o_d_adv_amt.setText("₹ "+new_full_amount_jsondata);
                                        } else{
                                            q_val.setText("NA");
                                            agreed_f_amt.setText("NA");
                                            o_d_adv_amt.setText("NA");
                                        }

                                        if(jobj22.has("new_advance_amount")){
                                            String new_advance_amount_jsondata=jobj22.getString("new_advance_amount");
                                            String new_advance_amount_from_json = new_advance_amount_jsondata.substring(2, new_advance_amount_jsondata.toString().length() - 2);

                                            advance.setText("");
                                            advance.setText("₹ "+new_advance_amount_jsondata);

                                        } else{
                                            advance.setText("NA");
                                        }

                                    }


                                }


                            }

                            if(post_status1.trim().toString().equals("sent_by_truck")){

                                Log.d("sent_by_truck","RUNNING");
                                c_accept_btn.setVisibility(View.VISIBLE);
                                c_pending_btn.setVisibility(View.GONE);
                                //  c_done_btn.setVisibility(View.GONE);
                                c_cancel_btn.setVisibility(View.GONE);
                                driver_license_number_at_accept_quote.setVisibility(View.GONE);
                                dn_at_accept_quote.setVisibility(View.GONE);
                                dc_at_accept_quote.setVisibility(View.GONE);
                                pa_at_accept_quote.setVisibility(View.VISIBLE);
                                da_at_accept_quote.setVisibility(View.VISIBLE);

//                                vehicle_number_heading.setVisibility(View.VISIBLE);
//                                vehicle_number.setVisibility(View.VISIBLE);
                                load_id_heading.setVisibility(View.VISIBLE);
                                load_id.setVisibility(View.VISIBLE);
                                pickup_city_heading.setVisibility(View.VISIBLE);
                                pickup_city.setVisibility(View.VISIBLE);
                                drop_city_heading.setVisibility(View.VISIBLE);
                                drop_city.setVisibility(View.VISIBLE);
                                weight_heading.setVisibility(View.VISIBLE);
                                weight.setVisibility(View.VISIBLE);
                                pickup_date_heading.setVisibility(View.VISIBLE);
                                pickup_date.setVisibility(View.VISIBLE);
                                exp_val_heading.setVisibility(View.VISIBLE);
                                exp_val.setVisibility(View.VISIBLE);
                                q_val_heading.setVisibility(View.VISIBLE);
                                q_val.setVisibility(View.VISIBLE);
                                advance_heading.setVisibility(View.VISIBLE);
                                advance.setVisibility(View.VISIBLE);
                                pickup_time_heading.setVisibility(View.VISIBLE);
                                pickup_time.setVisibility(View.VISIBLE);

//                              line_under_vehicle_number.setVisibility(View.VISIBLE);
                                line_under_load_id.setVisibility(View.VISIBLE);
                                line_under_pickup_city.setVisibility(View.VISIBLE);
                                line_under_drop_city.setVisibility(View.VISIBLE);
                                line_under_weight.setVisibility(View.VISIBLE);
                                line_under_pickup_date.setVisibility(View.VISIBLE);
                                line_under_expected_value.setVisibility(View.VISIBLE);
                                line_under_quoted_value.setVisibility(View.VISIBLE);
                                line_under_advance.setVisibility(View.VISIBLE);
                                line_under_pickup_time.setVisibility(View.VISIBLE);
                            }

                            if(post_status1.trim().toString().equals("req_cust")){

                                Log.d("req_cust","RUNNING");
                                c_pending_btn.setVisibility(View.VISIBLE);
                                c_accept_btn.setVisibility(View.GONE);
                                //    c_done_btn.setVisibility(View.GONE);
                                c_cancel_btn.setVisibility(View.GONE);

                                load_id_heading.setVisibility(View.VISIBLE);
                                load_id.setVisibility(View.VISIBLE);
                                pickup_city_heading.setVisibility(View.VISIBLE);
                                pickup_city.setVisibility(View.VISIBLE);
                                drop_city_heading.setVisibility(View.VISIBLE);
                                drop_city.setVisibility(View.VISIBLE);
                                weight_heading.setVisibility(View.VISIBLE);
                                weight.setVisibility(View.VISIBLE);
                                pickup_date_heading.setVisibility(View.VISIBLE);
                                pickup_date.setVisibility(View.VISIBLE);
                                exp_val_heading.setVisibility(View.VISIBLE);
                                exp_val.setVisibility(View.VISIBLE);
                                q_val_heading.setVisibility(View.VISIBLE);
                                q_val.setVisibility(View.VISIBLE);

                                line_under_load_id.setVisibility(View.VISIBLE);
                                line_under_pickup_city.setVisibility(View.VISIBLE);
                                line_under_drop_city.setVisibility(View.VISIBLE);
                                line_under_weight.setVisibility(View.VISIBLE);
                                line_under_pickup_date.setVisibility(View.VISIBLE);
                                line_under_expected_value.setVisibility(View.VISIBLE);
                                line_under_quoted_value.setVisibility(View.VISIBLE);
                            }

                            if(post_status1.trim().toString().equals("truck_pay_ok")){
                                Log.d("truck_pay_ok","RUNNING");
                                //   c_done_btn.setVisibility(View.VISIBLE);
                                c_cancel_btn.setVisibility(View.VISIBLE);
                                c_pending_btn.setVisibility(View.GONE);
                                c_accept_btn.setVisibility(View.GONE);

                                our_details_heading.setVisibility(View.VISIBLE);
                                load_id_for_customer.setText("YLL"+id);
                                load_id_heading_for_customer.setVisibility(View.VISIBLE);
                                load_id_for_customer.setVisibility(View.VISIBLE);
                                line_under_load_id_for_customer.setVisibility(View.VISIBLE);
                                o_d_loading_point_heading.setVisibility(View.VISIBLE);
                                o_d_loading_point.setVisibility(View.VISIBLE);
                                line_under_o_d_loading_point.setVisibility(View.VISIBLE);
                                o_d_unloading_point.setVisibility(View.VISIBLE);
                                o_d_unloading_point_heading.setVisibility(View.VISIBLE);
                                line_under_o_d_unloading_point.setVisibility(View.VISIBLE);
                                o_d_pickup_time.setVisibility(View.VISIBLE);
                                o_d_pickup_time_heading.setVisibility(View.VISIBLE);
                                line_under_o_d_pickup_time1.setVisibility(View.VISIBLE);
                                o_d_adv_amt.setVisibility(View.VISIBLE);
                                o_d_adv_amt_heading.setVisibility(View.VISIBLE);
                                line_under_o_d_advance_amount.setVisibility(View.VISIBLE);

                                truck_supplier_heading.setVisibility(View.VISIBLE);
                                t_s_company_name_heading.setVisibility(View.VISIBLE);
                                t_s_company_name.setVisibility(View.VISIBLE);
                                t_s_name_heading.setVisibility(View.VISIBLE);
                                t_s_name.setVisibility(View.VISIBLE);
                                t_s_mob_heading.setVisibility(View.VISIBLE);
                                t_s_mob.setVisibility(View.VISIBLE);
                                t_s_driver_name_heading.setVisibility(View.VISIBLE);
                                t_s_driver_name.setVisibility(View.VISIBLE);
                                t_s_driver_number_heading.setVisibility(View.VISIBLE);
                                t_s_driver_number.setVisibility(View.VISIBLE);
                                t_s_driver_license_number_heading.setVisibility(View.VISIBLE);
                                t_s_driver_license_number.setVisibility(View.VISIBLE);

                                line_under_t_s_company_name.setVisibility(View.VISIBLE);
                                line_under_t_s_freight_provider_name.setVisibility(View.VISIBLE);
                                line_under_t_s_mobile_number.setVisibility(View.VISIBLE);
                                line_under_driver_name.setVisibility(View.VISIBLE);
                                line_under_t_s_driver_number.setVisibility(View.VISIBLE);
                                line_under_t_s_driver_license_number.setVisibility(View.VISIBLE);
                            }
                            if(post_status1.trim().toString().equals("cancel_by_truck") || post_status1.trim().toString().equals("cancel_by_load")){
                                Log.d("cancel_by_truck_or_load","Running");
                                //    c_done_btn.setVisibility(View.VISIBLE);
                                c_cancel_btn.setVisibility(View.GONE);
                                c_pending_btn.setVisibility(View.GONE);
                                c_accept_btn.setVisibility(View.GONE);

                                vehicle_number_heading.setVisibility(View.VISIBLE);
                                vehicle_number.setVisibility(View.VISIBLE);
                                load_id_heading.setVisibility(View.VISIBLE);
                                load_id.setVisibility(View.VISIBLE);
                                pickup_city_heading.setVisibility(View.VISIBLE);
                                pickup_city.setVisibility(View.VISIBLE);
                                drop_city_heading.setVisibility(View.VISIBLE);
                                drop_city.setVisibility(View.VISIBLE);
                                weight_heading.setVisibility(View.VISIBLE);
                                weight.setVisibility(View.VISIBLE);
                                pickup_date_heading.setVisibility(View.VISIBLE);
                                pickup_date.setVisibility(View.VISIBLE);
                                exp_val_heading.setVisibility(View.VISIBLE);
                                exp_val.setVisibility(View.VISIBLE);
                                q_val_heading.setVisibility(View.VISIBLE);
                                q_val.setVisibility(View.VISIBLE);

                                line_under_vehicle_number.setVisibility(View.VISIBLE);
                                line_under_load_id.setVisibility(View.VISIBLE);
                                line_under_pickup_city.setVisibility(View.VISIBLE);
                                line_under_drop_city.setVisibility(View.VISIBLE);
                                line_under_weight.setVisibility(View.VISIBLE);
                                line_under_pickup_date.setVisibility(View.VISIBLE);
                                line_under_expected_value.setVisibility(View.VISIBLE);
                                line_under_quoted_value.setVisibility(View.VISIBLE);
                            }
                        }

                        if(new YoulorrySession(context).getrole().equals("transporter")){

                            id = jsonObject.optString("ID").toString();
                            Log.d("quote_id" , id+"");


                            post_status1 = jsonObject.optString("post_status").toString();
                            Log.d("post_status", post_status1);

                            if(fetching_page_id.trim().toString().equals("cancelled_req")){
                                getWindow().setLayout((int) (width*0.950),(int)(height*0.340));
                            } else if(fetching_page_id.trim().toString().equals("pending_req")){
                                if(new YoulorrySession(context).getrole().equals("transporter")){
                                    if(post_status1.equals("sent_by_truck")){
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.420));
                                    } else if(post_status1.equals("req_cust")) {
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.750));
                                    }
                                } else if(new YoulorrySession(context).getrole().equals("customer")){
                                    if(post_status1.equals("sent_by_truck")){
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.7));
                                    } else if(post_status1.equals("req_cust")) {
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.5));
                                    }
                                }

                            } else if(fetching_page_id.trim().toString().equals("sent_req")){
                                if(new YoulorrySession(context).getrole().equals("transporter")){
                                    if(post_status1.equals("sent_by_truck")){
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.420));
                                    } else if(post_status1.equals("req_cust")) {
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.750));
                                    }
                                } else if(new YoulorrySession(context).getrole().equals("customer")){
                                    if(post_status1.equals("sent_by_truck")){
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.7));
                                    } else if(post_status1.equals("req_cust")) {
                                        getWindow().setLayout((int) (width*0.950),(int)(height*0.5));
                                    }
                                }

                            } else if(fetching_page_id.trim().toString().equals("confirmed_req")){
                                getWindow().setLayout((int) (width*0.950),(int)(height*0.9));
                            }


                            if(jsonObject.has("loader_details")){

                                String loader_details_jsondata = jsonObject.optString("loader_details").toString();
                                Log.d("loader_details", loader_details_jsondata);

                                JSONObject jobj3 = new JSONObject(loader_details_jsondata);

                                if(jobj3.has("first_name")){
                                    String first_name_jsondata=jobj3.getString("first_name");
                                    String first_name_from_json = first_name_jsondata.substring(2, first_name_jsondata.toString().length() - 2);
                                    name.setText("");
                                    name.setText(first_name_from_json);
                                    Log.d("name", first_name_from_json);
                                }else {
                                    name.setText("NA");
                                }

                                if(jobj3.has("customer_contact_number")){
                                    String customer_contact_number_jsondata=jobj3.getString("customer_contact_number");
                                    String customer_contact_number_from_json = customer_contact_number_jsondata.substring(2, customer_contact_number_jsondata.toString().length() - 2);
                                    mob.setText("");
                                    mob.setText(customer_contact_number_from_json);
                                    Log.d("mob", customer_contact_number_from_json);
                                }else {
                                    mob.setText("NA");
                                }

                                if(jobj3.has("customer_transport_name")){
                                    String customer_transport_name_jsondata=jobj3.getString("customer_transport_name");
                                    String customer_transport_name_from_json = customer_transport_name_jsondata.substring(2, customer_transport_name_jsondata.toString().length() - 2);
                                    company_name.setText("");
                                    company_name.setText(customer_transport_name_from_json);
                                    Log.d("company_name", customer_transport_name_from_json);
                                }else {
                                    company_name.setText("NA");
                                }

                            }

 /*                               String truck_s_city_jsondata=jobj1.getString("source_city");
                            if(jsonObject.has("truck_details")){

                                String truck_details_jsondata = jsonObject.optString("truck_details").toString();
                                Log.d("truck_datails", truck_details_jsondata);
                                JSONObject jobj1 = new JSONObject(truck_details_jsondata);

                                String truck_s_city_from_json = truck_s_city_jsondata.substring(2, truck_s_city_jsondata.toString().length() - 2);

                                String truck_d_city_jsondata=jobj1.getString("destination_city");
                                String truck_d_city_from_json = truck_d_city_jsondata.substring(2, truck_d_city_jsondata.toString().length() - 2);

                                String truck_expected_frieght_value_jsondata=jobj1.getString("expected_frieght_value");
                                String truck_expected_frieght_value_from_json = truck_expected_frieght_value_jsondata.substring(2, truck_expected_frieght_value_jsondata.toString().length() - 2);

                                String truck_truck_type_jsondata=jobj1.getString("truck_type");
                                String truck_truck_type_from_json = truck_truck_type_jsondata.substring(2, truck_truck_type_jsondata.toString().length() - 2);

                                String truck_date_jsondata=jobj1.getString("date");
                                String truck_date_from_json = truck_date_jsondata.substring(2, truck_date_jsondata.toString().length() - 2);

                            }
*/

                            if(jsonObject.has("meta_keys")){
                                String meta_keys_jsondata = jsonObject.optString("meta_keys").toString();
                                Log.d("meta_keys", meta_keys_jsondata);

                                JSONObject jobj = new JSONObject(meta_keys_jsondata);

                                if (jobj.has("load_loading_address")){
                                    String load_loading_address_jsondata=jobj.getString("load_loading_address");
                                    String load_loading_address_from_json = load_loading_address_jsondata.substring(2, load_loading_address_jsondata.toString().length() - 2);
                                    String[] lp_a_split =load_loading_address_from_json.split(",");
                                    o_d_loading_point.setText("");
                                    o_d_loading_point.setText(lp_a_split[0]);
                                    loading_point.setText("");
                                    loading_point.setText(lp_a_split[0]);
                                } else {
                                    o_d_loading_point.setText("NA");
                                    loading_point.setText("NA");
                                }

                                if(jobj.has("load_unloading_address")){
                                    String load_unloading_address_jsondata=jobj.getString("load_unloading_address");
                                    String load_unloading_address_from_json = load_unloading_address_jsondata.substring(2, load_unloading_address_jsondata.toString().length() - 2);
                                    String[] unlp_a_split =load_unloading_address_from_json.split(",");
                                    o_d_unloading_point.setText("");
                                    o_d_unloading_point.setText(unlp_a_split[0]);
                                    unloading_point.setText("");
                                    unloading_point.setText(unlp_a_split[0]);
                                } else {
                                    o_d_unloading_point.setText("NA");
                                    unloading_point.setText("NA");
                                }

                                if(jobj.has("initiated_by")){
                                    String initiated_by_jsondata=jobj.getString("initiated_by");
                                    String initiated_by_from_json = initiated_by_jsondata.substring(2, initiated_by_jsondata.toString().length() - 2);

                                }else {

                                }

                                if(jobj.has("load_ID")){
                                    String load_ID_jsondata=jobj.getString("load_ID");
                                    String load_ID_from_json = load_ID_jsondata.substring(2, load_ID_jsondata.toString().length() - 2);
                                    load_id.setText("");
                                    load_id.setText("YLL"+load_ID_from_json);
                                }else {
                                    load_id.setText("NA");
                                }

/*                                if(jobj.has("quoted_amount")){
                                    String quoted_amt_jsondata=jobj.getString("quoted_amount");
                                    String quoted_amt_from_json = quoted_amt_jsondata.substring(2, quoted_amt_jsondata.toString().length() - 2);
                                    q_val.setText("");
                                    agreed_f_amt.setText("");
                                    q_val.setText("₹ "+quoted_amt_from_json);
                                    agreed_f_amt.setText("₹ "+quoted_amt_from_json);

                                }else {
                                    q_val.setText("NA");
                                    agreed_f_amt.setText("NA");
                                }
*/
                                if(jsonObject.has("truck_details")) {

                                    String truck_details_jsondata = jsonObject.optString("truck_details").toString();
                                    Log.d("truck_datails", truck_details_jsondata);
                                    if(truck_details_jsondata.equals("false")){
                                        vehicle_number.setText("NA");
                                    } else {
                                        JSONObject jobj1 = new JSONObject(truck_details_jsondata);

                                        if(jobj1.has("vehicle_number")){
                                            String truck_vehicle_number_jsondata=jobj1.getString("vehicle_number");
                                            String truck_vehicle_number_from_json = truck_vehicle_number_jsondata.substring(2, truck_vehicle_number_jsondata.toString().length() - 2);

                                            vehicle_number.setText("");
                                            vehicle_number.setText(truck_vehicle_number_from_json);
                                        } else {
                                            vehicle_number.setText("NA");
                                        }
                                    }
                                } else {

                                    if(jobj.has("quotation_vehicle_number")){
                                        String truck_vehicle_number_jsondata=jobj.getString("quotation_vehicle_number");
                                        String truck_vehicle_number_from_json = truck_vehicle_number_jsondata.substring(2, truck_vehicle_number_jsondata.toString().length() - 2);

                                        vehicle_number.setText("");
                                        vehicle_number.setText(truck_vehicle_number_from_json);
                                    } else {
                                        vehicle_number.setText("");
                                        vehicle_number.setText("NA");
                                    }
                                }

                                String load_details_jsondata = jsonObject.optString("load_details").toString();
                                JSONObject jobj2 = new JSONObject(load_details_jsondata);

                                if(jobj2.has("load_weight_capacity") && jobj.has("left_count")){
                                    String load_weight_capacity_jsondata=jobj2.getString("load_weight_capacity");
                                    String load_weight_capacity_from_json = load_weight_capacity_jsondata.substring(2, load_weight_capacity_jsondata.toString().length() - 2);

                                    String left_count_jsondata=jobj.getString("left_count");
                                    String left_count_from_json = left_count_jsondata.substring(2, left_count_jsondata.toString().length() - 2);

                                    Log.d("wt_data_fetched", left_count_from_json+"/"+load_weight_capacity_from_json);

                                    if(post_status1.equals("truck_pay_ok")){
                                        weight.setText("");
                                        Log.d("wt_for_truck_pay_ok", left_count_from_json);
                                        weight.setText(left_count_from_json+" MT");
                                    }
                                    if(post_status1.equals("req_cust")){
                                        weight.setText("");
                                        Log.d("wt_for_req_cust", left_count_from_json);
                                        weight.setText(left_count_from_json+" MT");
                                    }
                                    if(post_status1.equals("sent_by_truck")  || post_status1.equals("cancel_by_load") || post_status1.equals("cancel_by_truck")){
                                        weight.setText("");
                                        Log.d("wt_for_sent_by_truck_or_cancel_by_truck", left_count_from_json);
                                        weight.setText(left_count_from_json+" MT");
                                    }

                                } else {
                                    weight.setText("NA");
                                }
                                if(post_status1.equals("truck_pay_ok")){
                                    if(jobj.has("driver_name")){

                                        String driver_name_jsondata=jobj.getString("driver_name");
                                        String driver_name_from_json = driver_name_jsondata.substring(2, driver_name_jsondata.toString().length() - 2);

                                        t_o_d_driver_name_in_modal_view.setText(driver_name_from_json);
                                    } else {
                                        t_o_d_driver_name_in_modal_view.setText("NA");
                                    }

                                    if(jobj.has("driver_number")){
                                        String driver_number_jsondata=jobj.getString("driver_number");
                                        String driver_number_from_json = driver_number_jsondata.substring(2, driver_number_jsondata.toString().length() - 2);
                                        t_o_d_driver_number_in_modal_view.setText(driver_number_from_json);
                                    } else {
                                        t_o_d_driver_number_in_modal_view.setText("NA");
                                    }


                                    if(jobj.has("driver_license_number")){
                                        String driver_license_number_jsondata=jobj.getString("driver_license_number");
                                        String driver_license_number_from_json = driver_license_number_jsondata.substring(2, driver_license_number_jsondata.toString().length() - 2);

                                        t_o_d_driver_license_number_in_modal_view.setText(driver_license_number_from_json);
                                    }else {
                                        t_o_d_driver_license_number_in_modal_view.setText("Unavailable");
                                    }

                                }

                            data="";
                            }

//                            if(jsonObject.has("")){}else {}
                            if(jsonObject.has("load_details")){

                                String load_details_jsondata = jsonObject.optString("load_details").toString();
                                Log.d("load_details", load_details_jsondata);
                                JSONObject jobj2 = new JSONObject(load_details_jsondata);

                            if(jobj2.has("load_source_city")){
                                String load_source_city_jsondata=jobj2.getString("load_source_city");
                                String load_source_city_from_json = load_source_city_jsondata.substring(2, load_source_city_jsondata.toString().length() - 2);
                                pickup_city.setText("");
                                pickup_city.setText(load_source_city_from_json);
                            }else {
                                pickup_city.setText("NA");
                            }

/*                            if(jobj2.has("load_expected_frieght_value")){
                                String load_expected_frieght_value_jsondata=jobj2.getString("load_expected_frieght_value");
                                String load_expected_frieght_value_from_json = load_expected_frieght_value_jsondata.substring(2, load_expected_frieght_value_jsondata.toString().length() - 2);
                                exp_val.setText("");
                                exp_val.setText("₹ "+load_expected_frieght_value_from_json);
                            }else {
                                exp_val.setText("NA");
                            }
*/
                            if(jobj2.has("load_destination_city")){
                                String load_destination_city_jsondata=jobj2.getString("load_destination_city");
                                String load_destination_city_from_json = load_destination_city_jsondata.substring(2, load_destination_city_jsondata.toString().length() - 2);
                                drop_city.setText("");
                                drop_city.setText(load_destination_city_from_json);
                            }else {
                                drop_city.setText("NA");
                            }

/*                            if(jobj2.has("load_material")){
                                String load_material_jsondata=jobj2.getString("load_material");
                                String load_material_from_json = load_material_jsondata.substring(2, load_material_jsondata.toString().length() - 2);

                            }else {

                            }

                            if(jobj2.has("load_truck_type")){
                                String load_truck_type_jsondata=jobj2.getString("load_truck_type");
                                String load_truck_type_from_json = load_truck_type_jsondata.substring(2, load_truck_type_jsondata.toString().length() - 2);

                            }else {

                            }
*/
                            if(jobj2.has("load_date")){
                                String load_date_jsondata=jobj2.getString("load_date");
                                String load_date_from_json = load_date_jsondata.substring(2, load_date_jsondata.toString().length() - 2);
                                pickup_date.setText("");
                                pickup_date.setText(load_date_from_json);
                            }else {
                                pickup_date.setText("NA");
                            }

                            if(jobj2.has("load_time")){
                                String load_time_jsondata=jobj2.getString("load_time");
                                String load_time_from_json = load_time_jsondata.substring(2, load_time_jsondata.toString().length() - 2);
                                pickup_time.setText("");
                                f_p_pickup_time.setText("");
                                pickup_time.setText(load_time_from_json);
                                f_p_pickup_time.setText(load_time_from_json);
                            }else {
                                pickup_time.setText("NA");
                                f_p_pickup_time.setText("NA");
                            }

                            if(jobj2.has("load_pickup_point")){
                                String load_pickup_point_jsondata = jobj2.getString("load_pickup_point");
                                String load_pickup_point_from_json = load_pickup_point_jsondata.substring(2, load_pickup_point_jsondata.toString().length() - 2);
                                pickup_point.setText("");
                                pickup_point.setText(load_pickup_point_from_json);
                            }else {
                                pickup_point.setText("Inside City");
                            }

                            if(jobj2.has("load_drop_point")){
                                String load_drop_point_jsondata = jobj2.getString("load_drop_point");
                                String load_drop_point_from_json = load_drop_point_jsondata.substring(2, load_drop_point_jsondata.toString().length() - 2);
                                drop_point.setText("");
                                drop_point.setText(load_drop_point_from_json);
                            }else {
                                drop_point.setText("Inside City");
                            }

/*                            if(jobj2.has("load_advance_in_percentage") && jobj2.has("load_expected_frieght_value")){
                                String load_advance_in_percentage_jsondata=jobj2.getString("load_advance_in_percentage");
                                String load_advance_in_percentage_from_json = load_advance_in_percentage_jsondata.substring(2, load_advance_in_percentage_jsondata.toString().length() - 2);

                                String load_expected_frieght_value_jsondata=jobj2.getString("load_expected_frieght_value");
                                String load_expected_frieght_value_from_json = load_expected_frieght_value_jsondata.substring(2, load_expected_frieght_value_jsondata.toString().length() - 2);

                                String meta_keys_jsondata = jsonObject.optString("meta_keys").toString();
                                JSONObject jobj = new JSONObject(meta_keys_jsondata);

                                String quoted_amt_jsondata=jobj.getString("quoted_amount");
                                String quoted_amt_from_json = quoted_amt_jsondata.substring(2, quoted_amt_jsondata.toString().length() - 2);

                                int adv_amt_result = Integer.parseInt(load_expected_frieght_value_from_json)*Integer.parseInt(load_advance_in_percentage_from_json)/100;
                                int adv_amt_result1 = Integer.parseInt(quoted_amt_from_json)*Integer.parseInt(load_advance_in_percentage_from_json)/100;
                                f_p_adv_amt.setText("");
                                advance.setText("");
                                f_p_adv_amt.setText("₹ "+adv_amt_result1);
                                advance.setText("₹ "+adv_amt_result);
                            }else {
                                f_p_adv_amt.setText("NA");
                                advance.setText("NA");
                            }
*/

                                if(jsonObject.has("new_data")){
                                    String new_data_jsondata = jsonObject.optString("new_data").toString();
                                    Log.d("new_data", new_data_jsondata);

                                    JSONObject jobj22 = new JSONObject(new_data_jsondata);

                                    if(jobj22.has("new_part_amount")){
                                        String new_part_amount_jsondata=jobj22.getString("new_part_amount");
//                                        String new_part_amount_from_json = new_part_amount_jsondata.substring(2, new_part_amount_jsondata.toString().length() - 2);

                                        exp_val.setText("");
                                        exp_val.setText("₹ "+new_part_amount_jsondata);
                                    } else{
                                        exp_val.setText("NA");
                                    }

                                    if(jobj22.has("new_full_amount")){
                                        String new_full_amount_jsondata=jobj22.getString("new_full_amount");
//                                        String new_full_amount_from_json = new_full_amount_jsondata.substring(2, new_full_amount_jsondata.toString().length() - 2);

                                        q_val.setText("");
                                        agreed_f_amt.setText("");
                                        q_val.setText("₹ "+new_full_amount_jsondata);
                                        agreed_f_amt.setText("₹ "+new_full_amount_jsondata);

                                    } else{
                                        q_val.setText("NA");
                                        agreed_f_amt.setText("NA");
                                    }

                                    if(jobj22.has("new_advance_amount")){
                                        String new_advance_amount_jsondata=jobj22.getString("new_advance_amount");
  //                                      String new_advance_amount_from_json = new_advance_amount_jsondata.substring(2, new_advance_amount_jsondata.toString().length() - 2);
                                        f_p_adv_amt.setText("");
                                        advance.setText("");
                                        f_p_adv_amt.setText("₹ "+new_advance_amount_jsondata);
                                        advance.setText("₹ "+new_advance_amount_jsondata);

                                    } else{
                                        f_p_adv_amt.setText("NA");
                                        advance.setText("NA");
                                    }

                                }

                            }

                            if(post_status1.trim().toString().equals("req_cust")){
                                 Log.d("req_cust","Running");
                                c_accept_btn.setVisibility(View.VISIBLE);
                                c_pending_btn.setVisibility(View.GONE);
                             //   c_done_btn.setVisibility(View.GONE);
                                c_cancel_btn.setVisibility(View.GONE);
                                    driver_license_number_at_accept_quote.setVisibility(View.VISIBLE);
                                    dn_at_accept_quote.setVisibility(View.VISIBLE);
                                    dc_at_accept_quote.setVisibility(View.VISIBLE);
                                    pa_at_accept_quote.setVisibility(View.GONE);
                                    da_at_accept_quote.setVisibility(View.GONE);

                                vehicle_number_heading.setVisibility(View.VISIBLE);
                                vehicle_number.setVisibility(View.VISIBLE);
                                load_id_heading.setVisibility(View.VISIBLE);
                                load_id.setVisibility(View.VISIBLE);
                                pickup_city_heading.setVisibility(View.VISIBLE);
                                pickup_city.setVisibility(View.VISIBLE);
                                drop_city_heading.setVisibility(View.VISIBLE);
                                drop_city.setVisibility(View.VISIBLE);
                                weight_heading.setVisibility(View.VISIBLE);
                                weight.setVisibility(View.VISIBLE);
                                pickup_date_heading.setVisibility(View.VISIBLE);
                                pickup_date.setVisibility(View.VISIBLE);
                                exp_val_heading.setVisibility(View.VISIBLE);
                                exp_val.setVisibility(View.VISIBLE);
                                q_val_heading.setVisibility(View.VISIBLE);
                                q_val.setVisibility(View.VISIBLE);
                                pickup_point_heading.setVisibility(View.VISIBLE);
                                pickup_point.setVisibility(View.VISIBLE);
                                drop_point_heading.setVisibility(View.VISIBLE);
                                drop_point.setVisibility(View.VISIBLE);
                                advance_heading.setVisibility(View.VISIBLE);
                                advance.setVisibility(View.VISIBLE);
                                pickup_time_heading.setVisibility(View.VISIBLE);
                                pickup_time.setVisibility(View.VISIBLE);

                                line_under_vehicle_number.setVisibility(View.VISIBLE);
                                line_under_load_id.setVisibility(View.VISIBLE);
                                line_under_pickup_city.setVisibility(View.VISIBLE);
                                line_under_drop_city.setVisibility(View.VISIBLE);
                                line_under_weight.setVisibility(View.VISIBLE);
                                line_under_pickup_date.setVisibility(View.VISIBLE);
                                line_under_expected_value.setVisibility(View.VISIBLE);
                                line_under_quoted_value.setVisibility(View.VISIBLE);
                                line_under_pickup_point.setVisibility(View.VISIBLE);
                                line_under_drop_point.setVisibility(View.VISIBLE);
                                line_under_advance.setVisibility(View.VISIBLE);
                                line_under_pickup_time.setVisibility(View.VISIBLE);

                            }

                            if(post_status1.trim().toString().equals("sent_by_truck")){

                                Log.d("sent_by_truck","Running");
                                c_pending_btn.setVisibility(View.VISIBLE);
                                c_accept_btn.setVisibility(View.GONE);
                            //    c_done_btn.setVisibility(View.GONE);
                                c_cancel_btn.setVisibility(View.GONE);

                                vehicle_number_heading.setVisibility(View.VISIBLE);
                                vehicle_number.setVisibility(View.VISIBLE);
                                load_id_heading.setVisibility(View.VISIBLE);
                                load_id.setVisibility(View.VISIBLE);
                                pickup_city_heading.setVisibility(View.VISIBLE);
                                pickup_city.setVisibility(View.VISIBLE);
                                drop_city_heading.setVisibility(View.VISIBLE);
                                drop_city.setVisibility(View.VISIBLE);
                                weight_heading.setVisibility(View.VISIBLE);
                                weight.setVisibility(View.VISIBLE);
                                pickup_date_heading.setVisibility(View.VISIBLE);
                                pickup_date.setVisibility(View.VISIBLE);
                                exp_val_heading.setVisibility(View.VISIBLE);
                                exp_val.setVisibility(View.VISIBLE);
                                q_val_heading.setVisibility(View.VISIBLE);
                                q_val.setVisibility(View.VISIBLE);

                                line_under_vehicle_number.setVisibility(View.VISIBLE);
                                line_under_load_id.setVisibility(View.VISIBLE);
                                line_under_pickup_city.setVisibility(View.VISIBLE);
                                line_under_drop_city.setVisibility(View.VISIBLE);
                                line_under_weight.setVisibility(View.VISIBLE);
                                line_under_pickup_date.setVisibility(View.VISIBLE);
                                line_under_expected_value.setVisibility(View.VISIBLE);
                                line_under_quoted_value.setVisibility(View.VISIBLE);
                            }

                            if(post_status1.trim().toString().equals("truck_pay_ok")){
                                Log.d("truck_pay_ok","Running");
                            //    c_done_btn.setVisibility(View.VISIBLE);
                                c_cancel_btn.setVisibility(View.VISIBLE);
                                c_pending_btn.setVisibility(View.GONE);
                                c_accept_btn.setVisibility(View.GONE);

                                our_details_for_tansporter.setVisibility(View.VISIBLE);
                                vehicle_number_heading.setVisibility(View.VISIBLE);
                                vehicle_number.setVisibility(View.VISIBLE);
                                load_id_heading.setVisibility(View.VISIBLE);
                                load_id.setVisibility(View.VISIBLE);
                                pickup_city_heading.setVisibility(View.VISIBLE);
                                pickup_city.setVisibility(View.VISIBLE);
                                drop_city_heading.setVisibility(View.VISIBLE);
                                drop_city.setVisibility(View.VISIBLE);
                                weight_heading.setVisibility(View.VISIBLE);
                                weight.setVisibility(View.VISIBLE);
                                pickup_date_heading.setVisibility(View.VISIBLE);
                                pickup_date.setVisibility(View.VISIBLE);
                                exp_val_heading.setVisibility(View.VISIBLE);
                                exp_val.setVisibility(View.VISIBLE);
                                q_val_heading.setVisibility(View.VISIBLE);
                                q_val.setVisibility(View.VISIBLE);
                                t_o_d_driver_name_inmodalview.setVisibility(View.VISIBLE);
                                t_o_d_driver_name_in_modal_view.setVisibility(View.VISIBLE);
                                line_under_t_o_d_driver_name.setVisibility(View.VISIBLE);
                                t_o_d_driver_number_inmodalview.setVisibility(View.VISIBLE);
                                t_o_d_driver_number_in_modal_view.setVisibility(View.VISIBLE);
                                line_under_t_o_d_driver_number.setVisibility(View.VISIBLE);
                                t_o_d_driver_license_number_inmodalview.setVisibility(View.VISIBLE);
                                t_o_d_driver_license_number_in_modal_view.setVisibility(View.VISIBLE);
                                line_under_t_o_d_driver_license_number.setVisibility(View.VISIBLE);

                                line_under_vehicle_number.setVisibility(View.VISIBLE);
                                line_under_load_id.setVisibility(View.VISIBLE);
                                line_under_pickup_city.setVisibility(View.VISIBLE);
                                line_under_drop_city.setVisibility(View.VISIBLE);
                                line_under_weight.setVisibility(View.VISIBLE);
                                line_under_pickup_date.setVisibility(View.VISIBLE);
                                line_under_expected_value.setVisibility(View.VISIBLE);
                                line_under_quoted_value.setVisibility(View.VISIBLE);

                                freight_provider_heading.setVisibility(View.VISIBLE);
                                company_name_heading.setVisibility(View.VISIBLE);
                                company_name.setVisibility(View.VISIBLE);
                                name_heading.setVisibility(View.VISIBLE);
                                name.setVisibility(View.VISIBLE);
                                mob_heading.setVisibility(View.VISIBLE);
                                mob.setVisibility(View.VISIBLE);
                                f_p_pickup_time_heading.setVisibility(View.VISIBLE);
                                f_p_pickup_time.setVisibility(View.VISIBLE);
                                agreed_f_amt_heading.setVisibility(View.VISIBLE);
                                agreed_f_amt.setVisibility(View.VISIBLE);
                                f_p_adv_amt_heading.setVisibility(View.VISIBLE);
                                f_p_adv_amt.setVisibility(View.VISIBLE);
                                loading_point_heading.setVisibility(View.VISIBLE);
                                loading_point.setVisibility(View.VISIBLE);
                                unloading_point_heading.setVisibility(View.VISIBLE);
                                unloading_point.setVisibility(View.VISIBLE);

                                line_under_company_name.setVisibility(View.VISIBLE);
                                line_under_freight_provider_name.setVisibility(View.VISIBLE);
                                line_under_mobile_number.setVisibility(View.VISIBLE);
                                line_under_pickup_time1.setVisibility(View.VISIBLE);
                                line_under_agreed_freight_amount.setVisibility(View.VISIBLE);
                                line_under_advance_amount.setVisibility(View.VISIBLE);
                                line_under_loading_point.setVisibility(View.VISIBLE);
                                line_under_unloading_point.setVisibility(View.VISIBLE);

                            }

                            if(post_status1.trim().toString().equals("cancel_by_truck") || post_status1.trim().toString().equals("cancel_by_load")){
                                Log.d("cancel_by_truck_or_load","Running");
                                //    c_done_btn.setVisibility(View.VISIBLE);
                                c_cancel_btn.setVisibility(View.GONE);
                                c_pending_btn.setVisibility(View.GONE);
                                c_accept_btn.setVisibility(View.GONE);

                                vehicle_number_heading.setVisibility(View.VISIBLE);
                                vehicle_number.setVisibility(View.VISIBLE);
                                load_id_heading.setVisibility(View.VISIBLE);
                                load_id.setVisibility(View.VISIBLE);
                                pickup_city_heading.setVisibility(View.VISIBLE);
                                pickup_city.setVisibility(View.VISIBLE);
                                drop_city_heading.setVisibility(View.VISIBLE);
                                drop_city.setVisibility(View.VISIBLE);
                                weight_heading.setVisibility(View.VISIBLE);
                                weight.setVisibility(View.VISIBLE);
                                pickup_date_heading.setVisibility(View.VISIBLE);
                                pickup_date.setVisibility(View.VISIBLE);
                                exp_val_heading.setVisibility(View.VISIBLE);
                                exp_val.setVisibility(View.VISIBLE);
                                q_val_heading.setVisibility(View.VISIBLE);
                                q_val.setVisibility(View.VISIBLE);

                                line_under_vehicle_number.setVisibility(View.VISIBLE);
                                line_under_load_id.setVisibility(View.VISIBLE);
                                line_under_pickup_city.setVisibility(View.VISIBLE);
                                line_under_drop_city.setVisibility(View.VISIBLE);
                                line_under_weight.setVisibility(View.VISIBLE);
                                line_under_pickup_date.setVisibility(View.VISIBLE);
                                line_under_expected_value.setVisibility(View.VISIBLE);
                                line_under_quoted_value.setVisibility(View.VISIBLE);
                            }
                        }

                        show_c_quotation_data.setText(data);

                    }

                }

            } catch (JSONException e) {e.printStackTrace();}

            dialogue.dismiss();
        }
    }
}
