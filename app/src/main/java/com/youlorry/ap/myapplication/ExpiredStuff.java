package com.youlorry.ap.myapplication;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Arpit Prajapati on 12/30/16.
 */

public class ExpiredStuff extends Fragment {

    ListView listView;
    TextView exp_stuff_header_text,t1;
    Context con;

    CustomAdapterforexpiredstufflistview adapter;
    public ArrayList<ListModelforloadlistrow> CustomListViewValuesArr_for_loads = new ArrayList<ListModelforloadlistrow>();
    public ArrayList<ListModelfortrucklistrow> CustomListViewValuesArr_for_trucks = new ArrayList<ListModelfortrucklistrow>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.expired_stuff,container,false);

        con = getActivity();

        YoulorrySession session = new YoulorrySession(con);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(con , LoginActivity.class);
            startActivity(in);

        }

        listView = (ListView) v.findViewById(R.id.expired_stuff_listview);
        exp_stuff_header_text = (TextView) v.findViewById(R.id.expired_stuff_header_text);
        t1 = (TextView) v.findViewById(R.id.no_matching_data_found);

        if(new YoulorrySession(con).getrole().toString().equals("transporter")){
            exp_stuff_header_text.setText("Expired Trucks");
        }
        if(new YoulorrySession(con).getrole().toString().equals("customer")){
            exp_stuff_header_text.setText("Expired Loads");
        }

        new TheTask().execute();

        return  v;
    }

    class TheTask extends AsyncTask<Void, Void, String> {


        ProgressDialog dialogue;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {



            YoulorrySession session = new YoulorrySession(con);
            String username = session.getusername();
            String password = session.getpass_word();
            String userid = session.getuser_id();
            String role = session.getrole();

            StringBuilder sb = new StringBuilder();
            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-fetch-my-expired-entity/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                //  urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);

                OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                wr.flush();
                wr.close();

                //display what returns the POST request

                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                    br.close();

//                    Toast.makeText(con, sb.toString() , Toast.LENGTH_LONG).show();

                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return sb.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.d("expiresd_stuff_data",s.toString());
            new YoulorrySession(con).setExpiredStuff(s.toString());
            try {

                if(s.equals("no_expired_entity")){
                    listView.setVisibility(View.GONE);
                    t1.setVisibility(View.VISIBLE);
                    t1.setText("No Data Found!!!");
                }else{

                    JSONArray jsonArray = new JSONArray(s.toString());
                    Resources res =getResources();
                    for(int i=0; i < jsonArray.length(); i++){

                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        if(new YoulorrySession(con).getrole().equals("transporter")){

                            int id = Integer.parseInt(jsonObject.optString("ID").toString());

                            String posted_by_name = new YoulorrySession(con).getFirstName();

                            String meta_keys_jsondata = jsonObject.optString("meta_keys").toString();

                            JSONObject jobj = new JSONObject(meta_keys_jsondata);
                            String source_city_jsondata=jobj.getString("source_city");
                            String s_city_from_json = source_city_jsondata.substring(2, source_city_jsondata.toString().length() - 2);

                            String destination_city_jsondata=jobj.getString("destination_city");
                            String d_city_from_json = destination_city_jsondata.substring(2, destination_city_jsondata.toString().length() - 2);

                            String expected_value_jsondata=jobj.getString("expected_frieght_value");
                            String exp_val_from_json = expected_value_jsondata.substring(2, expected_value_jsondata.toString().length() - 2);

                            String schedule_date_jsondata=jobj.getString("date");
                            String scheduled_date_from_json = schedule_date_jsondata.substring(2, schedule_date_jsondata.toString().length() - 2);

                            String truck_type_jsondata=jobj.getString("truck_type");
                            String truc_type_from_jsondata = truck_type_jsondata.substring(2, truck_type_jsondata.toString().length() - 2);

                            String vehicle_number_jsondata=jobj.getString("vehicle_number");
                            String vehicle_number_from_jsondata = vehicle_number_jsondata.substring(2, vehicle_number_jsondata.toString().length() - 2);

                            String weight_capacity_jsondata=jobj.getString("weight_capacity");
                            String weight_capacity_from_jsondata = weight_capacity_jsondata.substring(2, weight_capacity_jsondata.toString().length() - 2);

                            adapter=new CustomAdapterforexpiredstufflistview( con, CustomListViewValuesArr_for_trucks,res);
                            listView.setAdapter( adapter );

                            final ListModelfortrucklistrow sched = new ListModelfortrucklistrow();
                            String[] s_city_split =s_city_from_json.split(",");
                            String[] d_city_split =d_city_from_json.split(",");
                            /******* Firstly take data in model object ******/
                            sched.setSourcecity(s_city_split[0]);
                            sched.setDestinationcity(d_city_split[0]);
                            sched.setTrucktype(truc_type_from_jsondata);
                            sched.setPrice(exp_val_from_json);
                            sched.setPostedby(posted_by_name);
                            sched.setVehicleNumber( vehicle_number_from_jsondata );
                            sched.setWeightCapacity(weight_capacity_from_jsondata);
                            sched.setSchDate(scheduled_date_from_json);

                            /******** Take Model Object in ArrayList **********/
                            CustomListViewValuesArr_for_trucks.add(sched);

                        }
                        if(new YoulorrySession(con).getrole().equals("customer")){

                            int id = Integer.parseInt(jsonObject.optString("ID").toString());

                            String meta_keys_jsondata = jsonObject.optString("meta_keys").toString();

                            //      String posted_by_name = jsonObject.getString("posted_by");

                            JSONObject jobj = new JSONObject(meta_keys_jsondata);
                            String source_city_jsondata=jobj.getString("load_source_city");
                            String s_city_from_json = source_city_jsondata.substring(2, source_city_jsondata.toString().length() - 2);

                            String destination_city_jsondata=jobj.getString("load_destination_city");
                            String d_city_from_json = destination_city_jsondata.substring(2, destination_city_jsondata.toString().length() - 2);

//                            String expected_value_jsondata=jobj.getString("load_expected_frieght_value");
//                            String exp_val_from_json = expected_value_jsondata.substring(2, expected_value_jsondata.toString().length() - 2);

                            String schedule_date_jsondata=jobj.getString("load_date");
                            String scheduled_date_from_json = schedule_date_jsondata.substring(2, schedule_date_jsondata.toString().length() - 2);

                            String load_material_jsondata=jobj.getString("load_material");
                            String load_material_from_json = load_material_jsondata.substring(2, load_material_jsondata.toString().length() - 2);

                            String truck_type_jsondata=jobj.getString("load_truck_type");
                            String truc_type_from_jsondata = truck_type_jsondata.substring(2, truck_type_jsondata.toString().length() - 2);

/*                            String load_advance_in_percentage_from_json;
                            if(jobj.has("load_advance_in_percentage")){
                                String load_advance_in_percentage_jsondata=jobj.getString("load_advance_in_percentage");
                                load_advance_in_percentage_from_json = load_advance_in_percentage_jsondata.substring(2, load_advance_in_percentage_jsondata.toString().length() - 2);
                            } else {
                                load_advance_in_percentage_from_json = "0";
                            }
*/


                            String exp_val_from_json,load_advance_in_percentage_from_json;
                            if(jobj.has("part_amount")){
                                exp_val_from_json=jobj.getString("part_amount");
                                //    String expected_value_jsondata=jobj.getString("part_amount");
                                //    exp_val_from_json = expected_value_jsondata.substring(2, expected_value_jsondata.toString().length() - 2);
                            } else {
                                exp_val_from_json = "NA";
                            }

                            if(jobj.has("part_amount")){
                                load_advance_in_percentage_from_json=jobj.getString("part_advance_of_load");
//                        String load_advance_in_percentage_jsondata=jobj.getString("part_advance_of_load");
//                        String load_advance_in_percentage_from_json = load_advance_in_percentage_jsondata.substring(2, load_advance_in_percentage_jsondata.toString().length() - 2);
                            } else {
                                load_advance_in_percentage_from_json = "NA";
                            }

//                            String load_advance_in_percentage_jsondata=jobj.getString("load_advance_in_percentage");
//                            String load_advance_in_percentage_from_json = load_advance_in_percentage_jsondata.substring(2, load_advance_in_percentage_jsondata.toString().length() - 2);

                            adapter=new CustomAdapterforexpiredstufflistview( con, CustomListViewValuesArr_for_loads,res);
                            listView.setAdapter( adapter );

                            final ListModelforloadlistrow sched121 = new ListModelforloadlistrow();
                            String[] s_city_split =s_city_from_json.split(",");
                            String[] d_city_split =d_city_from_json.split(",");

                            sched121.setSourcecity2(s_city_split[0]);
                            sched121.setDestinationcity2(d_city_split[0]);
                            sched121.setStuffId("YLL"+id);
                            sched121.setPrice2(exp_val_from_json);
                            sched121.setMaterialType(load_material_from_json);
                            sched121.setAdvPrice(load_advance_in_percentage_from_json);
                            sched121.setDate2(scheduled_date_from_json);
                            CustomListViewValuesArr_for_loads.add(sched121);
                        }

                    }
                }

            } catch (JSONException e) {e.printStackTrace();}

            dialogue.dismiss();
        }
    }

    @Override
    public void onResume() {

        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button
//                    startActivity(new Intent(con,MainActivity.class));
                    getActivity().getFragmentManager().popBackStack();
                    return true;

                }

                return false;
            }
        });
    }

}
