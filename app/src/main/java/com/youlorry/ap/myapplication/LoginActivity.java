package com.youlorry.ap.myapplication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Arpit Prajapati on 11/26/16.
 */

public class LoginActivity extends YouLorryActivity implements View.OnClickListener{

    Button login_btn;
    public EditText username,password;
    TextView forgot_pass_tv,sign_up_tv;
    Context con;
    String data="";
    ProgressDialog dialogue;
    private YoulorrySession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);
        setupUI(findViewById(R.id.login_page_rl));

        if (android.os.Build.VERSION.SDK_INT > 11)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        con = this;
        login_btn = (Button) findViewById(R.id.login_btn);

        username = (EditText) findViewById(R.id.email_id);
        password = (EditText) findViewById(R.id.password);
        forgot_pass_tv = (TextView) findViewById(R.id.forgot_password_text_view);
        sign_up_tv = (TextView) findViewById(R.id.sign_up_text_view);

//        forgot_pass_tv.setPaintFlags(forgot_pass_tv.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
//        sign_up_tv.setPaintFlags(sign_up_tv.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);

        forgot_pass_tv.setOnClickListener(this);
        sign_up_tv.setOnClickListener(this);
        login_btn.setOnClickListener(this);

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    boolean doubleBackToExitPressedOnce = false;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "press again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }


    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.forgot_password_text_view){
            Intent intent = new Intent(this, ForgotPassword.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
//            startActivity(new Intent(this, ForgotPassword.class));
        }

        if(view.getId() == R.id.sign_up_text_view){
            Intent intent = new Intent(this, RegistrationFormActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
//            startActivity(new Intent(this, RegistrationFormActivity.class));
        }

        if (view.getId() == R.id.login_btn) {

            if (!isNetworkConnected()) {
                new AlertDialog.Builder(con)
                        .setTitle("Check Connection")
                        .setMessage("Internet connection not enable. Please enable it from settings!")
                        .setPositiveButton("try again", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                Intent intent = getIntent();
                                finish();
                                startActivity(intent);
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            } else if(username.getText().toString().length() == 0){
                username.setError("required field!");
            } else if(password.getText().toString().length() == 0){
                password.setError("required field!");
            } else {

                dialogue = new ProgressDialog(con);
                dialogue.setTitle("Loading ...");
                dialogue.show();

//            new Thread(new Runnable() {
 //               public void run() {

                    //  dialogue = new ProgressDialog(con);
                    //  dialogue.setTitle("Loading ...");
                    //  dialogue.show();

                    if (username.getText().length() != 0 || password.getText().length() != 0) {

                        URL urlObj = null;
                        try {
                            urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/youlorry-api/");
                            HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                            urlConnection.setDoOutput(true);
                            urlConnection.setDoInput(true);
                            urlConnection.setUseCaches(false);
                            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                            //  urlConnection.setRequestProperty("Accept", "application/json");
                            urlConnection.setRequestMethod("POST");
                            urlConnection.connect();

                            JSONObject cred = new JSONObject();

                            cred.put("username", username.getText().toString());
                            cred.put("password", password.getText().toString());

                            OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
                            wr.write(cred.toString());
                            wr.flush();
                            wr.close();

                            //  display what returns the POST request

                            StringBuilder sb = new StringBuilder();
                            int HttpResult = urlConnection.getResponseCode();
                            if (HttpResult == HttpURLConnection.HTTP_OK) {
                                BufferedReader br = new BufferedReader(
                                        new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                                String line = null;
                                while ((line = br.readLine()) != null) {
                                    sb.append(line);
                                }
                                br.close();
                                //    Toast.makeText(con, sb.toString(), Toast.LENGTH_SHORT).show();

                                Log.d("loggedin_user_detail", sb.toString());

                                if(sb.toString().equals("user_not_approved")){
                                    Toast.makeText(con, "Pending Approval!!!", Toast.LENGTH_SHORT).show();
                                    dialogue.dismiss();
                                } else if(sb.toString().equals("details_not_complete")) {
                                    startActivity(new Intent(this, NewUserAfterLoginForm.class));
                                } else {

                                    String is_user_factory_vendor, weekly_data,data_from_json, roles_from_json, invalid_u_or_p, user_main_data_from_json, user_extra_data_from_json;
                                    JSONObject data_for_id = new JSONObject(sb.toString());
                                    if (data_for_id.has("user_id") && data_for_id.has("roles") && data_for_id.has("user_main_data")) {
//  && data_for_id.has("user_extra_data") && data_for_id.has("dashboard_info")
                                        data_from_json = data_for_id.getString("user_id");
                                        roles_from_json = data_for_id.getString("roles");
                                        is_user_factory_vendor=data_for_id.getString("is_user_factory_vendor");

                                        user_main_data_from_json = data_for_id.getString("user_main_data");
                                        user_extra_data_from_json = data_for_id.getString("user_extra_data");
                                        weekly_data = data_for_id.getString("dashboard_info");

                                        String first_name1,last_name1,pan_number,pan_number1;
                                        JSONObject jobj1 = new JSONObject(user_extra_data_from_json);

                                        first_name1 = jobj1.getString("first_name");
                                        last_name1 = jobj1.getString("last_name");
                                        String first_name = first_name1.substring(2, first_name1.toString().length() - 2);
                                        String last_name = last_name1.substring(2, last_name1.toString().length() - 2);

                                        if(jobj1.has("pan_number")){
                                            pan_number = jobj1.getString("pan_number");
                                            pan_number1 = pan_number.substring(2, pan_number.toString().length() - 2);
                                        } else if (jobj1.has("transporter_pan")){
                                            pan_number = jobj1.getString("transporter_pan");
                                            pan_number1 = pan_number.substring(2, pan_number.toString().length() - 2);
                                        } else if (jobj1.has("customer_pan")){
                                            pan_number = jobj1.getString("customer_pan");
                                            pan_number1 = pan_number.substring(2, pan_number.toString().length() - 2);
                                        } else {
                                            pan_number1="";
                                        }

                                        if (Integer.parseInt(data_from_json) > 0 && roles_from_json.toString().length() != 0) {

                                        Log.d("weekly_records_data", weekly_data);
                                            session = new YoulorrySession(con);

                                            session.setusername(username.getText().toString());
                                            session.setpass_word(password.getText().toString());
                                            session.setuser_id(data_from_json);
                                            session.setrole(roles_from_json);
                                            session.set_is_user_factory_vendor(is_user_factory_vendor);
                                            session.setFirstName(first_name+" "+last_name);
                                            session.setWeeklyRecord(weekly_data);
                                            session.setPANCardNumber(pan_number1);

                                            //  Toast.makeText(con, "  "+roles_from_json , Toast.LENGTH_SHORT).show();

                                            Intent in = new Intent(con, MainActivity.class);

                                            Bundle bd = new Bundle();

                                            bd.putString("user_id", data_from_json.toString());
                                            bd.putString("role", roles_from_json.toString());
                                            bd.putString("user_name", user_main_data_from_json.toString());

                                            in.putExtras(bd);

                                            startActivity(in);
                                            finish();
                                            dialogue.dismiss();
                                        }

                                    } else {


                                        Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            public void run() {
                                                dialogue.dismiss();
                                            }
                                        }, 1000);

                                        invalid_u_or_p = data_for_id.getString("error");
                                        Toast.makeText(con, invalid_u_or_p, Toast.LENGTH_SHORT).show();

                                        //JSONObject data_for_id1 = new JSONObject(sb.toString());

                                    }
                                }

                            } else {


                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        dialogue.dismiss();
                                    }
                                }, 1000);

                                Toast.makeText(con, urlConnection.getResponseMessage(), Toast.LENGTH_SHORT).show();
                                //  System.out.println(urlConnection.getResponseMessage());
                            }


                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        //     new TheTask().execute();

                    } else {

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                dialogue.dismiss();
                            }
                        }, 1000);

                        Toast.makeText(con, "Please fill all required field!", Toast.LENGTH_SHORT).show();

                    }
  //              }
  //          }).start();





        }
    }

    }

}
