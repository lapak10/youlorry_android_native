package com.youlorry.ap.myapplication;


import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Arpit Prajapati on 11/28/16.
 */

public class TrucklistActivity extends Fragment implements View.OnClickListener{

    TextView t1;
    String s_city,j_data,d_city,e_f_v_p_truck,l_p_address,ul_p_address,material_value,truck_type,schedule_date,schedule_time;
    String result,data="";
    ListView list;
    ListView listView;
    Context con;

    CustomAdapterfortrucklistview adapter;
    public ArrayList<ListModelfortrucklistrow> CustomListViewValuesArr = new ArrayList<ListModelfortrucklistrow>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        s_city = getArguments().getString("source_key");
        j_data = getArguments().getString("j_data");
        d_city = getArguments().getString("destination_key");
        e_f_v_p_truck = getArguments().getString("expected_freight_value_per_truck_key");
        material_value = getArguments().getString("material_key");
        truck_type = getArguments().getString("truck_type_key");
        schedule_time = getArguments().getString("schedule_time_key");
        schedule_date = getArguments().getString("schedule_date_key");

        View v = inflater.inflate(R.layout.truck_list,container,false);

        con = getActivity();

        YoulorrySession session = new YoulorrySession(con);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(con , LoginActivity.class);
            startActivity(in);

        }

        try {
            InputMethodManager input = (InputMethodManager) getActivity()
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            input.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }catch(Exception e) {
            e.printStackTrace();
        }


        listView = (ListView) v.findViewById(R.id.truck_filter_listview);
        t1 = (TextView) v.findViewById(R.id.no_matching_data_found);

        new TheTask().execute();
        return v;
    }


    class TheTask extends AsyncTask<Void, Void, String> {


        ProgressDialog dialogue;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            return j_data.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


/*
            listView.setVisibility(View.GONE);
            t1.setVisibility(View.VISIBLE);
            t1.setText("No Matching Data Found !!!");
*/
            try {

                Log.d("j_data_in_truckActivity",s.toString());

                JSONObject json_data = new JSONObject(s.toString());
                String load_post_id = json_data.getString("load_id");
                String matching_trucks_json_data = json_data.getString("matching_trucks");

                if(matching_trucks_json_data.length() < 10){
                    listView.setVisibility(View.GONE);
                    t1.setVisibility(View.VISIBLE);
                    t1.setText("No Matching Data Found !!!");
                    dialogue.dismiss();
                } else {

                    Log.d("matching_trucks_json_data",matching_trucks_json_data);

                    YoulorrySession youlorr_session = new YoulorrySession(con);
                    youlorr_session.setCurrentloadpostidforbooking(load_post_id);
                    youlorr_session.setMatchingtrucksforpostaload(matching_trucks_json_data);

                    JSONArray jsonArray = new JSONArray(matching_trucks_json_data);
                    String [] truck_list_for_list_view_id = new String[jsonArray.length()];

                    Resources res =getResources();
                    for(int i=0; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        int id = Integer.parseInt(jsonObject.optString("ID").toString());

                        String posted_by_name = jsonObject.getString("posted_by");

                        String meta_keys_jsondata = jsonObject.optString("meta_keys").toString();

                        JSONObject jobj = new JSONObject(meta_keys_jsondata);
                        String source_city_jsondata=jobj.getString("source_city");
                        String s_city_from_json = source_city_jsondata.substring(2, source_city_jsondata.toString().length() - 2);

                        String destination_city_jsondata=jobj.getString("destination_city");
                        String d_city_from_json = destination_city_jsondata.substring(2, destination_city_jsondata.toString().length() - 2);

                        String expected_value_jsondata=jobj.getString("expected_frieght_value");
                        String exp_val_from_json = expected_value_jsondata.substring(2, expected_value_jsondata.toString().length() - 2);

                        String schedule_date_jsondata=jobj.getString("date");
                        String scheduled_date_from_json = schedule_date_jsondata.substring(2, schedule_date_jsondata.toString().length() - 2);

                        String truck_type_jsondata=jobj.getString("truck_type");
                        String truc_type_from_jsondata = truck_type_jsondata.substring(2, truck_type_jsondata.toString().length() - 2);

                        String weight_capacity_jsondata=jobj.getString("weight_capacity");
                        String weight_capacity_from_jsondata = weight_capacity_jsondata.substring(2, weight_capacity_jsondata.toString().length() - 2);


                        if(s_city.equals(s_city_from_json))
                        {
                            adapter=new CustomAdapterfortrucklistview( con, CustomListViewValuesArr,res);
                            listView.setAdapter( adapter );
                        }else{
                            Toast.makeText(getActivity(), "FALSE", Toast.LENGTH_SHORT).show();
                        }

                        truck_list_for_list_view_id[i] = s_city_from_json;

                        String str_posted_by_name = "Transporter" ;
                        if(posted_by_name.equals("transporter_transporter")){
                            str_posted_by_name = "Transporter";
                        } else if(posted_by_name.equals("fleet_owner")) {
                            str_posted_by_name = "Fleet Owner";
                        } else if(posted_by_name.equals("broker")) {
                            str_posted_by_name = "broker";
                        } else {
                            str_posted_by_name = "Transporter";
                        }

                        final ListModelfortrucklistrow sched = new ListModelfortrucklistrow();
                        String[] s_city_split =s_city_from_json.split(",");
                        String[] d_city_split =d_city_from_json.split(",");
                        /******* Firstly take data in model object ******/
                        sched.setSourcecity(s_city_split[0]);
                        sched.setDestinationcity(d_city_split[0]);
                        sched.setTrucktype(truc_type_from_jsondata);
                        sched.setPrice(exp_val_from_json);
                        sched.setPostedby(str_posted_by_name);
                        sched.setSchDate(scheduled_date_from_json);
                        sched.setWeightCapacity(weight_capacity_from_jsondata);

                        /******** Take Model Object in ArrayList **********/
                        CustomListViewValuesArr.add(sched);
                    }
                    dialogue.dismiss();
                }

            } catch (JSONException e) {e.printStackTrace();}
//            dialogue.dismiss();
        }
    }

    @Override
    public void onClick(View view) {

    }


    public void onItemClick(int mPosition) {
//        Toast.makeText(con, "dhjkdghkjshg", Toast.LENGTH_SHORT).show();
    }



}