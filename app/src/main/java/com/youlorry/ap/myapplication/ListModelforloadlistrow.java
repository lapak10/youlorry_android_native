package com.youlorry.ap.myapplication;

/**
 * Created by Arpit Prajapati on 12/15/16.
 */
public class ListModelforloadlistrow {
    private  String S_city="";
    private  String D_city="";
    private  String Date="";
    private  String Price;
    private  String Adv_Price;
    private  String Truck_type;
    private  String Posted_by;
    private  String materialType;
    private  String stuffId;
    private  String weight;

    /*********** Set Methods ******************/

    public void setSourcecity2(String S_city){ this.S_city = S_city; }

    public void setDestinationcity2(String D_city)
    {
        this.D_city = D_city;
    }

    public void setDate2(String date2)
    {
        this.Date = date2;
    }

    public void setPrice2(String Price)
    {
        this.Price = Price;
    }

    public void setPostedby2(String posted_by)
    {
        this.Posted_by = posted_by;
    }

    public void setAdvPrice(String adv_price)
    {
        this.Adv_Price = adv_price;
    }

    public void setTrucktype(String truck_type1)
    {
        this.Truck_type = truck_type1;
    }

    public void setMaterialType(String materialType) { this.materialType = materialType; }

    public void setStuffId(String stuffId) { this.stuffId = stuffId; }

    public void setWeight(String weight) { this.weight = weight; }

    /*********** Get Methods ****************/

    public String getSourcecity2()
    {
        return this.S_city;
    }

    public String getDestinationcity2()
    {
        return this.D_city;
    }

    public String getDate2()
    {
        return this.Date;
    }

    public String getPrice2() { return this.Price; }

    public String getPostedby2()
    {
        return this.Posted_by;
    }

    public String getAdvPrice()
    {
        return this.Adv_Price;
    }

    public String getTrucktype()
    {
        return this.Truck_type;
    }

    public String getMaterialType() { return materialType; }

    public String getStuffId() { return stuffId; }

    public String getWeight() { return weight; }
}
