package com.youlorry.ap.myapplication;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by stark on 3/20/2017.
 */

public class PagerForFactoryLoad extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public PagerForFactoryLoad(FragmentManager fragmentManager, int tabCount) {
        super(fragmentManager);
        //Initializing tab count
        this.tabCount= tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                BidLayout tab1 = new BidLayout();
                return tab1;
            case 1:
                ConfirmedBidLayout tab2 = new ConfirmedBidLayout();
                return tab2;
            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}