package com.youlorry.ap.myapplication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Arpit Prajapati on 12/15/16.
 */
public class LoadInfoActivity extends YouLorryActivity implements View.OnClickListener{

    String load_list_value_for_json;
    String data="",wt_data_for_comparision;
    TextView load_info_in_modal_view;
    ImageView top_cancel_button;
    TextView s_city1,d_city1,exp_val,adv_in_percentage,material_type1,truck_type_1,date1,time1,wt_capacity,lp_a,unlp_a;
    Context context;
    Button book_now_btn;
    EditText quoted_amount_in_modal_view,driver_name,driver_contactinfo,t_wt_capacity_in_modal_view1;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.load_info_modal);

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        YoulorrySession session = new YoulorrySession(this);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(this , LoginActivity.class);
            startActivity(in);

        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        context = this;

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width*0.9),(int)(height*0.790));

        Bundle bundle = getIntent().getExtras();
        load_list_value_for_json = bundle.getString("truck_list_key_for_json");
        context = this;
        load_info_in_modal_view = (TextView) findViewById(R.id.load_info_in_modal_view);
        book_now_btn = (Button) findViewById(R.id.button_id_from_load_info_modal);
        quoted_amount_in_modal_view = (EditText) findViewById(R.id.t_quoted_amount_in_modal_view);
        driver_name = (EditText) findViewById(R.id.t_driver_name_in_modal_view);
        driver_contactinfo = (EditText) findViewById(R.id.t_driver_contact_in_modal_view);
        t_wt_capacity_in_modal_view1 = (EditText) findViewById(R.id.t_wt_capacity_in_modal_view1);

        s_city1 = (TextView) findViewById(R.id.s_city_in_modal_view);
        d_city1 = (TextView) findViewById(R.id.d_city_in_modal_view);
        exp_val = (TextView) findViewById(R.id.expected_frieght_value_in_modal_view);
        adv_in_percentage = (TextView) findViewById(R.id.advance_in_percent_in_modal_view);
        material_type1 = (TextView) findViewById(R.id.material_type_in_modal_view);
        truck_type_1 = (TextView) findViewById(R.id.truck_type_in_modal_view);
        date1 = (TextView) findViewById(R.id.date_in_modal_view);
        time1 = (TextView) findViewById(R.id.time_in_modal_view);
        wt_capacity = (TextView) findViewById(R.id.weight_capacity_in_modal_view);
        lp_a = (TextView) findViewById(R.id.loading_point_address_in_modal_view);
        unlp_a = (TextView) findViewById(R.id.unloading_point_address_in_modal_view);
        top_cancel_button = (ImageView) findViewById(R.id.top_cancel_btn);

        TextWatcher fieldValidatorTextWatcher = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (filterLongEnough()) {
                    compare_two_weights();
                }
            }

            private void compare_two_weights() {
                String str =t_wt_capacity_in_modal_view1.getText().toString();
                int a = Integer.parseInt(str);
                int b = Integer.parseInt(wt_data_for_comparision);
                if(a > b){
                    t_wt_capacity_in_modal_view1.setError("fill weight < "+wt_data_for_comparision);
                }else if (a < 1) {
                    t_wt_capacity_in_modal_view1.setError("fill weight < "+wt_data_for_comparision);
                }
            }

            private boolean filterLongEnough() {
                return t_wt_capacity_in_modal_view1.getText().toString().trim().length() > 0;
            }
        };

        t_wt_capacity_in_modal_view1.addTextChangedListener(fieldValidatorTextWatcher);


        book_now_btn.setOnClickListener(this);
        top_cancel_button.setOnClickListener(this);

        new TheTask().execute();



    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.top_cancel_btn){
            finish();
        }

        if(view.getId() == R.id.button_id_from_load_info_modal) {

            Log.d("wt_data_for_comparision", wt_data_for_comparision);
            Log.d("input_weight_value", t_wt_capacity_in_modal_view1.getText().toString());
            if(new YoulorrySession(context).getPANCardNumber().length() == 0 ){
                new AlertDialog.Builder(context)
                        .setTitle("Alert Message")
                        .setMessage("Your PAN Number not saved. Please update your PAN Number with us so that you will allow to send Quotations.")
                        .setIcon(R.drawable.ic_warning_black_24dp)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                startActivity(new Intent(context, MainActivity.class));
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
            } else if(t_wt_capacity_in_modal_view1.getText().toString().length() == 0) {
                t_wt_capacity_in_modal_view1.setError( "Weight is required!" );
            } else if(Integer.parseInt(t_wt_capacity_in_modal_view1.getText().toString()) > Integer.parseInt(wt_data_for_comparision)){
                t_wt_capacity_in_modal_view1.setError("fill weight < "+wt_data_for_comparision);
            }else if(quoted_amount_in_modal_view.getText().toString().length() == 0){
                quoted_amount_in_modal_view.setError( "Quoted Amount is required!" );
            } else if(driver_name.getText().toString().length() == 0) {
                driver_name.setError( "required field!" );
            } else if(driver_contactinfo.getText().toString().length() == 0) {
                driver_contactinfo.setError( "required field!" );
            } else {

                YoulorrySession session12 = new YoulorrySession(this);
                String username = session12.getusername();
                String password = session12.getpass_word();
                String userid = session12.getuser_id();
                String role = session12.getrole();
                String load_post_id_from_session = session12.getCurrentloadpostidforbooking();

                URL urlObj = null;
                try {
                    urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-add-new-truck/");
                    HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);
                    urlConnection.setUseCaches(false);
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    //  urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setRequestMethod("POST");
                    urlConnection.connect();

                    JSONObject cred = new JSONObject();

                    cred.put("username",username);
                    cred.put("password",password);
                    cred.put("userid",userid);
                    cred.put("role",role);
                    cred.put("load_id",id);
                    cred.put("truck_id",load_post_id_from_session);
                    cred.put("left_count",t_wt_capacity_in_modal_view1.getText().toString());
                    cred.put("quoted_amount",quoted_amount_in_modal_view.getText().toString());
                    cred.put("driver_name",driver_name.getText().toString());
                    cred.put("driver_number",driver_contactinfo.getText().toString());

                    OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(cred.toString());
                    wr.flush();
                    wr.close();

                    //display what returns the POST request

                    StringBuilder sb = new StringBuilder();
                    int HttpResult = urlConnection.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        BufferedReader br = new BufferedReader(
                                new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                        }
                        br.close();

                        if(sb.toString().equals("ok")){
                            new AlertDialog.Builder(this)
                                    .setTitle("Success Message")
                                    .setMessage("Quotation Sent Successfully!!!")
                                    .setIcon(R.drawable.ic_done_all_black_24dp)
                                    .show();

                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    startActivity(new Intent(context, MainActivity.class));
                                    finish();
                                }
                            },2000);
//                         startActivity(new Intent(this, MainActivity.class));
                        }else{

                            new AlertDialog.Builder(this)
                                    .setTitle("Failed Message")
                                    .setMessage("Your quotation was not added due to some error. Please try again!!!")
                                  //  .setMessage(sb.toString())
                                    .setIcon(R.drawable.ic_warning_black_24dp)
                                    .show();

                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    startActivity(new Intent(context, MainActivity.class));
                                    finish();
                                }
                            },4000);
                        }


                    }

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    class TheTask extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;
        String str;

        @Override
        protected void onPreExecute() {
            dialogue = new ProgressDialog(context);
            dialogue.setTitle("Loading ...");
            dialogue.show();
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session_1 = new YoulorrySession(context);
            str = session_1.getMatchingloadsforpostatruck();

            return str.toString();

        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

           // Toast.makeText(context, "hiiiiiii"+s.toString() , Toast.LENGTH_SHORT).show();

            try {

                JSONArray jsonArray = new JSONArray(s);
                //               String [] truck_list_for_list_view_id = new String[jsonArray.length()];

                int truck_list_value_for_json_int = Integer.parseInt(load_list_value_for_json);
                //             Resources res =getResources();
                for(int i=0; i < jsonArray.length(); i++){

                    if(i == truck_list_value_for_json_int ){

                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        id = Integer.parseInt(jsonObject.optString("ID").toString());

                        String meta_keys_jsondata = jsonObject.optString("meta_keys").toString();

                        JSONObject jobj = new JSONObject(meta_keys_jsondata);
                        String source_city_jsondata=jobj.getString("load_source_city");
                        String s_city_from_json = source_city_jsondata.substring(2, source_city_jsondata.toString().length() - 2);
                        s_city1.setText(s_city_from_json);

                        String destination_city_jsondata=jobj.getString("load_destination_city");
                        String d_city_from_json = destination_city_jsondata.substring(2, destination_city_jsondata.toString().length() - 2);
                        d_city1.setText(d_city_from_json);

                        String expected_value_jsondata=jobj.getString("load_expected_frieght_value");
                        String exp_val_from_json = expected_value_jsondata.substring(2, expected_value_jsondata.toString().length() - 2);
                        exp_val.setText("₹ "+exp_val_from_json);

                        String load_material_jsondata=jobj.getString("load_material");
                        String load_material_from_jsondata = load_material_jsondata.substring(2, load_material_jsondata.toString().length() - 2);
                        material_type1.setText(load_material_from_jsondata);

                        String schedule_date_jsondata=jobj.getString("load_date");
                        String scheduled_date_from_json = schedule_date_jsondata.substring(2, schedule_date_jsondata.toString().length() - 2);
                        date1.setText(scheduled_date_from_json);

                        String truck_type_jsondata=jobj.getString("load_truck_type");
                        String truck_type_from_jsondata = truck_type_jsondata.substring(2, truck_type_jsondata.toString().length() - 2);
                        String replaced_truck_type = truck_type_from_jsondata.replace("\\","");
                        truck_type_1.setText(replaced_truck_type);

                        String load_time_jsondata=jobj.getString("load_time");
                        String load_time_from_json = load_time_jsondata.substring(2, load_time_jsondata.toString().length() - 2);
                        time1.setText(load_time_from_json);

                        String left_count_jsondata=jobj.getString("left_count");
                        String left_count_from_json = left_count_jsondata.substring(2, left_count_jsondata.toString().length() - 2);
                        wt_data_for_comparision = left_count_jsondata.substring(2, left_count_jsondata.toString().length() - 2);
                        wt_capacity.setText(left_count_from_json+" MT");
                        Log.d("left_count", left_count_from_json);

                        String load_pickup_point_from_json,load_drop_point_from_json;
                        if(jobj.has("load_pickup_point")){
                            String load_pickup_point_jsondata = jobj.getString("load_pickup_point");
                            load_pickup_point_from_json = load_pickup_point_jsondata.substring(2, load_pickup_point_jsondata.toString().length() - 2);
                        } else {
                            load_pickup_point_from_json = "Inside City";
                        }

                        if(jobj.has("load_drop_point")){
                            String load_drop_point_jsondata = jobj.getString("load_drop_point");
                            load_drop_point_from_json = load_drop_point_jsondata.substring(2, load_drop_point_jsondata.toString().length() - 2);
                        } else {
                            load_drop_point_from_json = "Inside City";
                        }

                        lp_a.setText(load_pickup_point_from_json);
                        unlp_a.setText(load_drop_point_from_json);

                        String load_weight_capacity_jsondata=jobj.getString("load_weight_capacity");
                        String load_weight_capacity_from_json = load_weight_capacity_jsondata.substring(2, load_weight_capacity_jsondata.toString().length() - 2);
                        Log.d("weight", load_weight_capacity_from_json);
//                        wt_data_for_comparision = load_weight_capacity_jsondata.substring(2, load_weight_capacity_jsondata.toString().length() - 2);
//                        wt_capacity.setText(load_weight_capacity_from_json+" MT");
                      //  wt_capacity.setText(left_count_from_json+"/"+load_weight_capacity_from_json+" MT");

/*                        String load_unloading_point_address_jsondata=jobj.getString("load_unloading_point_address");
                        String load_unloading_point_address_from_json = load_unloading_point_address_jsondata.substring(2, load_unloading_point_address_jsondata.toString().length() - 2);
                        unlp_a.setText(load_unloading_point_address_from_json);

                        String load_loading_point_address_jsondata=jobj.getString("load_loading_point_address");
                        String load_loading_point_address_from_json = load_loading_point_address_jsondata.substring(2, load_loading_point_address_jsondata.toString().length() - 2);
                        lp_a.setText(load_loading_point_address_from_json);
*/
                        String load_advance_in_percentage_jsondata=jobj.getString("load_advance_in_percentage");
                        String load_advance_in_percentage_from_json = load_advance_in_percentage_jsondata.substring(2, load_advance_in_percentage_jsondata.toString().length() - 2);
                        Log.d("advance",load_advance_in_percentage_from_json);
                        int adv_res = Integer.parseInt(exp_val_from_json)*Integer.parseInt(load_advance_in_percentage_from_json)/100;
                        adv_in_percentage.setText("₹ "+adv_res);

                        String load_check_box_for_pickup_jsondata=jobj.getString("load_check_box_for_pickup");
                        String load_check_box_for_pickup_from_json = load_check_box_for_pickup_jsondata.substring(2, load_check_box_for_pickup_jsondata.toString().length() - 2);

                        String load_check_box_for_drop_jsondata=jobj.getString("load_check_box_for_drop");
                        String load_check_box_for_drop_from_json = load_check_box_for_drop_jsondata.substring(2, load_check_box_for_drop_jsondata.toString().length() - 2);

//                        data = "Truck ID : "+id+"\n"+"Source : "+s_city_from_json+"\n"+"Destination : "+d_city_from_json+"\n" +"Expected value : "+exp_val_from_json+"\n"+"Truck Type : "+truck_type_from_jsondata+"\n" +"Date : "+scheduled_date_from_json+"\n";
                        quoted_amount_in_modal_view.setText(exp_val_from_json);
  //                      load_info_in_modal_view.setText(data);

                    }

                }

            } catch (JSONException e) {e.printStackTrace();}

            dialogue.dismiss();
        }
    }
}
