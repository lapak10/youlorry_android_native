package com.youlorry.ap.myapplication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Arpit Prajapati on 12/10/16.
 */

public class PostaloadinfindtruckActivity extends YouLorryActivity implements View.OnClickListener,AdapterView.OnItemClickListener {

    private Spinner material_spinner,scheduletime_spinner;
    private EditText expected_freight_value_per_truck,advance_in_percentage,calculated_amt_in_percentage;
    CheckBox ch_box_for_pickup,ch_box_for_drop,ch_box_for_pickup1,ch_box_for_drop1;
    ImageView top_cancel_btn;
    AutoCompleteTextView pick_up_address;
    private static final String[]material_paths = {"Select Material","Jute","Plastic","Building Materials","Chemicals","Coal and Ash","Cotton Seed","Cement","Electronics/Consumer Durables","Fertilizers", "Fruits and Vegetables","Furniture and Wooden Products","Plywood","Tea","Carbon","Silicon Stone","House Hold Goods","Industrial Equipments", "Liquids/Oil","Machinery/Tools/Spares","Medical Instruments","Medicine","Metals","Agro Products","Refrigerated Goods","Textiles Products","Tyres/Rubbers", "Auto Parts","Automobile Vehicle"};
    private static final String[]scheduletime_paths = {"Pick Up Time","12.00 AM","12.30 AM","01.00 AM","01.30 AM","02.00 AM","02.30 AM","03.00 AM","03.30 AM","04.00 AM","04.30 AM","05.00 AM","05.30 AM","06.00 AM","06.30 AM","07.00 AM","07.30 AM","08.00 AM","08.30 AM","09.00 AM","09.30 AM","10.00 AM","10.30 AM","11.00 AM","11.30 AM","12.00 PM","12.30 PM","01.00 PM","01.30 PM","02.00 PM","02.30 PM","03.00 PM","03.30 PM","04.00 PM","04.30 PM","05.00 PM","05.30 PM ","06.00 PM","06.30 PM","07.00 PM","07.30 PM","08.00 PM","08.30 PM","09.00 PM","09.30 PM","10.00 PM","10.30 PM","11.00 PM","11.30 PM "};
    Context con;
    Button truck_post_submit_btn;
    TextView date_pick;
    String truck_id,quoted_amt,weight_capacity1,loading_address,unloading_address;
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";

    private static final String API_KEY = "AIzaSyDYclD4OCZvWnicWUqY66HlkhJ-h8WqmHA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_a_load_in_find_truck);

        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        YoulorrySession session = new YoulorrySession(this);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(this , LoginActivity.class);
            startActivity(in);

        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        con = this;
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width*0.9),(int)(height*0.7));

        Bundle bundle = getIntent().getExtras();
        truck_id = bundle.getString("truck_id");
        quoted_amt = bundle.getString("quoted_amount");
        weight_capacity1 = bundle.getString("weight_capacity");
        loading_address = bundle.getString("loading_address");
        unloading_address = bundle.getString("unloading_address");

        expected_freight_value_per_truck = (EditText) findViewById(R.id.expected_freight_value_per_truck);
        pick_up_address = (AutoCompleteTextView) findViewById(R.id.pickup_address);
        advance_in_percentage = (EditText) findViewById(R.id.advance_in_percent);
        calculated_amt_in_percentage = (EditText) findViewById(R.id.calculated_amt);
        date_pick = (TextView) findViewById(R.id.date_picker);
        top_cancel_btn = (ImageView) findViewById(R.id.top_cancel_btn);

        ch_box_for_pickup = (CheckBox) findViewById(R.id.checkBox_for_pickup_point);
        ch_box_for_drop = (CheckBox) findViewById(R.id.checkBox_for_drop_point);
        ch_box_for_pickup1 = (CheckBox) findViewById(R.id.checkBox_for_pickup_point1);
        ch_box_for_drop1 = (CheckBox) findViewById(R.id.checkBox_for_drop_point1);

        ch_box_for_pickup.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_for_pickup1.setChecked(false);
                ch_box_for_pickup.setChecked(b);
            }
        });

        ch_box_for_pickup1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_for_pickup.setChecked(false);
                ch_box_for_pickup1.setChecked(b);
            }
        });

        ch_box_for_drop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_for_drop1.setChecked(false);
                ch_box_for_drop.setChecked(b);
            }
        });

        ch_box_for_drop1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_for_drop.setChecked(false);
                ch_box_for_drop1.setChecked(b);
            }
        });

        if(expected_freight_value_per_truck.getText().toString().length()!= 0) {

            calculated_amt_in_percentage.setText(

                    "₹ "+(Integer.parseInt(expected_freight_value_per_truck.getText().toString())*Integer.parseInt(advance_in_percentage.getText().toString()) / 100)+""
            );
        }

        material_spinner = (Spinner)findViewById(R.id.material_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item,material_paths);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        material_spinner.setAdapter(adapter);

        scheduletime_spinner = (Spinner) findViewById(R.id.scheduletime_spinner);
        ArrayAdapter<String> adapter_2 = new ArrayAdapter<String>(con,
                android.R.layout.simple_spinner_item, scheduletime_paths);
        scheduletime_spinner.setAdapter(adapter_2);

        truck_post_submit_btn = (Button) findViewById(R.id.post_a_load_submit_button);

        truck_post_submit_btn.setOnClickListener(this);



        TextWatcher fieldValidatorTextWatcher = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (filterLongEnough()) {
                    calculate_amt_in_percentage();
                }
            }

            private void calculate_amt_in_percentage() {
                String str =advance_in_percentage.getText().toString();
                int a =Integer.parseInt(str);
                if(a<70){

                    //Toast.makeText(con, "Please enter more than 40", Toast.LENGTH_SHORT).show();
                    //advance_in_percentage.setFocusable(false);
                    advance_in_percentage.setError("Min percentage 70");

                }else if (a>100) {
                    advance_in_percentage.setError("Max percentage 100");
                }else{

                    if(expected_freight_value_per_truck.getText().toString().length() > 1){
                       float b = Integer.parseInt(expected_freight_value_per_truck.getText().toString());
                       float result = (b * a) / 100;

                        calculated_amt_in_percentage.setText("₹ "+result);
                    }else {

                        calculated_amt_in_percentage.setText("₹ "+0);

                    }


                }
            }

            private boolean filterLongEnough() {
                return advance_in_percentage.getText().toString().trim().length() > 0;
            }
        };

        TextWatcher getExpectedValue = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (filterLongEnough()) {
                    getExpectedValue1();
                }
            }

            private void getExpectedValue1() {
                String str1 =expected_freight_value_per_truck.getText().toString();
                int a =Integer.parseInt(str1);
                if(a<1){

                    //Toast.makeText(con, "Please enter more than 40", Toast.LENGTH_SHORT).show();
                    //advance_in_percentage.setFocusable(false);
                    expected_freight_value_per_truck.setError("only positive value accepted !");

                } else if(a>99999999) {
                    expected_freight_value_per_truck.setError("max length reached !");

                } else{

                        float b = Integer.parseInt(advance_in_percentage.getText().toString());
                        float result = (b * a) / 100;

                        calculated_amt_in_percentage.setText("₹ "+result);

                }
            }

            private boolean filterLongEnough() {
                return expected_freight_value_per_truck.getText().toString().trim().length() > 0;
            }
        };
        advance_in_percentage.addTextChangedListener(fieldValidatorTextWatcher);
        expected_freight_value_per_truck.addTextChangedListener(getExpectedValue);

        pick_up_address.setAdapter(new GooglePlacesAutocompleteAdapter(con, R.layout.listview));
        pick_up_address.setOnItemClickListener(this);
        top_cancel_btn.setOnClickListener(this);


    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String str = (String) adapterView.getItemAtPosition(i);
        // Toast.makeText(con, str, Toast.LENGTH_SHORT).show();
    }

    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:in");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return (String) resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new Filter.FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    @Override
    public void onClick(View view) {

        ProgressDialog dialogue;

        if(view.getId() == R.id.top_cancel_btn){
            finish();
        }

        if(view.getId() == R.id.post_a_load_submit_button)
        {

            String pickup_address,e_f_v_p_truck,str;
            String material,scheduletime,weightcapacity,advance1_in_percentage;

            material= material_spinner.getSelectedItem().toString();
            scheduletime= scheduletime_spinner.getSelectedItem().toString();

            e_f_v_p_truck = expected_freight_value_per_truck.getText().toString();
            pickup_address = pick_up_address.getText().toString();
            advance1_in_percentage = advance_in_percentage.getText().toString();
//            int adv1 = Integer.parseInt(advance_in_percentage.getText().toString());

            if(new YoulorrySession(con).getPANCardNumber().length() == 0 ){
                new AlertDialog.Builder(con)
                        .setTitle("Alert Message")
                        .setMessage("Your PAN Number not saved. Please update your PAN Number with us so that you will allow to Post or send Quotations.")
                        .setIcon(R.drawable.ic_warning_black_24dp)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                startActivity(new Intent(con, MainActivity.class));
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
            } else if(advance1_in_percentage.length() == 0){
                advance_in_percentage.setError( "70 to 100 value allowed !" );
            }else if(Integer.parseInt(advance_in_percentage.getText().toString()) < 70 || Integer.parseInt(advance_in_percentage.getText().toString()) > 100){
                advance_in_percentage.setError( "70 to 100 value allowed !" );
            } else if(material == "Select Material"){
                new AlertDialog.Builder(con)
                        .setTitle("Material Type Required")
                        .setMessage("Please select material type!")
                        .show();
            }else if(scheduletime == "Pick Up Time"){
                new AlertDialog.Builder(con)
                        .setTitle("Schedule Time Required")
                        .setMessage("Please select schedule time!")
                        .show();
            }else if( !ch_box_for_pickup.isChecked() && !ch_box_for_pickup1.isChecked() ){
                new AlertDialog.Builder(con)
                        .setTitle("Pickup Point Required")
                        .setMessage("Please select pickup point!")
                        .show();
            }else if( !ch_box_for_drop.isChecked() && !ch_box_for_drop1.isChecked() ){
                new AlertDialog.Builder(con)
                        .setTitle("Drop Point Required")
                        .setMessage("Please select drop point!")
                        .show();
            }else if(advance1_in_percentage.length() < 2){
                advance_in_percentage.setError( "Min percentage 70!" );
            }else {

                dialogue = new ProgressDialog(con);
                dialogue.setTitle("Loading ...");
                dialogue.show();

                YoulorrySession session = new YoulorrySession(con);
                String username = session.getusername();
                String password = session.getpass_word();
                String userid = session.getuser_id();
                String role = session.getrole();

                URL urlObj = null;
                try {
                    urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-add-new-load-with-quotation/");
                    HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);
                    urlConnection.setUseCaches(false);
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    //  urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setRequestMethod("POST");
                    urlConnection.connect();

                    JSONObject cred = new JSONObject();

                    cred.put("username",username);
                    cred.put("password",password);
                    cred.put("userid",userid);
                    cred.put("role",role);
                    cred.put("quoted_amount", quoted_amt);
                    cred.put("left_count", weight_capacity1);
                    cred.put("loading_address", loading_address);
                    cred.put("unloading_address", unloading_address);
                    cred.put("truck_id", truck_id);
                    cred.put("material",material);
                    cred.put("time",scheduletime);
                    cred.put("expected_frieght_value",e_f_v_p_truck);
//                    cred.put("loading_point_address",pickup_address);
//                    cred.put("unloading_point_address", "Unloading Address");
                    cred.put("advance_in_percentage", advance1_in_percentage);
                    if(ch_box_for_pickup.isChecked()){cred.put("pickup_point", "Inside City");
                    } else if(ch_box_for_pickup1.isChecked()){cred.put("pickup_point", "Outside City");}
                    if(ch_box_for_drop.isChecked()){cred.put("drop_point", "Inside City");
                    } else if(ch_box_for_drop1.isChecked()){cred.put("drop_point", "Outside City");}

                    Log.d("quoted_amount", quoted_amt);
                    Log.d("left_count", weight_capacity1);
                    Log.d("loading_address", loading_address);
                    Log.d("unloading_address", unloading_address);
                    Log.d("truck_id", truck_id);
                    Log.d("material",material);
                    Log.d("time",scheduletime);
                    Log.d("expected_frieght_value",e_f_v_p_truck);
                    Log.d("advance_in_percentage", advance1_in_percentage);
                    Log.d("pickup_point", ch_box_for_pickup.isChecked()+"");
                    Log.d("pickup_point1", ch_box_for_pickup1.isChecked()+"");
                    Log.d("drop_point", ch_box_for_drop.isChecked()+"");
                    Log.d("drop_point1", ch_box_for_drop.isChecked()+"");


                    OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(cred.toString());
                    wr.flush();
                    wr.close();

                    //display what returns the POST request

                    StringBuilder sb = new StringBuilder();
                    int HttpResult = urlConnection.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                        BufferedReader br = new BufferedReader(
                                new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                        }
                        br.close();
                    Log.d("result_for_post_load_in_find_truck", sb.toString());
                    //  Toast.makeText(con, sb.toString() , Toast.LENGTH_LONG).show();
                        if(sb.toString().equals("ok")){
                            new AlertDialog.Builder(this)
                                    .setTitle("Success Message")
                                    .setMessage("Quotation Added Successfully!!!")
                                    .setIcon(R.drawable.ic_done_all_black_24dp)
                                    .show();

                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    startActivity(new Intent(con, MainActivity.class));
                                    finish();
                                }
                            },2000);
//                         startActivity(new Intent(this, MainActivity.class));
                        }else{

                            new AlertDialog.Builder(this)
                                    .setTitle("Failed Message")
                                    .setMessage("Your quotation was not added due to some error. Please try again!!!")
                                    .setIcon(R.drawable.ic_warning_black_24dp)
                                    .show();

                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    startActivity(new Intent(con, MainActivity.class));
                                    finish();
                                }
                            },6000);
//                         startActivity(new Intent(this, MainActivity.class));

                        }
                    }

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            dialogue.dismiss();
            }

        }
    }
}
