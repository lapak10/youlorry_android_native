package com.youlorry.ap.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Arpit Prajapati on 12/30/16.
 */
public class CustomAdapterforexpiredstufflistview extends BaseAdapter implements View.OnClickListener {

    /*********** Declare Used Variables *********/
    private Context activity;
    private ArrayList data;
    private static LayoutInflater inflater=null;
    public Resources res;
    ListModelforloadlistrow tempValues=null;
    ListModelfortrucklistrow tempValues1=null;
    int i=0;

    /*************  CustomAdapterforexpiredstufflistview Constructor *****************/
    public CustomAdapterforexpiredstufflistview(Context a, ArrayList d, Resources resLocal) {

        /********** Take passed values **********/
        activity = a;
        data=d;
        res = resLocal;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = ( LayoutInflater )activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    /******** What is the size of Passed Arraylist Size ************/
    @Override
    public int getCount() {
        if(data.size()<=0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public void onClick(View view) {
        Log.v("CustomAdapter", "=====Row button clicked=====");
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView from_id,to_id,price_id,truck_type_id,adv_price_id,sch_date;
        public TextView from_id1,to_id1,price_id1,truck_type_id1,posted_by_name1,material_type,vehicle_number1,wt_for_truck;

        public TextView sch_date1;

    }

    /****** Depends upon data size called for each row , Create each ListView row *****/
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View vi = view;
        ViewHolder holder;

        if(new YoulorrySession(activity).getrole().toString().equals("customer")){
            if(view==null){

                /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
                vi = inflater.inflate(R.layout.table_item_for_load_list_view, null);

                /****** View Holder Object to contain tabitem.xml file elements ******/

                holder = new CustomAdapterforexpiredstufflistview.ViewHolder();
                holder.from_id = (TextView) vi.findViewById(R.id.from_id_1);
                holder.truck_type_id = (TextView) vi.findViewById(R.id.truck_type_id1);
                holder.to_id = (TextView) vi.findViewById(R.id.to_id_1);
                holder.price_id = (TextView) vi.findViewById(R.id.price_id_1);
                holder.adv_price_id = (TextView) vi.findViewById(R.id.adv_price_id_1);
                holder.sch_date = (TextView) vi.findViewById(R.id.sch_date);
                holder.material_type = (TextView) vi.findViewById(R.id.material_type_in_matching_loads);

                /************  Set holder with LayoutInflater ************/
                vi.setTag( holder );
            }
            else
                holder= (CustomAdapterforexpiredstufflistview.ViewHolder) vi.getTag();

            if(data.size()<=0)
            {
                holder.from_id.setText("No Data");
                holder.truck_type_id.setText("No Data");
                holder.to_id.setText("No Data");
                holder.price_id.setText("No Data");
                holder.adv_price_id.setText("No Data");
                holder.sch_date.setText("No Data");
                holder.material_type.setText("No Data");

            }
            else
            {
                /***** Get each Model object from Arraylist ********/
                tempValues=null;
                tempValues = ( ListModelforloadlistrow ) data.get( i );

/*                int res0;
                String res;
                if(Integer.parseInt(tempValues.getPrice2()) == 0){
                    res0  = (Integer.parseInt(tempValues.getPrice2()) * Integer.parseInt(tempValues.getAdvPrice())) / 100;
                    res = res0+"";
                } else {
                    res = "NA";
                }
*/
                int res = (Integer.parseInt(tempValues.getPrice2()) * Integer.parseInt(tempValues.getAdvPrice())) / 100;
                //     String replaced_truck_type = tempValues.getTrucktype().toString().replace("\\","");
                String replaced_material_type = tempValues.getMaterialType().toString().replace("\\","");

                holder.from_id.setText( tempValues.getSourcecity2() );
                holder.to_id.setText( tempValues.getDestinationcity2() );
                holder.truck_type_id.setText( tempValues.getStuffId() );
                holder.price_id.setText( "₹ "+tempValues.getPrice2() );
                holder.adv_price_id.setText( "₹ "+res );
                holder.sch_date.setText( tempValues.getDate2() );
                holder.material_type.setText( replaced_material_type );
                /******** Set Item Click Listner for LayoutInflater for each row *******/

                vi.setOnClickListener(new CustomAdapterforexpiredstufflistview.OnItemClickListener( i ));
            }
        }
        if(new YoulorrySession(activity).getrole().toString().equals("transporter")){
            if(view==null){

                /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
                vi = inflater.inflate(R.layout.table_item_for_truck_list_view_in_my_stuff, null);

                /****** View Holder Object to contain tabitem.xml file elements ******/

                holder = new CustomAdapterforexpiredstufflistview.ViewHolder();
                holder.from_id1 = (TextView) vi.findViewById(R.id.from_id);
                holder.truck_type_id1 = (TextView) vi.findViewById(R.id.truck_type_id);
                holder.to_id1 = (TextView) vi.findViewById(R.id.to_id);
                holder.price_id1 = (TextView) vi.findViewById(R.id.price_id);
                holder.sch_date1 = (TextView) vi.findViewById(R.id.sch_date1);
                holder.vehicle_number1 = (TextView) vi.findViewById(R.id.vehicle_number);
                holder.wt_for_truck = (TextView) vi.findViewById(R.id.wt_capacity_in_matching_trucks);

                /************  Set holder with LayoutInflater ************/
                vi.setTag( holder );
            }
            else
                holder= (CustomAdapterforexpiredstufflistview.ViewHolder) vi.getTag();

            if(data.size()<=0)
            {
                holder.from_id1.setText("No Data");
                holder.truck_type_id1.setText("No Data");
                holder.to_id1.setText("No Data");
                holder.price_id1.setText("No Data");
                holder.sch_date1.setText("No Data");
                holder.vehicle_number1.setText("No Data");
                holder.wt_for_truck.setText("No Data");

            }
            else
            {
                /***** Get each Model object from Arraylist ********/
                tempValues1 =null;
                tempValues1 = ( ListModelfortrucklistrow ) data.get( i );

                /************  Set Model values in Holder elements ***********/
                String replaced_truck_type = tempValues1.getTruck_type().toString().replace("\\","");

                holder.from_id1.setText( tempValues1.getSourcecity() );
                holder.to_id1.setText( tempValues1.getDestinationcity() );
                holder.truck_type_id1.setText( replaced_truck_type );
                holder.price_id1.setText( tempValues1.getPrice() );
                holder.sch_date1.setText( tempValues1.getSchDate() );
                holder.vehicle_number1.setText(tempValues1.getVehicleNumber());
                holder.wt_for_truck.setText(tempValues1.getWeightCapacity()+" MT");
                /******** Set Item Click Listner for LayoutInflater for each row *******/

                vi.setOnClickListener(new CustomAdapterforexpiredstufflistview.OnItemClickListener( i ));
            }
        }



        return vi;
    }

    /********* Called when Item click in ListView ************/
    private class OnItemClickListener  implements View.OnClickListener {
        private int mPosition;
        ProgressDialog dialogue;

        OnItemClickListener(int position){
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {

            dialogue = new ProgressDialog(activity);
            dialogue.setTitle("Loading ...");
            dialogue.show();

            String str = new YoulorrySession(activity).getExpiredStuff();
            String repost_id,meta_keys;
            Intent in = new Intent(activity,RePostofTruckOrLoad.class);
            Bundle args = new Bundle();

            try{

                JSONArray jsonArray = new JSONArray(str);
                for(int i=0; i < jsonArray.length(); i++) {

                    if(i == mPosition){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        repost_id = jsonObject.optString("ID").toString();
                        meta_keys = jsonObject.optString("meta_keys").toString();

                        args.putString("expired_stuff_position", String.valueOf(mPosition));
                        args.putString("repost_id", repost_id);
                        args.putString("meta_keys", meta_keys);
                        in.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        in.putExtras(args);
                        activity.startActivity(in);

                    }
                }

                } catch (JSONException e) {
                e.printStackTrace();
            }

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    dialogue.dismiss();
                }
            }, 2000);
//            Toast.makeText(activity, mPosition+"", Toast.LENGTH_SHORT).show();

        }
    }
}
