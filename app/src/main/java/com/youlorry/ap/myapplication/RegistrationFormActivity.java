package com.youlorry.ap.myapplication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Arpit Prajapati on 1/23/17.
 */

public class RegistrationFormActivity extends YouLorryActivity implements View.OnClickListener {

    EditText name,mobile_no,pass,c_pass,pan_number,aadhar_card_number;
    CheckBox ch_box_truck_supplier,ch_box_freight_provider,ch_box_transporter,ch_box_transporter_in_f_p,ch_box_fleet_owner,ch_box_broker,
            ch_box_logistic_company,ch_box_mfg_company,ch_box_agree_t_and_c;
    Button registration_btn;
    TextView terms_text_view;
    ProgressDialog dialogue;
    private RadioGroup radioGroup1,radioGroup2;
    private RadioButton radioButton,radioButtonForGP2,radioButton2,radioButton11,radioButton21,radioButton31,radioButton41,radioButton51,radioButton61;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_form);
        setupUI(findViewById(R.id.registration_form_rl));

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        radioGroup1=(RadioGroup)findViewById(R.id.radioGroup);
        radioGroup2=(RadioGroup)findViewById(R.id.radioGroup1);
        radioButton11 = (RadioButton) findViewById(R.id.radioButton11);
        radioButton21 = (RadioButton) findViewById(R.id.radioButton21);
        radioButton31 = (RadioButton) findViewById(R.id.radioButton31);
        radioButton41 = (RadioButton) findViewById(R.id.radioButton41);
        radioButton51 = (RadioButton) findViewById(R.id.radioButton51);
        radioButton61 = (RadioButton) findViewById(R.id.radioButton61);

        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                radioButton=(RadioButton)findViewById(checkedId);

                if(checkedId == R.id.radioButton){
//                    Toast.makeText(getApplicationContext(), radioButton.getText(), Toast.LENGTH_SHORT).show();
                    radioButton11.setVisibility(View.VISIBLE);
                    radioButton21.setVisibility(View.VISIBLE);
                    radioButton31.setVisibility(View.VISIBLE);
                    radioButton41.setVisibility(View.GONE);
                    radioButton51.setVisibility(View.GONE);
                    radioButton61.setVisibility(View.GONE);
                } else if(checkedId == R.id.radioButton2){
//                    Toast.makeText(getApplicationContext(), radioButton.getText(), Toast.LENGTH_SHORT).show();
                    radioButton11.setVisibility(View.GONE);
                    radioButton21.setVisibility(View.GONE);
                    radioButton31.setVisibility(View.GONE);
                    radioButton41.setVisibility(View.VISIBLE);
                    radioButton51.setVisibility(View.VISIBLE);
                    radioButton61.setVisibility(View.VISIBLE);
                }
//               Toast.makeText(getApplicationContext(), radioSexButton.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        name = (EditText) findViewById(R.id.name);
        mobile_no = (EditText) findViewById(R.id.mobile_number);
        pass = (EditText) findViewById(R.id.password);
        c_pass = (EditText) findViewById(R.id.confirm_password);
        pan_number = (EditText) findViewById(R.id.pan_number);
        aadhar_card_number = (EditText) findViewById(R.id.aadhar_card_number);

        ch_box_truck_supplier = (CheckBox) findViewById(R.id.checkBox_for_truck_supplier);
        ch_box_freight_provider = (CheckBox) findViewById(R.id.checkBox_for_freight_provider);
        ch_box_transporter = (CheckBox) findViewById(R.id.checkBox_for_transporter);
        ch_box_transporter_in_f_p = (CheckBox) findViewById(R.id.checkBox_for_transporter_in_f_p);
        ch_box_fleet_owner = (CheckBox) findViewById(R.id.checkBox_for_fleet_owner);
        ch_box_broker = (CheckBox) findViewById(R.id.checkBox_for_broker);
        ch_box_logistic_company = (CheckBox) findViewById(R.id.checkBox_for_logistic_company);
        ch_box_mfg_company = (CheckBox) findViewById(R.id.checkBox_for_mfg_company);
        ch_box_agree_t_and_c = (CheckBox) findViewById(R.id.checkBox_for_agree_terms_and_conditions);

        terms_text_view = (TextView) findViewById(R.id.terms_textview);
        terms_text_view.setOnClickListener(this);

        if (!isNetworkConnected()) {
            new AlertDialog.Builder(this)
                    .setTitle("Check Connection")
                    .setMessage("Internet connection not enable. Please enable it from settings!")
                    .setPositiveButton("try again", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            Intent intent = getIntent();
                            finish();
                            startActivity(intent);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        }

        ch_box_truck_supplier.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_freight_provider.setChecked(false);
                ch_box_truck_supplier.setChecked(b);
                ch_box_transporter.setVisibility(View.VISIBLE);
                ch_box_fleet_owner.setVisibility(View.VISIBLE);
                ch_box_broker.setVisibility(View.VISIBLE);
                ch_box_transporter_in_f_p.setVisibility(View.GONE);
                ch_box_logistic_company.setVisibility(View.GONE);
                ch_box_mfg_company.setVisibility(View.GONE);
                pan_number.setVisibility(View.GONE);
                aadhar_card_number.setVisibility(View.GONE);
            }
        });

        ch_box_freight_provider.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_truck_supplier.setChecked(false);
                ch_box_freight_provider.setChecked(b);
                ch_box_transporter_in_f_p.setVisibility(View.VISIBLE);
                ch_box_logistic_company.setVisibility(View.VISIBLE);
                ch_box_mfg_company.setVisibility(View.VISIBLE);
                ch_box_transporter.setVisibility(View.GONE);
                ch_box_fleet_owner.setVisibility(View.GONE);
                ch_box_broker.setVisibility(View.GONE);
                pan_number.setVisibility(View.GONE);
                aadhar_card_number.setVisibility(View.GONE);
            }
        });

        ch_box_transporter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_fleet_owner.setChecked(false);
                ch_box_broker.setChecked(false);
                ch_box_transporter.setChecked(b);
                ch_box_transporter_in_f_p.setVisibility(View.GONE);
                ch_box_logistic_company.setVisibility(View.GONE);
                ch_box_mfg_company.setVisibility(View.GONE);
                pan_number.setVisibility(View.GONE);
                aadhar_card_number.setVisibility(View.GONE);
            }
        });

        ch_box_fleet_owner.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_transporter.setChecked(false);
                ch_box_broker.setChecked(false);
                ch_box_fleet_owner.setChecked(b);
                ch_box_transporter_in_f_p.setVisibility(View.GONE);
                ch_box_logistic_company.setVisibility(View.GONE);
                ch_box_mfg_company.setVisibility(View.GONE);
                pan_number.setVisibility(View.GONE);
                aadhar_card_number.setVisibility(View.GONE);
            }
        });

        ch_box_broker.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_transporter.setChecked(false);
                ch_box_fleet_owner.setChecked(false);
                ch_box_broker.setChecked(b);
                ch_box_transporter_in_f_p.setVisibility(View.GONE);
                ch_box_logistic_company.setVisibility(View.GONE);
                ch_box_mfg_company.setVisibility(View.GONE);
//                pan_number.setVisibility(View.VISIBLE);
//                aadhar_card_number.setVisibility(View.VISIBLE);
                pan_number.setVisibility(View.GONE);
                aadhar_card_number.setVisibility(View.GONE);
            }
        });


        ch_box_transporter_in_f_p.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_logistic_company.setChecked(false);
                ch_box_mfg_company.setChecked(false);
                ch_box_transporter_in_f_p.setChecked(b);
                ch_box_transporter.setVisibility(View.GONE);
                ch_box_fleet_owner.setVisibility(View.GONE);
                ch_box_broker.setVisibility(View.GONE);
//                pan_number.setVisibility(View.VISIBLE);
//                aadhar_card_number.setVisibility(View.VISIBLE);
                pan_number.setVisibility(View.GONE);
                aadhar_card_number.setVisibility(View.GONE);
            }
        });

        ch_box_logistic_company.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_transporter_in_f_p.setChecked(false);
                ch_box_mfg_company.setChecked(false);
                ch_box_logistic_company.setChecked(b);
                ch_box_transporter.setVisibility(View.GONE);
                ch_box_fleet_owner.setVisibility(View.GONE);
                ch_box_broker.setVisibility(View.GONE);
//                pan_number.setVisibility(View.VISIBLE);
//                aadhar_card_number.setVisibility(View.VISIBLE);
                pan_number.setVisibility(View.GONE);
                aadhar_card_number.setVisibility(View.GONE);
            }
        });

        ch_box_mfg_company.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_logistic_company.setChecked(false);
                ch_box_transporter_in_f_p.setChecked(false);
                ch_box_mfg_company.setChecked(b);
                ch_box_transporter.setVisibility(View.GONE);
                ch_box_fleet_owner.setVisibility(View.GONE);
                ch_box_broker.setVisibility(View.GONE);
//                pan_number.setVisibility(View.VISIBLE);
//                aadhar_card_number.setVisibility(View.VISIBLE);
                pan_number.setVisibility(View.GONE);
                aadhar_card_number.setVisibility(View.GONE);
            }
        });


        registration_btn = (Button) findViewById(R.id.register_btn);
        registration_btn.setOnClickListener(this);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.terms_textview){
            Uri uri = Uri.parse("http://"+getString(R.string.server_api_url)+".com/terms-and-conditions/");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }

        if(view.getId() == R.id.register_btn){

            String truck_supplier="false",freight_provider="false",transporter="false",fleet_owner="false",broker="false",transporter1="false",logistic_comp="false",mfg_comp="false";

            int selectedId=radioGroup1.getCheckedRadioButtonId();
            radioButton=(RadioButton)findViewById(selectedId);

            int selectedId1=radioGroup2.getCheckedRadioButtonId();
            radioButtonForGP2=(RadioButton)findViewById(selectedId1);
//            Log.d("transporter_in_truck_supplier", "*********************************************************");

             String pan_number_str = pan_number.getText().toString();
             Pattern pattern = Pattern.compile("[A-Za-z]{5}[0-9]{4}[A-Za-z]{1}");
             Matcher matcher = pattern.matcher(pan_number_str);

            if(name.getText().toString().length() == 0){
                name.setError("name required!");
            } else if(radioGroup1.getCheckedRadioButtonId() == -1 || radioGroup2.getCheckedRadioButtonId() == -1){
                Toast.makeText(this, "Please select your role", Toast.LENGTH_LONG).show();
            } else if (mobile_no.getText().toString().length() == 0 || mobile_no.getText().toString().length() < 10){
                    mobile_no.setError("mobile number required!");
            } else if(pass.getText().toString().length() == 0 || pass.getText().toString().length() < 5 ){
                pass.setError("password required!");
            } else if (c_pass.getText().toString().length() == 0 || c_pass.getText().toString().length() < 5){
                c_pass.setError("required field!");
            } else if (pass.getText().toString().compareTo(c_pass.getText().toString()) < 0 || pass.getText().toString().compareTo(c_pass.getText().toString()) > 0){
                new AlertDialog.Builder(this)
                        .setTitle("Password Not Matched")
                        .setMessage("Please enter equal password!")
                        .show();
            } else {

                dialogue = new ProgressDialog(this);
                dialogue.setTitle("Loading ...");
                dialogue.show();

                if(radioButton.getText().equals("Truck Supplier")){
                    truck_supplier = "true";
                    freight_provider ="false";
                    if(radioButtonForGP2.getText().equals("Transporter")){
                        transporter = "true";
                        fleet_owner = "false";
                        broker = "false";
                    } else if (radioButtonForGP2.getText().equals("Fleet Owner")){
                        transporter = "false";
                        fleet_owner = "true";
                        broker = "false";
                    } else if (radioButtonForGP2.getText().equals("Broker")){
                        transporter = "false";
                        fleet_owner = "false";
                        broker = "true";
                    }

                } else if (radioButton.getText().equals("Freight Provider")){
                    truck_supplier = "false";
                    freight_provider ="true";
                    if(radioButtonForGP2.getText().equals("Transporter")){
                        transporter1 = "true";
                        logistic_comp = "false";
                        mfg_comp = "false";
                    } else if (radioButtonForGP2.getText().equals("Logistic Com.")){
                        transporter1 = "false";
                        logistic_comp = "true";
                        mfg_comp = "false";
                    } else if (radioButtonForGP2.getText().equals("Mfg Com.")){
                        transporter1 = "false";
                        logistic_comp = "false";
                        mfg_comp = "true";
                    }
                }

                URL urlObj = null;
                try {
                    urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-registration/");
                    HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);
                    urlConnection.setUseCaches(false);
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    //  urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setRequestMethod("POST");
                    urlConnection.connect();

                    JSONObject cred = new JSONObject();

                    cred.put("send_otp_for_registration", "Send OTP");
                    cred.put("contact_number", mobile_no.getText().toString());

                    Log.d("contact_number", mobile_no.getText().toString());

                    OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(cred.toString());
                    wr.flush();
                    wr.close();

                    StringBuilder sb = new StringBuilder();
                    int HttpResult = urlConnection.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        BufferedReader br = new BufferedReader(
                                new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line);
                        }
                        br.close();

                        Log.d("response_data_for_new_user_registration", sb.toString());
                        if(sb.toString().equals("user_already_exists")){
                            Toast.makeText(this, "User Already Exists!!!", Toast.LENGTH_SHORT).show();
                        } else if(sb.toString().equals("sent")){

            Log.d("name", name.getText().toString());
            Log.d("mobile_number", mobile_no.getText().toString());
            Log.d("password", pass.getText().toString());
            Log.d("new_password", c_pass.getText().toString());
            Log.d("pan_number", pan_number.getText().toString());
            Log.d("aadhar_number", aadhar_card_number.getText().toString());
            Log.d("truck_supplier", truck_supplier);
            Log.d("freight_provider", freight_provider);
            Log.d("transporter_in_truck_supplier", transporter);
            Log.d("transporter_in_freight_provider", transporter1);
            Log.d("fleet_owner", fleet_owner);
            Log.d("broker", broker);
            Log.d("logistic_company", logistic_comp);
            Log.d("MFG_company", mfg_comp);

/*
                            Log.d("name", name.getText().toString());
                            Log.d("mobile_number", mobile_no.getText().toString());
                            Log.d("password", pass.getText().toString());
                            Log.d("new_password", c_pass.getText().toString());
                            Log.d("pan_number", pan_number.getText().toString());
                            Log.d("aadhar_number", aadhar_card_number.getText().toString());
                            Log.d("truck_supplier", String.valueOf(ch_box_truck_supplier.isChecked()));
                            Log.d("freight_provider", String.valueOf(ch_box_freight_provider.isChecked()));
                            Log.d("transporter_in_truck_supplier", String.valueOf(ch_box_transporter.isChecked()));
                            Log.d("transporter_in_freight_provider", String.valueOf(ch_box_transporter_in_f_p.isChecked()));
                            Log.d("fleet_owner", String.valueOf(ch_box_fleet_owner.isChecked()));
                            Log.d("broker", String.valueOf(ch_box_broker.isChecked()));
                            Log.d("logistic_company", String.valueOf(ch_box_logistic_company.isChecked()));
                            Log.d("MFG_company", String.valueOf(ch_box_mfg_company.isChecked()));
*/
                            finish();
                            Intent in = new Intent(this, OTPNumber.class);
                            in.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            Bundle bd = new Bundle();
                            bd.putString("first_name", name.getText().toString());
                            bd.putString("contact_number", mobile_no.getText().toString());
                            bd.putString("password", c_pass.getText().toString());
                            bd.putString("pan_number", pan_number.getText().toString());
                            bd.putString("aadhar_number", aadhar_card_number.getText().toString());
                            if(radioButton.getText().equals("Truck Supplier")){bd.putString("type_of_user", "transporter");
                            } else if(radioButton.getText().equals("Freight Provider")){bd.putString("type_of_user", "customer");}
                            if(radioButton.getText().equals("Truck Supplier") && radioButtonForGP2.getText().equals("Transporter")){bd.putString("user_sub_type", "transporter_transporter");
                            } else if(radioButton.getText().equals("Truck Supplier") && radioButtonForGP2.getText().equals("Fleet Owner")){bd.putString("user_sub_type", "fleet_owner");
                            } else if(radioButton.getText().equals("Truck Supplier") && radioButtonForGP2.getText().equals("Broker")){bd.putString("user_sub_type", "broker");}
                            if(radioButton.getText().equals("Freight Provider") && radioButtonForGP2.getText().equals("Transporter")){bd.putString("user_sub_type", "freight_transporter");
                            } else if(radioButton.getText().equals("Freight Provider") && radioButtonForGP2.getText().equals("Logistic Com.")){bd.putString("user_sub_type", "logistic_company");
                            } else if(radioButton.getText().equals("Freight Provider") && radioButtonForGP2.getText().equals("Mfg Com.")){bd.putString("user_sub_type", "manufacturing_company");}

/*                            if(ch_box_truck_supplier.isChecked()){bd.putString("type_of_user", "transporter");
                            } else if(ch_box_freight_provider.isChecked()){bd.putString("type_of_user", "customer");}
                            if(ch_box_truck_supplier.isChecked() && ch_box_transporter.isChecked()){bd.putString("user_sub_type", "transporter_transporter");
                            } else if(ch_box_truck_supplier.isChecked() && ch_box_fleet_owner.isChecked()){bd.putString("user_sub_type", "fleet_owner");
                            } else if(ch_box_truck_supplier.isChecked() && ch_box_broker.isChecked()){bd.putString("user_sub_type", "broker");}
                            if(ch_box_freight_provider.isChecked() && ch_box_transporter_in_f_p.isChecked()){bd.putString("user_sub_type", "freight_transporter");
                            } else if(ch_box_freight_provider.isChecked() && ch_box_logistic_company.isChecked()){bd.putString("user_sub_type", "logistic_company");
                            } else if(ch_box_freight_provider.isChecked() && ch_box_mfg_company.isChecked()){bd.putString("user_sub_type", "manufacturing_company");}
*/
                            bd.putString("from_key", "registration_form");
                            in.putExtras(bd);
                            startActivity(in);
                        } else {
                            Toast.makeText(this, "Some error occurs. Please try later!!!", Toast.LENGTH_SHORT).show();
                        }

                        dialogue.dismiss();
                    }

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
