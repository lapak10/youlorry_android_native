package com.youlorry.ap.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Arpit Prajapati on 11/26/16.
 */

public class FindtruckActivity extends Fragment implements View.OnClickListener,AdapterView.OnItemClickListener{

    private Spinner material_spinner,trucktype_spinner,scheduletime_spinner,weightcapacity_spinner;
    private static final String[]trucktype_paths = {"Select Truck Type","Any","Container Close Body (20-40 Feet)","Container Fixed (40-70 Feet)","Container Open Body (20-40 Feet)","Container Trucks","Double Dacker","Canter 4.5MT (17/6/6 ft) 4 Wheel","Canter 4MT (9/6/6 ft) 4 Wheel","Canter 7.5MT (19/7/7 ft) 6 Wheel","Canters Jumbo (20/7/7 ft)","Flat Bed Trailers (20-32 ft)","Flat Bed Trailers (40-54 ft)","HCV (Trucks/Trailers)","LCV (Light Comercial Vehicle)", "Low Bed Trailer","Open Body Truck","10 Axle Trailer","Truck 14 Wheel","Truck 15MT (22/7/7 ft) 10 Wheel","Truck 20 MT (28/8/8 ft) 12 Wheel","Truck 9 MT (17/7/7 ft) 6 Wheel","Vehicle/Car Carrier (20-80 ft) Closed"};
    Context con;
    AutoCompleteTextView s_city,d_city;
    TextView date_pick,t1,note_description_in_find_truck,adv_search;
    Button find_truck_search_button;
    ProgressDialog dialogue;
    ListView listview_for_available_trucks;
    CustomAdapterforfindtruckresult adapter;
    ImageView spinner_img,cancel_btn,expand_more;
    public ArrayList<ListModelfortrucklistrow> CustomListViewValuesArr = new ArrayList<ListModelfortrucklistrow>();
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";

    private static final String API_KEY = "AIzaSyDYclD4OCZvWnicWUqY66HlkhJ-h8WqmHA";


    Calendar myCalendar = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    String curr_date = sdf.format(myCalendar.getTime());

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=	inflater.inflate(R.layout.find_truck,container,false);

        con=getActivity();
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        YoulorrySession session = new YoulorrySession(con);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(con , LoginActivity.class);
            startActivity(in);

        }

        try {
            InputMethodManager input = (InputMethodManager) getActivity()
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            input.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }catch(Exception e) {
            e.printStackTrace();
        }

        trucktype_spinner = (Spinner) v.findViewById(R.id.trucktype_spinner);
        ArrayAdapter<String> adapter_1 = new ArrayAdapter<String>(con,
                android.R.layout.simple_spinner_item, trucktype_paths);
        trucktype_spinner.setAdapter(adapter_1);

        s_city = (AutoCompleteTextView) v.findViewById(R.id.source_city);
        d_city = (AutoCompleteTextView) v.findViewById(R.id.destination_city);

        s_city.setAdapter(new GooglePlacesAutocompleteAdapter(con, R.layout.listview));
        d_city.setAdapter(new GooglePlacesAutocompleteAdapter(con, R.layout.listview));

        s_city.setOnItemClickListener(this);
        d_city.setOnItemClickListener(this);


        //    schedule_date1 = (EditText) v.findViewById(R.id.schedule_date);
        date_pick = (TextView) v.findViewById(R.id.date_picker);
//        schedule_date = (DatePicker) v.findViewById(R.id.datePicker);

        find_truck_search_button = (Button) v.findViewById(R.id.find_truck_search_button);

        listview_for_available_trucks = (ListView) v.findViewById(R.id.load_filter_listview_for_available_truck);
        t1 = (TextView) v.findViewById(R.id.no_matching_data_found);
        spinner_img = (ImageView) v.findViewById(R.id.truck_type_spinner_img1);
        note_description_in_find_truck = (TextView) v.findViewById(R.id.note_description_in_find_truck);
        cancel_btn = (ImageView) v.findViewById(R.id.top_cancel_btn);
        expand_more = (ImageView) v.findViewById(R.id.expand_more);

        cancel_btn.setOnClickListener(this);

        new FetchAllAvailableTrucks().execute();
        adv_search = (TextView) v.findViewById(R.id.adv_search);
        adv_search.setOnClickListener(this);

        find_truck_search_button.setOnClickListener(this);
        date_pick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(con, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        return v;
    }

    private void updateLabel() {

        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        String pick_date = sdf.format(myCalendar.getTime());

        if(curr_date.compareTo(pick_date) <= 0) {
            date_pick.setText(sdf.format(myCalendar.getTime()));
        }else{
            new AlertDialog.Builder(con)
                    .setTitle("Possible Date Required")
                    .setMessage("Please select date from today!")
                    .show();
        }
    }

    class FetchAllAvailableTrucks extends AsyncTask<Void, Void, String> {


        ProgressDialog dialogue;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {



            YoulorrySession session12 = new YoulorrySession(con);
            String username = session12.getusername();
            String password = session12.getpass_word();
            String userid = session12.getuser_id();
            String role = session12.getrole();
            StringBuilder sb = new StringBuilder();

            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-find-truck/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                //  urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);
                cred.put("source_key","");
                cred.put("destination_key","");
                cred.put("truck_type_key","Any");
                cred.put("date_key","");

                //   DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
                //OutputStream os = urlConnection.getOutputStream();
                //OutputStreamWriter wr = new OutputStreamWriter(os, "UTF-8");
                OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                wr.flush();
                wr.close();

                //display what returns the POST request
                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();

                    // Toast.makeText(con, sb.toString() , Toast.LENGTH_LONG).show();

                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return sb.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            new YoulorrySession(con).setMatchingtrucksforfindtruck(s.toString());

            if(s.length() < 20){
                listview_for_available_trucks.setVisibility(View.GONE);
                t1.setVisibility(View.VISIBLE);
                t1.setText("No Available Trucks Found !!!");
            }

            Log.d("Matching Trucks" , s.toString());

            try {

                JSONArray jsonArray = new JSONArray(s);
                String [] truck_list_for_list_view_id = new String[jsonArray.length()];

                Resources res =getResources();
                for(int i=0; i < jsonArray.length(); i++){

                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    int id = Integer.parseInt(jsonObject.optString("ID").toString());

                    String posted_by_name = jsonObject.getString("posted_by");
                    String posted_author = jsonObject.getString("post_author");

                    String meta_keys_jsondata = jsonObject.optString("meta_keys").toString();

                    JSONObject jobj = new JSONObject(meta_keys_jsondata);
                    String source_city_jsondata=jobj.getString("source_city");
                    String s_city_from_json = source_city_jsondata.substring(2, source_city_jsondata.toString().length() - 2);

                    String destination_city_jsondata=jobj.getString("destination_city");
                    String d_city_from_json = destination_city_jsondata.substring(2, destination_city_jsondata.toString().length() - 2);

                    String expected_value_jsondata=jobj.getString("expected_frieght_value");
                    String exp_val_from_json = expected_value_jsondata.substring(2, expected_value_jsondata.toString().length() - 2);

                    String schedule_date_jsondata=jobj.getString("date");
                    String scheduled_date_from_json = schedule_date_jsondata.substring(2, schedule_date_jsondata.toString().length() - 2);

                    String truck_type_jsondata=jobj.getString("truck_type");
                    String truc_type_from_jsondata = truck_type_jsondata.substring(2, truck_type_jsondata.toString().length() - 2);

                    String weight_capacity_jsondata=jobj.getString("weight_capacity");
                    String weight_capacity_from_jsondata = weight_capacity_jsondata.substring(2, weight_capacity_jsondata.toString().length() - 2);

                    adapter=new CustomAdapterforfindtruckresult( con, CustomListViewValuesArr,res );
                    listview_for_available_trucks.setAdapter( adapter );

                    truck_list_for_list_view_id[i] = s_city_from_json;

                    final ListModelfortrucklistrow sched = new ListModelfortrucklistrow();
                    String[] s_city_split =s_city_from_json.split(",");
                    String[] d_city_split =d_city_from_json.split(",");
                    /******* Firstly take data in model object ******/

                String str_posted_by_name = "Transporter" ;
                    if(posted_by_name.equals("transporter_transporter")){
                        str_posted_by_name = "Transporter";
                    } else if(posted_by_name.equals("fleet_owner")) {
                        str_posted_by_name = "Fleet Owner";
                    } else if(posted_by_name.equals("broker") && posted_author.equals("35")) {
                        str_posted_by_name = "Youlorry";
                    } else if(posted_by_name.equals("broker")) {
                        str_posted_by_name = "broker";
                    } else {
                        str_posted_by_name = "Transporter";
                    }

                    Log.d("str_posted_by_name", str_posted_by_name);

                    sched.setSourcecity(s_city_split[0]);
                    sched.setDestinationcity(d_city_split[0]);
                    sched.setTrucktype(truc_type_from_jsondata);
                    sched.setPrice(exp_val_from_json);
                    sched.setPostedby(str_posted_by_name);
                    sched.setSchDate(scheduled_date_from_json);
                    sched.setWeightCapacity(weight_capacity_from_jsondata);

                    /******** Take Model Object in ArrayList **********/
                    CustomListViewValuesArr.add(sched);
                }

            } catch (JSONException e) {e.printStackTrace();}
            dialogue.dismiss();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String str = (String) adapterView.getItemAtPosition(i);
      //  Toast.makeText(con, str, Toast.LENGTH_SHORT).show();
    }

    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:in");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return (String) resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new Filter.FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.adv_search){
            s_city.setVisibility(View.VISIBLE);
            d_city.setVisibility(View.VISIBLE);
            trucktype_spinner.setVisibility(View.VISIBLE);
            find_truck_search_button.setVisibility(View.VISIBLE);
            spinner_img.setVisibility(View.VISIBLE);
            date_pick.setVisibility(View.VISIBLE);
            note_description_in_find_truck.setVisibility(View.VISIBLE);
            cancel_btn.setVisibility(View.VISIBLE);
            expand_more.setVisibility(View.GONE);
        }

        if(view.getId() == R.id.top_cancel_btn){
            s_city.setVisibility(View.GONE);
            d_city.setVisibility(View.GONE);
            trucktype_spinner.setVisibility(View.GONE);
            find_truck_search_button.setVisibility(View.GONE);
            spinner_img.setVisibility(View.GONE);
            date_pick.setVisibility(View.GONE);
            note_description_in_find_truck.setVisibility(View.GONE);
            cancel_btn.setVisibility(View.GONE);
            expand_more.setVisibility(View.VISIBLE);
        }

        if(view.getId() == R.id.find_truck_search_button){

            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();

            String s_city1,d_city1,scheduled_date1,truck_type;

            truck_type= trucktype_spinner.getSelectedItem().toString();
            scheduled_date1 = date_pick.getText().toString();

            s_city1 = s_city.getText().toString();
            d_city1 = d_city.getText().toString();

            Log.d("source_key",s_city1+"/");
            Log.d("destination_key",d_city1+"/");
            Log.d("truck_type_key",truck_type+"/");
            Log.d("date_key",scheduled_date1+"/");

                YoulorrySession session12 = new YoulorrySession(con);
                String username = session12.getusername();
                String password = session12.getpass_word();
                String userid = session12.getuser_id();
                String role = session12.getrole();

                URL urlObj = null;
                try {
                    urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-find-truck/");
                    HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);
                    urlConnection.setUseCaches(false);
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    //  urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setRequestMethod("POST");
                    urlConnection.connect();

                    JSONObject cred = new JSONObject();

                    cred.put("username",username);
                    cred.put("password",password);
                    cred.put("userid",userid);
                    cred.put("role",role);
                    cred.put("source_key",s_city1);
                    cred.put("destination_key",d_city1);
                    cred.put("truck_type_key",truck_type);
                    cred.put("date_key",scheduled_date1);

                    //   DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
                    //OutputStream os = urlConnection.getOutputStream();
                    //OutputStreamWriter wr = new OutputStreamWriter(os, "UTF-8");
                    OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(cred.toString());
                    // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                    wr.flush();
                    wr.close();

                    //display what returns the POST request

                    StringBuilder sb = new StringBuilder();
                    int HttpResult = urlConnection.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                        BufferedReader br = new BufferedReader(
                                new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        br.close();

                        session12.setMatchingtrucksforfindtruck(sb.toString());
                       // Toast.makeText(con, sb.toString() , Toast.LENGTH_LONG).show();


                        Resultforfindtruck ldf = new Resultforfindtruck ();
                        Bundle args = new Bundle();
                        args.putString("source_key", s_city1);
                        args.putString("destination_key", d_city1);
                        args.putString("json_data_for_truck_list", sb.toString());
                        ldf.setArguments(args);


                        FragmentTransaction transaction=getFragmentManager().beginTransaction();

                        transaction.replace(R.id.content_main,ldf);
                        transaction.addToBackStack(null);
                        transaction.commit();
                        dialogue.dismiss();

                    }

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

        }

    }

    @Override
    public void onResume() {

        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button
//                    startActivity(new Intent(con,MainActivity.class));
                    getActivity().getFragmentManager().popBackStack();
                    return true;

                }

                return false;
            }
        });
    }

}
