package com.youlorry.ap.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Arpit Prajapati on 12/14/16.
 */

public class Customerconfirmedquotes extends Fragment {

    ListView listView;
    TextView t1;
    Context con;
    CustomAdapterforquotationview1 adapter;
    public ArrayList<ListmodalforQuotations1> CustomListViewValuesArr2 = new ArrayList<ListmodalforQuotations1>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.customer_confirmed_quotes,container,false);

        con = getActivity();

        YoulorrySession session = new YoulorrySession(con);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(con , LoginActivity.class);
            startActivity(in);

        }

        listView = (ListView) v.findViewById(R.id.c_confirmed_quotes_listview);
        t1 = (TextView) v.findViewById(R.id.no_matching_data_found);
        new TheTask1().execute();
        return v;
    }

    class TheTask1 extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session = new YoulorrySession(con);
            String username = session.getusername();
            String password = session.getpass_word();
            String userid = session.getuser_id();
            String role = session.getrole();

            StringBuilder sb = new StringBuilder();
            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-fetch-my-all-quotations/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                //  urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);
                cred.put("quotation_status", "confirmed");

                OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                wr.flush();
                wr.close();

                //display what returns the POST request

                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                    br.close();

//                    Toast.makeText(con, sb.toString() , Toast.LENGTH_LONG).show();

                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            YoulorrySession ss = new YoulorrySession(con);
            ss.setConfirmedQuotations(sb.toString());
            return sb.toString();
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                if(s.equals("error_no_quotations_of_yours")){
                    listView.setVisibility(View.GONE);
                    t1.setVisibility(View.VISIBLE);
                    t1.setText("No Quotations !!!");
                } else {

//                Toast.makeText(con, s.toString(), Toast.LENGTH_SHORT).show();
                    Log.d("confirmed quotation", s.toString());

                    JSONArray jsonArray = new JSONArray(s);
                    String[] truck_list_for_list_view_id = new String[jsonArray.length()];

                    Resources res = getResources();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);


                        String id = jsonObject.optString("ID").toString();
                        String post_status1 = jsonObject.optString("post_status").toString();

                        String meta_keys_jsondata = jsonObject.optString("meta_keys").toString();
                        String load_details_jsondata = jsonObject.optString("load_details").toString();

                        JSONObject jobj = new JSONObject(meta_keys_jsondata);
                        String quoted_amount_jsondata = jobj.getString("quoted_amount");
                        String quoted_amount_from_json = quoted_amount_jsondata.substring(2, quoted_amount_jsondata.toString().length() - 2);

                        JSONObject jobj2 = new JSONObject(load_details_jsondata);

                        String load_source_city_jsondata = jobj2.getString("load_source_city");
                        String load_source_city_from_json = load_source_city_jsondata.substring(2, load_source_city_jsondata.toString().length() - 2);

                        String load_destination_city_jsondata = jobj2.getString("load_destination_city");
                        String load_destination_city_from_json = load_destination_city_jsondata.substring(2, load_destination_city_jsondata.toString().length() - 2);

                        String load_date_jsondata = jobj2.getString("load_date");
                        String load_date_from_json = load_date_jsondata.substring(2, load_date_jsondata.toString().length() - 2);

                        String truck_vehicle_number_from_json = "", left_count_from_json = "", load_weight_capacity_from_json = "";

                        if (jobj2.has("left_count")) {
                            String left_count_jsondata = jobj.getString("left_count");
                            left_count_from_json = left_count_jsondata.substring(2, left_count_jsondata.toString().length() - 2);
                        } else {
                            left_count_from_json = "NA";
                        }

                        if (jobj2.has("load_weight_capacity")) {
                            String load_weight_capacity_jsondata = jobj2.getString("load_weight_capacity");
                            load_weight_capacity_from_json = load_weight_capacity_jsondata.substring(2, load_weight_capacity_jsondata.toString().length() - 2);

                        } else {
                            load_weight_capacity_from_json = "NA";
                        }

 /*                       if (jsonObject.has("truck_details")) {

                            String truck_details_jsondata = jsonObject.optString("truck_details").toString();
                            Log.d("truck_datails", truck_details_jsondata);
                            if (truck_details_jsondata.equals("false")) {
                            } else {
                                JSONObject jobj1 = new JSONObject(truck_details_jsondata);

                                if (jobj1.has("vehicle_number")) {
                                    String truck_vehicle_number_jsondata = jobj1.getString("vehicle_number");
                                    truck_vehicle_number_from_json = truck_vehicle_number_jsondata.substring(2, truck_vehicle_number_jsondata.toString().length() - 2);

                                } else if (jobj.has("vehicle_number")) {
                                    String truck_vehicle_number_jsondata = jobj.getString("vehicle_number");
                                    truck_vehicle_number_from_json = truck_vehicle_number_jsondata.substring(2, truck_vehicle_number_jsondata.toString().length() - 2);

                                } else {
                                    truck_vehicle_number_from_json = "NA";
                                }
                            }
                        }
*/
                        if(jsonObject.has("truck_details")) {

                            String truck_details_jsondata = jsonObject.optString("truck_details").toString();
                            Log.d("truck_datails", truck_details_jsondata);
                            if(truck_details_jsondata.equals("false")){
                                truck_vehicle_number_from_json = "NA";
                            } else {
                                JSONObject jobj1 = new JSONObject(truck_details_jsondata);

                                if(jobj1.has("vehicle_number")){
                                    String truck_vehicle_number_jsondata=jobj1.getString("vehicle_number");
                                    truck_vehicle_number_from_json = truck_vehicle_number_jsondata.substring(2, truck_vehicle_number_jsondata.toString().length() - 2);

                                } else {
                                    truck_vehicle_number_from_json = "NA";
                                }
                            }
                        }else {
                            if(jobj.has("quotation_vehicle_number")){
                                String truck_vehicle_number_jsondata=jobj.getString("quotation_vehicle_number");
                                truck_vehicle_number_from_json = truck_vehicle_number_jsondata.substring(2, truck_vehicle_number_jsondata.toString().length() - 2);

                            } else {
                                truck_vehicle_number_from_json = "NA";
                            }
                        }

                        String new_part_amount_jsondata = "", new_full_amount_jsondata = "", new_advance_amount_jsondata = "";

                        if (jsonObject.has("new_data")) {
                            String new_data_jsondata = jsonObject.optString("new_data").toString();
                            Log.d("new_data", new_data_jsondata);

                            JSONObject jobj22 = new JSONObject(new_data_jsondata);

                            if (jobj22.has("new_part_amount")) {
                                new_part_amount_jsondata = jobj22.getString("new_part_amount");
                            } else {
                                new_part_amount_jsondata = "NA";
                            }

                            if (jobj22.has("new_full_amount")) {
                                new_full_amount_jsondata = jobj22.getString("new_full_amount");

                            } else {
                                new_full_amount_jsondata = "NA";
                            }

                            if (jobj22.has("new_advance_amount")) {
                                new_advance_amount_jsondata = jobj22.getString("new_advance_amount");
                            } else {
                                new_advance_amount_jsondata = "NA";
                            }

                        }

                        String load_material_from_json = "";

                        if (jsonObject.has("load_details")) {

                            String load_details_json_data = jsonObject.optString("load_details").toString();
                            Log.d("load_details", load_details_jsondata);
                            JSONObject jobj3 = new JSONObject(load_details_jsondata);

                            if (jobj3.has("load_material")) {
                                String load_material_jsondata = jobj3.getString("load_material");
                                load_material_from_json = load_material_jsondata.substring(2, load_material_jsondata.toString().length() - 2);

                            } else {
                                load_material_from_json = "NA";
                            }
                        }

                        adapter = new CustomAdapterforquotationview1(con, CustomListViewValuesArr2, res);
                        listView.setAdapter(adapter);

                        final ListmodalforQuotations1 sched1 = new ListmodalforQuotations1();
                        String[] lp_a_split = load_source_city_from_json.split(",");
                        String[] unlp_a_split = load_destination_city_from_json.split(",");
                        /******* Firstly take data in model object ******/

                        if (post_status1.equals("truck_pay_ok")) {

                            if (new YoulorrySession(con).getrole().equals("transporter")) {
//                                sched1.setVehicleNumber( truck_vehicle_number_from_json );
                                sched1.setWeight(left_count_from_json);
                            } else if (new YoulorrySession(con).getrole().equals("customer")) {
                                //                               sched1.setVehicleNumber( "YLL"+id );
                                sched1.setWeight(left_count_from_json + "/" + load_weight_capacity_from_json);
                            }
                            sched1.setVehicleNumber(truck_vehicle_number_from_json);
                            sched1.setQuote_ID(id);
                            sched1.setSourcecity1(lp_a_split[0]);
                            sched1.setPostStatus(post_status1);
                            sched1.setDestinationcity1(unlp_a_split[0]);
                            sched1.setScheduleDate(load_date_from_json);
                            sched1.setAdvanceAmount(new_advance_amount_jsondata);
                            //      sched1.setWeight( left_count_from_json );
                            sched1.setMaterialType(load_material_from_json);
                            sched1.setQuotedAmt(new_full_amount_jsondata);

                        } else {
                            continue;
                        }

                        /******** Take Model Object in ArrayList **********/
                        CustomListViewValuesArr2.add(sched1);
                    }
                }
            } catch (JSONException e) {e.printStackTrace();}
            dialogue.dismiss();
        }
    }
}
