package com.youlorry.ap.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Arpit Prajapati on 11/26/16.
 */

public class PostaloadActivity extends Fragment implements View.OnClickListener,AdapterView.OnItemClickListener {

    private Spinner material_spinner,trucktype_spinner,scheduletime_spinner;
    private AutoCompleteTextView source_city,destination_city ;
    private EditText expected_freight_value_per_truck,advance_in_percentage,calculated_amt_in_percentage,wt_edit_text;
    CheckBox ch_box_for_pickup,ch_box_for_drop,ch_box_for_pickup1,ch_box_for_drop1;
    private RadioGroup rg1,rg2;
    private RadioButton rg1_pickup_point,rg2_drop_point;
    String pickup_pt,drop_pt;
    ProgressDialog dialogue;
    private static final String[]material_paths = {"Select Material","Jute","Plastic","Building Materials","Chemicals","Coal and Ash","Cotton Seed","Cement","Electronics/Consumer Durables","Fertilizers", "Fruits and Vegetables","Furniture and Wooden Products","Plywood","Tea","Carbon","Silicon Stone","House Hold Goods","Industrial Equipments", "Liquids/Oil","Machinery/Tools/Spares","Medical Instruments","Medicine","Metals","Agro Products","Refrigerated Goods","Textiles Products","Tyres/Rubbers", "Auto Parts","Automobile Vehicle"};
    private static final String[]trucktype_paths = {"Select Truck Type","Container Close Body (20-40 Feet)","Container Fixed (40-70 Feet)","Container Open Body (20-40 Feet)","Container Trucks","Double Dacker", "Canter 4.5MT (17/6/6 ft) 4 Wheel","Canter 4MT (9/6/6 ft) 4 Wheel","Canter 7.5MT (19/7/7 ft) 6 Wheel","Canters Jumbo (20/7/7 ft)", "Flat Bed Trailers (20-32 ft)","Flat Bed Trailers (40-54 ft)","HCV (Trucks/Trailers)","LCV (Light Comercial Vehicle)","Low Bed Trailer","Open Body Truck", "10 Axle Trailer","Truck 14 Wheel","Truck 15MT (22/7/7 ft) 10 Wheel","Truck 20 MT (28/8/8 ft) 12 Wheel","Truck 9 MT (17/7/7 ft) 6 Wheel", "Vehicle/Car Carrier (20-80 ft) Closed"};
    private static final String[]scheduletime_paths = {"Pick Up Time","12.00 AM","12.30 AM","01.00 AM","01.30 AM","02.00 AM","02.30 AM","03.00 AM","03.30 AM","04.00 AM","04.30 AM","05.00 AM","05.30 AM","06.00 AM","06.30 AM","07.00 AM","07.30 AM","08.00 AM","08.30 AM","09.00 AM","09.30 AM","10.00 AM","10.30 AM","11.00 AM","11.30 AM","12.00 PM","12.30 PM","01.00 PM","01.30 PM","02.00 PM","02.30 PM","03.00 PM","03.30 PM","04.00 PM","04.30 PM","05.00 PM","05.30 PM ","06.00 PM","06.30 PM","07.00 PM","07.30 PM","08.00 PM","08.30 PM","09.00 PM","09.30 PM","10.00 PM","10.30 PM","11.00 PM","11.30 PM "};
    private static final String[]weightcapacity_paths = {"----------","0-3","3-5","5-9","9-15","15-20","20-30","30-40"};
    Context con;
    Button truck_post_submit_btn;
    TextView date_pick;
    int value_of_pickup_point,value_of_drop_point;
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";

    private static final String API_KEY = "AIzaSyDYclD4OCZvWnicWUqY66HlkhJ-h8WqmHA";

    Calendar myCalendar = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            try {
                updateLabel();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    };

    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    String curr_date = sdf.format(myCalendar.getTime());

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=	inflater.inflate(R.layout.post_a_load,container,false);

        con = getActivity();
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        YoulorrySession session = new YoulorrySession(con);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(con , LoginActivity.class);
            startActivity(in);

        }

        try {
            InputMethodManager input = (InputMethodManager) getActivity()
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            input.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }catch(Exception e) {
            e.printStackTrace();
        }


        source_city = (AutoCompleteTextView) v.findViewById(R.id.source_city);
        destination_city = (AutoCompleteTextView) v.findViewById(R.id.destination_city);
        expected_freight_value_per_truck = (EditText) v.findViewById(R.id.expected_freight_value_per_truck);
        advance_in_percentage = (EditText) v.findViewById(R.id.advance_in_percent);
        calculated_amt_in_percentage = (EditText) v.findViewById(R.id.calculated_amt);
        date_pick = (TextView) v.findViewById(R.id.date_picker);
        wt_edit_text = (EditText) v.findViewById(R.id.weightcapacity_edittext);

        ch_box_for_pickup = (CheckBox) v.findViewById(R.id.checkBox_for_pickup_point_inside);
        ch_box_for_pickup1 = (CheckBox) v.findViewById(R.id.checkBox_for_pickup_point_outside);
        ch_box_for_drop = (CheckBox) v.findViewById(R.id.checkBox_for_drop_point_inside);
        ch_box_for_drop1 = (CheckBox) v.findViewById(R.id.checkBox_for_drop_point_outside);


        ch_box_for_pickup.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_for_pickup1.setChecked(false);
                ch_box_for_pickup.setChecked(b);
            }
        });

        ch_box_for_pickup1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_for_pickup.setChecked(false);
                ch_box_for_pickup1.setChecked(b);
            }
        });

        ch_box_for_drop.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_for_drop1.setChecked(false);
                ch_box_for_drop.setChecked(b);
            }
        });

        ch_box_for_drop1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ch_box_for_drop.setChecked(false);
                ch_box_for_drop1.setChecked(b);
            }
        });

        source_city.setAdapter(new GooglePlacesAutocompleteAdapter(con, R.layout.listview));
        destination_city.setAdapter(new GooglePlacesAutocompleteAdapter(con, R.layout.listview));

        source_city.setOnItemClickListener(this);
        destination_city.setOnItemClickListener(this);

        if(expected_freight_value_per_truck.getText().toString().length()!= 0) {

            calculated_amt_in_percentage.setText(

                    "₹ "+(Integer.parseInt(expected_freight_value_per_truck.getText().toString())*Integer.parseInt(advance_in_percentage.getText().toString()) / 100)+""
            );

        }


        material_spinner = (Spinner)v.findViewById(R.id.material_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(con,
                android.R.layout.simple_spinner_item,material_paths);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        material_spinner.setAdapter(adapter);

        trucktype_spinner = (Spinner) v.findViewById(R.id.trucktype_spinner);
        ArrayAdapter<String> adapter_1 = new ArrayAdapter<String>(con,
                android.R.layout.simple_spinner_item, trucktype_paths);
        trucktype_spinner.setAdapter(adapter_1);

        scheduletime_spinner = (Spinner) v.findViewById(R.id.scheduletime_spinner);
        ArrayAdapter<String> adapter_2 = new ArrayAdapter<String>(con,
                android.R.layout.simple_spinner_item, scheduletime_paths);
        scheduletime_spinner.setAdapter(adapter_2);

   /*     weightcapacity_spinner = (Spinner) v.findViewById(R.id.weightcapacity_spinner);
        ArrayAdapter<String> adapter_3 = new ArrayAdapter<String>(con,
                android.R.layout.simple_spinner_item, weightcapacity_paths);
        weightcapacity_spinner.setAdapter(adapter_3);
*/
        truck_post_submit_btn = (Button) v.findViewById(R.id.post_a_load_submit_button);

        truck_post_submit_btn.setOnClickListener(this);



        TextWatcher fieldValidatorTextWatcher = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (filterLongEnough()) {
                    calculate_amt_in_percentage();
                }
            }

            private void calculate_amt_in_percentage() {
                String str =advance_in_percentage.getText().toString();
                int a =Integer.parseInt(str);
                if(a<70){

                    //Toast.makeText(con, "Please enter more than 40", Toast.LENGTH_SHORT).show();
                    //advance_in_percentage.setFocusable(false);
                    advance_in_percentage.setError("Min percentage 70");

                }else if (a>100) {
                    advance_in_percentage.setError("Max percentage 100");
                }else{
                    if(expected_freight_value_per_truck.getText().toString().length() > 1){
                        float b = Integer.parseInt(expected_freight_value_per_truck.getText().toString());
                        float result = (b * a) / 100;

                        calculated_amt_in_percentage.setText("₹ "+result);
                    }else {

                        calculated_amt_in_percentage.setText("₹ "+0);

                    }
                }
            }

            private boolean filterLongEnough() {
                return advance_in_percentage.getText().toString().trim().length() > 0;
            }
        };
        TextWatcher getExpectedValue = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (filterLongEnough()) {
                    getExpectedValue1();
                }
            }

            private void getExpectedValue1() {
                String str1 =expected_freight_value_per_truck.getText().toString();
                int a =Integer.parseInt(str1);
                if(a<1){

                    //Toast.makeText(con, "Please enter more than 40", Toast.LENGTH_SHORT).show();
                    //advance_in_percentage.setFocusable(false);
                    expected_freight_value_per_truck.setError("only positive value accepted !");

                } else if(a>99999999) {
                    expected_freight_value_per_truck.setError("max length reached !");

                } else{

                    float b = Integer.parseInt(advance_in_percentage.getText().toString());
                    float result = (b * a) / 100;

                    calculated_amt_in_percentage.setText("₹ "+result);

                }
            }

            private boolean filterLongEnough() {
                return expected_freight_value_per_truck.getText().toString().trim().length() > 0;
            }
        };
        advance_in_percentage.addTextChangedListener(fieldValidatorTextWatcher);
        expected_freight_value_per_truck.addTextChangedListener(getExpectedValue);

        date_pick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(con, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        return v;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String str = (String) adapterView.getItemAtPosition(i);
       // Toast.makeText(con, str, Toast.LENGTH_SHORT).show();
    }

    private void updateLabel() throws ParseException {

        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        String pick_date = sdf.format(myCalendar.getTime());

        if(sdf.parse(pick_date).before(sdf.parse(curr_date))) {
            new AlertDialog.Builder(con)
                    .setTitle("Possible Date Required")
                    .setMessage("Please select date from today!")
                    .show();
        } else{
            date_pick.setText(sdf.format(myCalendar.getTime()));
        }
    }


    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:in");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return (String) resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new Filter.FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }


    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.post_a_load_submit_button)
        {

        String s_city,d_city,pickup_address,e_f_v_p_truck,str,wt_in_mt;
        String material,truck_type,scheduletime,weightcapacity,scheduled_date1,advance1_in_percentage;

            material= material_spinner.getSelectedItem().toString();
            truck_type= trucktype_spinner.getSelectedItem().toString();
            scheduletime= scheduletime_spinner.getSelectedItem().toString();
            wt_in_mt = wt_edit_text.getText().toString();

//            weightcapacity= weightcapacity_spinner.getSelectedItem().toString();

            scheduled_date1 = date_pick.getText().toString();
            s_city = source_city.getText().toString();
            d_city = destination_city.getText().toString();
            e_f_v_p_truck = expected_freight_value_per_truck.getText().toString();
            advance1_in_percentage = advance_in_percentage.getText().toString();
//            int adv1 = Integer.parseInt(advance_in_percentage.getText().toString());

            if(new YoulorrySession(con).getPANCardNumber().length() == 0 ){
                new AlertDialog.Builder(con)
                        .setTitle("Alert Message")
                        .setMessage("Your PAN Number not saved. Please update your PAN Number with us so that you will allow to Post.")
                        .setIcon(R.drawable.ic_warning_black_24dp)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                startActivity(new Intent(con, MainActivity.class));
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
            } else if(advance_in_percentage.getText().toString().length() == 0){
                advance_in_percentage.setError( "70 to 100 value allowed !" );
            } else if(Integer.parseInt(advance_in_percentage.getText().toString()) < 70 || Integer.parseInt(advance_in_percentage.getText().toString()) > 100){
                advance_in_percentage.setError( "70 to 100 value allowed !" );
            } else if(s_city.length() == 0 ){
                source_city.setError( "Source City required!" );
            } else if(d_city.length() == 0){
                destination_city.setError( "Destination City required!" );
            } else if(expected_freight_value_per_truck.length() == 0){
                expected_freight_value_per_truck.setError( "Required field!" );
            } else if(material == "Select Material"){
                new AlertDialog.Builder(con)
                        .setTitle("Material Type Required")
                        .setMessage("Please select material type!")
                        .show();
            } else if(truck_type == "Select Truck Type"){
                new AlertDialog.Builder(con)
                        .setTitle("Truck Type Required")
                        .setMessage("Please select truck type!")
                        .show();
            } else if(scheduletime == "Pick Up Time"){
                new AlertDialog.Builder(con)
                        .setTitle("Schedule Time Required")
                        .setMessage("Please select schedule time!")
                        .show();
            } else if(wt_in_mt.length() == 0){
                wt_edit_text.setError("Wt. required !");
            } else if( !ch_box_for_pickup.isChecked() && !ch_box_for_pickup1.isChecked() ){
                new AlertDialog.Builder(con)
                        .setTitle("Pickup Point Required")
                        .setMessage("Please select pickup point!")
                        .show();
            } else if( !ch_box_for_drop.isChecked() && !ch_box_for_drop1.isChecked() ){
                new AlertDialog.Builder(con)
                        .setTitle("Drop Point Required")
                        .setMessage("Please select drop point!")
                        .show();
            } else if(advance1_in_percentage.length() < 2){
                advance_in_percentage.setError( "Min percentage 70!" );
            } else{


                dialogue = new ProgressDialog(con);
                dialogue.setTitle("Loading ...");
                dialogue.show();

                YoulorrySession session = new YoulorrySession(con);
                String username = session.getusername();
                String password = session.getpass_word();
                String userid = session.getuser_id();
                String role = session.getrole();

                URL urlObj = null;
                try {
                    urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-add-new-load/");
                    HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);
                    urlConnection.setUseCaches(false);
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    //  urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setRequestMethod("POST");
                    urlConnection.connect();

                    JSONObject cred = new JSONObject();

                    cred.put("username",username);
                    cred.put("password",password);
                    cred.put("userid",userid);
                    cred.put("role",role);
                    cred.put("source_city",s_city);
                    cred.put("destination_city",d_city);
                    cred.put("material",material);
                    cred.put("truck_type",truck_type);
                    cred.put("time",scheduletime);
                    cred.put("date",scheduled_date1);
                    cred.put("expected_frieght_value",e_f_v_p_truck);
                    cred.put("weight_capacity",wt_in_mt);
//                   cred.put("loading_point_address","loading address");
//                   cred.put("unloading_point_address", "Unloading Address");
                    cred.put("advance_in_percentage", advance1_in_percentage);
                    if(ch_box_for_pickup.isChecked()){cred.put("pickup_point", "Inside City");
                    } else if(ch_box_for_pickup1.isChecked()){cred.put("pickup_point", "Outside City");}
                    if(ch_box_for_drop.isChecked()){cred.put("drop_point", "Inside City");
                    } else if(ch_box_for_drop1.isChecked()){cred.put("drop_point", "Outside City");}

                    Log.d("s_city",s_city);
                    Log.d("d_city",d_city);
                    Log.d("material",material);
                    Log.d("truck_type",truck_type);
                    Log.d("time",scheduletime);
                    Log.d("date",scheduled_date1);
                    Log.d("e_f_v_p_truck",e_f_v_p_truck);
                    Log.d("weight_capacity",wt_in_mt);
                    Log.d("advance",advance1_in_percentage);
                    Log.d("pickup_point", String.valueOf(ch_box_for_pickup.isChecked()));
                    Log.d("pickup_point1", String.valueOf(ch_box_for_pickup1.isChecked()));
                    Log.d("drop_point", String.valueOf(ch_box_for_drop.isChecked()));
                    Log.d("drop_point1", String.valueOf(ch_box_for_drop1.isChecked()));


                    //   DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
                    //OutputStream os = urlConnection.getOutputStream();
                    //OutputStreamWriter wr = new OutputStreamWriter(os, "UTF-8");
                    OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(cred.toString());
                    // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                    wr.flush();
                    wr.close();

                    //display what returns the POST request

                    StringBuilder sb = new StringBuilder();
                    int HttpResult = urlConnection.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                        BufferedReader br = new BufferedReader(
                                new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line+"\n");
                        }
                        br.close();

                    //    Toast.makeText(con, sb.toString() , Toast.LENGTH_LONG).show();
                        Toast.makeText(con, "Load Posted Successfully!" , Toast.LENGTH_LONG).show();

                        Log.d("data_from_post_load", sb.toString());
                        TrucklistActivity ldf = new TrucklistActivity ();
                        Bundle args = new Bundle();
                        args.putString("source_key", s_city);
                        args.putString("destination_key", d_city);
                        args.putString("material_key", material);
                        args.putString("truck_type_key", truck_type);
                        args.putString("schedule_time_key", scheduletime);
                        args.putString("schedule_date_key", scheduled_date1);
                        args.putString("expected_freight_value_per_truck_key", e_f_v_p_truck);
                        args.putString("weight_capacity_key", wt_in_mt);
                        args.putString("pickup_address","pickup address");
//                        args.putString("check_box_for_pickup", ch_box_for_pickup.isChecked());
//                        args.putString("check_box_for_drop", ch_box_for_drop.isChecked());
                        args.putString("j_data", sb.toString());
                        ldf.setArguments(args);

                        FragmentTransaction transaction=getFragmentManager().beginTransaction();

                        transaction.replace(R.id.content_main,ldf);
                        transaction.addToBackStack(null);
                        transaction.commit();

                        dialogue.dismiss();

                }

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }

    }

    @Override
    public void onResume() {

        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button
                    getActivity().getFragmentManager().popBackStack();
                    return true;

                }

                return false;
            }
        });
    }

}

