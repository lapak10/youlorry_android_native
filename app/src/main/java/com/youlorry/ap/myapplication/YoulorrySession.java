package com.youlorry.ap.myapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Arpit Prajapati on 12/5/16.
 */

public class YoulorrySession {

    private SharedPreferences prefs;

    public YoulorrySession(Context con) {

        prefs = PreferenceManager.getDefaultSharedPreferences(con);
    }

    public void setusername(String usename) {
        prefs.edit().putString("usename", usename).commit();
      //  prefsCommit();
    }

    public void setFirstName(String firstName) {
        prefs.edit().putString("first_name", firstName).commit();
        //  prefsCommit();
    }

    public void setpass_word(String pass_word) {
        prefs.edit().putString("pass", pass_word).commit();

    }

    public void setuser_id(String userid){

        prefs.edit().putString("user_id", userid).commit();
    }

    public void setrole(String user_role){
        prefs.edit().putString("user_roles", user_role).commit();
    }

    public void setCurrentloadpostidforbooking(String load_id){
        prefs.edit().putString("load_post_id", load_id).commit();
    }

    public void setCurrenttruckpostidforbooking(String truck_id){
        prefs.edit().putString("load_post_id", truck_id).commit();
    }

    public void setMatchingtrucksforpostaload(String matched_trucks){
        prefs.edit().putString("matching_trucks_for_post_load", matched_trucks).commit();
    }

    public void setMatchingtrucksforfindtruck(String matched_trucks1){
        prefs.edit().putString("matching_trucks_for_find_truck", matched_trucks1).commit();
    }

    public void setCustomerQuotations(String customer_quotations){
        prefs.edit().putString("customer_all_quotations", customer_quotations).commit();
    }

    public void setTransporterQuotations(String transporter_quotations){
        prefs.edit().putString("transporter_all_quotations", transporter_quotations).commit();
    }

    public void setMatchingloadsforpostatruck(String matched_loads){
        prefs.edit().putString("matching_loads_for_post_truck", matched_loads).commit();
    }

    public void setMatchingloadsforfindload(String matched_loads_1){
        prefs.edit().putString("matching_loads_for_find_load", matched_loads_1).commit();
    }

    public void setPendingQuotations(String pending_quotes){
        prefs.edit().putString("pending_quotation_json_data", pending_quotes).commit();
    }

    public void setSentQuotations(String sent_quotes){
        prefs.edit().putString("sent_quotation_json_data", sent_quotes).commit();
    }

    public void setConfirmedQuotations(String confirmed_quotes){
        prefs.edit().putString("confirmed_quotation_json_data", confirmed_quotes).commit();
    }

    public void setCancelledQuotations(String cancelled_quotes){
        prefs.edit().putString("cancelled_quote_json_data", cancelled_quotes).commit();
    }

    public void setHistoryData(String historyData){
        prefs.edit().putString("historyData_json_data", historyData).commit();
    }

    public void setExpiredStuff(String expiredStuff){
        prefs.edit().putString("expiredStuff_json_data", expiredStuff).commit();
    }

    public void setMyStuff(String myStuff) {
        prefs.edit().putString("myStuff_json_data", myStuff).commit();
    }

    public void setWeeklyRecord(String weeklyRecord) {
        prefs.edit().putString("weekly_record", weeklyRecord).commit();
    }

    public void setPANCardNumber(String panCardNumber) {
        prefs.edit().putString("pan_card_number", panCardNumber).commit();
    }

    /************************** GET METHODS ********************************/

    public String getusername() {
        String usename = prefs.getString("usename","");
        return usename;
    }

    public String getFirstName() {
        String firstName = prefs.getString("first_name","");
        return firstName;
    }

    public String getpass_word() {
        String pass_word = prefs.getString("pass","");
        return pass_word;
    }

    public String getuser_id(){
        String userid = prefs.getString("user_id", "");
        return userid;
    }

    public String getrole(){
        String user_role = prefs.getString("user_roles", "");
        return user_role;
    }

    public String getCurrentloadpostidforbooking(){
        String load_id = prefs.getString("load_post_id", "");
        return load_id;
    }

    public String getCurrenttruckpostidforbooking(){
        String truck_id = prefs.getString("truck_post_id", "");
        return truck_id;
    }

    public String getMatchingtrucksforpostaload(){
        String matched_trucks = prefs.getString("matching_trucks_for_post_load", "");
        return matched_trucks;
    }

    public String getMatchingtrucksforfindtruck(){
        String matched_trucks1 = prefs.getString("matching_trucks_for_find_truck", "");
        return matched_trucks1;
    }

    public String getCustomerQuotations(){
        String customer_quotations = prefs.getString("customer_all_quotations", "");
        return customer_quotations;
    }

    public String getMatchingloadsforpostatruck(){
        String matched_loads = prefs.getString("matching_loads_for_post_truck", "");
        return matched_loads;
    }

    public String getMatchingloadsforfindload(){
        String matched_loads_1 = prefs.getString("matching_loads_for_find_load", "");
        return matched_loads_1;
    }

    public String getTransporterQuotations(){
        String transporter_quotations = prefs.getString("transporter_all_quotations", "");
        return transporter_quotations;
    }

    public String getPendingQuotations(){
        String pending_quotations = prefs.getString("pending_quotation_json_data", "");
        return pending_quotations;
    }

    public String getSentQuotations(){
        String sent_quotations = prefs.getString("sent_quotation_json_data", "");
        return sent_quotations;
    }

    public String getConfirmedQuotations(){
        String confirmed_quotations = prefs.getString("confirmed_quotation_json_data", "");
        return confirmed_quotations;
    }

    public String getCancelledQuotations(){
        String cancelled_quotes = prefs.getString("cancelled_quote_json_data", "");
        return cancelled_quotes;
    }

    public String getHistoryData(){
        String historyData = prefs.getString("historyData_json_data", "");
        return historyData;
    }

    public String getExpiredStuff(){
        String expiredStuff = prefs.getString("expiredStuff_json_data", "");
        return expiredStuff;
    }

    public String getMyStuff(){
        String myStuff = prefs.getString("myStuff_json_data", "");
        return myStuff;
    }

    public String getWeeklyRecord(){
        String weeklyRecord = prefs.getString("weekly_record", "");
        return weeklyRecord;
    }

    public String getPANCardNumber(){
        String panCardNumber = prefs.getString("pan_card_number", "");
        return panCardNumber;
    }

    public void set_is_user_factory_vendor(String is_user_factory_vendor) {
        prefs.edit().putString("is_user_factory_vendor", is_user_factory_vendor).commit();
    }

    public String getis_user_factory_vendor()
    {
        String is_user_factory_vendor = prefs.getString("is_user_factory_vendor", "");
        return is_user_factory_vendor;
    }

}
