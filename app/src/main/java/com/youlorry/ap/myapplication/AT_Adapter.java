package com.youlorry.ap.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/**
 * Created by Arpit Prajapati on 12/13/16..
 * hello world
 */

public class AT_Adapter extends RecyclerView.Adapter<AT_Adapter.PredictionHolder> implements Filterable{


    private ArrayList<AT_Place> myResultList;
    private GoogleApiClient myApiCLient;
    private LatLngBounds myBounds;
    private AutocompleteFilter myACFilter;
    private Context myContext;
    private int layout;

    public AT_Adapter(Context context, int resource, GoogleApiClient googleApiClient,
                      LatLngBounds bounds, AutocompleteFilter filter){
                myContext = context;
                layout = resource;
                myApiCLient = googleApiClient;
                myBounds = bounds;
                myACFilter = filter;
    }

    public void setMyBounds(LatLngBounds bounds){ myBounds = bounds;}

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                // Skip the autocomplete query if no constraints are given.
                if (constraint != null) {
                    // Query the autocomplete API for the (constraint) search string.
                    myResultList = getAutocomplete(constraint);
                    if (myResultList != null) {
                        // The API successfully returned results.
                        results.values = myResultList;
                        results.count = myResultList.size();
                    }
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    // The API returned at least one result, update the data.
                    notifyDataSetChanged();
                } else {
                    // The API did not return any results, invalidate the data set.
                    //notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    private ArrayList<AT_Place> getAutocomplete(CharSequence constraint){

        if(myApiCLient.isConnected()){
            PendingResult<AutocompletePredictionBuffer> results = Places.GeoDataApi.getAutocompletePredictions(myApiCLient, constraint.toString(), myBounds, myACFilter);

            AutocompletePredictionBuffer autocompletePredictions = results.await(60, TimeUnit.SECONDS);

            final Status status = autocompletePredictions.getStatus();

            if(!status.isSuccess()){
                Toast.makeText(myContext, "Error Connecting API"+status.toString(), Toast.LENGTH_SHORT).show();
                autocompletePredictions.release();
                return null;
            }


            Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
            ArrayList resultList = new ArrayList<>(autocompletePredictions.getCount());
            while (iterator.hasNext()){

                AutocompletePrediction prediction = iterator.next();
                resultList.add(new AT_Place(prediction.getPlaceId(), prediction.getPlaceId()));

            }

            autocompletePredictions.release();
            return resultList;

        }

        return null;
    }

    @Override
    public PredictionHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(layout, viewGroup, false);
        PredictionHolder myPredictionHolder = new PredictionHolder(convertView);
        return myPredictionHolder;
    }

    @Override
    public void onBindViewHolder(PredictionHolder mPredictionHolder, final int i) {
        mPredictionHolder.myPrediction.setText(myResultList.get(i).description);
        /*mPredictionHolder.mRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGetLatLonCallback.getLocation(resultList.get(i).toString());
            }
        });*/
    }

    @Override
    public int getItemCount() {
        if(myResultList != null)
            return myResultList.size();
        else
            return 0;
    }

    public AT_Place getItem(int position){ return myResultList.get(position);}

    public class PredictionHolder extends RecyclerView.ViewHolder{

        private TextView myPrediction;
        private RelativeLayout myRow;

        public PredictionHolder(View itemView) {
            super(itemView);
            myPrediction = (TextView) itemView.findViewById(R.id.address);
            myRow = (RelativeLayout) itemView.findViewById(R.id.predictedRow);
        }
    }

    public class AT_Place{

        public CharSequence placeId;
        public CharSequence description;

        AT_Place(CharSequence placeId, CharSequence description){
            this.placeId = placeId;
            this.description = description;
        }

        @Override
        public String toString() {
            return description.toString();
        }
    }

}
