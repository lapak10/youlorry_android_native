package com.youlorry.ap.myapplication;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Arpit Prajapati on 12/14/16.
 */

public class Pager extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public Pager(FragmentManager fragmentManager, int tabCount) {
        super(fragmentManager);
        //Initializing tab count
        this.tabCount= tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                CustomerSentQuotes tab1 = new CustomerSentQuotes();
                return tab1;
            case 1:
                Customerpendingquotes tab2 = new Customerpendingquotes();
                return tab2;
            case 2:
                Customerconfirmedquotes tab3 = new Customerconfirmedquotes();
                return tab3;
            case 3:
                Customercancelledquotes tab4 = new Customercancelledquotes();
                return tab4;
            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}