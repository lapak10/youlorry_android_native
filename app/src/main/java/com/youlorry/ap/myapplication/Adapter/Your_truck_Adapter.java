package com.youlorry.ap.myapplication.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.youlorry.ap.myapplication.DetailsPageforConfirmedBid;
import com.youlorry.ap.myapplication.Model.Details_Page_confirmBid_Model;
import com.youlorry.ap.myapplication.R;

import java.util.List;

/**
 * Created by Tycho on 5/7/2017.
 */

public class Your_truck_Adapter extends RecyclerView.Adapter<Your_truck_Adapter.Viewholder>   {
    Context context;
    Incoming_Adapter incoming_adapter;

    List<Details_Page_confirmBid_Model.MyTrucksBean> myTrucksBeanList;

    public Your_truck_Adapter(DetailsPageforConfirmedBid detailsPageforConfirmedBid, List<Details_Page_confirmBid_Model.MyTrucksBean> myTrucksBeen) {
        this.context=detailsPageforConfirmedBid;
        this.myTrucksBeanList=myTrucksBeen;
    }
    @Override
    public Your_truck_Adapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_yourtruck, parent, false);
        Your_truck_Adapter.Viewholder viewHolder=new Your_truck_Adapter.Viewholder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Viewholder holder, int position) {
        Details_Page_confirmBid_Model.MyTrucksBean myTrucksBean=myTrucksBeanList.get(position);
        if(myTrucksBean!=null ) {
            List<String> driver_name = myTrucksBean.getFactory_details_driver_name();
            if(driver_name.size()>0)
                holder.textView_driverName.setText((driver_name.get(0).toString()));

            List<String> vechile_Number = myTrucksBean.getQuotation_vehicle_number();
            if(vechile_Number.size()>0)
                holder.textView_Vechile_Number.setText((vechile_Number.get(0).toString()));

            List<String> leftCount = myTrucksBean.getLeft_count();
            if(leftCount.size()>0)
                holder.textView_LeftCount.setText((leftCount.get(0).toString()));

            List<String> number = myTrucksBean.getFactory_details_drivers_mobile_number();
            if(number.size()>0)
                holder.textView_number.setText((number.get(0).toString()));
        }
    }




    @Override
    public int getItemCount() {
        return myTrucksBeanList.size();
        // return details_page_confirmBid_model.getIncoming_quotations();
    }
    public class Viewholder extends RecyclerView.ViewHolder {
        Button delete;
        TextView textView_number, days1, hours1, minutes1, seconds1, from_id, to_id, quoted_amt_id, material_id, expairyTime, weight_id;
        TextView textView_driverName, textView_Vechile_Number,textView_LeftCount;

        public Viewholder(View itemView) {
            super(itemView);
            textView_driverName=(TextView) itemView.findViewById(R.id.textView_driverName);
            textView_Vechile_Number=(TextView) itemView.findViewById(R.id.textView_Vechile_Number);
            textView_LeftCount=(TextView) itemView.findViewById(R.id.textView_LeftCount);
            textView_number=(TextView) itemView.findViewById(R.id.textView_number);
            delete=(Button) itemView.findViewById(R.id.delete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((DetailsPageforConfirmedBid)context).delete_action(myTrucksBeanList.get(getAdapterPosition()).getID(),myTrucksBeanList.get(getAdapterPosition()).getLoad_ID());
                }
            });
        }
    }

}
