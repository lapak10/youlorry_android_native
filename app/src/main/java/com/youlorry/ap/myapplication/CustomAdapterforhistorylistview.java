package com.youlorry.ap.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Arpit Prajapati on 12/30/16.
 */

public class CustomAdapterforhistorylistview extends BaseAdapter implements View.OnClickListener {

    /*********** Declare Used Variables *********/
    private Context activity;
    private ArrayList data;
    private static LayoutInflater inflater=null;
    public Resources res;
    ListModelforhistorylistrow tempValues=null;
    int i=0;

    /*************  CustomAdapterforhistorylistview Constructor *****************/
    public CustomAdapterforhistorylistview(Context a, ArrayList d, Resources resLocal) {

        /********** Take passed values **********/
        activity = a;
        data=d;
        res = resLocal;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = ( LayoutInflater )activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    /******** What is the size of Passed Arraylist Size ************/
    @Override
    public int getCount() {
        if(data.size()<=0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public void onClick(View view) {
        Log.v("CustomAdapter", "===== Row button clicked =====");
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView month_id,no_of_posting,no_of_booking,no_of_cancellation,total_freight_amt,total_material_wt;

    }

    /****** Depends upon data size called for each row , Create each ListView row *****/
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View vi = view;
        ViewHolder holder;

        if(view==null){

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.table_for_history, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new CustomAdapterforhistorylistview.ViewHolder();
            holder.month_id = (TextView) vi.findViewById(R.id.month_id);
            holder.no_of_posting = (TextView) vi.findViewById(R.id.no_of_posting_id);
            holder.no_of_booking = (TextView) vi.findViewById(R.id.no_of_booking_id);
            holder.no_of_cancellation = (TextView) vi.findViewById(R.id.no_of_cancellation_id);
            holder.total_material_wt = (TextView) vi.findViewById(R.id.total_material_wt_id);
            holder.total_freight_amt = (TextView) vi.findViewById(R.id.total_freight_amt_id);

            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        }
        else
            holder= (ViewHolder) vi.getTag();

        if(data.size()<=0)
        {
            holder.month_id.setText("No Data");
            holder.no_of_posting.setText("No Data");
            holder.no_of_booking.setText("No Data");
            holder.no_of_cancellation.setText("No Data");
            holder.total_material_wt.setText("No Data");
            holder.total_freight_amt.setText("No Data");

        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            tempValues=null;
            tempValues = ( ListModelforhistorylistrow ) data.get( i );

            /************  Set Model values in Holder elements ***********/

            holder.month_id.setText( tempValues.getHistoryMonth() );
            holder.no_of_posting.setText( tempValues.getHistoryNoOfPosting() );
            holder.no_of_booking.setText( tempValues.getHistoryNoOfBooking() );
            holder.no_of_cancellation.setText( tempValues.getHistoryNoOfCancellation() );
            holder.total_material_wt.setText( tempValues.getHistoryTotalMaterialWt() );
            holder.total_freight_amt.setText( tempValues.getHistoryTotalFreightAmt() );
            /******** Set Item Click Listner for LayoutInflater for each row *******/

            vi.setOnClickListener(new CustomAdapterforhistorylistview.OnItemClickListener( i ));
        }
        return vi;
    }

    /********* Called when Item click in ListView ************/
    private class OnItemClickListener  implements View.OnClickListener {
        private int mPosition;
        ProgressDialog dialogue;

        OnItemClickListener(int position){
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {

            dialogue = new ProgressDialog(activity);
            dialogue.setTitle("Loading ...");
            dialogue.show();

            Intent in = new Intent(activity,HistoryDataSummary.class);

            in.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            Bundle args = new Bundle();
            args.putString("history_item_position_for_json", String.valueOf(mPosition));

            in.putExtras(args);
            activity.startActivity(in);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    dialogue.dismiss();
                }
            }, 2000);

            //            Toast.makeText(activity, mPosition+"", Toast.LENGTH_SHORT).show();

        }
    }

}
