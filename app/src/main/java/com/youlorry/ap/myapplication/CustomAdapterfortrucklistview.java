package com.youlorry.ap.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Arpit Prajapati on 12/1/16.
 */

public class CustomAdapterfortrucklistview extends BaseAdapter implements View.OnClickListener {

        /*********** Declare Used Variables *********/
        private Context activity;
        private ArrayList data;
        private static LayoutInflater inflater=null;
        public Resources res;
        ListModelfortrucklistrow tempValues=null;
        int i=0;

        /*************  CustomAdapterfortrucklistview Constructor *****************/
        public CustomAdapterfortrucklistview(Context a, ArrayList d, Resources resLocal) {

            /********** Take passed values **********/
            activity = a;
            data=d;
            res = resLocal;

            /***********  Layout inflator to call external xml layout () ***********/
            inflater = ( LayoutInflater )activity.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        /******** What is the size of Passed Arraylist Size ************/
        @Override
        public int getCount() {
            if(data.size()<=0)
                return 1;
            return data.size();
        }

        @Override
        public Object getItem(int i) {
        return i;
        }

        @Override
        public long getItemId(int i) {
        return i;
        }

        @Override
        public void onClick(View view) {
             Log.v("CustomAdapter", "=====Row button clicked=====");
        }

        /********* Create a holder Class to contain inflated xml file elements *********/
        public static class ViewHolder{

            public TextView from_id,to_id,price_id,truck_type_id,posted_by_name;

            public TextView sch_date1,wt_capacity;
        }

        /****** Depends upon data size called for each row , Create each ListView row *****/
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            View vi = view;
            ViewHolder holder;

            if(view==null){

                /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
                vi = inflater.inflate(R.layout.table_item_for_truck_list_view, null);

                /****** View Holder Object to contain tabitem.xml file elements ******/

                holder = new ViewHolder();
                holder.from_id = (TextView) vi.findViewById(R.id.from_id);
                holder.truck_type_id = (TextView) vi.findViewById(R.id.truck_type_id);
                holder.to_id = (TextView) vi.findViewById(R.id.to_id);
                holder.price_id = (TextView) vi.findViewById(R.id.price_id);
                holder.posted_by_name = (TextView) vi.findViewById(R.id.posted_by);
                holder.sch_date1 = (TextView) vi.findViewById(R.id.sch_date1);
                holder.wt_capacity = (TextView) vi.findViewById(R.id.wt_capacity_in_matching_trucks);

                /************  Set holder with LayoutInflater ************/
                vi.setTag( holder );
            }
            else
                holder=(ViewHolder)vi.getTag();

            if(data.size()<=0)
            {
                holder.from_id.setText("No Data");
                holder.truck_type_id.setText("No Data");
                holder.to_id.setText("No Data");
                holder.price_id.setText("No Data");
                holder.posted_by_name.setText("No Data");
                holder.sch_date1.setText("No Data");
                holder.wt_capacity.setText("No Data");

            }
            else
            {
                /***** Get each Model object from Arraylist ********/
                tempValues=null;
                tempValues = ( ListModelfortrucklistrow ) data.get( i );

                /************  Set Model values in Holder elements ***********/
                String replaced_truck_type = tempValues.getTruck_type().toString().replace("\\","");

                holder.from_id.setText( tempValues.getSourcecity() );
                holder.to_id.setText( tempValues.getDestinationcity() );
                holder.truck_type_id.setText( replaced_truck_type );
                holder.price_id.setText( tempValues.getPrice() );
                holder.posted_by_name.setText( tempValues.getPostedby() );
                holder.sch_date1.setText( tempValues.getSchDate() );
                holder.wt_capacity.setText( tempValues.getWeightCapacity()+" MT" );
                /******** Set Item Click Listner for LayoutInflater for each row *******/

                vi.setOnClickListener(new OnItemClickListener( i ));
            }
            return vi;
        }




        /********* Called when Item click in ListView ************/
        private class OnItemClickListener  implements View.OnClickListener {
            private int mPosition;
            ProgressDialog dialogue;

            OnItemClickListener(int position){
                mPosition = position;
            }

            @Override
            public void onClick(View arg0) {

                dialogue = new ProgressDialog(activity);
                dialogue.setTitle("Loading ...");
                dialogue.show();

                Intent in = new Intent(activity,TruckInfoActivity.class);

                in.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                Bundle args = new Bundle();
                args.putString("truck_list_key_for_json", String.valueOf(mPosition));

                in.putExtras(args);
                activity.startActivity(in);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        dialogue.dismiss();
                    }
                }, 2000);

//                Toast.makeText(activity, "HIIIIIIIIIIIIIIIIIII", Toast.LENGTH_SHORT).show();
              //  TrucklistActivity sct = (TrucklistActivity) activity;

                /****  Call  onItemClick Method inside CustomListViewAndroidExample Class ( See Below )****/

              //  sct.onItemClick(mPosition);
            }
        }



}
