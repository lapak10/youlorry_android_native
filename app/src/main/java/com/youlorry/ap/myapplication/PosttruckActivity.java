package com.youlorry.ap.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Arpit Prajapati on 12/13/16.
 */

public class PosttruckActivity extends Fragment implements View.OnClickListener,AdapterView.OnItemClickListener {

    private Spinner t_trucktype_spinner;
    private EditText t_expected_freight_value_in_INR,t_vehicle_no,t_wt_in_mt;
    private AutoCompleteTextView t_source_city,t_destination_city;
    private static final String[]t_trucktype_paths = {"Select Truck Type","Container Close Body (20-40 Feet)","Container Fixed (40-70 Feet)","Container Open Body (20-40 Feet)","Container Trucks","Double Dacker", "Canter 4.5MT (17/6/6 ft) 4 Wheel","Canter 4MT (9/6/6 ft) 4 Wheel","Canter 7.5MT (19/7/7 ft) 6 Wheel","Canters Jumbo (20/7/7 ft)", "Flat Bed Trailers (20-32 ft)","Flat Bed Trailers (40-54 ft)","HCV (Trucks/Trailers)","LCV (Light Comercial Vehicle)","Low Bed Trailer","Open Body Truck", "10 Axle Trailer","Truck 14 Wheel","Truck 15MT (22/7/7 ft) 10 Wheel","Truck 20 MT (28/8/8 ft) 12 Wheel","Truck 9 MT (17/7/7 ft) 6 Wheel", "Vehicle/Car Carrier (20-80 ft) Closed"};
    private static final String[]t_weightcapacity_paths = {"----------","0-3","3-5","5-9","9-15","15-20","20-30","30-40"};
    Context con;
    Button truck_post_submit_btn;
    TextView t_date_pick;
    ProgressDialog dialogue;
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";

    private static final String API_KEY = "AIzaSyDYclD4OCZvWnicWUqY66HlkhJ-h8WqmHA";

    Calendar myCalendar = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            try {
                updateLabel();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    };

    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    String curr_date = sdf.format(myCalendar.getTime());

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.post_a_truck,container,false);


        con = getActivity();
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        YoulorrySession session = new YoulorrySession(con);

        if(session.getuser_id().length() == 0 && session.getusername().length() == 0 && session.getpass_word().length() == 0 && session.getrole().length() == 0){

            Intent in = new Intent(con , LoginActivity.class);
            startActivity(in);

        }

        try {
            InputMethodManager input = (InputMethodManager) getActivity()
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            input.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }catch(Exception e) {
            e.printStackTrace();
        }

        t_vehicle_no = (EditText) v.findViewById(R.id.vehicle_number);
        t_wt_in_mt = (EditText) v.findViewById(R.id.t_weightcapacity_edittext);
        t_source_city = (AutoCompleteTextView) v.findViewById(R.id.t_source_city);
        t_destination_city = (AutoCompleteTextView) v.findViewById(R.id.t_destination_city);

        t_source_city.setAdapter(new GooglePlacesAutocompleteAdapter(con, R.layout.listview));
        t_destination_city.setAdapter(new GooglePlacesAutocompleteAdapter(con, R.layout.listview));

        t_source_city.setOnItemClickListener(this);
        t_destination_city.setOnItemClickListener(this);

//        opt1_d_city = (EditText) v.findViewById(R.id.op1_destination_city);
//        opt2_d_city = (EditText) v.findViewById(R.id.op2_destination_city);
        t_expected_freight_value_in_INR = (EditText) v.findViewById(R.id.t_expected_freight_value_in_inr);
        t_date_pick = (TextView) v.findViewById(R.id.t_date_picker);


        t_trucktype_spinner = (Spinner) v.findViewById(R.id.t_trucktype_spinner);
        ArrayAdapter<String> adapter_1 = new ArrayAdapter<String>(con,
                android.R.layout.simple_spinner_item, t_trucktype_paths);
        t_trucktype_spinner.setAdapter(adapter_1);

        truck_post_submit_btn = (Button) v.findViewById(R.id.post_a_truck_submit_button);

        truck_post_submit_btn.setOnClickListener(this);

        t_date_pick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(con, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        return v;
    }

/*    public static void closeKeyboard(Context c, IBinder windowToken) {
        InputMethodManager mgr = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(windowToken, 0);
    }
*/

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String str = (String) adapterView.getItemAtPosition(i);
        //  Toast.makeText(con, str, Toast.LENGTH_SHORT).show();
    }

    public static ArrayList autocomplete(String input) {
        ArrayList resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:in");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return (String) resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new Filter.FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void updateLabel() throws ParseException {

        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        String pick_date = sdf.format(myCalendar.getTime());
        Date today = new Date();
        Log.d("today_date", curr_date);
        Log.d("picked_date", pick_date);

        if(sdf.parse(pick_date).before(sdf.parse(curr_date))) {
            new AlertDialog.Builder(con)
                    .setTitle("Possible Date Required")
                    .setMessage("Please select date from today!")
                    .show();
        } else{
            t_date_pick.setText(sdf.format(myCalendar.getTime()));
        }
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == R.id.post_a_truck_submit_button)
        {

            String s_city,d_city,e_f_v_in_INR,str,vehicle_number;
            String truck_type,weightcapacity,scheduled_date1;

            truck_type= t_trucktype_spinner.getSelectedItem().toString();
            weightcapacity= t_wt_in_mt.getText().toString();

            scheduled_date1 = t_date_pick.getText().toString();

            vehicle_number = t_vehicle_no.getText().toString();
            s_city = t_source_city.getText().toString();
            d_city = t_destination_city.getText().toString();
            e_f_v_in_INR = t_expected_freight_value_in_INR.getText().toString();

            if(new YoulorrySession(con).getPANCardNumber().length() == 0 ){
                new AlertDialog.Builder(con)
                        .setTitle("Alert Message")
                        .setMessage("Your PAN Number not saved. Please update your PAN Number with us so that you will allow to Post.")
                        .setIcon(R.drawable.ic_warning_black_24dp)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                startActivity(new Intent(con, MainActivity.class));
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
            } else if(t_vehicle_no.length() == 0 ){
                t_vehicle_no.setError( "Vehicle Number required!" );
            }else if(s_city.length() == 0 ){
            t_source_city.setError( "Source City required!" );
            }else if(d_city.length() == 0){
                t_destination_city.setError( "Destination City required!" );
            }else if(t_expected_freight_value_in_INR.length() == 0){
                t_expected_freight_value_in_INR.setError( "Required Field!" );
            }else if(scheduled_date1.length() == 0){
                new AlertDialog.Builder(con)
                        .setTitle("Available Date Required")
                        .setMessage("Please select available date!")
                        .show();
            }else if(truck_type == "Select Truck Type"){
                new AlertDialog.Builder(con)
                        .setTitle("Truck Type Required")
                        .setMessage("Please select truck type!")
                        .show();
            }else if(weightcapacity == "----------"){
                new AlertDialog.Builder(con)
                        .setTitle("Weight Capacity Required")
                        .setMessage("Please select weight capacity!")
                        .show();
            }else{

                dialogue = new ProgressDialog(con);
                dialogue.setTitle("Loading ...");
                dialogue.show();

                YoulorrySession session = new YoulorrySession(con);
                String username = session.getusername();
                String password = session.getpass_word();
                String userid = session.getuser_id();
                String role = session.getrole();

                URL urlObj = null;
                try {
                    urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-add-new-truck/");
                    HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                    urlConnection.setDoOutput(true);
                    urlConnection.setDoInput(true);
                    urlConnection.setUseCaches(false);
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    urlConnection.setRequestMethod("POST");
                    urlConnection.connect();

                    JSONObject cred = new JSONObject();

                    cred.put("username",username);
                    cred.put("password",password);
                    cred.put("userid",userid);
                    cred.put("role",role);
                    cred.put("vehicle_number",vehicle_number);
                    cred.put("source_city",s_city);
                    cred.put("destination_city",d_city);
                    cred.put("truck_type",truck_type);
                    cred.put("date",scheduled_date1);
                    cred.put("expected_frieght_value",e_f_v_in_INR);
                    cred.put("weight_capacity",weightcapacity);

                    OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                    wr.write(cred.toString());
                    wr.flush();
                    wr.close();

                    StringBuilder sb = new StringBuilder();
                    int HttpResult = urlConnection.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        BufferedReader br = new BufferedReader(
                                new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line+"\n");
                        }
                        br.close();

                        //    Toast.makeText(con, sb.toString() , Toast.LENGTH_LONG).show();
                        Toast.makeText(con, "Truck Posted Successfully!" , Toast.LENGTH_LONG).show();

                        Log.d("json_data_to_loadlistActivity" , sb.toString());

                        LoadlistActivity ldf = new LoadlistActivity ();
                        Bundle args = new Bundle();
                        args.putString("vehicle_no", vehicle_number);
                        args.putString("source_key", s_city);
                        args.putString("destination_key", d_city);
                        args.putString("truck_type_key", truck_type);
                        args.putString("schedule_date_key", scheduled_date1);
                        args.putString("weight_capacity_key", weightcapacity);
                        args.putString("j_data", sb.toString());
                        ldf.setArguments(args);

                        FragmentTransaction transaction=getFragmentManager().beginTransaction();

                        transaction.replace(R.id.content_main,ldf);
                        transaction.addToBackStack(null);
                        transaction.commit();

                        dialogue.dismiss();
                    }

                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    @Override
    public void onResume() {

        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    // handle back button
//                    startActivity(new Intent(con, MainActivity.class));
                    getActivity().getFragmentManager().popBackStack();
                    return true;

                }

                return false;
            }
        });
    }

}
