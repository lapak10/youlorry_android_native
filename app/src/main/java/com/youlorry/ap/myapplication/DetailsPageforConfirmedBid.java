package com.youlorry.ap.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.youlorry.ap.myapplication.Adapter.Accepted_Adapter;
import com.youlorry.ap.myapplication.Adapter.Confirmend_bid_Adapter;
import com.youlorry.ap.myapplication.Adapter.Incoming_Adapter;
import com.youlorry.ap.myapplication.Adapter.Your_truck_Adapter;
import com.youlorry.ap.myapplication.JavaClasses.InputFilterMinMax;
import com.youlorry.ap.myapplication.Model.Confirmed_Bid_Model;
import com.youlorry.ap.myapplication.Model.Details_Page_confirmBid_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.widget.Toast.LENGTH_SHORT;

/**
 * Created by Arpit Prajapati on 3/20/2017.
 */

public class DetailsPageforConfirmedBid extends AppCompatActivity implements View.OnClickListener {
    Context con;
    Details_Page_confirmBid_Model details_page_confirmBid_model;
    TextView textView_totalBooked, textView_booked;
    String addvechile_number,addweightCapacity,adddriverName,addmobileNumber;
    EditText et_rateperton,et_vechile_number,et_weightCapacity,et_DriverName,et_MobileNumber;
    Your_truck_Adapter your_truck_adapter;
    RecyclerView truck_recycler_view;
    String deleteAcceptValue,deleteLoadValue;
    String incomingAcceptValue,incomingLoadValue;
    Button button_opentomarket;
    Button button_addnewtruck;
    String metakeyid,bidmetakey;
    String spinner_truck_value,spinner_to_pay_value, spinner_pod_value,rateperton_value;
    RelativeLayout ralativelayout_trucktype,ralativelayout_topay,relative_layout_to_pod;
    ProgressBar progress;
    TextView text_view_destination,textView_Source,textview_load_material,textview_loadWeight,textView_trucktype,textView_to_pay,textView_pod,textView_rateperton;
    List<Details_Page_confirmBid_Model.IncomingQuotationsBean> incomingQuotationsBeanArrayList=new ArrayList<>();
    List<Details_Page_confirmBid_Model.IncomingQuotationsBean> acceptedArray = new ArrayList<>();
    Details_Page_confirmBid_Model.FloatMetaKeysBean floatMetaKeysBean;
    List<Details_Page_confirmBid_Model.MyTrucksBean> myTrucksBeanList=new ArrayList<>();
    ImageView show_btn,hide_btn,show_btn1,hide_btn1,show_btn2,hide_btn2,show_btn3,
               hide_btn3,show_btn4,hide_btn4;
    private static final String[]trucktype_paths = {"Container Close Body (20-40 Feet)","Container Fixed (40-70 Feet)","Container Open Body (20-40 Feet)","Container Trucks","Double Dacker", "Canter 4.5MT (17/6/6 ft) 4 Wheel","Canter 4MT (9/6/6 ft) 4 Wheel","Canter 7.5MT (19/7/7 ft) 6 Wheel","Canters Jumbo (20/7/7 ft)", "Flat Bed Trailers (20-32 ft)","Flat Bed Trailers (40-54 ft)","HCV (Trucks/Trailers)","LCV (Light Comercial Vehicle)","Low Bed Trailer","Open Body Truck", "10 Axle Trailer","Truck 14 Wheel","Truck 15MT (22/7/7 ft) 10 Wheel","Truck 20 MT (28/8/8 ft) 12 Wheel","Truck 9 MT (17/7/7 ft) 6 Wheel", "Vehicle/Car Carrier (20-80 ft) Closed"};
  private static final String[] spinner_toPay_array={"YouLorry","Direct Trucker"};
    private static final String[] spinner_pod_Submission_array={"After 7 Days","After 10 Days","After 15 Days"};
    Spinner truckType_Spinner,spinner_to_Pay,spinner_Balance_Payment;
    ArrayAdapter<CharSequence> adapter;
    RecyclerView incoming_recycler_view;
    RecyclerView accepted_recycler_view;
    Incoming_Adapter incoming_adapter;
    Accepted_Adapter accepted_adapter;
    RecyclerView.LayoutManager layoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_page_for_confirmed_bid);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        con=this;
        new detailpageConfirmedBids().execute();
     //   Intent in = new Intent();
       String dburl = getIntent().getStringExtra("ID");
        truck_recycler_view=(RecyclerView) findViewById(R.id.truck_recycler_view);
        textView_Source=(TextView) findViewById(R.id.textView_Source);
        button_opentomarket=(Button) findViewById(R.id.button_opentomarket);
        et_rateperton=(EditText)findViewById(R.id.et_rateperton);
        et_vechile_number=(EditText) findViewById(R.id.et_vechile_number);
        et_weightCapacity=(EditText) findViewById(R.id.et_weightCapacity);
        et_DriverName=(EditText)findViewById(R.id.et_DriverName);
        et_MobileNumber=(EditText)findViewById(R.id.et_MobileNumber);
        button_addnewtruck=(Button) findViewById(R.id.button_addnewtruck);
        progress=(ProgressBar) findViewById(R.id.progress);
        textView_rateperton=(TextView) findViewById(R.id.textView_rateperton);
        text_view_destination=(TextView) findViewById(R.id.text_view_destination);
        textview_load_material=(TextView) findViewById(R.id.textview_load_material);
        textview_loadWeight=(TextView) findViewById(R.id.textview_loadWeight);
        incoming_recycler_view=(RecyclerView) findViewById(R.id.incoming_recycler_view);
        accepted_recycler_view=(RecyclerView) findViewById(R.id.accepted_recycler_view);
        textView_pod=(TextView) findViewById(R.id.textView_pod);
        textView_to_pay= (TextView) findViewById(R.id.textView_to_pay);
        textView_trucktype= (TextView) findViewById(R.id.textView_trucktype);
        relative_layout_to_pod=(RelativeLayout)findViewById(R.id.relative_layout_to_pod);
        ralativelayout_topay=(RelativeLayout)findViewById(R.id.ralativelayout_topay);
        ralativelayout_trucktype=(RelativeLayout)findViewById(R.id.ralativelayout_trucktype);
        textView_totalBooked=(TextView) findViewById(R.id.textView_totalBooked);
                textView_booked=(TextView) findViewById(R.id.textView_booked);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        int a =  (displaymetrics.heightPixels*30)/100;
      //  et_rateperton.setFilters(new InputFilter[]{ new InputFilterMinMax("1","100000000000000000000000")});
       // et_rateperton.setFilters(new InputFilter[]{ new InputFilterMinMax("1", String.valueOf(""))});


        et_rateperton.addTextChangedListener(new TextWatcher()
        {
            public void afterTextChanged(Editable s)
            {
                String x = s.toString();
                if(x.startsWith("0"))
                {
                    et_rateperton.setText("");
                    //your stuff here
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }
        });


        incoming_recycler_view.getLayoutParams().height =a;
        accepted_recycler_view.getLayoutParams().height =a;
        truck_recycler_view.getLayoutParams().height=a;
        show_btn = (ImageView) findViewById(R.id.expand_more);
        hide_btn = (ImageView) findViewById(R.id.top_cancel_btn);
        show_btn1 = (ImageView) findViewById(R.id.expand_more1);
        hide_btn1 = (ImageView) findViewById(R.id.top_cancel_btn1);
        show_btn2 = (ImageView) findViewById(R.id.expand_more2);
        hide_btn2 = (ImageView) findViewById(R.id.top_cancel_btn2);
        show_btn3 = (ImageView) findViewById(R.id.expand_more3);
        hide_btn3 = (ImageView) findViewById(R.id.top_cancel_btn3);
        show_btn4 = (ImageView) findViewById(R.id.expand_more4);
        hide_btn4 = (ImageView) findViewById(R.id.top_cancel_btn4);

        final LinearLayout load_details_layout = (LinearLayout) findViewById(R.id.load_details_layout);
        final LinearLayout your_trucks_layout = (LinearLayout) findViewById(R.id.your_trucks_layout);
        final LinearLayout incoming_quotes_layout = (LinearLayout) findViewById(R.id.incoming_quotations_layout);
        final LinearLayout open_to_market_layout = (LinearLayout) findViewById(R.id.open_to_market);

        truckType_Spinner = (Spinner) findViewById(R.id.trucktype_spinner_in_confirm_bid_detail);
        spinner_to_Pay=(Spinner) findViewById(R.id.spinner_to_Pay);
        spinner_Balance_Payment=(Spinner) findViewById(R.id.spinner_Balance_Payment);

        ArrayAdapter<String> truckAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,trucktype_paths);
        truckAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        truckType_Spinner.setAdapter(truckAdapter);
        spinner_truck_value=trucktype_paths[0];
        truckType_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> adapterView,
                                       View view, int i, long l) {
                // TODO Auto-generated method stub
                if (i != 0)
                    spinner_truck_value=trucktype_paths[i];
                   // Toast.makeText(getActivity(), "You have Selected: " + trucktype_paths[i], Toast.LENGTH_SHORT).show();
            }

            // If no option selected
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });


//        ArrayAdapter<String> topayadapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,spinner_toPay_array);
//        truckAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner_to_Pay.setAdapter(topayadapter);
        adapter = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item, spinner_toPay_array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_to_Pay.setAdapter(adapter);
        spinner_to_pay_value=spinner_toPay_array[0];
        spinner_to_Pay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> adapterView,
                                       View view, int i, long l) {
                // TODO Auto-generated method stub
                if (i != 0)
                    spinner_to_pay_value=spinner_toPay_array[i];
                // Toast.makeText(getActivity(), "You have Selected: " + trucktype_paths[i], Toast.LENGTH_SHORT).show();
            }

            // If no option selected
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });

//        ArrayAdapter<String> podadapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,spinner_pod_Submission_array);
//        truckAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner_Balance_Payment.setAdapter(podadapter);


        adapter = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item, spinner_pod_Submission_array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Balance_Payment.setAdapter(adapter);
        spinner_pod_value=spinner_pod_Submission_array[0];
        spinner_Balance_Payment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> adapterView,
                                       View view, int i, long l) {
                // TODO Auto-generated method stub
                if (i != 0)
                    spinner_pod_value=spinner_pod_Submission_array[i];
                // Toast.makeText(getActivity(), "You have Selected: " + trucktype_paths[i], Toast.LENGTH_SHORT).show();
            }

            // If no option selected
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });


        show_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(load_details_layout.getVisibility() == View.GONE){
                    load_details_layout.setVisibility(View.VISIBLE);
                    hide_btn.setVisibility(View.VISIBLE);
                    show_btn.setVisibility(View.GONE);
                }
            }
        });

        hide_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(load_details_layout.getVisibility() == View.VISIBLE){
                    load_details_layout.setVisibility(View.GONE);
                    show_btn.setVisibility(View.VISIBLE);
                    hide_btn.setVisibility(View.GONE);
                }
            }
        });

        show_btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(your_trucks_layout.getVisibility() == View.GONE){
                    your_trucks_layout.setVisibility(View.VISIBLE);
                    hide_btn1.setVisibility(View.VISIBLE);
                    show_btn1.setVisibility(View.GONE);
                }
            }
        });
        hide_btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(your_trucks_layout.getVisibility() == View.VISIBLE){
                    your_trucks_layout.setVisibility(View.GONE);
                    show_btn1.setVisibility(View.VISIBLE);
                    hide_btn1.setVisibility(View.GONE);
                }
            }
        });
//     buttons for accepted quatations
        show_btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(incoming_quotes_layout.getVisibility() == View.GONE){
                    incoming_quotes_layout.setVisibility(View.VISIBLE);
                    hide_btn2.setVisibility(View.VISIBLE);
                    show_btn2.setVisibility(View.GONE);
                }
            }
        });
        hide_btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(incoming_quotes_layout.getVisibility() == View.VISIBLE){
                    incoming_quotes_layout.setVisibility(View.GONE);
                    show_btn2.setVisibility(View.VISIBLE);
                    hide_btn2.setVisibility(View.GONE);
                }
            }
        });

        show_btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(open_to_market_layout.getVisibility() == View.GONE){
                    open_to_market_layout.setVisibility(View.VISIBLE);
                    hide_btn3.setVisibility(View.VISIBLE);
                    show_btn3.setVisibility(View.GONE);
                }
            }
        });
        hide_btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(open_to_market_layout.getVisibility() == View.VISIBLE){
                    open_to_market_layout.setVisibility(View.GONE);
                    show_btn3.setVisibility(View.VISIBLE);
                    hide_btn3.setVisibility(View.GONE);
                }
            }
        });
//     buttons for incoming quatations
        show_btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(incoming_quotes_layout.getVisibility() == View.GONE){
                    incoming_quotes_layout.setVisibility(View.VISIBLE);
                    hide_btn4.setVisibility(View.VISIBLE);
                    show_btn4.setVisibility(View.GONE);
                }
            }
        });
        hide_btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(incoming_quotes_layout.getVisibility() == View.VISIBLE){
                    incoming_quotes_layout.setVisibility(View.GONE);
                    show_btn4.setVisibility(View.VISIBLE);
                    hide_btn4.setVisibility(View.GONE);
                }
            }
        });


        button_opentomarket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rateperton_value=et_rateperton.getText().toString();
                if(validatevechile_number(et_rateperton))
                new openMarket().execute();
            }
        });
        button_addnewtruck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(validatevechile_number(et_vechile_number)&&validatevechile_number(et_weightCapacity)&&validatevechile_number(et_DriverName)&&validateMobile())
                {
                    addvechile_number=et_vechile_number.getText().toString();
                    addweightCapacity=et_weightCapacity.getText().toString();
                    adddriverName=et_DriverName.getText().toString();
                    addmobileNumber=et_MobileNumber.getText().toString();
              new AddYourTruck().execute();
                }
//                else {
//                    Toast.makeText(getApplicationContext(),"Something Missing",LENGTH_SHORT).show();
//                }

            }
        });



    }
    private boolean validateMobile() {
        if (et_MobileNumber.getText().toString().trim().isEmpty()) {
            et_MobileNumber.setError(getResources().getText(R.string.mobile_error));
            return false;
        }
        else if(et_MobileNumber.length()==10){
            et_MobileNumber.setError(null);
            return true;
        }
        else
        {
            et_MobileNumber.setError(getResources().getText(R.string.mobile_invalid));
            return false;

        }


    }

    private boolean validatevechile_number(  EditText editText) {

        if (editText.getText().toString().trim().isEmpty()) {
            editText.setError(getResources().getText(R.string.emptyfield));
            //   name_Seperator_V.setBackgroundColor(getResources().getColor(R.color.red));
            return false;
        } else {
            //  name_RL.setErrorEnabled(false);
            return true;
        }
    }

    class openMarket extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session12 = new YoulorrySession(con);
            String username = session12.getusername();
            String password = session12.getpass_word();
            String userid = session12.getuser_id();
            String role = session12.getrole();
            StringBuilder sb = new StringBuilder();

            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-factory-load-details/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                //  urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);
                if(!bidmetakey.isEmpty())
                cred.put("bid_ID",bidmetakey); //optional

                cred.put("factory_load_ID",metakeyid);
             System.out.print(metakeyid);

                cred.put("market_rate",rateperton_value);
                cred.put("to_pay",spinner_to_pay_value);
                cred.put("truck_type",spinner_truck_value);
                cred.put("pod_days",spinner_pod_value);

                cred.put("req_key", "get_confirmed_bids" );
                cred.put("action", "add_new_market_rate" );

                OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                wr.flush();
                wr.close();

                //display what returns the POST request
                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();

                    Log.d("Confirmed bids data :", sb.toString());
                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return sb.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialogue.dismiss();

            JSONArray jsonArray = null;



            }

    }

    private void fillupdata() {
        List<String> source_city = details_page_confirmBid_model.getMeta_keys().getLoad_source_city();
        String sourcecity=(source_city.get(0).toString());
        textView_Source.setText(sourcecity);
        System.out.print(myTrucksBeanList);
        List<String> destination = details_page_confirmBid_model.getMeta_keys().getLoad_destination_city();
        String destinationcity=(destination.get(0).toString());
        text_view_destination.setText(destinationcity);
        List<String> loadmetrial = details_page_confirmBid_model.getMeta_keys().getLoad_material();
        textview_load_material.setText((loadmetrial.get(0).toString()));

        if(!details_page_confirmBid_model.getMeta_keys().getPinged().isEmpty())
        {
            progress.setProgress(Integer.parseInt(details_page_confirmBid_model.getMeta_keys().getPinged()));
        }

        if( details_page_confirmBid_model.getBid_meta_keys()!=null) {
            List<String> weightcapacity = details_page_confirmBid_model.getBid_meta_keys().getBid_weight_capacity();
            if (weightcapacity != null) {
                textView_totalBooked.setText(weightcapacity.get(0).toString());
            }
        }
        else {
            if(( details_page_confirmBid_model.getMeta_keys()!=null)) {
                List<String> weight_capacity = details_page_confirmBid_model.getMeta_keys().getLoad_weight_capacity();
                textView_totalBooked.setText(weight_capacity.get(0).toString());
            }
        }

        if( details_page_confirmBid_model.getBid_meta_keys()!=null) {
            List<String> weightcapacity = details_page_confirmBid_model.getBid_meta_keys().getBid_weight_capacity();
            if (weightcapacity != null) {
                textview_loadWeight.setText(weightcapacity.get(0).toString());
            }
        }
        else {
            if(( details_page_confirmBid_model.getMeta_keys()!=null)) {
                List<String> weight_capacity = details_page_confirmBid_model.getMeta_keys().getLoad_weight_capacity();
                textview_loadWeight.setText(weight_capacity.get(0).toString());
            }
        }
//        if( details_page_confirmBid_model.getBid_meta_keys()!=null) {
//            List<String> weightcapacity = details_page_confirmBid_model.getBid_meta_keys().getBid_weight_capacity();
//            if (weightcapacity != null) {
//                textview_loadWeight.setText(weightcapacity.get(0).toString());
//            }
//        }
//        else {
            if(( details_page_confirmBid_model.getMeta_keys()!=null)) {
                String toping=details_page_confirmBid_model.getMeta_keys().getTo_ping();
                if(toping!=null)
                    textView_booked.setText(String.valueOf(Integer.parseInt(toping)));
            }


     //   }

        if( details_page_confirmBid_model.getBid_meta_keys()!=null) {
            List<String> weightcapacity = details_page_confirmBid_model.getBid_meta_keys().getBid_weight_capacity();
            String toping=details_page_confirmBid_model.getMeta_keys().getTo_ping();
            String weightcvalue=weightcapacity.get(0).toString();
            if ((weightcapacity != null) &&(toping != null)  ) {
                et_weightCapacity.setFilters(new InputFilter[]{ new InputFilterMinMax("1", String.valueOf(Integer.parseInt(weightcvalue)-Integer.parseInt(toping)))});
              //  textView_booked.setText(String.valueOf(Integer.parseInt(weightcvalue)-Integer.parseInt(toping)));
            }
        }
        else {
            if(( details_page_confirmBid_model.getMeta_keys()!=null)) {
                List<String> weight_capacity = details_page_confirmBid_model.getMeta_keys().getLoad_weight_capacity();
                String toping=details_page_confirmBid_model.getMeta_keys().getTo_ping();
                String weightcapacityvalue=weight_capacity.get(0).toString();
                if ((weightcapacityvalue != null) &&(toping != null)  ) {
                    et_weightCapacity.setFilters(new InputFilter[]{ new InputFilterMinMax("1",String.valueOf(Integer.parseInt(weightcapacityvalue)-Integer.parseInt(toping)))});
                 //   textView_booked.setText(String.valueOf(Integer.parseInt(weightcapacityvalue)-Integer.parseInt(toping)));
                }
            }
        }
//  incoming quotations
        if(incomingQuotationsBeanArrayList.size()>0) {
            incoming_recycler_view.setVisibility(View.VISIBLE);
            layoutManager = new LinearLayoutManager(this);
            incoming_recycler_view.setLayoutManager(layoutManager);
            incoming_adapter = new Incoming_Adapter(this, incomingQuotationsBeanArrayList);
            incoming_recycler_view.setAdapter(incoming_adapter);
        }
        else {
            incoming_recycler_view.setVisibility(View.GONE);
        }
//  accepted quotations
        if(acceptedArray.size()>0) {
            accepted_recycler_view.setVisibility(View.VISIBLE);
            layoutManager = new LinearLayoutManager(this);
            accepted_recycler_view.setLayoutManager(layoutManager);
            accepted_adapter = new Accepted_Adapter(this, acceptedArray);
            accepted_recycler_view.setAdapter(accepted_adapter);
        }
        else {
            accepted_recycler_view.setVisibility(View.GONE);
        }
        if(myTrucksBeanList.size()>0) {
            truck_recycler_view.setVisibility(View.VISIBLE);
            layoutManager = new LinearLayoutManager(this);
            truck_recycler_view.setLayoutManager(layoutManager);
            your_truck_adapter = new Your_truck_Adapter(this, myTrucksBeanList);
            truck_recycler_view.setAdapter(your_truck_adapter);
        }
        else {
            truck_recycler_view.setVisibility(View.GONE);
        }
        if(floatMetaKeysBean==null)
        {
           button_opentomarket.setVisibility(View.VISIBLE);
            textView_trucktype.setVisibility(View.GONE);
            textView_to_pay.setVisibility(View.GONE);
            textView_pod.setVisibility(View.GONE);
            et_rateperton.setVisibility(View.VISIBLE);
            relative_layout_to_pod.setVisibility(View.VISIBLE);
            ralativelayout_topay.setVisibility(View.VISIBLE);
            ralativelayout_trucktype.setVisibility(View.VISIBLE);


        }
        else
        {
            button_opentomarket.setVisibility(View.GONE);
            textView_trucktype.setVisibility(View.VISIBLE);
            textView_to_pay.setVisibility(View.VISIBLE);
            textView_pod.setVisibility(View.VISIBLE);
            et_rateperton.setVisibility(View.GONE);
            relative_layout_to_pod.setVisibility(View.GONE);
            ralativelayout_topay.setVisibility(View.GONE);
            ralativelayout_trucktype.setVisibility(View.GONE);
            textView_rateperton.setVisibility(View.VISIBLE);
            List<String> marketrate = floatMetaKeysBean.getMarket_rate();
            if(!marketrate.isEmpty())
            textView_rateperton.setText((marketrate.get(0).toString()));
          //  et_rateperton.setText((marketrate.get(0).toString()));


            List<String> truck_type = floatMetaKeysBean.getTruck_type();
            if(!truck_type.get(0).isEmpty())
            textView_trucktype.setText((truck_type.get(0).toString()));

            List<String> topay = floatMetaKeysBean.getTo_pay();
            if(!topay.get(0).isEmpty())
                textView_to_pay.setText((topay.get(0).toString()));


            List<String> pod_days = floatMetaKeysBean.getPod_days();
            if(!pod_days.get(0).isEmpty())
                textView_pod.setText((pod_days.get(0).toString()));

        }

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
          //      startActivity(new Intent(this, FactoryLoad.class));
                finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        startActivity(new Intent(this, MainActivity.class));
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    class detailpageConfirmedBids extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session12 = new YoulorrySession(con);
            String username = session12.getusername();
            String password = session12.getpass_word();
            String userid = session12.getuser_id();
            String role = session12.getrole();
            StringBuilder sb = new StringBuilder();

            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-factory-load-details/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                //  urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);
             cred.put("bid_ID",getIntent().getStringExtra("factory_load_ID"));
             cred.put("factory_load_ID",getIntent().getStringExtra("ID"));

                cred.put("req_key", "get_confirmed_bids" );
                cred.put("action", "fetch_factory_load_details" );

                OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                wr.flush();
                wr.close();

                //display what returns the POST request
                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();

                    Log.d("Confirmed bids data :", sb.toString());
                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return sb.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialogue.dismiss();

            JSONArray jsonArray = null;
            try {


                JSONObject  jsonObject= new JSONObject(s.toString());
                     details_page_confirmBid_model = new Details_Page_confirmBid_Model();
                    try {
                       // details_page_confirmBid_model.setFloat_meta_keys(jsonObject.getString("float_meta_keys"));


                        String floatmeta= jsonObject.getString("float_meta_keys");
                        if(!floatmeta.contains("null")) {
                            JSONObject float_meta_keys = jsonObject.getJSONObject("float_meta_keys");
                         floatMetaKeysBean = new Details_Page_confirmBid_Model.FloatMetaKeysBean(float_meta_keys);
                            details_page_confirmBid_model.setFloat_meta_keys(floatMetaKeysBean);
                        }
                       // details_page_confirmBid_model.setMy_trucks(jsonObject.getString("my_trucks"));
                       // details_page_confirmBid_model.setIncoming_quotations(jsonObject.getString("incoming_quotations"));
//                        bidding_model.set(obj.getString("lowest_bid_value"));
//                        bidding_model.setMy_bid_value(obj.getString("my_bid_value"));
//                        bidding_model.setHour_limit(obj.getInt("hour_limit"));

                        JSONObject meta_keys = jsonObject.getJSONObject("meta_keys");

                        Details_Page_confirmBid_Model.MetaKeysBean metaKeysBean = new Details_Page_confirmBid_Model.MetaKeysBean(meta_keys);
                        // metakeyid=String.valueOf(metaKeysBean.getID());
                        details_page_confirmBid_model.setMeta_keys(metaKeysBean);
                        metakeyid = String.valueOf(details_page_confirmBid_model.getMeta_keys().getID());

                        String bid_meta_keys= jsonObject.getString("bid_meta_keys");
                        if(!bid_meta_keys.contains("null")) {
                            JSONObject bidMetaKeys = jsonObject.getJSONObject("bid_meta_keys");
                            Details_Page_confirmBid_Model.BidMetaKeysBean bidMetaKeysBean = new Details_Page_confirmBid_Model.BidMetaKeysBean(bidMetaKeys);
                            details_page_confirmBid_model.setBid_meta_keys(bidMetaKeysBean);
                           // if((bidMetaKeysBean.getID()>0))
                            bidmetakey=String.valueOf(details_page_confirmBid_model.getBid_meta_keys().getID());
                        }

                        jsonArray = new JSONArray(jsonObject.getString("my_trucks"));
                        int arraysize=jsonArray.length();
                        System.out.print(arraysize);
                        for (int i = 0; i < arraysize; i++) {
                            JSONObject obj = null;
                            try {
                                obj = jsonArray.getJSONObject(i);
                           if(obj!=null)
                           {
                            JSONObject object = jsonArray.getJSONObject(i);
                                Details_Page_confirmBid_Model.MyTrucksBean myTrucksBean = new Details_Page_confirmBid_Model.MyTrucksBean(object);

                                   details_page_confirmBid_model.setMy_trucks(myTrucksBean);
                            myTrucksBeanList.add(myTrucksBean);
                           }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        jsonArray = new JSONArray(jsonObject.getString("incoming_quotations"));
                        int incomingsize=jsonArray.length();
                        System.out.print(arraysize);
                        for (int i = 0; i < incomingsize; i++) {
                            JSONObject obj = null;
                            try {
                                obj = jsonArray.getJSONObject(i);

                            if(obj!=null)
                            {
                            JSONObject object = jsonArray.getJSONObject(i);
                            Details_Page_confirmBid_Model.IncomingQuotationsBean incomingQuotationsBean = new Details_Page_confirmBid_Model.IncomingQuotationsBean(object);

                            details_page_confirmBid_model.setIncoming_quotations(incomingQuotationsBean);
                            incomingQuotationsBeanArrayList.add(incomingQuotationsBean);
                             }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


//                        String incoming= jsonObject.getString("incoming_quotations");
//                        if(!incoming.contains("null")) {
//                            JSONObject bidMetaKeys = jsonObject.getJSONObject("incoming_quotations");
//                            Details_Page_confirmBid_Model.IncomingQuotationsBean myTrucksBean = new Details_Page_confirmBid_Model.IncomingQuotationsBean(bidMetaKeys);
//                            details_page_confirmBid_model.setIncoming_quotations(myTrucksBean);
//                            incomingQuotationsBeanArrayList.add(details_page_confirmBid_model.getIncoming_quotations());
//
//                        }

                        jsonArray = new JSONArray(jsonObject.getString("accepted_quotations"));
                        int acceptedSize=jsonArray.length();
                        System.out.print(arraysize);

                        for (int i = 0; i < incomingsize; i++) {
                            JSONObject obj = null;
                            try {
                                obj = jsonArray.getJSONObject(i);

                                if(obj!=null)
                                {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    Details_Page_confirmBid_Model.IncomingQuotationsBean incomingQuotationsBean = new Details_Page_confirmBid_Model.IncomingQuotationsBean(object);

                                    details_page_confirmBid_model.setIncoming_quotations(incomingQuotationsBean);
                                    acceptedArray.add(incomingQuotationsBean);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        fillupdata();


//                    {
//
//                    }
                        //    bidding_model.setMeta_keys(obj.getJSONArray("meta_keys"));

//                    organizationsListingModel.setPost_date(obj.getString("name"));
//                    organizationsListingModel.setPost_date_gmt(obj.getString("logo"));
//                    organizationsListingModel.setPost_content(obj.getInt("id"));
//                    organizationsListingModel.setFollowers_count(obj.getInt("followers_count"));
//                    organizationsListingModel.setCampaigns_count(obj.getInt("campaigns_count"));
//                    organizationsListingModelArrayList.add(organizationsListingModel);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void onClick(View v) {

    }
    public void acceptincoming_Quataion(int id, List<String> loadid){
        incomingAcceptValue=String.valueOf(id);
        incomingLoadValue=loadid.get(0).toString();
        new  AcceptIncoming().execute();
    }

    class AcceptIncoming extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session12 = new YoulorrySession(con);
            String username = session12.getusername();
            String password = session12.getpass_word();
            String userid = session12.getuser_id();
            String role = session12.getrole();
            StringBuilder sb = new StringBuilder();

            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-factory-load-details/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                //  urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);


                cred.put("quote_ID",incomingAcceptValue);
                cred.put("factory_load_ID",incomingLoadValue);
                System.out.print(incomingAcceptValue);

                cred.put("action", "accept_incoming_quotation" );

                OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                wr.flush();
                wr.close();

                //display what returns the POST request
                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();

                    Log.d("Confirmed bids data :", sb.toString());
                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return sb.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialogue.dismiss();
            finish();
            startActivity(getIntent());
           if(s.contains("false"))
            Toast.makeText(getApplicationContext(),"Something Error",Toast.LENGTH_SHORT).show();
            else {
               Toast.makeText(getApplicationContext(),"You Accept Quotations",LENGTH_SHORT).show();
           }
            JSONArray jsonArray = null;

        }

    }



    public void delete_action(int id, List<String> loadid){
        deleteAcceptValue=String.valueOf(id);
        deleteLoadValue=loadid.get(0).toString();
        new DeleteAction().execute();
    }

    class DeleteAction extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session12 = new YoulorrySession(con);
            String username = session12.getusername();
            String password = session12.getpass_word();
            String userid = session12.getuser_id();
            String role = session12.getrole();
            StringBuilder sb = new StringBuilder();

            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-factory-load-details/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                //  urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);


                cred.put("quote_ID",deleteAcceptValue);
                cred.put("factory_load_ID",deleteLoadValue);
                System.out.print(incomingAcceptValue);

                cred.put("action", "delete_my_truck" );

                OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                // Toast.makeText(this, "data sent!" , Toast.LENGTH_SHORT).show();
                wr.flush();
                wr.close();

                //display what returns the POST request
                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();

                    Log.d("Confirmed bids data :", sb.toString());
                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return sb.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialogue.dismiss();
            finish();
            startActivity(getIntent());
            if(s.contains("false"))
                Toast.makeText(getApplicationContext(),"Something Error",Toast.LENGTH_SHORT).show();
            else {
                Toast.makeText(getApplicationContext(),"You Accept Quotations",LENGTH_SHORT).show();
            }
            JSONArray jsonArray = null;



        }

    }


    class AddYourTruck extends AsyncTask<Void, Void, String> {

        ProgressDialog dialogue;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogue = new ProgressDialog(con);
            dialogue.setTitle("Loading ...");
            dialogue.show();
        }

        @Override
        protected String doInBackground(Void... voids) {

            YoulorrySession session12 = new YoulorrySession(con);
            String username = session12.getusername();
            String password = session12.getpass_word();
            String userid = session12.getuser_id();
            String role = session12.getrole();
            StringBuilder sb = new StringBuilder();

            URL urlObj = null;
            try {
                urlObj = new URL("http://"+getString(R.string.server_api_url)+".com/api-factory-load-details/");
                HttpURLConnection urlConnection = (HttpURLConnection) urlObj.openConnection();

                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                //  urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();

                JSONObject cred = new JSONObject();

                cred.put("username",username);
                cred.put("password",password);
                cred.put("userid",userid);
                cred.put("role",role);

                if(!bidmetakey.isEmpty())
                cred.put("bid_ID",bidmetakey);

                cred.put("factory_load_ID",metakeyid);

                cred.put("truck_vehicle_number",addvechile_number);
                cred.put("truck_weight",addweightCapacity);
                cred.put("factory_details_drivers_mobile_number",addmobileNumber);
                cred.put("factory_details_driver_name",adddriverName);

                System.out.print(incomingAcceptValue);

                cred.put("action", "add_my_truck" );

                OutputStreamWriter wr= new OutputStreamWriter(urlConnection.getOutputStream());
                wr.write(cred.toString());
                // Toast.makeText(this, fata sent!" , Toast.LENGTH_SHORT).show();
                wr.flush();
                wr.close();

                //display what returns the POST request
                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    //    Toast.makeText(this, "response ok!" , Toast.LENGTH_SHORT).show();
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();

                    Log.d("Confirmed bids data :", sb.toString());
                }

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return sb.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialogue.dismiss();

            if(s.contains("some error"))
               Toast.makeText(getApplicationContext(),"Something Error",Toast.LENGTH_SHORT).show();
            else {
                Toast.makeText(getApplicationContext(),"You Added new Truck",LENGTH_SHORT).show();
                finish();
                startActivity(getIntent());
            }
            JSONArray jsonArray = null;



        }

    }

}
