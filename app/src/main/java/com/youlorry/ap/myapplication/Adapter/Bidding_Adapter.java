package com.youlorry.ap.myapplication.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.youlorry.ap.myapplication.BidLayout;
import com.youlorry.ap.myapplication.Model.Bidding_Model;
import com.youlorry.ap.myapplication.Model.Date_Difference;
import com.youlorry.ap.myapplication.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Tycho on 5/4/2017.
 */

public class Bidding_Adapter extends RecyclerView.Adapter<Bidding_Adapter.Viewholder> {

    Context context;
    Bidding_Adapter bidding_adapter;
    private final   List<Bidding_Model> bidding_models;
    private List<Bidding_Model.MetaKeysBean> metaKeysBeanList=new ArrayList<>();
    int postid;
    int currentposition=0;
    BidLayout bidLayout;

    public Bidding_Adapter(Context context, List<Bidding_Model> bidding_models, BidLayout bidLayout) {
        this.context = context;
        this.bidding_models = bidding_models;
        this.bidLayout=bidLayout;
    }


    @Override
    public Bidding_Adapter.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_bid_layout, parent, false);
        Bidding_Adapter.Viewholder viewHolder=new Bidding_Adapter.Viewholder(v);
        return viewHolder;
    }



    @Override
    public void onBindViewHolder(Bidding_Adapter.Viewholder holder, int position) {
        Bidding_Model bidding_model=bidding_models.get(position);
        if(bidding_model!=null) {
            List<String> source_city = bidding_model.getMeta_keys().getLoad_source_city();
            holder.from_id.setText((source_city.get(0).toString()));

            List<String> destination_city = bidding_model.getMeta_keys().getLoad_destination_city();
            holder.to_id.setText((destination_city.get(0).toString()));

            List<String> load_date = bidding_model.getMeta_keys().getLoad_date();
            holder.quoted_amt_id.setText((load_date.get(0).toString()));

            List<String> load_material = bidding_model.getMeta_keys().getLoad_material();
            holder.material_id.setText((load_material.get(0).toString()));


            List<String> load_weight_capacity = bidding_model.getMeta_keys().getLoad_weight_capacity();
            String load_capacity = load_weight_capacity.get(0).toString()  ;
            if(load_capacity!=null)
            holder.weight_id.setText((Html.fromHtml("<b><font color=\"#52869c\" size=\"6\"  >" + "&nbsp;("+ "</font></b>" +
                    "<font color=\"#52869c\">" + "&nbsp;" + load_capacity + " MT &nbsp;" + ")"+"</font>")));

            holder.lowest_bid.setText(bidding_model.getLowest_bid_value());

            holder.your_bid.setText(bidding_model.getMy_bid_value());
          //  postid=bidding_model.getID();
//            String biddingtime=Date_Difference.getBiddingTime(bidding_model.getServer_current_time().toString(),bidding_model.getExpiry_time());
//            countdown();
//           if((biddingtime==null) || (biddingtime.contains("negative")))
//           {
//               holder.bid_btn.setVisibility(View.GONE);
//               holder.expairyTime.setText("");
//           }
//           else {
//               holder.bid_btn.setVisibility(View.VISIBLE);
//               holder.expairyTime.setText(biddingtime);
//           }
            String timeLeft = bidding_model.getTimeRemaining();
            if(timeLeft.length() > 0){
                holder.bid_btn.setVisibility(View.VISIBLE);
              holder.expairyTime.setText(timeLeft);
            }else {
//                holder.bid_btn.setVisibility(View.GONE);
                holder.bid_btn.setText("EXPIRED");
                holder.bid_btn.setTextColor(Color.RED);
               holder.expairyTime.setText("");
            }



        }
    }

    @Override
    public int getItemCount() {
        System.out.print(bidding_models.size());
        return bidding_models.size();
    }
//
//    public class Viewholder extends RecyclerView.ViewHolder {
//        TextView lowest_bid,your_bid;
//        public Viewholder(View itemView) {
//            super(itemView);
//            your_bid=(TextView) itemView.findViewById(R.id.your_bid);
//        }
//    }


    public void countdown(){

        SimpleDateFormat serverFormat;
        Calendar c = Calendar.getInstance();
        serverFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        String curr_date = serverFormat.format(c.getTime());

        Log.d("my date", curr_date);

        Time TimerSet = new Time();
        TimerSet.set(00, 19, 17, 30, 3, 2017); //day month year
        TimerSet.normalize(true);
        long millis = TimerSet.toMillis(true);

        Time TimeNow = new Time();
        TimeNow.setToNow(); // set the date to Current Time
        TimeNow.normalize(true);
        long millis2 = TimeNow.toMillis(true);

        Log.d("future date", millis+"");
        Log.d("current date", millis2+"");

        long millisset = millis - millis2; //subtract current from future to set the time remaining

        Log.d("difference of dates", millisset+"");

        final int smillis = (int) (millis); //convert long to integer to display conversion results
        final int smillis2 = (int) (millis2);

        new CountDownTimer(millisset, 1000) {

            public void onTick(long millisUntilFinished) {

                // decompose difference into days, hours, minutes and seconds
                int weeks = (int) ((millisUntilFinished / 1000) / 604800);
                int days = (int) ((millisUntilFinished / 1000) / 86400);
                int hours = (int) (((millisUntilFinished / 1000) - (days * 86400)) / 3600);
                int minutes = (int) (((millisUntilFinished / 1000) - ((days * 86400) + (hours * 3600))) / 60);
                int seconds = (int) ((millisUntilFinished / 1000) % 60);
                int millicn = (int) (millisUntilFinished / 1000);

//                weeks1.setText("Left :- ");
//                days1.setText(days+"d");
//                hours1.setText(":" +hours+"h");
//                minutes1.setText(":" +minutes+"m");
//                seconds1.setText(":" +seconds+"s");

            }

            public void onFinish() {
//                weeks1.setText("done!");
//                days1.setText("");
//                hours1.setText("");
//                minutes1.setText("");
//                seconds1.setText("");
            }
        }.start();
    }
    public class Viewholder extends RecyclerView.ViewHolder {
            Button bid_btn;
    TextView weeks1,days1,hours1,minutes1,seconds1,from_id,to_id,quoted_amt_id,material_id,expairyTime,weight_id;
        TextView lowest_bid,your_bid;
        public Viewholder(View itemView) {
            super(itemView);


//        weeks1 = (TextView) itemView.findViewById(R.id.w);
//        days1 = (TextView) itemView.findViewById(R.id.d);
//        hours1 = (TextView) itemView.findViewById(R.id.h);
//        minutes1 = (TextView) itemView.findViewById(R.id.m);
//        seconds1 = (TextView) itemView.findViewById(R.id.s);

            expairyTime=(TextView) itemView.findViewById(R.id.expairyTime);
           lowest_bid=(TextView) itemView.findViewById(R.id.exp_val_id);
            your_bid=(TextView) itemView.findViewById(R.id.your_bid);
        bid_btn = (Button) itemView.findViewById(R.id.bid_btn_id);
            from_id=(TextView) itemView.findViewById(R.id.from_id);
            to_id=(TextView) itemView.findViewById(R.id.to_id);
            quoted_amt_id=(TextView) itemView.findViewById(R.id.quoted_amt_id);
            material_id=(TextView) itemView.findViewById(R.id.material_id);
            weight_id=(TextView)itemView.findViewById(R.id.weight_id);
            if(bid_btn.getText().toString().equalsIgnoreCase("BID")) {
                bid_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bidLayout.onitemclick(bidding_models.get(getAdapterPosition()).getID());

                        // (bidLayout)bidnow(getAdapterPosition());
                    }
                });
            }
            else {
                bid_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(context, "Your bid has expired",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }
}
